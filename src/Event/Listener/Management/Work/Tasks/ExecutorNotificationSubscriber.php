<?php

declare(strict_types=1);

namespace App\Event\Listener\Management\Work\Tasks;

use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\Event\TaskEdited;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Swift_Mailer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;

class ExecutorNotificationSubscriber implements EventSubscriberInterface
{
    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @var MemberRepository
     */
    private $members;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $view;

    public function __construct(
        TaskRepository $tasks,
        MemberRepository $members,
        Swift_Mailer $mailer,
        Environment $view
    ) {
        $this->tasks = $tasks;
        $this->members = $members;
        $this->mailer = $mailer;
        $this->view = $view;
    }

    public static function getSubscribedEvents()
    {
        return [
            TaskEdited::NAME => 'onTaskEdited'
        ];
    }

    public function onTaskEdited(TaskEdited $event)
    {
        if ($event->executor->isEqual($event->actor)) {
            return;
        }

        /** @var Task $task
         */
        $task = $this->tasks->findById($event->taskId);

        if (!$task->hasExecutor()) {
            return;
        }

        /** @var Member $executor */
        $executor = $this->members->get($event->executor->getId()->toString());

        $message = (new \Swift_Message())
            ->setTo($executor->getEmail()->getValue())
            ->setFrom('hello@proglib.io')
            ->setBody($this->view->render('user/mail/executor-email.html.twig', [
                'task' => $task,
                'executor' => $executor
            ]), 'text/html');

        if (!$this->mailer->send($message)) {
            throw new \RuntimeException('Unable to send message.');
        }

    }
}