<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class SQLFixtureLoader extends Fixture
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws DBALException
     */
    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->in(static::getSqlDir());
        $finder->name('*.sql');
        $finder->sortByName();

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $lines = file($file->getRealPath());

            foreach ($lines as $line) {
                if ($line == '') {
                    continue;
                }

                $stmt = $this->em->getConnection()->prepare($line);
                $stmt->execute();
            }
        }
    }

    /**
     * @return string
     */
    protected static function getSqlDir(): string
    {
        return __DIR__;
    }
}