<?php

declare(strict_types=1);

namespace App\DataFixtures\ORM;

use App\Domain\Advertising\Model\Entity\Advertising;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Faker\Factory;
use Faker\Generator;

class AdvertisingFixtures extends Fixture
{
    public const SIDEBAR = 'sidebar';
    public const CONTENT = 'content';

    /**
     * @var Generator
     */
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    public function load(ObjectManager $manager): void
    {
        $this->loadSimpleAds($manager);
        $this->loadCompanyAds($manager);
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    private function loadSimpleAds(ObjectManager $manager): void
    {
        for ($i = 0; $i < 5; $i++) {
            $ads = Advertising::fromSimpleRequest(
                $this->faker->imageUrl(280, 280),
                $this->faker->url,
                null
            );

            $manager->persist($ads);
        }

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     *
     * @throws Exception
     */
    private function loadCompanyAds(ObjectManager $manager): void
    {
        $publishOn = random_int(2, 5);

        for ($i = 0; $i < 5; $i++) {
            $ads = Advertising::fromCompanyRequest(
                $this->faker->text(),
                $this->faker->imageUrl(280, 280),
                $this->faker->url,
                $this->createModifiedDate($publishOn),
                $this->createModifiedDate(),
                $this->createModifiedDate($publishOn + random_int(2, 5)),
                self::SIDEBAR,
                $this->faker->company,
                $this->faker->companyEmail

            );

            $manager->persist($ads);
        }

        $manager->flush();
    }

    /**
     * @param int $dateInterval
     *
     * @return DateTime
     *
     * @throws Exception
     */
    private function createModifiedDate(int $dateInterval = 0): DateTime
    {
        return (new DateTime(sprintf('+%s days', $dateInterval)));
    }
}
