<?php

declare(strict_types=1);

namespace App\Domain\Notification\Model\Entity;

use App\Domain\CommonEntityTrait;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comment_notification_posts")
 */
class CommentNotificationPost
{
    use CommonEntityTrait,
        NotificationTrait;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\User\Model\Entity\User", inversedBy="uuid")
     * @ORM\JoinColumn(name="commented_by", referencedColumnName="id")
     */
    private $commentedBy;

    /**
     * @param User $user
     * @param Post $post
     * @param User $commentedBy
     * @param bool $seen
     *
     * @throws Exception
     */
    public function __construct(
        User $user,
        Post $post,
        User $commentedBy,
        bool $seen
    )
    {
        $this->id = Uuid::uuid4();
        $this->userId = $user;
        $this->postId = $post;
        $this->commentedBy = $commentedBy;
        $this->seen = $seen;
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return Post
     */
    public function getPostId(): Post
    {
        return $this->postId;
    }

    /**
     * @return User
     */
    public function getCommentedBy(): User
    {
        return $this->commentedBy;
    }
}
