<?php

declare(strict_types=1);

namespace App\Domain\Notification\Model\Entity;

use App\Domain\CommonEntityTrait;
use App\Domain\User\Model\Entity\User;
use App\Domain\Post\Model\Entity\Post;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="like_notification_posts")
 */
class LikeNotificationPost
{
    use CommonEntityTrait,
        NotificationTrait;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\User\Model\Entity\User", inversedBy="uuid")
     * @ORM\JoinColumn(name="liked_by", referencedColumnName="id")
     */
    private $likedBy;

    /**
     * @param Post $post
     * @param User $user
     * @param User $likedBy
     * @param bool $seen
     *
     * @throws Exception
     */
    public function __construct(
        Post $post,
        User $user,
        User $likedBy,
        bool $seen
    ) {
        $this->id = Uuid::uuid4();
        $this->postId = $post;
        $this->userId = $user;
        $this->likedBy = $likedBy;
        $this->seen = $seen;
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @return Post
     */
    public function getPostId(): Post
    {
        return $this->postId;
    }

    /**
     * @return User
     */
    public function getLikedBy(): User
    {
        return $this->likedBy;
    }
}
