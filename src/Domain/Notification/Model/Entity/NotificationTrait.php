<?php

namespace App\Domain\Notification\Model\Entity;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;

trait NotificationTrait
{
    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\Post\Model\Entity\Post", inversedBy="uuid")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $postId;

    /**
     * @var boolean
     *
     * @ORM\Column(
     *     type="boolean",
     *     nullable=false,
     *     options={"comment": "Seen"}
     *     )
     */
    private $seen;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Domain\User\Model\Entity\User", inversedBy="uuid")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $userId;

    /**
     * @return bool
     */
    public function isSeen(): bool
    {
        return $this->seen;
    }

    /**
     * @return User
     */
    public function getUserId(): User
    {
        return $this->userId;
    }

    /**
     * @return Post
     */
    public function getPostId(): Post
    {
        return $this->postId;
    }
}