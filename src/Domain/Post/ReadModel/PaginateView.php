<?php

declare(strict_types=1);

namespace App\Domain\Post\ReadModel;


class PaginateView
{
    public $items = [];

    /**
     * @var int
     */
    public $remainingPages;

    /**
     * @var int
     */
    public $itemsCount;

    /**
     * @param array|null $items
     * @param int $remainingPages
     * @param int|null $itemsCount
     */
    public function __construct(?array $items, int $remainingPages, ?int $itemsCount = null)
    {
        $this->items = $items;
        $this->remainingPages = $remainingPages;
        $this->itemsCount = $itemsCount;
    }
}
