<?php

declare(strict_types=1);

namespace App\Domain\Post\ReadModel;

use Ramsey\Uuid\Uuid;

class BookMarksView
{
    /**
     * @var Uuid
     */
    public $id;
}
