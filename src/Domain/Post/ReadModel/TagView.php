<?php

declare(strict_types=1);

namespace App\Domain\Post\ReadModel;

class TagView
{
    /**
     * @var string
     */
    public $tagName;
}