<?php

declare(strict_types=1);

namespace App\Domain\Post\ReadModel;

class PostView
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $slug;

    /**
     * @var string
     */
    public $preview;

    /**
     * @var TagView[]
     */
    public $tags;
}