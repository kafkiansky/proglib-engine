<?php

declare(strict_types=1);

namespace App\Domain\Post\Model\Entity;

use App\Domain\CommonEntityTrait;
use App\Domain\User\Model\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use App\Model\Test\Entity\Test;
use App\Model\Vacancy\Entity\Vacancy;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tags")
 */
class Tag
{
    use CommonEntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=false,
     *     options={"comment": "Tag name"}
     *     )
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Domain\Post\Model\Entity\Post", mappedBy="tags", cascade={"persist"})
     */
    private $post;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=Vacancy::class, mappedBy="tags")
     */
    private $vacancy;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=Test::class, mappedBy="tags", cascade={"persist"})
     */
    private $test;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Domain\User\Model\Entity\User", mappedBy="tags", cascade={"persist"})
     */
    private $user;

    /**
     * @param string $name
     *
     * @throws Exception
     */
    public function __construct(string $name)
    {
        $this->id = Uuid::uuid4();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
        $this->post = new ArrayCollection();
        $this->user = new ArrayCollection();
        $this->test = new ArrayCollection();
        $this->vacancy = new ArrayCollection();
        $this->name = $name;
    }

    /**
     * @param string $name
     * @throws Exception
     */
    public function edit(string $name): void
    {
        $this->name = $name;
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ArrayCollection
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @return ArrayCollection
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * @return ArrayCollection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post): void
    {
        if (!$this->post->contains($post)) {
            $this->post[] = $post;

            $post->addTag($this);
        }
    }

    /**
     * @param Test $test
     */
    public function addTest(Test $test): void
    {
        if (!$this->test->contains($test)) {
            $this->test[] = $test;

            $test->addTag($this);
        }
    }

    /**
     * @param Vacancy $vacancy
     */
    public function addVacancy(Vacancy $vacancy): void
    {
        if (!$this->vacancy->contains($vacancy)) {
            $this->vacancy[] = $vacancy;

            $vacancy->addTag($this);
        }
    }

    /**
     * @param User $user
     */
    public function addUser(User $user): void
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;

            $user->addTag($this);
        }
    }

    /**
     * @param User $user
     */
    public function removeUser(User $user): void
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            $user->removeTag($this);
        }
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post): void
    {
        if ($this->post->contains($post)) {
            $this->post->removeElement($post);
            $post->removeTag($this);
        }
    }

    /**
     * @param Test $test
     */
    public function removeTest(Test $test)
    {
        if (!$this->test->contains($test)) {
            return;
        }

        $this->test->removeElement($test);
        $test->removeTag($this);
    }

    public function removeVacancy(Vacancy $vacancy)
    {
        if (!$this->vacancy->contains($vacancy)) {
            return;
        }

        $this->vacancy->removeElement($vacancy);
        $vacancy->removeTag($this);
    }
}
