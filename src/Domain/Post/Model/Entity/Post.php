<?php

declare(strict_types=1);

namespace App\Domain\Post\Model\Entity;

use App\Domain\Post\Event\CommentCommented;
use App\Domain\Post\Event\Direct\PostCreated;
use App\Domain\Post\Event\PostCommented;
use App\Domain\Post\Event\PostLiked;
use App\Domain\User\Model\Entity\User;
use App\Model\AggregateRoot;
use App\Model\Comment\Entity\Comment;
use App\Model\EventsTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use App\Model\Notification\Entity\Notification;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="posts")
 */
class Post implements AggregateRoot
{
    use EventsTrait;

    public const DRAFT = 'draft';
    public const PUBLISHED = 'published';
    public const DELETED = 'deleted';

    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $renderedContent;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $views;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="post", cascade={"remove"})
     */
    private $notifications;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="post", cascade={"persist"})
     * @ORM\JoinTable(name="post_tags")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tags;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="postsLiked")
     * @ORM\JoinTable(name="posts_likes",
     *     joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *     )
     */
    private $userLikes;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="postsBookmarked")
     * @ORM\JoinTable(name="posts_bookmarks",
     *     joinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *     )
     */
    private $userBookmarks;

    /**
     * @var string
     * @ORM\Column(type="string", name="preview_image", nullable=true)
     */
    private $previewImage;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="post", cascade={"persist"})
     */
    private $comments;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"default": 0})
     */
    private $isDump;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $brokenLinks;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $bounceRate;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $publishedAt;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->tags = new ArrayCollection();
        $this->userLikes = new ArrayCollection();
        $this->userBookmarks = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->views = 0;
        $this->bounceRate = 0;
    }

    /**
     * @param string $uuid
     * @param User $user
     * @param string $title
     * @param string $slug
     * @param string|null $renderedContent
     * @param string|null $preview
     * @param string|null $previewImage
     *
     * @return Post
     *
     * @throws Exception
     */
    public static function fromDraftRequest(
        string $uuid,
        User $user,
        string $title,
        string $slug,
        ?string $renderedContent = null,
        ?string $preview = null,
        ?string $previewImage = null
    ): Post {
        $post = new self();
        $post->id = $uuid;
        $post->user = $user;
        $post->title = $title;
        $post->slug = $slug;
        $post->renderedContent = $renderedContent;
        $post->preview = $preview;
        $post->previewImage = $previewImage;
        $post->status = self::DRAFT;

        return $post;
    }

    /**
     * @param string $uuid
     * @param User $user
     * @param string $title
     * @param string $slug
     * @param string $renderedContent
     * @param string $preview
     * @param string|null $previewImage
     *
     * @return Post
     *
     * @throws Exception
     */
    public static function fromPublishedRequest(
        string $uuid,
        User $user,
        string $title,
        string $slug,
        string $renderedContent,
        string $preview,
        ?string $previewImage = null
    ): Post {
        $post = new self();
        $post->id = $uuid;
        $post->user = $user;
        $post->title = $title;
        $post->slug = $slug;
        $post->renderedContent = $renderedContent;
        $post->preview = $preview;
        $post->previewImage = $previewImage;
        $post->status = self::PUBLISHED;

        return $post;
    }

    /**
     * @param string|null $title
     * @param string|null $slug
     * @param string|null $renderedContent
     * @param string|null $preview
     * @param User|null $user
     * @param string|null $previewImage
     */
    public function edit(
        string $title = null,
        string $slug = null,
        string $renderedContent = null,
        string $preview = null,
        User $user = null,
        string $previewImage = null
    ): void {
        $this->title = $title;
        $this->slug = $slug;
        $this->renderedContent = $renderedContent;
        $this->preview = $preview;
        $this->user = $user;
        $this->previewImage = $previewImage;
    }

    /**
     * @param string $title
     * @param string $slug
     * @param string $renderedContent
     * @param string $preview
     * @param string $previewImage
     * @param User $user
     * @throws Exception
     */
    public function publish(
        string $title,
        string $slug,
        string $renderedContent,
        string $preview,
        string $previewImage,
        User $user
    ): void {
        $this->title = $title;
        $this->slug = $slug;
        $this->renderedContent = $renderedContent;
        $this->preview = $preview;
        $this->previewImage = $previewImage;
        $this->user = $user;
        $this->updatedAt = new \DateTimeImmutable();
        $this->status = self::PUBLISHED;

        if (!$this->publishedAt instanceof \DateTimeImmutable) {
            $this->publishedAt = new \DateTimeImmutable();
            $this->recordEvent(new PostCreated($this));
        }
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getRenderedContent()
    {
        return $this->renderedContent;
    }

    /**
     * @return string
     */
    public function getPreview(): ?string
    {
        return $this->preview;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     *
     * @return $this
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;

            $tag->addPost($this);
        }

        return $this;
    }

    public function addComment(Comment $comment)
    {
        if ($this->comments->contains($comment)) {
            return;
        }

        $this->comments->add($comment);

        if (null === $comment->getParentId()) {
            $this->recordEvent(new PostCommented($this->getUser(), $comment->getUser()));
        } else {
            $this->recordEvent(new CommentCommented($comment->getParent()->getUser(), $comment->getUser()));
        }
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag): void
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removePost($this);
        }

    }

    public function getUserLikes()
    {
        return $this->userLikes;
    }

    /**
     * @param User $actor
     * @param User $executor
     */
    public function likePost(User $actor, User $executor): void
    {
        if ($this->userLikes->contains($executor)) {
            return;
        }

        $this->userLikes->add($executor);
        $this->recordEvent(new PostLiked($actor, $executor));
    }

    /**
     * @param User $user
     */
    public function unlike(User $user): void
    {
        if (!$this->userLikes->contains($user)) {
            return;
        }

        $this->userLikes->removeElement($user);
    }

    public function getUserBookmarks()
    {
        return $this->userBookmarks;
    }

    /**
     * @param User $user
     */
    public function bookmarkPost(User $user): void
    {
        if ($this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->add($user);
    }

    /**
     * @param User $user
     */
    public function unBookmark(User $user): void
    {
        if (!$this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->removeElement($user);
    }

    /**
     * @param User $user
     * @throws Exception
     */
    public function delete(User $user)
    {
        if ($this->user->getId() !== $user->getId()) {
            throw new \DomainException('Нельзя удалить чужую публикацию.');
        }

        $this->status = self::DELETED;
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function getCommentsCount()
    {
        return $this->comments->count();
    }

    /**
     * @return bool
     */
    public function isDraft(): bool
    {
        return $this->status === self::DRAFT;
    }

    public function getPreviewImage()
    {
        return $this->previewImage;
    }

    /**
     * @return bool
     */
    public function isDump(): bool
    {
        return $this->isDump === 1;
    }

    /**
     * @return string|null
     */
    public function getBrokenLinks(): ?string
    {
        return $this->brokenLinks;
    }

    /**
     * @param string $brokenLinks
     */
    public function addBrokenLinks(string $brokenLinks): void
    {
        $this->brokenLinks = $brokenLinks;
    }

    /**
     * @return int|null
     */
    public function getBounceRate(): ?int
    {
        return $this->bounceRate;
    }

    /**
     * @param int $bounceRate
     */
    public function updateBounceRate(int $bounceRate): void
    {
        $this->bounceRate = $bounceRate;
    }

    /**
     * @param string $previewImage
     */
    public function changePreviewImage(string $previewImage)
    {
        $this->previewImage = $previewImage;
    }

    public function increaseViewsCount()
    {
        $this->views = $this->getViews() + 1;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getPublishedAt(): \DateTimeImmutable
    {
        return $this->publishedAt;
    }
}
