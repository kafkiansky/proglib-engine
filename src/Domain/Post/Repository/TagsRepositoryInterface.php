<?php

declare(strict_types=1);

namespace App\Domain\Post\Repository;

use App\Domain\Post\Model\Entity\Tag;
use Knp\Component\Pager\Pagination\PaginationInterface;

interface TagsRepositoryInterface
{
    public function findAll();

    public function all(int $page, int $limit, string $sort, string $direction): PaginationInterface;

    public function add(Tag $tag);

    public function findOneBy(array $criteria);
}