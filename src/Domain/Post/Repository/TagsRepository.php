<?php

declare(strict_types=1);

namespace App\Domain\Post\Repository;

use App\Domain\Post\Model\Entity\Tag;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class TagsRepository implements TagsRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(
        EntityManagerInterface $entityManager,
        Connection $connection,
        PaginatorInterface $paginator
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Tag::class);
        $this->connection = $connection;
        $this->paginator = $paginator;
    }

    /**
     * @param string $name
     * @return array|object[]
     * @throws EntityNotFoundException
     */
    public function findByName(string $name)
    {
        $entity = $this->repository->findOneBy(['name' => $name]);

        if (!$entity) {
            throw new EntityNotFoundException(sprintf('Tag with id %s does not exists', $name));
        }

        return $entity;
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        return $this->repository
            ->createQueryBuilder('t')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $page
     * @param int $limit
     * @param string $sort
     * @param string $direction
     *
     * @return PaginationInterface
     */
    public function all(int $page, int $limit, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('t.id', 't.name', 't.created_at AS createdAt')
            ->from('tags', 't');

        if (!\in_array($sort, ['createdAt'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'asc' ? 'asc' : 'desc');

        return $this->paginator->paginate($qb, $page, $limit);
    }

    public function add(Tag $tag)
    {
        $this->entityManager->persist($tag);
    }

    /**
     * @param array $criteria
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository
            ->createQueryBuilder('t')
            ->where('t.'. key($criteria) . ' = :criteria')
            ->setParameter('criteria', reset($criteria))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
