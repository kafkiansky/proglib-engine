<?php

declare(strict_types=1);

namespace App\Domain\Post\Repository;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\ReadModel\BookMarksView;
use App\Domain\User\Model\Entity\User;
use App\Domain\Post\ReadModel\PostView;
use App\Domain\Post\ReadModel\TagView;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\DBAL\Connection;

class PostRepository implements PostRepositoryInterface
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(Connection $connection, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->em = $em;
        $this->repository = $em->getRepository(Post::class);
    }

    /**
     * @return PostView[]|null
     */
    public function findLatest()
    {
        $stmtPost = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.slug',
                'p.preview'
            )
            ->from('post', 'p')
            ->execute();

        $stmtPost->setFetchMode(FetchMode::CUSTOM_OBJECT, PostView::class);

        /** @var PostView $postView */
        $postView = $stmtPost->fetchAll();

        $stmtTags = $this->connection
            ->createQueryBuilder()
            ->select(
                't.tag_name AS tagName'
            )
            ->from('tag', 't')
            ->innerJoin('t', 'post_tags', 'pt', 'pt.tag_id = t.id')
            ->where('pt.post_id = :id');

        /** @var PostView $post */
        foreach ($postView as $post) {
            $stmt = $stmtTags
                ->setParameter('id', $post->id)
                ->execute();

            $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, TagView::class);

            $post->tags = $stmt->fetch();

        }

        return $postView ?: null;
    }

    /**
     * @param int $offset
     *
     * @return mixed
     */
    public function findPublished(int $offset = 0)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('p.status = :status')
            ->setParameter('status', Post::PUBLISHED)
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->groupBy('p.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param string $tag
     * @param int $limit
     * @param int $offset
     * @param bool $published
     *
     * @return mixed
     */
    public function findPostByTags(string $tag, int $limit = null, int $offset = 0, bool $published = true)
    {
        $query = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->setFirstResult($offset);

        if ($limit !== null) {
            $query->setMaxResults($limit);
        }

        $query
            ->orderBy('p.views', 'DESC');

        if ($published === true) {
            $query
                ->where('p.status = :status')
                ->andWhere('t.id = :id')
                ->setParameter('status', Post::PUBLISHED)
                ->setParameter('id', $tag);
        }

        return $query->groupBy('p.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getCountPostsForTag(string $tag)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(p.id) count')
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->where('p.status = :status')
            ->andWhere('t.id = :id')
            ->setParameter('status', Post::PUBLISHED)
            ->setParameter('id', $tag)
            ->groupBy('p.id')
            ->execute()
            ->fetchColumn();
    }

    /**
     * @param string $tag
     * @return string
     */
    public function findRecommendationIds(string $tag)
    {
        return $this->connection->createQueryBuilder()
            ->select('p.id')
            ->from('post_tags', 'pt')
            ->innerJoin('pt', 'posts', 'p', 'pt.post_id = p.id')
            ->innerJoin('pt', 'tags', 't', 'pt.tag_id = t.id')
            ->where('t.id = :id')
            ->andWhere('p.status = :status')
            ->setParameter('status', Post::PUBLISHED)
            ->setParameter('id', $tag)
            ->orderBy('p.views', 'DESC')
            ->execute()
            ->fetchAll(\PDO::FETCH_COLUMN);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function findRecommendations(string $id)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview'
            )
            ->from('posts', 'p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * @param array $criteria
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findOnePostBy(array $criteria)
    {
        return $this->repository
            ->createQueryBuilder('p')
            ->where('p.'. key($criteria) . ' = :criteria')
            ->setParameter('criteria', reset($criteria))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPostBy(array $criteria)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('p.' . key($criteria) . '= :criteria')
            ->setParameter('criteria', reset($criteria))
            ->groupBy('p.id')
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * @param string $query
     * @param int $limit
     * @param int $offset
     * @return array|mixed|null
     */
    public function searchByQuery(string $query, int $limit = 10, int $offset = 0)
    {
        $posts = $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('p.title LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->groupBy('p.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);

        if ($posts) {
            return $posts;
        }

        return null;
    }

    /**
     * @param string $query
     *
     * @return mixed
     */
    public function searchByQueryCount(string $query)
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select('COUNT(*) AS count')
            ->from('posts', 'p')
            ->where('p.title LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        return $stmt->count;
    }

    /**
     * @param User $user
     * @param string $status
     * @param int $offset
     *
     * @return mixed
     */
    public function findUserPostByStatus(User $user, string $status, int $offset = 0)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('p.status = :status')
            ->andWhere('p.user = :user')
            ->setParameter('user', $user->getId()->toString())
            ->setParameter('status', $status)
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->groupBy('p.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param User $user
     * @param int $offset
     *
     * @return mixed
     */
    public function findPublishedSubscriptions(User $user, int $offset = 0)
    {
        $subscriptions = $user->getTags();

        if ($subscriptions->count() === 0) {
            return $this->findPublished($offset);
        }

        $subscriptionsIds = [];
        foreach ($subscriptions as $tag) {
            $subscriptionsIds[] = $tag->getId();
        }

        $qb = $this->repository->createQueryBuilder('[');

        return $this->connection->createQueryBuilder()->select(
                'p.id',
                'p.title',
                'p.preview_image AS previewImage',
                'p.preview',
                'p.slug',
                'p.status',
                'p.user',
                'u.nickname',
                'p.created_at AS createdAt',
                'p.updated_at AS updatedAt',
                'GROUP_CONCAT(DISTINCT pl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT t.name) AS postTags',
                'GROUP_CONCAT(DISTINCT pb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('posts', 'p')
            ->leftJoin('p', 'post_tags', 'pt', 'p.id = pt.post_id')
            ->leftJoin('p', 'users', 'u', 'p.user = u.id')
            ->leftJoin('p', 'tags', 't', 'pt.tag_id = t.id')
            ->leftJoin('p', 'posts_likes', 'pl', 'pl.post_id = p.id')
            ->leftJoin('p', 'comments', 'c', 'c.post_id = p.id')
            ->leftJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('p.status = :status')
            ->andWhere($qb->expr()->in('t.id', $subscriptionsIds))
            ->setParameter('status', Post::PUBLISHED)
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->groupBy('p.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param User $user
     *
     * @return int|mixed
     *
     * @throws NonUniqueResultException
     */
    public function findPublishedSubscriptionsCount(User $user)
    {
        $subscriptions = $user->getTags();

        if ($subscriptions->count() === 0) {
            return $this->getCountOfAllPosts();
        }

        $count = 0;

        /** @var Tag $tag */
        foreach ($subscriptions as $tag) {
            $count += $tag->getPost()->count();
        }

        return $count;
    }

    /**
     * @param string $status
     * @param User $user
     *
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getCount(string $status, User $user)
    {
        return $this->repository->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.status = :status')
            ->andWhere('p.user = :user')
            ->setParameter('status', $status)
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param User $user
     * @param int $offset
     *
     * @return array
     */
    public function findBookMarks(User $user, int $offset = 0): ?array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select('p.id')
            ->from('posts', 'p')
            ->innerJoin('p', 'posts_bookmarks', 'pb', 'pb.post_id = p.id')
            ->where('pb.user_id = :user')
            ->setParameter('user', $user->getId())
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->execute();

        $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, BookMarksView::class);

        $bookMarksView = $stmt->fetchAll();

        $posts = [];

        /** @var BookMarksView $bookMarkView */
        foreach ($bookMarksView as $bookMarkView) {
            $posts[] = $this->findPostBy(['id' => $bookMarkView->id]);
        }

        return $posts ?: null;
    }

    /**
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function getCountOfAllPosts()
    {
        return $this->repository->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->where('p.status = :status')
            ->setParameter('status', Post::PUBLISHED)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $user
     *
     * @return mixed
     */
    public function findPublicationsCount(string $user)
    {
        $qb = $this->connection->createQueryBuilder();

        $stmt = $qb->select('COUNT(*) AS count')
            ->from('posts', 'p')
            ->Where('p.user = :user')
            ->andWhere('p.status IN (:published, :draft)')
            ->setParameter('published', Post::PUBLISHED)
            ->setParameter('draft', Post::DRAFT)
            ->setParameter('user', $user)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        return $stmt->count;
    }

    /**
     * @param string $user
     * @return mixed
     */
    public function findForeignUserPublicationsCount(string $user)
    {
        $qb = $this->connection->createQueryBuilder();

        $stmt = $qb->select('COUNT(*) AS count')
            ->from('posts', 'p')
            ->Where('p.user = :user')
            ->andWhere('p.status = :published')
            ->setParameter('published', Post::PUBLISHED)
            ->setParameter('user', $user)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        return $stmt->count;
    }

    /**
     * @param Post $post
     */
    public function add(Post $post): void
    {
        $this->em->persist($post);
    }

    /**
     * @param $id
     * @return object|null
     * @throws EntityNotFoundException
     */
    public function get($id)
    {
        if (!$entity = $this->repository->find($id)) {
            throw new EntityNotFoundException(sprintf('Entity with id %s not found', $id));
        }

        return $entity;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws EntityNotFoundException
     * @throws DBALException
     */
    public function favoriteByWeek(int $offset, int $limit)
    {
        $sql = '
            SELECT
	            pl.post_id AS postId
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 WEEK 
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
	        DESC
	        LIMIT :offset, :limit
        ';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindValue('limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();

        $favoritePostsByWeek = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $post) {
            $favoritePostsByWeek[] = $this->findPostBy(['id' => $post->postId]);
        }

        return $favoritePostsByWeek;
    }

    /**
     * @return int
     * @throws DBALException
     */
    public function countFavoritePostsByWeek(): int
    {
        $sql = '
            SELECT
	            COUNT(*) AS cp
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 WEEK 
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
        ';

        $stmt = $this->connection->query($sql);
        $cp = $stmt->fetchColumn();

        return \count($cp);
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws EntityNotFoundException
     * @throws DBALException
     */
    public function favoriteByMonth(int $offset, int $limit)
    {
        $sql = '
            SELECT
	            pl.post_id AS postId
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 MONTH
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
	        DESC
	        LIMIT :offset, :limit
        ';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindValue('limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();

        $favoritePostsByWeek = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $post) {
            $favoritePostsByWeek[] = $this->findPostBy(['id' => $post->postId]);
        }

        return $favoritePostsByWeek;
    }

    /**
     * @return int
     * @throws DBALException
     */
    public function countFavoritePostsByMonth(): int
    {
        $sql = '
            SELECT
	            COUNT(*) AS cp
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 MONTH
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
        ';

        $stmt = $this->connection->query($sql);
        $cp = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return \count($cp);
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws EntityNotFoundException
     * @throws DBALException
     */
    public function favoriteByYear(int $offset, int $limit)
    {
        $sql = '
            SELECT
	            pl.post_id AS postId
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 YEAR
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
	        DESC
	        LIMIT :offset, :limit
        ';

        $stmt = $this->connection->prepare($sql);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->bindValue('limit', $limit, \PDO::PARAM_INT);
        $stmt->execute();

        $favoritePostsByWeek = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $post) {
            $favoritePostsByWeek[] = $this->findPostBy(['id' => $post->postId]);
        }

        return $favoritePostsByWeek;
    }

    /**
     * @return int
     * @throws DBALException
     */
    public function countFavoritePostsByYear(): int
    {
        $sql = '
            SELECT
	            COUNT(*) AS cp
	        FROM 
	            posts_likes pl
	        WHERE 
	            pl.post_id
	        IN 
	        (
	            SELECT 
	                id 
	            FROM 
	                posts
	            WHERE 
	                created_at 
	            BETWEEN 
	                CURDATE()-INTERVAL 1 YEAR
	            AND 
	                NOW()
	        )
	        GROUP BY pl.post_id
	        ORDER BY COUNT(post_id) 
        ';

        $stmt = $this->connection->query($sql);
        $cp = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return \count($cp);
    }

    public function findRelativePosts()
    {

    }
}
