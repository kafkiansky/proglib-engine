<?php

declare(strict_types=1);

namespace App\Domain\Post\Repository;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;

interface PostRepositoryInterface
{
    public function findLatest();

    public function findPublished(int $offset = 0);

    public function findPostByTags(string $tag, int $limit = null, int $offset = 0, bool $published = true);

    public function findOnePostBy(array $criteria);

    public function searchByQuery(string $query, int $limit = 10, int $offset = 0);

    public function searchByQueryCount(string $query);

    public function findUserPostByStatus(User $user, string $status, int $offset = 0);

    public function findPublishedSubscriptions(User $user, int $offset = 0);

    public function getCount(string $status, User $user);

    public function findBookMarks(User $user, int $offset = 0): ?array;

    public function getCountOfAllPosts();

    public function findPublishedSubscriptionsCount(User $user);

    public function findPublicationsCount(string $user);

    public function findForeignUserPublicationsCount(string $user);

    public function add(Post $post);

    public function favoriteByWeek(int $offset, int $limit);

    public function countFavoritePostsByWeek();

    public function favoriteByMonth(int $offset, int $limit);

    public function countFavoritePostsByMonth(): int;

    public function favoriteByYear(int $offset, int $limit);

    public function countFavoritePostsByYear(): int;
}
