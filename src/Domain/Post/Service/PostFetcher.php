<?php

declare(strict_types=1);

namespace App\Domain\Post\Service;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\PostRepository;

class PostFetcher
{
    /**
     * @var PostRepository
     */
    private $posts;

    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     * @return array
     */
    public function recommendationsForPost(Post $post): array
    {
        $forFind = [];

        /** @var Tag $tag */
        foreach ($post->getTags()->toArray() as $tag) {
            $forFind[] = $this->posts->findRecommendationIds($tag->getId()->toString());
        }

        $ids = [];

        array_walk_recursive($forFind, function ($item, $key) use (&$ids) {
            $ids[] = $item;
        });

        $posts = [];

        foreach ($ids as $id) {
            if ($id === $post->getId()->toString()) {
                continue;
            }

            $posts[] = $this->posts->findRecommendations($id);
        }

        return $posts;
    }
}
