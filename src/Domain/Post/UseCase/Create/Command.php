<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Create;

use App\Domain\User\Model\Entity\User;

class Command
{
    public $id;
    public $title;
    public $preview;
    public $body;
    public $image;
    public $tags;
    public $user;

    public function __construct(array $args, User $user)
    {
        $this->id = $args['id'];
        $this->title = $args['title'];
        $this->preview = $args['preview'];
        $this->body = $args['editorData'];
        $this->image = $args['image'];
        $this->tags = $args['tags'] ?? null;
        $this->user = $user;
    }
}
