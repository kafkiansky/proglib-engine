<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Publish;

use App\Domain\User\Model\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     *
     * @Assert\Uuid(message="no-uuid")
     */
    public $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-title")
     * @Assert\Length(min="20", minMessage="too-short-title", max="130", maxMessage="too-long-title")
     */
    public $title;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-preview")
     * @Assert\Length(min="50", minMessage="too-short-preview", max="600", maxMessage="too-long-preview")
     */
    public $preview;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-body")
     * @Assert\Length(min="500", minMessage="too-short-body")
     */
    public $body;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $image;

    /**
     * @var array
     *
     * @Assert\NotBlank(message="no-tags")
     * @Assert\Count(min="1", minMessage="too-few-tags", max="3", maxMessage="too-many-tags")
     */
    public $tags;

    public function __construct(array $args, User $user)
    {
        $this->id = $args['id'];
        $this->title = $args['title'];
        $this->preview = $args['preview'];
        $this->body = $args['editorData'];
        $this->user = $user;
        $this->image = $args['image'];
        $this->tags = $args['tags'] ?? null;
    }
}
