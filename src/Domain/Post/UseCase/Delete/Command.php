<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Delete;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var Post
     */
    public $post;

    /**
     * @var User
     */
    public $user;

    public function __construct(Post $post, User $user)
    {
        $this->post = $post;
        $this->user = $user;
    }
}
