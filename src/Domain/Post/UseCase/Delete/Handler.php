<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Delete;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     *
     * @throws \Exception
     */
    public function handle(Command $command)
    {
        if (null === $command->post) {
            throw new \DomainException('Такой публикации не существует.');
        }

        $command->post->delete($command->user);
        $this->flusher->flush();
    }
}
