<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\BrokenLinks;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class BrokenLinksCommand extends Command
{
    /**
     * @var BrokenLinksManager
     */
    private $brokenLinksManager;

    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param BrokenLinksManager $brokenLinksManager
     * @param PostRepositoryInterface $postRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        BrokenLinksManager $brokenLinksManager,
        PostRepositoryInterface $postRepository,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->brokenLinksManager = $brokenLinksManager;
        $this->postRepository = $postRepository;
        $this->logger = $logger;
    }

    protected function configure(): void
    {
        $this->setName('links:delete');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $posts = $this->postRepository->findPublished(0);

        $brokenLinks = [];

        foreach ($posts as $post) {
            $brokenLinks[] = $this->parseContent($post);
        }

        $result = [];

        /** @var BrokenLinksDto $brokenLink */
        foreach ($brokenLinks as $brokenLink) {
            $this->checkIfLinkIsBroken($brokenLink);
        }
    }

    /**
     * @param BrokenLinksDto $brokenLinksDto
     */
    private function checkIfLinkIsBroken(BrokenLinksDto $brokenLinksDto)
    {
        $client = new Client();
        $brokenLinks = [];

        foreach ($brokenLinksDto->links->links as $link) {
            try {
                $client->get($link);

            } catch (\Exception $e) {
                $brokenLinks[] = $link;

                $this->logger->error($e->getMessage());
            }
        }

        $this->brokenLinksManager->refreshBrokenLinks(
            $brokenLinksDto->getId(),
            json_encode($brokenLinks)
        );
    }

    /**
     * @param Post $post
     *
     * @return BrokenLinksDto
     */
    private function parseContent(Post $post): BrokenLinksDto
    {
        $crawler = new Crawler($post->getRenderedContent());

        $id = $post->getId()->toString();
        $links = $crawler->filter('a')->each(function (Crawler $node, $i) {
            return trim($node->attr('href'), '\"');
        });

        return new BrokenLinksDto(
            $id,
            new LinksDto(
                $links
            )
        );
    }
}