<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\BrokenLinks;

class LinksDto
{
    /**
     * @var array
     */
    public $links = [];

    /**
     * @param array $links
     */
    public function __construct(array $links)
    {
        $this->links = $links;
    }

    /**
     * @return array
     */
    public function getLinks(): array
    {
        return $this->links;
    }
}
