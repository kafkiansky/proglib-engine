<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\BrokenLinks;

class BrokenLinksDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var LinksDto
     */
    public $links;

    /**
     * @param string $id
     * @param LinksDto $links
     */
    public function __construct(
        string $id,
        LinksDto $links
    ) {
        $this->id = $id;
        $this->links = $links;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return LinksDto
     */
    public function getLinks(): LinksDto
    {
        return $this->links;
    }
}
