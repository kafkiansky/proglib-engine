<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\BrokenLinks;

use App\Application\Manager\AbstractManager;
use Doctrine\DBAL\DBALException;

class BrokenLinksManager extends AbstractManager
{
    /**
     * @param string $id
     * @param array $links
     */
    public function refreshBrokenLinks(string $id, string $links): void
    {
        $sql = '
            UPDATE
                post
            SET
                broken_links = :brokenLinks
            WHERE
                id = :id
        ';

        try {
            $this->getConnection()->prepare($sql)
                ->execute([
                    'brokenLinks' => $links,
                    'id' => $id
                ]);

        } catch (DBALException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }
}
