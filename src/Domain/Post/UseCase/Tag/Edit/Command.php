<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Edit;

use App\Domain\Post\Model\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Нельзя сохранить пустой тег")
     */
    public $name;

    public function __construct(Tag $tag)
    {
        $this->id = $tag->getId()->toString();
        $this->name = $tag->getName();
    }
}
