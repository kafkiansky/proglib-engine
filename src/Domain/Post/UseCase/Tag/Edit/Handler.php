<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Edit;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TagsRepositoryInterface
     */
    private $tags;

    public function __construct(Flusher $flusher, TagsRepositoryInterface $tags)
    {
        $this->flusher = $flusher;
        $this->tags = $tags;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        if ($this->tags->findOneBy(['name' => $command->name ])) {
            throw new \DomainException('Такого тег уже существует');
        }

        /** @var Tag $tag */
        $tag = $this->tags->findOneBy(['id' => $command->id]);

        $tag->edit($command->name);
        $this->flusher->flush();
    }
}
