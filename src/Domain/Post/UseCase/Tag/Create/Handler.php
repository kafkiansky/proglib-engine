<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Create;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var TagsRepositoryInterface
     */
    private $tags;

    public function __construct(Flusher $flusher, TagsRepositoryInterface $tagsRepository)
    {

        $this->flusher = $flusher;
        $this->tags = $tagsRepository;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        if ($this->tags->findOneBy(['name' => $command->name])) {
            throw new \DomainException('Такой тег уже существует');
        }

        $tag = new Tag($command->name);

        $this->tags->add($tag);
        $this->flusher->flush();
    }
}
