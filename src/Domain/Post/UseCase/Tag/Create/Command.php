<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Нельзя создать пустой тег")
     */
    public $name;
}
