<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Delete;

use App\Domain\Post\Model\Entity\Tag;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $name;

    public function __construct(Tag $tag)
    {
        $this->name = $tag->getName();
    }
}
