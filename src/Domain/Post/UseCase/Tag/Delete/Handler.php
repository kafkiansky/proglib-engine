<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Tag\Delete;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var TagsRepositoryInterface
     */
    private $tags;

    public function __construct(Flusher $flusher, TagsRepositoryInterface $tags)
    {
        $this->flusher = $flusher;
        $this->tags = $tags;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        /** @var Tag $tag */
        $tag = $this->tags->findOneBy(['name' => $command->name]);

        if (!$tag) {
            throw new \DomainException('Такой тег не существует');
        }

        $this->flusher->remove($tag);
        $this->flusher->flush();
    }
}
