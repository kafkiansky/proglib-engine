<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Admin\Create;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\PostRepository;
use App\Domain\Post\Repository\TagsRepository;
use Cocur\Slugify\SlugifyInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;

class Handler
{
    /**
     * @var PostRepository
     */
    private $posts;

    /**
     * @var TagsRepository
     */
    private $tags;

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    public function __construct(
        PostRepository $posts,
        TagsRepository $tags,
        SlugifyInterface $slugify
    ) {
        $this->posts = $posts;
        $this->tags = $tags;
        $this->slugify = $slugify;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command)
    {
        if (null === $command->id) {
            throw new \DomainException('Cannot save post without identifier');
        }

        try {
            /** @var Post $post */
            $post = $this->posts->get($command->id);
            $post->edit(
                $command->title,
                $this->slugify->slugify($command->title),
                $command->body,
                $command->preview,
                $command->user,
                $command->image
            );

            $this->tags($command->tags, $post);

        } catch (EntityNotFoundException $e) {
            $post = Post::fromDraftRequest(
                $command->id,
                $command->user,
                $command->title,
                $this->slugify->slugify($command->title),
                $command->body,
                $command->preview,
                $command->image
            );

            $this->posts->add($post);
            $this->tags($command->tags, $post);
        }
    }

    /**
     * @param array|null $tags
     * @param Post $post
     * @throws NonUniqueResultException
     */
    private function tags(?array $tags, Post $post): void
    {
        $savedTags = [];

        /** @var Tag $testTag */
        foreach ($post->getTags() as $testTag) {
            $savedTags[] = $testTag->getId()->toString();
        }

        $arraysAreEqual = ($savedTags === $tags);

        if ($arraysAreEqual) {
            return;
        }

        /** @var Tag $existingTag */
        foreach ($post->getTags() as $existingTag) {
            $post->removeTag($existingTag);
        }

        $existingTags = [];

        if (null !== $tags) {
            foreach ($tags as $tagId) {
                $existingTags[] = $this->tags->findOneBy(['id' => $tagId]);
            }

            /** @var Tag $existingTag */
            foreach ($existingTags as $existingTag) {
                $post->addTag($existingTag);
            }
        }
    }
}
