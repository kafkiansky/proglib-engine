<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Admin\Publish;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\PostRepository;
use App\Domain\Post\Repository\TagsRepository;
use Cocur\Slugify\SlugifyInterface;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var TagsRepository
     */
    private $tags;

    /**
     * @var PostRepository
     */
    private $posts;

    /**
     * @var SlugifyInterface
     */
    private $slugify;

    public function __construct(TagsRepository $tags, PostRepository $posts, SlugifyInterface $slugify)
    {
        $this->tags = $tags;
        $this->posts = $posts;
        $this->slugify = $slugify;
    }

    public function handle(Command $command)
    {
        try {
            /** @var Post $post */
            $post = $this->posts->get($command->id);
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        $post->publish(
            $command->title,
            $this->slugify->slugify($command->title),
            $command->body,
            $command->preview,
            $command->image,
            $command->user
        );

        $this->refreshTags($command->tags, $post);
    }

    /**
     * @param array $tags
     * @param Post $post
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function refreshTags(array $tags, Post $post)
    {
        $savedTags = [];

        /** @var Tag $testTag */
        foreach ($post->getTags() as $testTag) {
            $savedTags[] = $testTag->getId()->toString();
        }

        $arraysAreEqual = ($savedTags === $tags);

        if ($arraysAreEqual) {
            return;
        }

        /** @var Tag $existingTag */
        foreach ($post->getTags() as $existingTag) {
            $post->removeTag($existingTag);
        }

        $existingTags = [];

        foreach ($tags as $tagId) {
            $existingTags[] = $this->tags->findOneBy(['id' => $tagId]);
        }

        /** @var Tag $existingTag */
        foreach ($existingTags as $existingTag) {
            $post->addTag($existingTag);
        }
    }
}
