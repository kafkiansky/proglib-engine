<?php

declare(strict_types=1);

namespace App\Domain\Post\UseCase\Admin\Publish;

use App\Domain\User\Model\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     *
     * @Assert\Uuid(message="wrong-uuid")
     */
    public $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-title")
     * @Assert\Length(min="46", minMessage="too-short-title", max="65", maxMessage="too-long-title")
     */
    public $title;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-preview")
     * @Assert\Length(min="121", minMessage="too-short-preview", max="156", maxMessage="too-long-preview")
     */
    public $preview;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-content")
     */
    public $body;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="no-image")
     */
    public $image;

    /**
     * @var string
     *
     * @Assert\Count(min="1", minMessage="no-tags")
     */
    public $tags;

    /**
     * @var User
     *
     * @Assert\NotBlank(message="no-user")
     */
    public $user;

    public function __construct(array $args, User $user)
    {
        $this->id = $args['id'];
        $this->title = $args['title'];
        $this->preview = $args['preview'];
        $this->body = $args['editorData'];
        $this->image = $args['image'];
        $this->tags = $args['tags'];
        $this->user = $user;
    }
}
