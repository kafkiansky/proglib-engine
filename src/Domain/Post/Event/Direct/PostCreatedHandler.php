<?php

declare(strict_types=1);

namespace App\Domain\Post\Event\Direct;

use phpcent\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PostCreatedHandler implements MessageHandlerInterface
{
    public const CHANNEL = 'public:wall';

    /**
     * @var Client
     */
    private $message;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    public function __construct(Client $message, LoggerInterface $logger, TokenStorageInterface $token)
    {
        $this->message = $message;
        $this->logger = $logger;
        $this->token = $token;
    }

    public function __invoke(PostCreated $postCreated)
    {
        try {
            $onlineChannels = $this->message->channels();
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => 'Centrifugo not working.']);
            return;
        }

        if (!in_array(self::CHANNEL, $onlineChannels->result->channels)) {
            return;
        }

        $this->message->publish(self::CHANNEL, [
            'newPost' => true
        ]);
    }
}
