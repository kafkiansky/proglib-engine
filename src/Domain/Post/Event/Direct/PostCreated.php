<?php

declare(strict_types=1);

namespace App\Domain\Post\Event\Direct;

use App\Domain\Post\Model\Entity\Post;

class PostCreated
{
    /**
     * @var Post
     */
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }
}
