<?php

declare(strict_types=1);

namespace App\Domain\Post\Event\Direct;

use App\Model\Comment\Entity\Comment;

class CommentCreated
{
    public $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }
}
