<?php

declare(strict_types=1);

namespace App\Domain\Post\Event\Direct;

use phpcent\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CommentCreatedHandler implements MessageHandlerInterface
{
    private const COMMENT_CHANNEL = 'public:comments';

    /**
     * @var Client
     */
    private $message;

    /**
     * @var Environment
     */
    private $view;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var TokenStorageInterface
     */
    private $token;

    public function __construct(Client $message, Environment $view, LoggerInterface $logger, TokenStorageInterface $token)
    {
        $this->message = $message;
        $this->view = $view;
        $this->logger = $logger;
        $this->token = $token;
    }

    /**
     * @param CommentCreated $commentCreated
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function __invoke(CommentCreated $commentCreated): void
    {
        try {
            $onlineChannels = $this->message->channels();
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => 'Centrifugo not working.']);
            return;
        }

        if (!in_array(self::COMMENT_CHANNEL, $onlineChannels->result->channels)) {
            return;
        }

        $commentTemplated = $this->view->render('components/comments/commentCard.html.twig', [
            'item' => $commentCreated->comment
        ]);

        $this->message->publish(self::COMMENT_CHANNEL, [
            'comment' => $commentTemplated
        ]);
    }
}
