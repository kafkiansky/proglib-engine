<?php

declare(strict_types=1);

namespace App\Domain\Post\Event;

use App\Domain\User\Model\Entity\User;

class CommentCommented
{
    public const NAME = 'comment-comment';

    /**
     * @var User
     */
    public $actor;

    /**
     * @var User
     */
    public $executor;

    public function __construct(User $actor, User $executor)
    {
        $this->actor = $actor;
        $this->executor = $executor;
    }
}
