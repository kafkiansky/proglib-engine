<?php

declare(strict_types=1);

namespace App\Domain\Post\Event;

use App\Model\Notification\Entity\NotificationRepository;
use phpcent\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class PostCommentedHandler implements MessageHandlerInterface
{
    /**
     * @var Client
     */
    private $message;

    /**
     * @var NotificationRepository
     */
    private $notifications;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Client $message, NotificationRepository $notifications, LoggerInterface $logger)
    {
        $this->message = $message;
        $this->notifications = $notifications;
        $this->logger = $logger;
    }

    public function __invoke(PostCommented $postCommented): void
    {
        if ($postCommented->actor === $postCommented->executor) {
            return;
        }

        $actorId = $postCommented->actor->getId();

        $userChannel = 'alerts#' . $actorId;

        try {
            $onlineChannels = $this->message->channels();
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => 'Centrifugo not working.']);
            return;
        }

        if (!in_array($userChannel, $onlineChannels->result->channels)) {
            return;
        }

        $this->message->publish($userChannel, [
            'notificationsCount' =>
                $this->notifications->actualNotificationCount($postCommented->actor)
        ]);
    }
}
