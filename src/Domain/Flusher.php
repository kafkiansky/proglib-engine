<?php

declare(strict_types=1);

namespace App\Domain;

use App\Model\AggregateRoot;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class Flusher
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MessageBusInterface
     */
    private $bus;

    /**
     * @param EntityManagerInterface $em
     * @param MessageBusInterface $bus
     */
    public function __construct(EntityManagerInterface $em, MessageBusInterface $bus)
    {
        $this->em = $em;
        $this->bus = $bus;
    }

    public function save($object): void
    {
        $this->em->persist($object);
        $this->em->flush();
    }

    public function flush(AggregateRoot ...$roots): void
    {
        $this->em->flush();

        foreach ($roots as $root) {
            foreach ($root->releaseEvents() as $message) {
                $this->bus->dispatch($message);
            }
        }
    }

    /**
     * @param $object
     * @param bool $andFlush
     */
    public function persist($object, $andFlush = false): void
    {
        if (true === $andFlush) {
            $this->em->persist($object);
            $this->flush();
        } else {
            $this->em->persist($object);
        }

    }

    /**
     * @param $object
     * @param bool $andFlush
     */
    public function remove($object, $andFlush = false): void
    {
        if (true === $andFlush) {
            $this->em->persist($object);
            $this->em->flush();
        } else {
            $this->em->remove($object);
        }
    }
}
