<?php

namespace App\Domain;

use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

trait CommonEntityTrait
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     options={"comment": "Show datetime when was created"}
     *     )
     */
    private $createdAt;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(
     *     type="datetime",
     *     nullable=false,
     *     options={"comment": "Show datetime when was updated."}
     *     )
     */
    private $updatedAt;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface
     */
    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }
}