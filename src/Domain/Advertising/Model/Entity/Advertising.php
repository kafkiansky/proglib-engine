<?php

declare(strict_types=1);

namespace App\Domain\Advertising\Model\Entity;

use App\Domain\CommonEntityTrait;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Advertising\Repository\AdvertisingRepository")
 * @ORM\Table(name="advertisings")
 */
class Advertising
{
    use CommonEntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $adContent;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $adImage;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $adLink;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyName;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=false, options={"default": 0})
     */
    private $views;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false, options={"default": 0})
     */
    private $isPaid;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $adLocation;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $companyEmail;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishOn;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
    }

    /**
     * @param string $adContent
     * @param string $adImage
     * @param string $adLink
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param string $adLocation
     * @param string|null $companyName
     * @param string|null $companyEmail
     * @param DateTime $publishOn
     *
     * @return Advertising
     *
     * @throws Exception
     */
    public static function fromCompanyRequest(
        string $adContent,
        string $adImage,
        string $adLink,
        DateTime $publishOn,
        DateTime $dateStart,
        DateTime $dateEnd,
        string $adLocation,
        ?string $companyName = null,
        ?string $companyEmail = null
    ): Advertising {

        $ad = new self();
        $ad->adContent = $adContent;
        $ad->adImage = $adImage;
        $ad->adLink = $adLink;
        $ad->companyName = $companyName;
        $ad->companyEmail = $companyEmail;
        $ad->adLocation = $adLocation;
        $ad->dateStart = $dateStart;
        $ad->dateEnd = $dateEnd;
        $ad->publishOn = $publishOn;
        $ad->isPaid = 1;
        $ad->views = 0;

        return $ad;
    }

    /**
     * @param string $adImage
     * @param string $adLink
     * @param string|null $adContent
     *
     * @return Advertising
     *
     * @throws Exception
     */
    public static function fromSimpleRequest(
        string $adImage,
        string $adLink,
        ?string $adContent = null
    ): Advertising {

        $ad = new self();
        $ad->adImage = $adImage;
        $ad->adLink = $adLink;
        $ad->adContent = $adContent;
        $ad->views = 0;
        $ad->isPaid = 0;

        return $ad;
    }

    /**
     * @return string|null
     */
    public function getAdContent(): ?string
    {
        return $this->adContent;
    }

    /**
     * @return string|null
     */
    public function getAdImage(): ?string
    {
        return $this->adImage;
    }

    /**
     * @return string
     */
    public function getAdLink(): string
    {
        return $this->adLink;
    }

    /**
     * @return string|null
     */
    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->isPaid === 1;
    }

    /**
     * @return DateTime|null
     */
    public function getDateStart(): ?DateTime
    {
        return $this->dateStart;
    }

    /**
     * @return DateTime|null
     */
    public function getDateEnd(): ?DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @return string|null
     */
    public function getAdLocation(): ?string
    {
        return $this->adLocation;
    }

    /**
     * @return string|null
     */
    public function getCompanyEmail(): ?string
    {
        return $this->companyEmail;
    }
}
