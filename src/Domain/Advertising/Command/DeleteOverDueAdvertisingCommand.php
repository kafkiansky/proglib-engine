<?php

declare(strict_types=1);

namespace App\Domain\Advertising\Command;

use App\Domain\Advertising\Model\Entity\Advertising;
use App\Domain\Advertising\Repository\AdvertisingRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use MyBuilder\Bundle\CronosBundle\Annotation\Cron;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @Cron(minute="0", hour="21", noLogs=false)
 */
class DeleteOverDueAdvertisingCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var AdvertisingRepository
     */
    private $advertisingRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EntityManagerInterface $entityManager
     * @param AdvertisingRepository $advertisingRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        AdvertisingRepository $advertisingRepository,
        LoggerInterface $logger
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->advertisingRepository = $advertisingRepository;
        $this->logger = $logger;
    }

    protected function configure(): void
    {
        $this->setName('advertising:delete');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): void
    {
        $ads = $this->advertisingRepository->findPaidAds();

        $deletedItems = [];

        /** @var Advertising $ad */
        foreach ($ads as $ad) {
            if ($ad->getDateEnd()->format('Y-m-d') !==
                (new \DateTime())->format('Y-m-d')
            ) {
                continue;
            }

            $deletedItems[] = $ad->getCompanyName();

            $this->entityManager->remove($ad);
        }

        $this->entityManager->flush();

        $this->logger->info(
            sprintf('Удалены следующие баннеры: %s',
                implode('; ', $deletedItems)
            )
        );
    }
}
