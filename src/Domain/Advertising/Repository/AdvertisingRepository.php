<?php

declare(strict_types=1);

namespace App\Domain\Advertising\Repository;

use App\Domain\Advertising\Model\Entity\Advertising;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class AdvertisingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Advertising::class);
    }

    /**
     * @return mixed
     */
    public function findPaidAds()
    {
        return $this->createQueryBuilder('a')
            ->where('a.isPaid = 1')
            ->getQuery()
            ->getResult();
    }
}