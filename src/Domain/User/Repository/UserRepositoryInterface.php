<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\User\UseCase\Admin\Filter\Command;
use Knp\Component\Pager\Pagination\PaginationInterface;

interface UserRepositoryInterface
{
    public function findByNetworkAndClientId(string $network, string $clientId);

    public function findOneUserBy(array $criteria);

    public function all(Command $command, int $page, int $size, string $sort, string $direction): PaginationInterface;

    public function allByUsername(string $username): array;
}
