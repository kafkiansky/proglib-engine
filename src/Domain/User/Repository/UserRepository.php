<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\User\Model\Entity\User;
use App\Domain\User\UseCase\Admin\Filter\Command;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(
        EntityManagerInterface $em,
        Connection $connection,
        PaginatorInterface $paginator
    ) {
        $this->em = $em;
        $this->connection = $connection;
        $this->paginator = $paginator;
        $this->repository = $em->getRepository(User::class);
    }

    /**
     * @param string $network
     * @param string $clientId
     *
     * @return bool
     *
     * @throws NonUniqueResultException
     */
    public function findByNetworkAndClientId(string $network, string $clientId)
    {
        return $this->repository->createQueryBuilder('u')
                ->select('COUNT(u.id)')
                ->innerJoin('u.networks', 'n')
                ->andWhere('n.network = :network and n.clientId = :clientId')
                ->setParameter(':network', $network)
                ->setParameter(':clientId', $clientId)
                ->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @param Command $command
     * @param int $page
     * @param int $size
     * @param string $sort
     * @param string $direction
     *
     * @return PaginationInterface
     */
    public function all(Command $command, int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'u.id',
                'u.email',
                'u.role',
                'u.oauthType AS network',
                'u.status'
            )
            ->from('users', 'u');

        if ($command->email) {
            $qb->andWhere($qb->expr()->like('u.email', ':email'));
            $qb->setParameter(':email', '%' . mb_strtolower($command->email) . '%');
        }

        if ($command->network) {
            $qb->andWhere('u.oauthType = :network');
            $qb->setParameter('network', $command->network);
        }

        if ($command->role) {
            $qb->andWhere('u.role = :role');
            $qb->setParameter('role', $command->role);
        }

        if ($command->status) {
            $qb->andWhere('u.status = :status');
            $qb->setParameter('status', $command->status);
        }

        if (!\in_array($sort, ['email', 'network', 'role', 'status'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($qb, $page, $size);
    }

    /**
     * @param array $criteria
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findOneUserBy(array $criteria)
    {
        return $this->repository
            ->createQueryBuilder('u')
            ->where('u.'. key($criteria) . ' = :criteria')
            ->setParameter('criteria', reset($criteria))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $username
     *
     * @return array
     */
    public function allByUsername(string $username): array
    {
        return $this->connection->createQueryBuilder()
            ->select('u.email AS username', 'u.userPic')
            ->from('users', 'u')
            ->where('u.email LIKE :username')
            ->setParameter('username', $username . '%')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param $id
     * @return object|null
     * @throws EntityNotFoundException
     */
    public function get($id)
    {
        $user = $this->repository->find($id);

        if (!$user) {
            throw new EntityNotFoundException(sprintf('User with id %s not found', $id));
        }

        return $user;
    }
}
