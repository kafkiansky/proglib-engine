<?php

declare(strict_types=1);

namespace App\Domain\User\Repository;

use App\Domain\User\Model\Entity\SubscribedOnMailUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class SubscribedOnMailUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubscribedOnMailUser::class);
    }
}
