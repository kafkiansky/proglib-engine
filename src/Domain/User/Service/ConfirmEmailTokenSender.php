<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Swift_Message;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ConfirmEmailTokenSender
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param Swift_Mailer $mailer
     * @param Environment $twig
     * @param LoggerInterface $logger
     */
    public function __construct(Swift_Mailer $mailer, Environment $twig, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->logger = $logger;
    }

    /**
     * @param string $email
     * @param string $token
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function send(string $email, string $token): void
    {
        $message = (new Swift_Message())
            ->setTo($email)
            ->setFrom('hello@proglib.io')
            ->setBody(
                $this->twig->render('user/mail/confirmEmail.html.twig', [
                    'token' => $token
                ]),'text/html');

        if (!$this->mailer->send($message)) {
            $this->logger->error(sprintf('Unable to send message to user with email %s',
                $email
            ));
        }
    }
}
