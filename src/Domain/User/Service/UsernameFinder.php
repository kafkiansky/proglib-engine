<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use Psr\Log\LoggerInterface;

class UsernameFinder
{
    /**
     * @var UserRepositoryInterface
     */
    private $users;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(UserRepositoryInterface $users, LoggerInterface $logger)
    {
        $this->users = $users;
        $this->logger = $logger;
    }

    /**
     * @param string $content
     * @return string
     */
    public function find(string $content): string
    {
        $pattern = '/@[a-zA-Z.]+(@[a-zA-Z.]+)?/';

        preg_match_all($pattern, $content, $matches);

        if (!$matches) {
            return $content;
        }

        return preg_replace_callback($pattern, [self::class, 'replaceAllMentionsByUsername'], $content);
    }

    /**
     * @param $matches
     * @return string|null
     */
    private function replaceAllMentionsByUsername($matches): ?string
    {
        list($username, ) = $matches;
        $usernameToFind = ltrim($username, '@');

        /** @var User $user */
        $user = $this->users->findOneUserBy(['email' => $usernameToFind]);

        if (!$user) {
            return null;
        }

        return "[@{$user->getEmail()}](https://google.com)";
    }

    /**
     * @param string $content
     * @return string
     */
    public function replace(string $content): string
    {
        $pattern = '/\[(@.+?)\](\((.+?)\))/';

        preg_match_all($pattern, $content, $matches);

        if (!$matches) {
            return $content;
        }

        return preg_replace_callback($pattern, [self::class, 'replaceAllUsernamesByMentions'], $content);
    }

    /**
     * @param $matches
     * @return string
     */
    private function replaceAllUsernamesByMentions($matches): string
    {
        list(, $mention) = $matches;

        return $mention;
    }
}
