<?php

declare(strict_types=1);

namespace App\Domain\User\Service;

use Exception;
use Ramsey\Uuid\Uuid;

class ConfirmEmailTokenizer
{
    /**
     * @return string
     *
     * @throws Exception
     */
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
