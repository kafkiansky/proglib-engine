<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use App\Domain\CommonEntityTrait;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use Ramsey\Uuid\Uuid;
use League\OAuth2\Client\Provider\FacebookUser;
use League\OAuth2\Client\Provider\GoogleUser;

/**
 * @ORM\Entity(repositoryClass="App\Domain\User\Repository\UserOnConfirmRepository")
 * @ORM\Table(name="users_on_confirm")
 */
class UserOnConfirm
{
    use CommonEntityTrait;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    private $clientId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $locale;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $avatarUrl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $githubHtmlUrl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $githubApiUrl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $oauthType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->createdAt = new DateTime('now');
        $this->updatedAt = new DateTime('now');
    }

    /**
     * @param GithubResourceOwner $githubUser
     *
     * @return UserOnConfirm
     *
     * @throws Exception
     */
    public static function fromGithub(GithubResourceOwner $githubUser): UserOnConfirm
    {
        $userOnConfirm = new self();

        $userOnConfirm->clientId = $githubUser->getId();
        $userOnConfirm->avatarUrl = $githubUser->toArray()['avatar_url'];
        $userOnConfirm->githubHtmlUrl = $githubUser->toArray()['html_url'];
        $userOnConfirm->githubApiUrl = $githubUser->toArray()['url'];
        $userOnConfirm->locale = $githubUser->toArray()['location'];
        $userOnConfirm->name = $githubUser->getName();
        $userOnConfirm->oauthType = Request::github()->getRequest();

        return $userOnConfirm;
    }

    /**
     * @param FacebookUser $facebookUser
     *
     * @return UserOnConfirm
     *
     * @throws Exception
     */
    public static function fromFacebook(FacebookUser $facebookUser): UserOnConfirm
    {
        $userOnConfirm = new self();

        $userOnConfirm->clientId = $facebookUser->getId();
        $userOnConfirm->avatarUrl = $facebookUser->getPictureUrl();
        $userOnConfirm->name = $facebookUser->getName();
        $userOnConfirm->locale = $facebookUser->getLocale();
        $userOnConfirm->oauthType = Request::facebook()->getRequest();

        return $userOnConfirm;
    }

    /**
     * @param GoogleUser $googleUser
     *
     * @return UserOnConfirm
     *
     * @throws Exception
     */
    public static function fromGoogle(GoogleUser $googleUser): UserOnConfirm
    {
        $userOnConfirm = new self();

        $userOnConfirm->clientId = $googleUser->getId();
        $userOnConfirm->avatarUrl = $googleUser->getAvatar() ?? null;
        $userOnConfirm->name = $googleUser->getName();
        $userOnConfirm->locale = $googleUser->getLocale();
        $userOnConfirm->oauthType = Request::google()->getRequest();

        return $userOnConfirm;
    }

    /**
     * @param string $clientId
     *
     * @return UserOnConfirm
     *
     * @throws Exception
     */
    public static function fromVk(int $clientId): UserOnConfirm
    {
        $userOnConfirm = new self();

        $userOnConfirm->clientId = $clientId;
        $userOnConfirm->oauthType = Request::vk()->getRequest();

        return $userOnConfirm;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $email
     * @param string $token
     */
    public function emailToConfirm(string $email, string $token)
    {
        $this->email = $email;
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->avatarUrl;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getGithubHtmlUrl()
    {
        return $this->githubHtmlUrl;
    }

    /**
     * @return mixed
     */
    public function getGithubApiUrl()
    {
        return $this->githubApiUrl;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getOauthType()
    {
        return $this->oauthType;
    }

    /**
     * @param string $oauthType
     *
     * @return bool
     */
    public function isOAuthType(string $oauthType): bool
    {
        return $this->oauthType === $oauthType;
    }
}
