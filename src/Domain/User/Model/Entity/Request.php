<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use Webmozart\Assert\Assert;

class Request
{
    public const GOOGLE = 'Google';
    public const GITHUB = 'Github';
    public const FACEBOOK = 'Facebook';
    public const VK = 'ВКонтакте';

    /**
     * @var string
     */
    private $request;

    /**
     * @param string $request
     */
    public function __construct(string $request)
    {
        Assert::oneOf($request,[
            self::GOOGLE,
            self::GITHUB,
            self::VK,
            self::FACEBOOK
        ]);

        $this->request = $request;
    }

    /**
     * @return Request
     */
    public static function google(): self
    {
        return new self(self::GOOGLE);
    }

    /**
     * @return Request
     */
    public static function github(): self
    {
        return new self(self::GITHUB);
    }

    /**
     * @return Request
     */
    public static function vk(): self
    {
        return new self(self::VK);
    }

    /**
     * @return Request
     */
    public static function facebook(): self
    {
        return new self(self::FACEBOOK);
    }

    /**
     * @return string
     */
    public function getRequest(): string
    {
        return $this->request;
    }
}
