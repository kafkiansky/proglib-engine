<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use App\Domain\CommonEntityTrait;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="networks")
 */
class Network
{
    public const GOOGLE = 'Google';
    public const FACEBOOK = 'Facebook';
    public const VK = 'Vk';
    public const GITHUB = 'Github';

    use CommonEntityTrait;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="networks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private $clientId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $network;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $githubHtmlUrl = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $githubApiUrl = null;

    /**
     * @param User $user
     * @param string $network
     * @param $clientId
     * @param string $avatar
     * @param string|null $email
     * @param string|null $githubHtmlUrl
     * @param string|null $githubApiUrl
     * @throws Exception
     */
    public function __construct(
        User $user,
        string $network,
        $clientId,
        string $avatar,
        string $email = null,
        string $githubHtmlUrl = null,
        string $githubApiUrl = null
    ) {
        $this->id = Uuid::uuid4()->toString();
        $this->user = $user;
        $this->network = $network;
        $this->clientId = $clientId;
        $this->githubHtmlUrl = $githubHtmlUrl;
        $this->githubApiUrl = $githubApiUrl;
        $this->email = $email;
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
        $this->avatar = $avatar;
    }

    /**
     * @param string $network
     * @param string $clientId
     *
     * @return bool
     */
    public function isFor(string $network, string $clientId): bool
    {
        return $this->network === $network && $this->clientId === $clientId;
    }

    /**
     * @param string $network
     *
     * @return bool
     */
    public function isForNetwork(string $network): bool
    {
        return $this->network === $network;
    }

    /**
     * @return string
     */
    public function getNetwork(): string
    {
        return $this->network;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $githubHtmlUrl
     */
    public function changeGithubUrl(string $githubHtmlUrl)
    {
        $this->githubHtmlUrl = $githubHtmlUrl;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public static function networks(): array
    {
        return [
            self::GOOGLE,
            self::GITHUB,
            self::VK,
            self::FACEBOOK
        ];
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }
}
