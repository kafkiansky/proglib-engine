<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use App\Domain\User\Model\ValueObject\Email;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass="App\Domain\User\Repository\SubscribedOnMailUserRepository")
 * @ORM\Table(name="subscribed_on_mail_users")
 */
class SubscribedOnMailUser
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Embedded(class="App\Domain\User\Model\ValueObject\Email", columnPrefix=false)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=false, options={"default": 1})
     */
    private $isStillSubscribe;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $subscribedAt;

    /**
     * @throws Exception
     */
    private function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->isStillSubscribe = 1;
        $this->subscribedAt = new \DateTimeImmutable();
    }

    /**
     * @param Email $email
     *
     * @return SubscribedOnMailUser
     *
     * @throws Exception
     */
    public static function subscribe(Email $email): SubscribedOnMailUser
    {
        $user = new self();
        $user->email = $email;

        return $user;
    }
}
