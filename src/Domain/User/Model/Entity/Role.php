<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use Webmozart\Assert\Assert;

class Role
{
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    public const ROLE_SIMPLE_ADMIN = 'ROLE_SIMPLE_ADMIN';
    public const ROLE_CHIEF_EDITOR = 'ROLE_CHIEF_EDITOR';
    public const ROLE_EDITOR = 'ROLE_EDITOR';
    public const ROLE_AUTHOR = 'ROLE_AUTHOR';
    public const ROLE_DEVELOPER = 'ROLE_DEVELOPER';
    public const ROLE_USER = 'ROLE_USER';

    /**
     * @var string
     */
    private $role;

    /**
     * @param string $role
     */
    public function __construct(string $role)
    {
        Assert::oneOf($role, [
            self::ROLE_SUPER_ADMIN,
            self::ROLE_SIMPLE_ADMIN,
            self::ROLE_CHIEF_EDITOR,
            self::ROLE_EDITOR,
            self::ROLE_AUTHOR,
            self::ROLE_DEVELOPER,
            self::ROLE_USER
        ]);

        $this->role = $role;
    }

    /**
     * @return Role
     */
    public static function assignUser(): Role
    {
        return new self(self::ROLE_USER);
    }

    /**
     * @return Role
     */
    public static function assignSimpleAdmin(): Role
    {
        return new self(self::ROLE_SIMPLE_ADMIN);
    }

    /**
     * @return Role
     */
    public static function assignAuthor(): Role
    {
        return new self(self::ROLE_AUTHOR);
    }

    /**
     * @return Role
     */
    public static function assignDeveloper(): Role
    {
        return new self(self::ROLE_DEVELOPER);
    }

    /**
     * @return Role
     */
    public static function assignChiefEditor(): Role
    {
        return new self(self::ROLE_CHIEF_EDITOR);
    }

    /**
     * @return Role
     */
    public static function assignEditor(): Role
    {
        return new self(self::ROLE_EDITOR);
    }


    /**
     * @return bool
     */
    public function isUser(): bool
    {
        return $this->role === self::ROLE_USER;
    }

    /**
     * @return bool
     */
    public function isEditor(): bool
    {
        return $this->role === self::ROLE_EDITOR;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role === self::ROLE_SIMPLE_ADMIN;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param Role $role
     *
     * @return bool
     */
    public function isEqual(Role $role): bool
    {
        return $this->getRole() === $role->getRole();
    }

    /**
     * @return array
     */
    public static function roles()
    {
        return [
            self::ROLE_SUPER_ADMIN,
            self::ROLE_SIMPLE_ADMIN,
            self::ROLE_CHIEF_EDITOR,
            self::ROLE_EDITOR,
            self::ROLE_AUTHOR,
            self::ROLE_DEVELOPER,
            self::ROLE_USER
        ];
    }

    /**
     * @return array
     */
    public static function premiumRoles(): array
    {
        return [
            self::ROLE_SUPER_ADMIN,
            self::ROLE_SIMPLE_ADMIN,
            self::ROLE_CHIEF_EDITOR,
            self::ROLE_EDITOR,
            self::ROLE_AUTHOR,
            self::ROLE_DEVELOPER,
        ];
    }

    /**
     * @return array
     */
    public static function highRoles(): array
    {
        return [
            self::ROLE_SUPER_ADMIN,
            self::ROLE_SIMPLE_ADMIN,
            self::ROLE_CHIEF_EDITOR,
            self::ROLE_EDITOR,
        ];
    }
}
