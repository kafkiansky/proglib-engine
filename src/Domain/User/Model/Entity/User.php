<?php

declare(strict_types=1);

namespace App\Domain\User\Model\Entity;

use App\Domain\CommonEntityTrait;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Model\Test\Entity\Test;
use App\Domain\User\Event\ChangedEmailMessage;
use App\Domain\User\Model\ValueObject\Email;
use App\Domain\User\Model\ValueObject\Token;
use App\Model\AggregateRoot;
use App\Model\EventsTrait;
use App\Model\Notification\Entity\Notification;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use DomainException;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User implements UserInterface, AggregateRoot
{
    use CommonEntityTrait;
    use EventsTrait;

    public const STATUS_ACTIVE = 'Active';
    public const STATUS_BLOCKED = 'Blocked';
    public const STATUS_WAIT = 'Wait';

    public const PUSH_ON = 'true';
    public const PUSH_OFF = 'false';

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="clientId", nullable=false, unique=true)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $nickname;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=false, name="avatar")
     */
    private $avatar;

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true, name="lastLogin")
     */
    private $lastLogin;

    /**
     * @var Role[]
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=25)
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, name="githubHtmlUrl")
     */
    private $githubHtmlUrl = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true, name="githubApiUrl")
     */
    private $githubApiUrl = null;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $locale = null;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, name="oauthType")
     */
    private $oauthType;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true, options={"default": "on"})
     */
    private $pushNotifications;

    /**
     * @var string
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isSubscribeForNotifications;


    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Domain\Post\Model\Entity\Post", mappedBy="userLikes")
     */
    private $postsLiked;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Test\Entity\Test", mappedBy="userLikes")
     */
    private $testsLiked;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Comment\Entity\Comment", mappedBy="userLikes")
     */
    private $commentsLiked;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Domain\Post\Model\Entity\Post", mappedBy="userBookmarks")
     */
    private $postsBookmarked;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Comment\Entity\Comment", mappedBy="userBookmarks")
     */
    private $commentsBookmarked;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Model\Test\Entity\Test", mappedBy="userBookmarks")
     */
    private $testsBookmarked;

    /**
     * @var null|Token $token
     *
     * @ORM\Embedded(class="App\Domain\User\Model\ValueObject\Token", columnPrefix=false)
     */
    private $token = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $changedEmail;

    /**
     * @var Post[]
     *
     * @ORM\OneToMany(targetEntity="App\Domain\Post\Model\Entity\Post", mappedBy="user", cascade={"persist", "remove"})
     */
    private $posts;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Domain\Post\Model\Entity\Tag", inversedBy="user", cascade={"persist"})
     * @ORM\JoinTable(name="user_tags")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Model\Comment\Entity\Comment", mappedBy="user")
     */
    private $comments;

    /**
     * @var Notification[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="actor")
     */
    private $notifications;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $resume;

    /**
     * @var Network[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Network", mappedBy="user", orphanRemoval=true, cascade={"persist"})
     */
    private $networks;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->role = Role::assignUser()->getRole();
        $this->status = self::STATUS_ACTIVE;
        $this->postsLiked = new ArrayCollection();
        $this->commentsLiked = new ArrayCollection();
        $this->postsBookmarked = new ArrayCollection();
        $this->commentsBookmarked = new ArrayCollection();
        $this->testsLiked = new ArrayCollection();
        $this->testsBookmarked = new ArrayCollection();
        $this->networks = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->lastLogin = new DateTime('now');
        $this->createdAt = new DateTime('now');
        $this->updatedAt = new DateTime('now');
        $this->comments = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->isSubscribeForNotifications = 1;
    }

    /**
     * @param string $clientId
     * @param string $email
     * @param string $nickname
     * @param string $avatar
     * @param string $locale
     *
     * @return User
     *
     * @throws Exception
     */
    public static function fromGoogleRequest(
        string $clientId,
        string $email,
        string $nickname,
        string $avatar,
        string $locale
    ): User {

        $user = new self();
        $user->clientId = $clientId;
        $user->email = $email;
        $user->nickname = $nickname;
        $user->oauthType = Request::google()->getRequest();
        $user->avatar = $avatar;
        $user->locale = $locale ?? null;

        return $user;
    }

    /**
     * @param string $clientId
     * @param string $email
     * @param string $nickname
     * @param string $avatar
     * @param string $locale
     * @param string $githubHtmlUrl
     * @param string $githubApiUrl
     *
     * @return User
     *
     * @throws Exception
     */
    public static function fromGithubRequest(
        string $clientId,
        string $email,
        string $avatar,
        string $githubHtmlUrl,
        string $githubApiUrl,
        ?string $locale = null,
        ?string $nickname = null
    ): User {

        $user = new self();
        $user->clientId = $clientId;
        $user->email = $email;
        $user->nickname = $nickname ?? null;
        $user->oauthType = Request::github()->getRequest();
        $user->avatar = $avatar;
        $user->locale = $locale ?? null;
        $user->githubHtmlUrl = $githubHtmlUrl ?? null;
        $user->githubApiUrl = $githubApiUrl ?? null;

        return $user;
    }

    /**
     * @param string $clientId
     * @param string $email
     * @param string $nickname
     * @param string $avatar
     * @param string $locale
     *
     * @return User
     *
     * @throws Exception
     */
    public static function fromFacebookRequest(
        string $clientId,
        string $email,
        string $nickname,
        string $avatar,
        string $locale
    ): User {

        $user = new self();
        $user->clientId = $clientId;
        $user->email = $email;
        $user->oauthType = Request::facebook()->getRequest();
        $user->nickname = $nickname ?? null;
        $user->avatar = $avatar;
        $user->locale = $locale ?? null;

        return $user;
    }

    /**
     * @param $clientId
     * @param string $email
     * @param string $avatar
     * @return User
     *
     * @throws Exception
     */
    public static function fromVkRequest($clientId, string $email, string $avatar): User
    {
        $user = new self();
        $user->clientId = (string) $clientId;
        $user->email = $email;
        $user->avatar = $avatar;
        $user->oauthType = Request::vk()->getRequest();

        return $user;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag): void
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;

            $tag->addUser($this);
        }
    }

    /**
     * @param $status
     * @throws Exception
     */
    public function chooseSubscribeStatus($status): void
    {
        $this->isSubscribeForNotifications = $status === "true" ? 1 : 0;
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag): void
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeUser($this);
        }
    }

    /**
     * @param Email $email
     * @param string $name
     * @param string $city
     *
     * @throws Exception
     */
    public function changeProfile(Email $email, string $name, string $city): void
    {
        if (!filter_var($email->getValue(), FILTER_VALIDATE_EMAIL)) {
            throw new DomainException('Неверный электронный адрес');
        }

        if ($this->email === $email->getValue()) {
            throw new DomainException('Пользователь с такой почтой уже существует');
        }

        $this->changedEmail = mb_strtolower($email->getValue());
        $this->name = $name;
        $this->city = $city;
        $this->token = Token::build();
        $this->updatedAt = new DateTimeImmutable();

        $this->recordEvent(new ChangedEmailMessage($this));
    }

    /**
     * @param string $name
     * @param string $city
     * @throws Exception
     */
    public function changeProfileWithoutEmail(string $name, string $city)
    {
        $this->name = $name;
        $this->city = $city;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeEmail(string $email)
    {
        $this->email = $email;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function destroyToken()
    {
        $this->token = null;
    }

    /**
     * @param string $city
     *
     * @throws Exception
     */
    public function changeCity(string $city)
    {
        $this->city = $city;
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param string $email
     */
    public function confirmNewEmail(string $email)
    {
        $this->email = $email;
        $this->token = null;
        $this->changedEmail = null;
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * @throws Exception
     */
    public function updateLastLoginTime(): void
    {
        $this->lastLogin = new DateTime('now');
    }

    /**
     * @param string $resume
     *
     * @throws Exception
     */
    public function changeResume(string $resume): void
    {
        $this->resume = $resume;
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @throws Exception
     */
    public function removeResume(): void
    {
        $this->resume = null;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeRole(string $role)
    {
        $this->role = $role;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeStatus(string $status)
    {
        $this->status = $status;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function changeGithub(string $githubHtml, string $githubApi)
    {
        $this->githubHtmlUrl = $githubHtml;
        $this->githubApiUrl = $githubApi;
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param string $network
     * @param string $clientId
     * @param string $avatar
     * @param string|null $email
     * @param string|null $githubUrl
     * @param string|null $githubApiUrl
     * @throws Exception
     */
    public function attachNetwork(
        string $network,
        string $clientId,
        string $avatar,
        string $email = null,
        string $githubUrl = null,
        string $githubApiUrl = null
    ): void {
        foreach ($this->networks as $existingNetwork) {
            if ($existingNetwork->isForNetwork($network)) {
                throw new DomainException('Социальная сеть уже привязана');
            }
        }

        $this->networks->add(new Network($this, $network, $clientId, $avatar, $email, $githubUrl, $githubApiUrl));
    }

    /**
     * @param string $network
     * @param string $clientId
     */
    public function detachNetwork(string $network, string $clientId): void
    {
        foreach ($this->networks as $existingNetwork) {
            if ($existingNetwork->isFor($network, $clientId)) {
                $this->networks->removeElement($existingNetwork);
                return;
            }
        }
    }

    /**
     * @param string $clientId
     * @param string $email
     *
     * @throws Exception
     */
    public function reinstateGrants(string $clientId, string $email)
    {
        $this->clientId = $clientId;
        $this->email = $email;
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param string $nickname
     * @throws Exception
     */
    public function changeNickname(string $nickname): void
    {
        $this->nickname = $nickname;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getGithubHtmlUrl(): ?string
    {
        return $this->githubHtmlUrl;
    }

    /**
     * @return null|string
     */
    public function getGithubApiUrl(): ?string
    {
        return $this->githubApiUrl;
    }

    /**
     * @return null|string
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getNickName(): ?string
    {
        return $this->nickname;
    }

    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return '';
    }

    /**
     * @return null|string
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @return DateTimeInterface
     */
    public function getLastLogin(): DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @return string
     */
    public function getOauthType(): string
    {
        return $this->oauthType;
    }

    /**
     * @return void
     */
    public function eraseCredentials(): void
    {
    }

    /**
     * @return mixed
     */
    public function getPostsLiked()
    {
        return $this->postsLiked;
    }

    /**
     * @return mixed
     */
    public function getCommentsLiked()
    {
        return $this->commentsLiked;
    }


    public function getPostsBookmarked()
    {
        return $this->postsBookmarked;
    }

    public function getCommentsBookmarked()
    {
        return $this->commentsBookmarked;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isSubscribeForPush(): bool
    {
        return $this->pushNotifications === self::PUSH_ON;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token->getValue();
    }

    /**
     * @return string|null
     */
    public function getChangedEmail(): ?string
    {
        return $this->changedEmail;
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param string $avatar
     */
    public function changeAvatar(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @return string
     */
    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function getNetworks()
    {
        return $this->networks;
    }

    public function getRoles()
    {
        return [$this->role];
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isSubscribeForNotifications(): bool
    {
        return $this->isSubscribeForNotifications === true;
    }
}
