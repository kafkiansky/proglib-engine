<?php

declare(strict_types=1);

namespace App\Domain\User\Model\ValueObject;

use App\Domain\User\Exception\InvalidEmailException;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
final class Email
{
    /**
     * @ORM\Column(
     *     type="string",
     *     unique=true,
     *     nullable=false,
     *     length=255,
     *     options={"comment": "Email must be unique."}
     *     )
     */
    private $email;

    /**
     * @param string $email
     *
     * @throws InvalidEmailException
     */
    public function __construct(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidEmailException(sprintf('%s - is not a valid email', $email));
        }

        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->email;
    }
}