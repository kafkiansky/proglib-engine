<?php

declare(strict_types=1);

namespace App\Domain\User\Model\ValueObject;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Embeddable()
 */
final class Token
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="token")
     */
    private $value;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->value = Uuid::uuid1()->toString();
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return Token
     * @throws Exception
     */
    public static function build(): Token
    {
        return new self();
    }
}