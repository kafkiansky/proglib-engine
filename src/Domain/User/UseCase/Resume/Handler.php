<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Resume;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use Exception;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @param User $user
     *
     * @throws Exception
     */
    public function handle(Command $command, User $user): void
    {
        $user->changeResume($command->resume);
        $this->flusher->flush();
    }
}
