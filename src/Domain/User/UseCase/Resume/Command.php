<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Resume;

class Command
{
    /**
     * @var string
     */
    public $resume;

    /**
     * @param string $resume
     */
    public function __construct(string $resume)
    {
        $this->resume = $resume;
    }
}