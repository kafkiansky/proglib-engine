<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Filter;

use App\Domain\User\Model\Entity\Network;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Почта',
                    'onchange' => 'this.form.submit()',
                ]
            ])
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'Пользователь' => Role::ROLE_USER,
                    'Автор' => Role::ROLE_AUTHOR,
                    'Редактор' => Role::ROLE_EDITOR,
                    'Главред' => Role::ROLE_CHIEF_EDITOR,
                    'Разработчик' => Role::ROLE_DEVELOPER,
                    'Администратор' => Role::ROLE_SIMPLE_ADMIN,
                    'Суперадминистратор' => Role::ROLE_SUPER_ADMIN
                ],
                'required' => false,
                'placeholder' => 'Роль',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Активный' => User::STATUS_ACTIVE,
                    'Заблокирован' => User::STATUS_BLOCKED,
                    'На подтверждении' =>  User::STATUS_WAIT
                ],
                'required' => false,
                'placeholder' => 'Статус',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('network', ChoiceType::class, [
                'choices' => [
                    'ВКонтакте' => Network::VK,
                    'Google' => Network::GOOGLE,
                    'Facebook' => Network::FACEBOOK,
                    'Github' => Network::GITHUB
                ],
                'required' => false,
                'placeholder' => 'Социальная сеть',
                'attr' => [
                    'onchange' => 'this.form.submit()',
                ]
            ]);


    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}
