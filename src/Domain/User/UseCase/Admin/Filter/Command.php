<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Filter;

class Command
{
    public $role;
    public $email;
    public $status;
    public $network;
}
