<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Role;

use App\Domain\User\Model\Entity\Role;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userId', HiddenType::class)
            ->add('role', ChoiceType::class, [
                'choices' => [
                    'Пользователь' => Role::ROLE_USER,
                    'Автор' => Role::ROLE_AUTHOR,
                    'Редактор' => Role::ROLE_EDITOR,
                    'Главред' => Role::ROLE_CHIEF_EDITOR,
                    'Разработчик' => Role::ROLE_DEVELOPER,
                    'Администратор' => Role::ROLE_SIMPLE_ADMIN,
                    'Суперадминистратор' => Role::ROLE_SUPER_ADMIN
                ],
                'label' => 'Роль'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class
        ]);
    }
}
