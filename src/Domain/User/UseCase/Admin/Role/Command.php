<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Role;

use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $selfId;

    /**
     * @var Role
     */
    public $role;

    public function __construct(User $user, User $selfId)
    {
        $this->userId = $user->getId()->toString();
        $this->role = $user->getRole();
        $this->selfId = $selfId->getId()->toString();
    }
}
