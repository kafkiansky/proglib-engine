<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Block;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var UserRepositoryInterface
     */
    private $users;

    public function __construct(Flusher $flusher, UserRepositoryInterface $users)
    {
        $this->flusher = $flusher;
        $this->users = $users;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        if ($command->userId === $command->selfId) {
            throw new \DomainException('Нельзя заблокировать самого себя');
        }

        /** @var User $user */
        $user = $this->users->findOneUserBy(['id' => $command->userId]);

        $user->changeStatus(User::STATUS_BLOCKED);
        $this->flusher->flush();
    }
}
