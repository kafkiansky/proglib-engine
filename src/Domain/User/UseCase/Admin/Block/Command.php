<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\Block;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $userId;

    /**
     * @var string
     */
    public $selfId;

    public function __construct(User $user, User $self)
    {
        $this->userId = $user->getId()->toString();
        $this->selfId = $self->getId()->toString();
    }
}
