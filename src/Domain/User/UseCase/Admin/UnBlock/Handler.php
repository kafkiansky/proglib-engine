<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\UnBlock;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var UserRepositoryInterface
     */
    private $users;

    public function __construct(Flusher $flusher, UserRepositoryInterface $users)
    {
        $this->flusher = $flusher;
        $this->users = $users;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        /** @var User $user */
        $user = $this->users->findOneUserBy(['id' => $command->userId]);

        if (!$user) {
            throw new \DomainException('Такого пользователя не существует');
        }

        $user->changeStatus(User::STATUS_ACTIVE);
        $this->flusher->flush();
    }
}
