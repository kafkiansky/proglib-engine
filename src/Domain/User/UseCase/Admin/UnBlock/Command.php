<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Admin\UnBlock;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $userId;

    public function __construct(User $user)
    {
        $this->userId = $user->getId()->toString();
    }
}
