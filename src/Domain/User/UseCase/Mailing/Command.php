<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Mailing;

use App\Domain\User\Model\ValueObject\Email;

class Command
{
    /**
     * @var string
     */
    public $email;

    /**
     * @param Email $email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }
}
