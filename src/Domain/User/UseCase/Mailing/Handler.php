<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Mailing;

use App\Domain\Flusher;
use App\Domain\User\Exception\InvalidEmailException;
use App\Domain\User\Model\Entity\SubscribedOnMailUser;
use App\Domain\User\Model\ValueObject\Email;
use Exception;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @param Flusher $flusher
     */
    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     *
     * @throws InvalidEmailException
     * @throws Exception
     */
    public function handle(Command $command): void
    {
        $subscribedUser = SubscribedOnMailUser::subscribe(
            new Email($command->email->getValue())
        );

        $this->flusher->save($subscribedUser);
    }
}
