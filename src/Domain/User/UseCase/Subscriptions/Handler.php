<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Subscriptions;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepository;
use App\Domain\User\Model\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TagsRepository
     */
    private $tagsRepository;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @param Flusher $flusher
     * @param TagsRepository $tagsRepository
     * @param TokenStorageInterface $token
     */
    public function __construct(Flusher $flusher, TagsRepository $tagsRepository, TokenStorageInterface $token)
    {
        $this->flusher = $flusher;
        $this->tagsRepository = $tagsRepository;
        $this->token = $token;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $tags = [];

        foreach ($command->tags as $tagId) {
            $tags[] = $this->tagsRepository->findOneBy(['id' => $tagId]);
        }

        /** @var User $user */
        $user = $this->token->getToken()->getUser();

        /** @var Tag $userTag */
        foreach ($user->getTags() as $userTag) {
            $user->removeTag($userTag);
        }

        /** @var Tag $tag */
        foreach ($tags as $tag) {
            $user->addTag($tag);
        }

        $this->flusher->flush();
    }
}