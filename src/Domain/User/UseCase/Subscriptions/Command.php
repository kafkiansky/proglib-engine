<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Subscriptions;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var array
     *
     * @Assert\NotBlank()
     */
    public $tags = [];

    public function __construct(array $tags)
    {
        $this->tags = $tags;
    }
}
