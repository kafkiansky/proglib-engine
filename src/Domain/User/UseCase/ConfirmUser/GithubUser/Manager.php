<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\ConfirmUser\GithubUser;

use App\Application\Manager\AbstractManager;

class Manager extends AbstractManager
{
    public function save(Command $command)
    {
        $sql = '
            INSERT INTO
                user (
                clientId,
                userPic,
                githubHtmlUrl,
                githubApiUrl,
                locale,
                roles,
                is_confirm,
                created_at,
                updated_at,
                oauthType,
                )
        ';
    }
}