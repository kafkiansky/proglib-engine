<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\ConfirmUser\GithubUser;

use League\OAuth2\Client\Provider\GithubResourceOwner;

class Command
{
    /**
     * @var string
     */
    private $clientId;

    /**
     * @var string
     */
    private $avatarUrl;

    /**
     * @var string
     */
    private $htmlGithubUrl;

    /**
     * @var string
     */
    private $apiGithubUrl;

    /**
     * @var string
     */
    private $location;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @param GithubResourceOwner $githubUser
     */
    public function __construct(GithubResourceOwner $githubUser)
    {
        $this->clientId = $githubUser->getId();
        $this->avatarUrl = $githubUser->toArray()['avatar_url'];
        $this->htmlGithubUrl = $githubUser->toArray()['html_url'];
        $this->apiGithubUrl = $githubUser->toArray()['url'];
        $this->location = $githubUser->toArray()['location'];
        $this->name = $githubUser->getName();
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    public function getAvatarUrl(): string
    {
        return $this->avatarUrl;
    }

    /**
     * @return string
     */
    public function getHtmlGithubUrl(): string
    {
        return $this->htmlGithubUrl;
    }

    /**
     * @return string
     */
    public function getApiGithubUrl(): string
    {
        return $this->apiGithubUrl;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
}
