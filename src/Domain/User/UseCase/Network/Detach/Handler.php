<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Network\Detach;

use App\Domain\Flusher;
use App\Domain\User\Repository\UserSocialRepository;

class Handler
{
    public const NETWORK_IS_LAST = 222;
    public const NETWORK_IS_MAIN = 233;
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var UserSocialRepository
     */
    private $userSocialRepository;

    public function __construct(Flusher $flusher, UserSocialRepository $userSocialRepository)
    {
        $this->flusher = $flusher;
        $this->userSocialRepository = $userSocialRepository;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command)
    {
        if (!$this->userSocialRepository->findOneBy(['clientId' => $command->clientId])) {
            throw new \DomainException('Такой соц. сети нет.');
        }

        if ($command->user->getNetworks()->count() === 1) {
            throw new \DomainException('lastNetwork', self::NETWORK_IS_LAST);
        }

        if ($command->user->getClientId() === $command->clientId) {
            throw new \DomainException('mainNetwork', self::NETWORK_IS_MAIN);
        }

        $command->user->detachNetwork(
            $command->network,
            $command->clientId
        );

        $this->flusher->flush();
    }
}
