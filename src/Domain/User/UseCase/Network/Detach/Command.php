<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Network\Detach;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    public $network;

    /**
     * @var string
     */
    public $clientId;

    public function __construct(User $user, string $network, string $clientId)
    {
        $this->user = $user;
        $this->network = $network;
        $this->clientId = $clientId;
    }
}
