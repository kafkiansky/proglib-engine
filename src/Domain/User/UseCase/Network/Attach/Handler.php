<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Network\Attach;

use App\Application\Services\User\Avatar;
use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepository;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use DomainException;

class Handler
{
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var Avatar
     */
    private $avatar;

    public function __construct(UserRepository $users, Flusher $flusher, Avatar $avatar)
    {
        $this->users = $users;
        $this->flusher = $flusher;
        $this->avatar = $avatar;
    }

    /**
     * @param Command $command
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        if ($this->users->findByNetworkAndClientId($command->network, $command->clientId)) {
            throw new DomainException('Социальная сеть уже используется');
        }

        /** @var User $user */
        $user = $this->users->get($command->user);

        $user->attachNetwork(
            $command->network,
            $command->clientId,
            $command->avatar ?? $this->avatar->getDefaultAvatar(),
            $command->email,
            $command->githubHtmlUrl,
            $command->githubApiUrl
        );


        $this->flusher->flush();
    }
}
