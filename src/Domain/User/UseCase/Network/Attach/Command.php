<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Network\Attach;

class Command
{
    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $network;

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $avatar;

    /**
     * @var string
     */
    public $githubHtmlUrl;

    /**
     * @var string
     */
    public $githubApiUrl;

    /**
     * @var string|null
     */
    public $email;

    public function __construct(
        string $user,
        string $network,
        string $clientId,
        ?string $avatar,
        string $email = null,
        string $githubHtmlUrl = null,
        string $githubApiUrl = null
    ) {
        $this->user = $user;
        $this->network = $network;
        $this->clientId = $clientId;
        $this->email = $email;
        $this->githubHtmlUrl = $githubHtmlUrl;
        $this->githubApiUrl = $githubApiUrl;
        $this->avatar = $avatar;
    }
}
