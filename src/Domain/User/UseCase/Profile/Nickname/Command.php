<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Profile\Nickname;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $nickname;

    /**
     * @var User
     */
    public $user;

    public function __construct(string $nickname, User $user)
    {
        $this->nickname = $nickname;
        $this->user = $user;
    }
}
