<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Profile\Nickname;

use App\Domain\Flusher;
use App\Domain\User\Repository\UserRepositoryInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var UserRepositoryInterface
     */
    private $users;

    public function __construct(Flusher $flusher, UserRepositoryInterface $users)
    {
        $this->flusher = $flusher;
        $this->users = $users;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        if ($this->users->findOneUserBy(['nickname' => $command->nickname]) &&
            $command->user->getNickName() !== $command->nickname
        ) {
            throw new \DomainException('User with same nickname already exists');
        }

        if (mb_strlen($command->nickname) < 3) {
            throw new \LengthException('Nickname is too short');
        }

        $command->user->changeNickname($command->nickname);
        $this->flusher->flush();
    }
}
