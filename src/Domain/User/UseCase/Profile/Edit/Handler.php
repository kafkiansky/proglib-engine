<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Profile\Edit;

use App\Domain\Flusher;
use App\Domain\User\Repository\UserRepository;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(Flusher $flusher, UserRepository $userRepository)
    {
        $this->flusher = $flusher;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command $command
     *
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        $user = $command->user;

        $existingUser = $this->userRepository->findOneBy(['email' => $command->email->getValue()]);

        if ($existingUser) {
            throw new \DomainException('Пользователь с такой почтой уже существует');
        }

        $user->changeProfile(
            $command->email,
            $command->name,
            $command->city
        );

        $this->flusher->flush($user);
    }
}
