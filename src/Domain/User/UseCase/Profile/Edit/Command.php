<?php

declare(strict_types=1);

namespace App\Domain\User\UseCase\Profile\Edit;

use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\ValueObject\Email;

class Command
{
    /**
     * @var Email
     */
    public $email;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $name;

    /**
     * @var User
     */
    public $user;

    public function __construct(User $user, Email $email, string $city, string $name)
    {
        $this->user = $user;
        $this->email = $email;
        $this->city = $city;
        $this->name = $name;
    }
}
