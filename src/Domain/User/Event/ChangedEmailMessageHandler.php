<?php

declare(strict_types=1);

namespace App\Domain\User\Event;

use Swift_Message;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Twig\Environment;

class ChangedEmailMessageHandler implements MessageHandlerInterface
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Environment
     */
    private $view;

    public function __construct(Environment $view, \Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->view = $view;
    }

    public function __invoke(ChangedEmailMessage $message)
    {
        $messageBody = $this->view->render('user/mail/changedEmailNotification.html.twig', [
            'user' => $message->getChangedEmailUser()
        ]);

        $message = (new Swift_Message())
            ->setSubject('Смена электронного адреса на сайте proglib.io')
            ->setFrom('hello@proglib.io')
            ->setTo($message->getChangedEmailUser()->getChangedEmail())
            ->setBody($messageBody, 'text/html');

        $this->mailer->send($message);
    }
}
