<?php

declare(strict_types=1);

namespace App\Domain\User\Event;

use App\Domain\User\Model\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class CreatedUserEvent extends Event
{
    public const NAME = 'user.create';

    /**
     * @var User
     */
    private $createdUser;

    /**
     * @param User $createdUser
     */
    public function __construct(User $createdUser)
    {
        $this->createdUser = $createdUser;
    }

    /**
     * @return User
     */
    public function getCreatedUser(): User
    {
        return $this->createdUser;
    }
}
