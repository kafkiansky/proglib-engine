<?php

declare(strict_types=1);

namespace App\Domain\User\Event;

use App\Domain\User\Model\Entity\User;

class ChangedEmailMessage
{
    /**
     * @var User $changedEmailUser
     */
    private $changedEmailUser;

    public function __construct(User $user)
    {
        $this->changedEmailUser = $user;
    }

    /**
     * @return User
     */
    public function getChangedEmailUser(): User
    {
        return $this->changedEmailUser;
    }
}
