<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805184906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comments ADD test_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:test_id_type)\', CHANGE comment comment LONGTEXT NOT NULL, CHANGE status status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A1E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('CREATE INDEX IDX_5F9E962A1E5D0459 ON comments (test_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A1E5D0459');
        $this->addSql('DROP INDEX IDX_5F9E962A1E5D0459 ON comments');
        $this->addSql('ALTER TABLE comments DROP test_id, CHANGE comment comment LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'Comment\', CHANGE status status VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci COMMENT \'Status\'');
    }
}
