<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190725135211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vacancies (id CHAR(36) NOT NULL COMMENT \'(DC2Type:vacancy_id_type)\', company_id CHAR(36) NOT NULL COMMENT \'(DC2Type:company_id_type)\', title VARCHAR(255) NOT NULL, salary_from INT DEFAULT NULL, salary_to INT DEFAULT NULL, work_type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:work_type_type)\', work_place VARCHAR(255) NOT NULL COMMENT \'(DC2Type:work_place_type)\', city VARCHAR(255) DEFAULT NULL, requirements LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', tasks LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', conditions LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', candidate_email VARCHAR(255) NOT NULL, image VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX company_vacancy_idx (company_id), INDEX company_city_idx (city), INDEX company_work_type_idx (work_type), INDEX company_work_place_idx (work_place), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vacancy_tags (vacancy_id CHAR(36) NOT NULL COMMENT \'(DC2Type:vacancy_id_type)\', tag_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_F7992ACF433B78C4 (vacancy_id), INDEX IDX_F7992ACFBAD26311 (tag_id), PRIMARY KEY(vacancy_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE companies (id CHAR(36) NOT NULL COMMENT \'(DC2Type:company_id_type)\', name VARCHAR(255) NOT NULL, site VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vacancies ADD CONSTRAINT FK_99165A59979B1AD6 FOREIGN KEY (company_id) REFERENCES companies (id)');
        $this->addSql('ALTER TABLE vacancy_tags ADD CONSTRAINT FK_F7992ACF433B78C4 FOREIGN KEY (vacancy_id) REFERENCES vacancies (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vacancy_tags ADD CONSTRAINT FK_F7992ACFBAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE vacancy_tags DROP FOREIGN KEY FK_F7992ACF433B78C4');
        $this->addSql('ALTER TABLE vacancies DROP FOREIGN KEY FK_99165A59979B1AD6');
        $this->addSql('DROP TABLE vacancies');
        $this->addSql('DROP TABLE vacancy_tags');
        $this->addSql('DROP TABLE companies');
    }
}
