<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190802174132 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE tests_likes (test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_5C36F6271E5D0459 (test_id), INDEX IDX_5C36F627A76ED395 (user_id), PRIMARY KEY(test_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tests_bookmarks (test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_C2FBCB741E5D0459 (test_id), INDEX IDX_C2FBCB74A76ED395 (user_id), PRIMARY KEY(test_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tests_likes ADD CONSTRAINT FK_5C36F6271E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE tests_likes ADD CONSTRAINT FK_5C36F627A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tests_bookmarks ADD CONSTRAINT FK_C2FBCB741E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE tests_bookmarks ADD CONSTRAINT FK_C2FBCB74A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tests ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD published_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP pub_date, CHANGE update_at update_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE tests_likes');
        $this->addSql('DROP TABLE tests_bookmarks');
        $this->addSql('ALTER TABLE tests ADD pub_date DATETIME NOT NULL, DROP created_at, DROP published_at, CHANGE update_at update_at DATETIME NOT NULL');
    }
}
