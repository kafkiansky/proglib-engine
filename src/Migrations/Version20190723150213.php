<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190723150213 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE users (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', clientId VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, nickname VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, userPic LONGTEXT DEFAULT NULL, lastLogin DATETIME DEFAULT NULL, role VARCHAR(255) NOT NULL, status VARCHAR(25) NOT NULL, githubHtmlUrl VARCHAR(255) DEFAULT NULL, githubApiUrl VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) DEFAULT NULL, oauthType VARCHAR(255) NOT NULL, city VARCHAR(255) DEFAULT NULL, push_notifications VARCHAR(255) DEFAULT \'on\', is_subscribe_for_notifications TINYINT(1) DEFAULT NULL, changed_email VARCHAR(255) DEFAULT NULL, resume VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_1483A5E9EA1CE9BE (clientId), UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_tags (user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', tag_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_153DD8EFA76ED395 (user_id), INDEX IDX_153DD8EFBAD26311 (tag_id), PRIMARY KEY(user_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(255) DEFAULT NULL, preview VARCHAR(255) DEFAULT NULL, rendered_content LONGTEXT DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, views INT DEFAULT 0, preview_image VARCHAR(255) DEFAULT NULL, is_dump SMALLINT DEFAULT 0, broken_links LONGTEXT DEFAULT NULL, bounce_rate INT DEFAULT 0, created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', INDEX IDX_885DBAFA8D93D649 (user), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE post_tags (post_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', tag_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_A6E9F32D4B89032C (post_id), INDEX IDX_A6E9F32DBAD26311 (tag_id), PRIMARY KEY(post_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts_likes (post_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_FBCD156C4B89032C (post_id), INDEX IDX_FBCD156CA76ED395 (user_id), PRIMARY KEY(post_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE posts_bookmarks (post_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_9AB0748C4B89032C (post_id), INDEX IDX_9AB0748CA76ED395 (user_id), PRIMARY KEY(post_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comments (id CHAR(36) NOT NULL COMMENT \'(DC2Type:comment_id)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', parent_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:comment_id)\', comment LONGTEXT NOT NULL COMMENT \'Comment\', status VARCHAR(255) NOT NULL COMMENT \'Status\', images LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_5F9E962A4B89032C (post_id), INDEX IDX_5F9E962AA76ED395 (user_id), INDEX IDX_5F9E962A727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comments_likes (comment_id CHAR(36) NOT NULL COMMENT \'(DC2Type:comment_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_1FCA91BF8697D13 (comment_id), INDEX IDX_1FCA91BA76ED395 (user_id), PRIMARY KEY(comment_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comments_bookmarks (comment_id CHAR(36) NOT NULL COMMENT \'(DC2Type:comment_id)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_3B29F76DF8697D13 (comment_id), INDEX IDX_3B29F76DA76ED395 (user_id), PRIMARY KEY(comment_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL COMMENT \'Tag name\', created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE networks (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', client_id VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, network VARCHAR(32) DEFAULT NULL, github_html_url VARCHAR(255) DEFAULT NULL, github_api_url VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', UNIQUE INDEX UNIQ_D9B9B42B19EB6921 (client_id), INDEX IDX_D9B9B42BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE advertisings (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', ad_content LONGTEXT DEFAULT NULL, ad_image VARCHAR(255) DEFAULT NULL, ad_link VARCHAR(255) NOT NULL, company_name VARCHAR(255) DEFAULT NULL, views BIGINT DEFAULT 0 NOT NULL, is_paid SMALLINT DEFAULT 0 NOT NULL, date_start DATETIME DEFAULT NULL, date_end DATETIME DEFAULT NULL, ad_location LONGTEXT DEFAULT NULL, company_email VARCHAR(255) DEFAULT NULL, publish_on DATETIME DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE like_notification_comments (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', comment_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:comment_id)\', liked_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', seen TINYINT(1) NOT NULL COMMENT \'Seen\', INDEX IDX_236DD12AF8697D13 (comment_id), INDEX IDX_236DD12A621FAD6B (liked_by), INDEX IDX_236DD12A4B89032C (post_id), INDEX IDX_236DD12AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment_notification_comments (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', comment_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:comment_id)\', commented_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', seen TINYINT(1) NOT NULL COMMENT \'Seen\', INDEX IDX_4166AF9FF8697D13 (comment_id), INDEX IDX_4166AF9F415514B3 (commented_by), INDEX IDX_4166AF9F4B89032C (post_id), INDEX IDX_4166AF9FA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment_notification_posts (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', commented_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', seen TINYINT(1) NOT NULL COMMENT \'Seen\', INDEX IDX_8DC7A33C415514B3 (commented_by), INDEX IDX_8DC7A33C4B89032C (post_id), INDEX IDX_8DC7A33CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE like_notification_posts (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', liked_by CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', seen TINYINT(1) NOT NULL COMMENT \'Seen\', INDEX IDX_94BC3281621FAD6B (liked_by), INDEX IDX_94BC32814B89032C (post_id), INDEX IDX_94BC3281A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users_on_confirm (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', client_id VARCHAR(255) NOT NULL, locale VARCHAR(255) DEFAULT NULL, avatar_url VARCHAR(255) DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, github_html_url VARCHAR(255) DEFAULT NULL, github_api_url VARCHAR(255) DEFAULT NULL, token VARCHAR(255) DEFAULT NULL, oauth_type VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'Show datetime when was created\', updated_at DATETIME NOT NULL COMMENT \'Show datetime when was updated.\', UNIQUE INDEX UNIQ_1E155F7919EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscribed_on_mail_users (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', is_still_subscribe SMALLINT DEFAULT 1 NOT NULL, subscribed_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', email VARCHAR(255) NOT NULL COMMENT \'Email must be unique.\', UNIQUE INDEX UNIQ_F0F1B48AE7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tasks (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', author_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', executor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', start_date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', end_date DATE NOT NULL, resume LONGTEXT NOT NULL, content LONGTEXT DEFAULT NULL, label VARCHAR(255) NOT NULL, current_state VARCHAR(255) DEFAULT \'new\' NOT NULL, created_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', updated_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_50586597F675F31B (author_id), INDEX IDX_505865978ABD09BB (executor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE members (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, avatar VARCHAR(255) DEFAULT NULL, date DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', email VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_45A0D2FFA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE work_events (id CHAR(36) NOT NULL COMMENT \'(DC2Type:management_notification_id_type)\', executor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', task_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', content VARCHAR(255) NOT NULL, executed_at DATETIME NOT NULL, INDEX IDX_F87E91688ABD09BB (executor_id), INDEX IDX_F87E91688DB60186 (task_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_complaints (id CHAR(36) NOT NULL COMMENT \'(DC2Type:complaint_id_type)\', sender_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', recipient_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', reasons VARCHAR(255) NOT NULL, complaint_text VARCHAR(255) DEFAULT NULL, complaint_type VARCHAR(255) NOT NULL, entity_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', sended_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX IDX_73D69D6F624B39D (sender_id), INDEX IDX_73D69D6E92F8F78 (recipient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notifications (id CHAR(36) NOT NULL COMMENT \'(DC2Type:notification_id)\', actor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', executor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', comment_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:comment_id)\', post_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', notification_type VARCHAR(255) NOT NULL COMMENT \'(DC2Type:notification_type)\', executed_at VARCHAR(255) NOT NULL, is_read SMALLINT DEFAULT 0 NOT NULL, INDEX IDX_6000B0D3F8697D13 (comment_id), INDEX IDX_6000B0D34B89032C (post_id), INDEX idx_actor (actor_id), INDEX idx_executor (executor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_preferences (id CHAR(36) NOT NULL COMMENT \'(DC2Type:notification_preferences_id_type)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', preferences LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', approved_at DATETIME NOT NULL, INDEX user_preferences_idx (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_results (id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_result_id_type)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', passes_number INT NOT NULL, result VARCHAR(255) NOT NULL, passed_at DATETIME NOT NULL, INDEX IDX_43E230DCA76ED395 (user_id), INDEX IDX_43E230DC1E5D0459 (test_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_questions (id CHAR(36) NOT NULL COMMENT \'(DC2Type:question_id_type)\', test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', order_question INT DEFAULT NULL, question LONGTEXT NOT NULL, answer LONGTEXT NOT NULL COMMENT \'(DC2Type:answer_type)\', explanation LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:explanation_type)\', is_multiple TINYINT(1) NOT NULL, variant LONGTEXT NOT NULL COMMENT \'(DC2Type:variant_type)\', created_at DATE NOT NULL COMMENT \'(DC2Type:date_immutable)\', INDEX test_question_idx (test_id), INDEX order_question_idx (order_question), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tests (id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', author_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(255) DEFAULT NULL, preview LONGTEXT DEFAULT NULL, passing INT DEFAULT 0, views INT DEFAULT 0, results LONGTEXT DEFAULT NULL, preview_image VARCHAR(255) DEFAULT NULL, pub_date DATETIME NOT NULL, slug VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:test_slug_type)\', status VARCHAR(255) NOT NULL, total INT DEFAULT NULL, update_at DATETIME NOT NULL, INDEX IDX_1260FC5EF675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE test_tags (test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', tag_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_6AC5BEE91E5D0459 (test_id), INDEX IDX_6AC5BEE9BAD26311 (tag_id), PRIMARY KEY(test_id, tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tests_process (id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_process_id_type)\', test_id CHAR(36) NOT NULL COMMENT \'(DC2Type:test_id_type)\', question_id CHAR(36) NOT NULL COMMENT \'(DC2Type:question_id_type)\', user_id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', result SMALLINT DEFAULT NULL, passed_at DATETIME NOT NULL, INDEX IDX_3F3830AA76ED395 (user_id), INDEX test_process_test_idx (test_id), INDEX test_process_question_idx (question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_tags ADD CONSTRAINT FK_153DD8EFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_tags ADD CONSTRAINT FK_153DD8EFBAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE posts ADD CONSTRAINT FK_885DBAFA8D93D649 FOREIGN KEY (user) REFERENCES users (id)');
        $this->addSql('ALTER TABLE post_tags ADD CONSTRAINT FK_A6E9F32D4B89032C FOREIGN KEY (post_id) REFERENCES posts (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE post_tags ADD CONSTRAINT FK_A6E9F32DBAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE posts_likes ADD CONSTRAINT FK_FBCD156C4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE posts_likes ADD CONSTRAINT FK_FBCD156CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE posts_bookmarks ADD CONSTRAINT FK_9AB0748C4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE posts_bookmarks ADD CONSTRAINT FK_9AB0748CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962A727ACA70 FOREIGN KEY (parent_id) REFERENCES comments (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comments_likes ADD CONSTRAINT FK_1FCA91BF8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id)');
        $this->addSql('ALTER TABLE comments_likes ADD CONSTRAINT FK_1FCA91BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comments_bookmarks ADD CONSTRAINT FK_3B29F76DF8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id)');
        $this->addSql('ALTER TABLE comments_bookmarks ADD CONSTRAINT FK_3B29F76DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE networks ADD CONSTRAINT FK_D9B9B42BA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE like_notification_comments ADD CONSTRAINT FK_236DD12AF8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id)');
        $this->addSql('ALTER TABLE like_notification_comments ADD CONSTRAINT FK_236DD12A621FAD6B FOREIGN KEY (liked_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE like_notification_comments ADD CONSTRAINT FK_236DD12A4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE like_notification_comments ADD CONSTRAINT FK_236DD12AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comment_notification_comments ADD CONSTRAINT FK_4166AF9FF8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment_notification_comments ADD CONSTRAINT FK_4166AF9F415514B3 FOREIGN KEY (commented_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comment_notification_comments ADD CONSTRAINT FK_4166AF9F4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE comment_notification_comments ADD CONSTRAINT FK_4166AF9FA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comment_notification_posts ADD CONSTRAINT FK_8DC7A33C415514B3 FOREIGN KEY (commented_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comment_notification_posts ADD CONSTRAINT FK_8DC7A33C4B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE comment_notification_posts ADD CONSTRAINT FK_8DC7A33CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE like_notification_posts ADD CONSTRAINT FK_94BC3281621FAD6B FOREIGN KEY (liked_by) REFERENCES users (id)');
        $this->addSql('ALTER TABLE like_notification_posts ADD CONSTRAINT FK_94BC32814B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE like_notification_posts ADD CONSTRAINT FK_94BC3281A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_50586597F675F31B FOREIGN KEY (author_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE tasks ADD CONSTRAINT FK_505865978ABD09BB FOREIGN KEY (executor_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE members ADD CONSTRAINT FK_45A0D2FFA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE work_events ADD CONSTRAINT FK_F87E91688ABD09BB FOREIGN KEY (executor_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE work_events ADD CONSTRAINT FK_F87E91688DB60186 FOREIGN KEY (task_id) REFERENCES tasks (id)');
        $this->addSql('ALTER TABLE user_complaints ADD CONSTRAINT FK_73D69D6F624B39D FOREIGN KEY (sender_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE user_complaints ADD CONSTRAINT FK_73D69D6E92F8F78 FOREIGN KEY (recipient_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D310DAF24A FOREIGN KEY (actor_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D38ABD09BB FOREIGN KEY (executor_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D3F8697D13 FOREIGN KEY (comment_id) REFERENCES comments (id)');
        $this->addSql('ALTER TABLE notifications ADD CONSTRAINT FK_6000B0D34B89032C FOREIGN KEY (post_id) REFERENCES posts (id)');
        $this->addSql('ALTER TABLE notification_preferences ADD CONSTRAINT FK_3CAA95B4A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE test_results ADD CONSTRAINT FK_43E230DCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE test_results ADD CONSTRAINT FK_43E230DC1E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE test_questions ADD CONSTRAINT FK_841C31F1E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE tests ADD CONSTRAINT FK_1260FC5EF675F31B FOREIGN KEY (author_id) REFERENCES members (id)');
        $this->addSql('ALTER TABLE test_tags ADD CONSTRAINT FK_6AC5BEE91E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE test_tags ADD CONSTRAINT FK_6AC5BEE9BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tests_process ADD CONSTRAINT FK_3F3830A1E5D0459 FOREIGN KEY (test_id) REFERENCES tests (id)');
        $this->addSql('ALTER TABLE tests_process ADD CONSTRAINT FK_3F3830A1E27F6BF FOREIGN KEY (question_id) REFERENCES test_questions (id)');
        $this->addSql('ALTER TABLE tests_process ADD CONSTRAINT FK_3F3830AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user_tags DROP FOREIGN KEY FK_153DD8EFA76ED395');
        $this->addSql('ALTER TABLE posts DROP FOREIGN KEY FK_885DBAFA8D93D649');
        $this->addSql('ALTER TABLE posts_likes DROP FOREIGN KEY FK_FBCD156CA76ED395');
        $this->addSql('ALTER TABLE posts_bookmarks DROP FOREIGN KEY FK_9AB0748CA76ED395');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962AA76ED395');
        $this->addSql('ALTER TABLE comments_likes DROP FOREIGN KEY FK_1FCA91BA76ED395');
        $this->addSql('ALTER TABLE comments_bookmarks DROP FOREIGN KEY FK_3B29F76DA76ED395');
        $this->addSql('ALTER TABLE networks DROP FOREIGN KEY FK_D9B9B42BA76ED395');
        $this->addSql('ALTER TABLE like_notification_comments DROP FOREIGN KEY FK_236DD12A621FAD6B');
        $this->addSql('ALTER TABLE like_notification_comments DROP FOREIGN KEY FK_236DD12AA76ED395');
        $this->addSql('ALTER TABLE comment_notification_comments DROP FOREIGN KEY FK_4166AF9F415514B3');
        $this->addSql('ALTER TABLE comment_notification_comments DROP FOREIGN KEY FK_4166AF9FA76ED395');
        $this->addSql('ALTER TABLE comment_notification_posts DROP FOREIGN KEY FK_8DC7A33C415514B3');
        $this->addSql('ALTER TABLE comment_notification_posts DROP FOREIGN KEY FK_8DC7A33CA76ED395');
        $this->addSql('ALTER TABLE like_notification_posts DROP FOREIGN KEY FK_94BC3281621FAD6B');
        $this->addSql('ALTER TABLE like_notification_posts DROP FOREIGN KEY FK_94BC3281A76ED395');
        $this->addSql('ALTER TABLE members DROP FOREIGN KEY FK_45A0D2FFA76ED395');
        $this->addSql('ALTER TABLE user_complaints DROP FOREIGN KEY FK_73D69D6F624B39D');
        $this->addSql('ALTER TABLE user_complaints DROP FOREIGN KEY FK_73D69D6E92F8F78');
        $this->addSql('ALTER TABLE notifications DROP FOREIGN KEY FK_6000B0D310DAF24A');
        $this->addSql('ALTER TABLE notifications DROP FOREIGN KEY FK_6000B0D38ABD09BB');
        $this->addSql('ALTER TABLE notification_preferences DROP FOREIGN KEY FK_3CAA95B4A76ED395');
        $this->addSql('ALTER TABLE test_results DROP FOREIGN KEY FK_43E230DCA76ED395');
        $this->addSql('ALTER TABLE tests_process DROP FOREIGN KEY FK_3F3830AA76ED395');
        $this->addSql('ALTER TABLE post_tags DROP FOREIGN KEY FK_A6E9F32D4B89032C');
        $this->addSql('ALTER TABLE posts_likes DROP FOREIGN KEY FK_FBCD156C4B89032C');
        $this->addSql('ALTER TABLE posts_bookmarks DROP FOREIGN KEY FK_9AB0748C4B89032C');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A4B89032C');
        $this->addSql('ALTER TABLE like_notification_comments DROP FOREIGN KEY FK_236DD12A4B89032C');
        $this->addSql('ALTER TABLE comment_notification_comments DROP FOREIGN KEY FK_4166AF9F4B89032C');
        $this->addSql('ALTER TABLE comment_notification_posts DROP FOREIGN KEY FK_8DC7A33C4B89032C');
        $this->addSql('ALTER TABLE like_notification_posts DROP FOREIGN KEY FK_94BC32814B89032C');
        $this->addSql('ALTER TABLE notifications DROP FOREIGN KEY FK_6000B0D34B89032C');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962A727ACA70');
        $this->addSql('ALTER TABLE comments_likes DROP FOREIGN KEY FK_1FCA91BF8697D13');
        $this->addSql('ALTER TABLE comments_bookmarks DROP FOREIGN KEY FK_3B29F76DF8697D13');
        $this->addSql('ALTER TABLE like_notification_comments DROP FOREIGN KEY FK_236DD12AF8697D13');
        $this->addSql('ALTER TABLE comment_notification_comments DROP FOREIGN KEY FK_4166AF9FF8697D13');
        $this->addSql('ALTER TABLE notifications DROP FOREIGN KEY FK_6000B0D3F8697D13');
        $this->addSql('ALTER TABLE user_tags DROP FOREIGN KEY FK_153DD8EFBAD26311');
        $this->addSql('ALTER TABLE post_tags DROP FOREIGN KEY FK_A6E9F32DBAD26311');
        $this->addSql('ALTER TABLE test_tags DROP FOREIGN KEY FK_6AC5BEE9BAD26311');
        $this->addSql('ALTER TABLE work_events DROP FOREIGN KEY FK_F87E91688DB60186');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_50586597F675F31B');
        $this->addSql('ALTER TABLE tasks DROP FOREIGN KEY FK_505865978ABD09BB');
        $this->addSql('ALTER TABLE work_events DROP FOREIGN KEY FK_F87E91688ABD09BB');
        $this->addSql('ALTER TABLE tests DROP FOREIGN KEY FK_1260FC5EF675F31B');
        $this->addSql('ALTER TABLE tests_process DROP FOREIGN KEY FK_3F3830A1E27F6BF');
        $this->addSql('ALTER TABLE test_results DROP FOREIGN KEY FK_43E230DC1E5D0459');
        $this->addSql('ALTER TABLE test_questions DROP FOREIGN KEY FK_841C31F1E5D0459');
        $this->addSql('ALTER TABLE test_tags DROP FOREIGN KEY FK_6AC5BEE91E5D0459');
        $this->addSql('ALTER TABLE tests_process DROP FOREIGN KEY FK_3F3830A1E5D0459');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE user_tags');
        $this->addSql('DROP TABLE posts');
        $this->addSql('DROP TABLE post_tags');
        $this->addSql('DROP TABLE posts_likes');
        $this->addSql('DROP TABLE posts_bookmarks');
        $this->addSql('DROP TABLE comments');
        $this->addSql('DROP TABLE comments_likes');
        $this->addSql('DROP TABLE comments_bookmarks');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE networks');
        $this->addSql('DROP TABLE advertisings');
        $this->addSql('DROP TABLE like_notification_comments');
        $this->addSql('DROP TABLE comment_notification_comments');
        $this->addSql('DROP TABLE comment_notification_posts');
        $this->addSql('DROP TABLE like_notification_posts');
        $this->addSql('DROP TABLE users_on_confirm');
        $this->addSql('DROP TABLE subscribed_on_mail_users');
        $this->addSql('DROP TABLE tasks');
        $this->addSql('DROP TABLE members');
        $this->addSql('DROP TABLE work_events');
        $this->addSql('DROP TABLE user_complaints');
        $this->addSql('DROP TABLE notifications');
        $this->addSql('DROP TABLE notification_preferences');
        $this->addSql('DROP TABLE test_results');
        $this->addSql('DROP TABLE test_questions');
        $this->addSql('DROP TABLE tests');
        $this->addSql('DROP TABLE test_tags');
        $this->addSql('DROP TABLE tests_process');
    }
}
