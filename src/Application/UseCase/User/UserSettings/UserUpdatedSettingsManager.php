<?php

declare(strict_types=1);

namespace App\Application\UseCase\User\UserSettings;

use App\Application\Manager\AbstractManager;
use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\DBALException;

class UserUpdatedSettingsManager extends AbstractManager
{

    /**
     * @param User $user
     * @param string $push
     */
    public function storeUserPushes(User $user, string $push): void
    {
        $sql = '
            UPDATE
                users
            SET
                push_notifications = :push
            WHERE
                id = :id
        ';

        try {
            $this->getConnection()
                ->prepare($sql)
                ->execute([
                    'push' => $push,
                    'id' => $user->getId()->toString()
                ]);

        } catch (DBALException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }
}