<?php

declare(strict_types=1);

namespace App\Application\Annotation;

use Doctrine\ORM\Mapping\Annotation;

/**
 * @Annotation
 */
class Permissions
{
    public $adminPermissions;
}
