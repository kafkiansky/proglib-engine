<?php

declare(strict_types=1);

namespace App\Application\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotEmptyArrayValues extends Constraint
{
    public $message = 'The value "{{ value }} must contain non-empty values"';
}
