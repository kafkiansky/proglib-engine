<?php

declare(strict_types=1);

namespace App\Application\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class NotEmptyArrayValuesValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof NotEmptyArrayValues) {
            throw new UnexpectedTypeException($constraint, NotEmptyArrayValues::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_array($value)) {
            throw new UnexpectedValueException($value, 'array');
        }

        array_pop($value);

        $cleanArray = [];

        foreach ($value as $arrayValue) {
            if (null === $arrayValue || '' === \trim($arrayValue)) {
                $cleanArray[] = $arrayValue;
            }
        }

        if ($cleanArray === $value) {
            foreach ($cleanArray as $emptyValue) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ value }}', $emptyValue)
                    ->addViolation();
            }
        }
    }
}
