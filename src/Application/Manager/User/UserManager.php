<?php

declare(strict_types=1);

namespace App\Application\Manager\User;

use App\Application\Manager\AbstractManager;
use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\DBALException;
use PDO;

class UserManager extends AbstractManager
{
    /**
     * @param string $username
     * @param string $data
     *
     * @throws DBALException
     */
    public function recordEvent(string $username, string $data)
    {
        $sql = '
            INSERT INTO
                events (username, data, isRead)
            VALUES (:username, :data, 0)
        ';

        $this
            ->getConnection()
            ->prepare($sql)
            ->execute([
                'username' => $username,
                'data' => $data
            ]);
    }

    /**
     * @param User $user
     *
     * @return mixed[]
     *
     * @throws DBALException
     */
    public function findNotifications(User $user = null)
    {
        $sql = '
            SELECT COUNT(id) AS notify
            FROM 
                events
            WHERE
                username = :username
            AND 
                isRead = 0;
        ';

        $stmt = $this->getConnection()->prepare($sql);
        $stmt->execute(['username' => $user->getUsername()]);

        return $stmt->fetch(PDO::FETCH_OBJ)->notify;
    }
}
