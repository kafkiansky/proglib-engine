<?php

declare(strict_types=1);

namespace App\Application\Manager\Post;

use App\Domain\Post\Model\Entity\Post;

interface PostManagerInterface
{
    public function increaseViewsCount(Post $post);
}