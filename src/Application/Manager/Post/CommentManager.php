<?php

declare(strict_types=1);

namespace App\Application\Manager\Post;

use App\Application\Manager\AbstractManager;
use App\Domain\Post\Model\Entity\Comment;
use Doctrine\DBAL\DBALException;

class CommentManager extends AbstractManager
{
    /**
     * @param string $id
     *
     * @return bool
     */
    public function markAsDeleted(string $id): bool
    {
        $sql = '
            UPDATE
                comment
            SET
                status = :status
            WHERE
                id = :id
        ';

        try {
            $stmt = $this->getConnection()->prepare($sql);
            return $stmt->execute([
                'status' => Comment::DELETED,
                'id' => $id
            ]);

        } catch (DBALException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }
}