<?php

declare(strict_types=1);

namespace App\Application\Manager\Post;

use App\Application\Manager\AbstractManager;
use App\Domain\Post\Model\Entity\Post;
use Doctrine\DBAL\DBALException;

class PostManager extends AbstractManager implements PostManagerInterface
{
    /**
     * @param Post $post
     *
     * @return void
     */
    public function increaseViewsCount(Post $post): void
    {
        $sql = '
            UPDATE
                post
            SET
                post.views = post.views + 1
            WHERE
                post.id = :id 
        ';

        try {
            $this->getConnection()->prepare($sql)->execute([
                'id' => $post->getId()
            ]);
        } catch (DBALException $e) {
            $this->getLogger()->error($e->getMessage());
        }

    }
}