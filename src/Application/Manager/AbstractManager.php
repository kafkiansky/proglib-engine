<?php

declare(strict_types=1);

namespace App\Application\Manager;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractManager
{
    public const INCREMENT = 'increment';
    public const DECREMENT = 'decrement';
    public const CONDITION = 'condition';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    public static $tableName;

    /**
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->getEntityManager()->getConnection();
    }

    /**
     * @param array $params
     * @param array $replaceFiledList
     * @param bool $isIgnore
     * @return bool
     *
     * @throws DBALException
     */
    public function upsert(
        array $params,
        array $replaceFiledList = [],
        $isIgnore = false
    ): bool {

        if (empty($params)) {
            return false;
        }

        $sql = $this->getUpsertSql($params, $replaceFiledList, $isIgnore);

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

        return $stmt->execute();
    }

    /**
     * @param string $tableName
     * @param array $params
     * @param array $replaceFiledList
     * @param bool $isIgnore
     * @return bool
     *
     * @throws DBALException
     */
    public function upsertToTable(
        string $tableName,
        array $params,
        array $replaceFiledList = [],
        $isIgnore = false
    ): bool {

        $originalTableName = static::$tableName;

        static::$tableName = $tableName;

        $result = $this->upsert($params, $replaceFiledList, $isIgnore);

        static::$tableName = $originalTableName;

        return $result;
    }

    /**
     * @param array $params
     * @param array $replaceFiledList
     * @param bool $isIgnore
     *
     * @return string
     */
    private function getUpsertSql(
        array $params,
        array $replaceFiledList = [],
        bool $isIgnore = false
    ): string {

        $fieldNameList = implode(', ', array_keys($params[0]));

        $sql = $isIgnore ? 'INSERT IGNORE' : 'INSERT';
        $sql = sprintf('%s INTO %s (%s) VALUES ', $sql, static::$tableName, $fieldNameList);

        $sqlPartList = [];

        foreach ($params as $param) {
            $sqlPartList[] = $this->getInsertPart($param);
        }

        $sql .= ' ' . implode(', ', $sqlPartList);

        if (empty($replaceFiledList)) {
            return $sql;
        }

        $sql .= ' ON DUPLICATE KEY UPDATE ';

        $replacePartList = [];

        foreach ($replaceFiledList as $replaceFiled) {
            if (!is_array($replaceFiled)) {
                $replacePartList[] = sprintf('%s = VALUES(%s)', $replaceFiled, $replaceFiled);

                continue;
            }

            $field = $replaceFiled[0];
            $replaceType = $replaceFiled[1];
            $condition = $replaceFiled[2] ?? '';

            if ($replaceType === self::INCREMENT) {
                $replacePartList[] = sprintf('%s = %s + VALUES(%s)', $field, $field, $field);
            } elseif ($replaceType === self::DECREMENT) {
                $replacePartList[] = sprintf('%s = %s - VALUES(%s)', $field, $field, $field);
            } elseif ($replaceType === self::CONDITION) {
                $replacePartList[] = sprintf('%s = %s', $field, $condition);
            }
        }

        $sql .= implode(', ', $replacePartList);

        return $sql;
    }

    /**
     * @param array $dataList
     *
     * @return string
     */
    private function getInsertPart(array $dataList): string
    {
        $items = [];

        foreach ($dataList as $data) {
            list($param, $type) = $this->getParamAndType($data);

            $items[] = $this->getQuoteItem($param, $type);
        }

        return sprintf('(%s)', implode(', ', $items));
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function getParamAndType($data): array
    {
        $param = $data;
        $type = null;

        if (is_array($data)) {
            $param = $data[0] ?? null;
            $type = $data[1] ?? null;
        }

        return [$param, $type];
    }

    /**
     * @param string $value
     * @param string|null $type
     *
     * @return string
     */
    private function getQuoteItem($value, $type = null): string
    {
        $connection = $this->getEntityManager()->getConnection();

        return $this->quote($connection, $value, $type);
    }

    /**
     * @param Connection $connection
     * @param $value
     * @param null $type
     *
     * @return string
     */
    private function quote(Connection $connection, $value, $type = null): string
    {
        if ($value === null) {
            return 'NULL';
        }

        return $connection->quote($value, $type);
    }
}
