<?php

declare(strict_types=1);

namespace App\Application\Services\Files;

use League\Flysystem\FileExistsException;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @var FilesystemInterface
     */
    private $storage;

    /**
     * @var string
     */
    private $storageBaseUrl;

    /**
     * @var string
     */
    private $storageRelativeUrl;

    /**
     * @var string
     */
    private $targetDirectory;

    public function __construct(
        FilesystemInterface $storage,
        string $storageBaseUrl,
        string $storageRelativeUrl,
        string $targetDirectory
    ) {
        $this->storage = $storage;
        $this->storageBaseUrl = $storageBaseUrl;
        $this->storageRelativeUrl = $storageRelativeUrl;
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @param UploadedFile $file
     * @param string $dir
     * @return File
     * @throws FileExistsException
     */
    public function upload(UploadedFile $file, string $dir): File
    {
        $path = $dir . '/' . date('Y-m-d');
        $name = md5(uniqid()) . '.' . $file->getClientOriginalExtension();

        $stream = fopen($file->getRealPath(), 'rb+');
        $this->storage->writeStream($path . '/' . $name, $stream);
        fclose($stream);

        $userPath = $this->storageRelativeUrl . $path;

        return new File($userPath, $name, $this->targetDirectory);
    }
}
