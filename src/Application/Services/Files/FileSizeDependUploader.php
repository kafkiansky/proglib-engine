<?php

declare(strict_types=1);

namespace App\Application\Services\Files;

use League\Flysystem\FileExistsException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileSizeDependUploader
{
    /**
     * @var FileUploader
     */
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param string $dir
     *
     * @return File
     *
     * @throws FileExistsException
     * @throws InvalidImageSizeException
     */
    public function handle(UploadedFile $uploadedFile, string $dir): File
    {
        /** @var File $file */
        $file = $this->uploader->upload($uploadedFile, $dir);

        $imagePath = $file->getSystemPath() . $file->getPath();
        $image = $imagePath . '/' . $file->getName();

        $imageSize = @getimagesize($image);
        list($width, $height) = $imageSize;

        if ($this->isNormalSize($width, $height) === false) {
            $this->delete($image);
            throw new InvalidImageSizeException();
        }

        return $file;
    }

    /**
     * @param string $image
     */
    public function delete(string $image): void
    {
        unlink($image);
    }

    /**
     * @param int $width
     * @param int $height
     *
     * @return bool
     */
    public function isNormalSize(int $width, int $height): bool
    {
        if ($height < 500 || $width < 1000 || $width / $height < 2) {
            return false;
        }

        return true;
    }
}
