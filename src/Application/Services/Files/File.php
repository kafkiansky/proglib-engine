<?php

declare(strict_types=1);

namespace App\Application\Services\Files;

class File
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $systemPath;

    public function __construct(string $path, string $name, string $systemPath)
    {
        $this->path = $path;
        $this->name = $name;
        $this->systemPath = $systemPath;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->path . '/' . $this->name;
    }

    /**
     * @return string
     */
    public function getSystemPath(): string
    {
        return $this->systemPath;
    }
}
