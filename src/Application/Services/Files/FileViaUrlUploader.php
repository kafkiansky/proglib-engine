<?php

declare(strict_types=1);

namespace App\Application\Services\Files;

use GuzzleHttp\Client;
use League\Flysystem\FilesystemInterface;

class FileViaUrlUploader
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $directory;

    /**
     * @var FilesystemInterface
     */
    private $storage;

    /**
     * @var string
     */
    private $realpath;

    /**
     * @var string
     */
    private $storageRelativeUrl;

    public function __construct(
        FilesystemInterface $storage,
        string $directory,
        string $realpath,
        string $storageRelativeUrl
    ) {
        $this->client = new Client();
        $this->directory = $directory;
        $this->storage = $storage;
        $this->realpath = $realpath;
        $this->storageRelativeUrl = $storageRelativeUrl;
    }

    /**
     * @param string $url
     * @param string $dir
     * @return File
     */
    public function upload(string $url, string $dir): File
    {
        $justPath = $dir . '/' . date('Y-m-d');
        $fullPath = $this->realpath . '/' .  $justPath;

        $imageName = uniqid() . substr($url, strrpos($url, '/') + 1);
        $fileName = $fullPath . '/' . $imageName;

        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        $image = $this->client->get($url)->getBody()->getContents();

        file_put_contents($fileName, $image);

        $imagePath = $this->storageRelativeUrl . '/' . $justPath;

        return new File(
            $imagePath,
            $imageName,
            $this->directory
        );
    }
}
