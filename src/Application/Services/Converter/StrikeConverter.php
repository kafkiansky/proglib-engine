<?php

declare(strict_types=1);

namespace App\Application\Services\Converter;

class StrikeConverter
{
    /**
     * @param string $text
     *
     * @return string
     */
    public function convert(string $text): string
    {
        $markdown = preg_replace('/<del\b[^>]*>/', '~~', $text);
        $markdown = str_replace('</del>', '~~', $markdown);

        return $markdown;
    }
}
