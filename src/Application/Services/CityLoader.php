<?php

declare(strict_types=1);

namespace App\Application\Services;

use App\Model\Geographer\City\Entity\City;
use App\Model\Geographer\City\Entity\CityRepository;
use App\Model\Geographer\City\Entity\Id;

class CityLoader
{
    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var CityRepository
     */
    private $cities;

    public function __construct(string $rootDir, CityRepository $cities)
    {
        $this->rootDir = $rootDir;
        $this->cities = $cities;
    }

    /**
     * @return string
     */
    public function getRootDir(): string
    {
        return $this->rootDir;
    }

    public function load()
    {
        $dirs = \scandir($this->rootDir);

        $implodedDirs = implode(',', $dirs);
        preg_match_all('/[a-zA-Z]+.json/', $implodedDirs, $m);

        $files = $m[0];

        foreach ($files as $file) {
            $this->commit($file);
        }
    }

    private function commit(string $file): void
    {
        $filePath = $this->rootDir . '/' . $file;
        $cities = json_decode(file_get_contents($filePath), true);

        foreach ($cities as $city) {
            $entity = new City(
                Id::next(),
                (int)$city['code'],
                $city['long']['default']
            );

            $this->cities->add($entity);
        }
    }
}
