<?php

declare(strict_types=1);

namespace App\Application\Services\User;

class Avatar
{
    /**
     * @var string
     */
    private $avatar;

    public function __construct(string $avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string
     */
    public function getDefaultAvatar(): string
    {
        return $this->avatar;
    }
}
