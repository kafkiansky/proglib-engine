<?php

declare(strict_types=1);

namespace App\Application\Web\Core\Service;

use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class FlashMessageService
{
    private const SUCCESS = 'success';
    private const ERROR = 'error';

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    public function __construct(FlashBagInterface $flashBag)
    {
        $this->flashBag = $flashBag;
    }

    /**
     * @param string $message
     */
    public function success(string $message): void
    {
        $this->flashBag->add(self::SUCCESS, $message);
    }

    /**
     * @param string $message
     */
    public function error(string $message): void
    {
        $this->flashBag->add(self::ERROR, $message);
    }
}
