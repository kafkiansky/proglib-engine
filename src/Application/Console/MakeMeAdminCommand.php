<?php

declare(strict_types=1);

namespace App\Application\Console;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class MakeMeAdminCommand extends Command
{
    /**
     * @var UserRepositoryInterface
     */
    private $users;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    public function __construct(UserRepositoryInterface $users, Flusher $flusher, TokenStorageInterface $token)
    {
        parent::__construct();
        $this->users = $users;
        $this->flusher = $flusher;
        $this->token = $token;
    }

    protected function configure()
    {
        $this
            ->setName('admin:me')
            ->addArgument('role', InputArgument::OPTIONAL)
            ->setDescription('Change current user role to chosen role')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $roles = [];
        $keys = [];

        $i = 0;
        foreach (ROLE::roles() as $role) {
            $roles[] = [$role, $i];
            $keys[] = $i;
            $i++;
        }

        $table = new Table($output);
        $table->setHeaders(['Роль', 'Номер'])->setRows($roles);
        $table->render();

        $answer = $io->ask('Выберите роль, для этого введите ее порядковый номер <comment>[ROLE_SUPER_ADMIN]</comment>');

        $chosenRole = null;

        if (!in_array($answer, $keys) && $answer !== null) {
            $io->warning('Такой роли нет в списке.');
        }

        if ($answer === null) {
            $chosenRole = Role::ROLE_SUPER_ADMIN;
        } else {
            $chosenRole = $roles[$answer][0];
        }

        /** @var User $myUser */
        $myUser = $this->users->findOneUserBy(['email' => getenv('test_auth_user')]);
        $myUser->changeRole($chosenRole);
        $this->flusher->flush();

        $this->token->setToken(new PostAuthenticationGuardToken($myUser, 'main', $myUser->getRoles()));

        $io->success(sprintf('Вы успешно поменяли свою роль на %s', $chosenRole));
    }
}
