<?php

declare(strict_types=1);

namespace App\Application\Console;

use App\Application\Services\CityLoader;
use App\Domain\Flusher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CityCommand extends Command
{
    /**
     * @var CityLoader
     */
    private $city;

    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(CityLoader $city, Flusher $flusher)
    {
        parent::__construct();
        $this->city = $city;
        $this->flusher = $flusher;
    }

    protected function configure()
    {
        $this
            ->setName('load:city')
            ->setDescription('Загружает города в базу.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $this->city->load();
        $this->flusher->flush();

        $io->success('Города успешно загружены');
    }
}