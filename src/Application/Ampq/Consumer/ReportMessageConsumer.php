<?php

declare(strict_types=1);

namespace App\Application\Ampq\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

class ReportMessageConsumer implements ConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = json_decode($msg->getBody());

        $this->logger->info($message->content);
    }
}
