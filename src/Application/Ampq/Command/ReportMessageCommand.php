<?php

declare(strict_types=1);

namespace App\Application\Ampq\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ReportMessageCommand extends Command
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('message:consume');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
    }
}
