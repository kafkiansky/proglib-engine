<?php

declare(strict_types=1);

namespace App\Controller\Admin\Profile;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/extra/admin/links/show", name="links_show")
     * @Security("is_granted('ROLE_USER')")
     */
    public function all()
    {

    }
}
