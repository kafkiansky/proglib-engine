<?php

declare(strict_types=1);

namespace App\Controller\Admin\Profile;

use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TasksController extends AbstractController
{
    /**
     * @var TaskRepository
     */
    private $tasks;

    public function __construct(TaskRepository $tasks)
    {
        $this->tasks = $tasks;
    }

    public function my()
    {

    }
}