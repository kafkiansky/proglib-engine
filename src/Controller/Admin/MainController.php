<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class MainController
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Environment
     */
    private $view;

    public function __construct(RouterInterface $router, Environment $view)
    {
        $this->router = $router;
        $this->view = $view;
    }

    /**
     * @Route("/extra/admin/profile", name="admin_profile")
     */
    public function index()
    {
        return Response::create($this->view->render('admin/adminLayout.html.twig'));
    }
}
