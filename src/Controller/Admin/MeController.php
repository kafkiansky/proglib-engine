<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Application\Annotation\Permissions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MeController extends AbstractController
{
    /**
     * @Route("/admin/me", name="admin_me")
     * @Permissions(adminPermissions=true)
     */
    public function me()
    {
        $user = $this->getUser();

        return $this->render('admin/me/main.html.twig', compact('user'));
    }
}
