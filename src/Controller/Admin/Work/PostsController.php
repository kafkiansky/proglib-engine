<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work;

use App\Application\Annotation\Permissions;
use App\Application\Services\Files\FileSizeDependUploader;
use App\Application\Services\Files\FileViaUrlUploader;
use App\Application\Services\Files\InvalidImageSizeException;
use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\TagsRepository;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Post\UseCase\Admin\Create;
use App\Domain\Post\UseCase\Admin\Publish;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Application\Services\Files\FileUploader;

class PostsController extends AbstractController
{
    private const POST_FILE_PATH = 'posts';

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Flusher $flusher, ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->flusher = $flusher;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    /**
     * @Route("/admin/work/posts/new-post", name="admin_work_post_create")
     * @Permissions(adminPermissions=true)
     *
     * @param TagsRepository $tags
     *
     * @return Response
     */
    public function new(TagsRepository $tags): Response
    {
        $tags = $tags->findAll();
        $rules = self::rules();

        return $this->render('admin/work/posts/create.html.twig', compact('tags', 'rules'));
    }

    /**
     * @Route("/admin/work/posts/{id}/edit", name="admin_work_post_edit")
     * @Permissions(adminPermissions=true)
     *
     * @param Post $post
     * @param TagsRepository $tags
     *
     * @return Response
     */
    public function edit(Post $post, TagsRepository $tags): Response
    {
        $tags = $tags->findAll();
        $rules = self::rules();

        return $this->render('admin/work/posts/create.html.twig', compact('tags', 'post', 'rules'));
    }

    /**
     * @Route("/admin/work/posts/post/keepDraft", name="admin_work_post_keep_draft")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param Create\Handler $handler
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function keep(Request $request, Create\Handler $handler): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Create\Command($request->request->all(), $this->getUser());

        try {
            $handler->handle($command);
            $this->flusher->flush();
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            $this->logger->warning($e->getMessage());
            return $this->json(['success' => 0], 500);
        }
    }

    /**
     * @Route("/admin/work/posts/post/publish", name="admin_work_post_publish_post")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param Publish\Handler $handler
     * @return JsonResponse
     */
    public function publish(Request $request, Publish\Handler $handler): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Publish\Command($request->request->all(), $this->getUser());

        $violations = $this->validator->validate($command);
        $errors = [];

        if (\count($violations) > 0) {
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }

            return $this->json(['error' => $errors], 200);
        }

        try {
            $handler->handle($command);
            $this->flusher->flush();
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            $this->logger->warning($e->getMessage());
            return $this->json(['success' => 0], 500);
        }
    }

    /**
     * @Route("/admin/work/posts/post/preview/upload", name="admin_work_post_preview_upload")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileSizeDependUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function preview(Request $request, FileSizeDependUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        try {
            $fileUploaded = $uploader->handle($file, self::POST_FILE_PATH);
            return JsonResponse::create([
                'success' => 1,
                'file' => [
                    'url' => $fileUploaded->getFullName()
                ]
            ], Response::HTTP_OK);
        } catch (InvalidImageSizeException $exception) {
            return JsonResponse::create([
                'success' => 0,
                'error' => 'dimensions'
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/admin/work/posts/post/files/upload", name="admin_work_post_files_upload")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function files(Request $request, FileUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');
        $fileUploaded = $uploader->upload($file, self::POST_FILE_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/admin/work/posts/post/urlFiles/upload", name="admin_work_post_urlFiles_upload")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileViaUrlUploader $uploader
     * @return JsonResponse
     */
    public function urlFiles(Request $request, FileViaUrlUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $url = $request->request->get('link');
        $image = $uploader->upload($url, self::POST_FILE_PATH);

        $response = [
            'success' => 1,
            'file' => [
                'url' => $image->getFullName()
            ]
        ];

        return JsonResponse::create($response, 200);
    }

    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'title' => [
                'required' => true, 'empty' => 'no-title',
                'min' => 46, 'minMessage' => 'too-short-title',
                'max' => 65, 'maxMessage' => 'too-long-title',
            ],
            'preview' => [
                'required' => true, 'empty' => 'no-preview',
                'min' => 121, 'minMessage' => 'too-short-preview',
                'max' => 156, 'maxMessage' => 'too-long-preview',
                
            ],
            'body' => [
                'required' => true, 'empty' => 'no-content',
                'max' => 12000, 'maxMessage' => 'too-long-body',
                'min' => 1000, 'minMessage' => 'too-short-body'
            ],
            'image' => [
                'required' => true, 'empty' => 'no-image'
            ],
            'tags' => [
                'required' => true, 'empty' => 'no-tags',
                'min' => 1, 'minMessage' => 'too-few-tags',
            ]
        ];
    }
}
