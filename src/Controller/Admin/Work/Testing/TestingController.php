<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Testing;

use App\Application\Services\Files\FileSizeDependUploader;
use App\Application\Services\Files\FileUploader;
use App\Application\Services\Files\FileViaUrlUploader;
use App\Application\Services\Files\InvalidImageSizeException;
use App\Domain\Flusher;
use App\Domain\Post\Repository\TagsRepository;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\UseCase\Question\Normalize\JsonNormalizer;
use Doctrine\ORM\EntityNotFoundException;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Annotation\Permissions;
use App\Model\Test\UseCase\Question\Create;
use App\Model\Test\UseCase\Question\Delete;
use App\Model\Test\UseCase\Create as TestCreate;
use App\Model\Test\UseCase\Delete\Handler as TestDeleteHandler;
use App\Model\Test\UseCase\Delete\Command as TestDeleteCommand;
use App\Model\Test\UseCase\Publish\Handler as PublishHandler;
use App\Model\Test\UseCase\Publish\Command as PublishCommand;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class TestingController extends AbstractController
{
    private const TEST_FILE_PATH = 'tests';

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TagsRepository
     */
    private $tags;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(
        Flusher $flusher,
        TestRepository $tests,
        QuestionRepository $questions,
        LoggerInterface $logger,
        TagsRepository $tags,
        ValidatorInterface $validator
    ) {
        $this->flusher = $flusher;
        $this->tests = $tests;
        $this->questions = $questions;
        $this->logger = $logger;
        $this->tags = $tags;
        $this->validator = $validator;
    }

    /**
     * @Route("/admin/tests/test/create", name="admin_test_create")
     * @Permissions(adminPermissions=true)
     */
    public function index()
    {
        $tags = $this->tags->findAll();
        $rules = self::rules();

        return $this->render('admin/work/testing/create.html.twig', compact('tags', 'rules'));
    }

    /**
     * @Route("/admin/tests/test/{id}/edit", name="admin_tests_test_edit")
     * @Permissions(adminPermissions=true)
     *
     * @param Test $test
     * @return Response
     */
    public function edit(Test $test): Response
    {
        $questions = [];
        $tags = $this->tags->findAll();

        /** @var Question $question */
        foreach ($test->getQuestions() as $question) {
            $questions[] = [
                'id' => $question->getId()->getValue(),
                'text' => $question->getQuestion(),
                'description' => $question->getExplanation()->getValue(),
                'variants' => json_decode($question->getVariant()->getValue(), true),
                'answers' => json_decode($question->getAnswer()->getValue(), true),
                'order' => $question->getOrderQuestion(),
                'multiple' => $question->getIsMultiple()
            ];
        }

        $results = $test->getResults();

        $encodedTest = [
            'id' => $test->getId()->getValue(),
            'title' => $test->getTitle(),
            'preview' => $test->getPreview(),
            'tags' => $test->getTags(),
            'questions' => $questions,
            'results' => $results ? json_decode($results) : null
        ];

        return $this->render('admin/work/testing/create.html.twig', [
            'test' => $encodedTest,
            'tags' => $tags,
            'rules' => self::rules()
        ]);
    }

    /**
     * @Route("/admin/tests/test/publish", name="admin_tests_test_publish")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param PublishHandler $handler
     * @return JsonResponse
     */
    public function publish(Request $request, PublishHandler $handler)
    {
        $command = new PublishCommand($request->request->all());

        $violations = $this->validator->validate($command);
        $errors = [];

        if (\count($violations) > 0) {
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }

            return $this->json(['error' => $errors], 200);
        }

        try {
            $handler->handle($command);
            $this->flusher->flush();
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            return $this->json(['error' => $e->getMessage()], 200);
        }
    }

    /**
     * @Route("/admin/tests/test/save", name="admin_tests_test_save")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param TestCreate\Handler $handler
     * @return JsonResponse
     */
    public function save(Request $request, TestCreate\Handler $handler): Response
    {
        $command = new TestCreate\Command($request->request->all());

        try {
            $handler->handle($command);
            $this->flusher->flush();
            return $this->json(['success' => 1], 200);
        } catch (\Exception $exception) {
            $this->logger->warning($exception->getMessage());
            return $this->json(['success' => 0], 200);
        }
    }

    /**
     * @Route("/admin/tests/test/question/save", name="admin_tests_test_question_save")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param JsonNormalizer $jsonNormalizer
     * @param Create\Handler $handler
     * @return JsonResponse
     */
    public function questionSave(Request $request, JsonNormalizer $jsonNormalizer, Create\Handler $handler): Response
    {
        $question = json_decode($request->request->get('question'));
        $testId = $request->request->get('testId');

        $questionDto = $jsonNormalizer->normalize($question, $testId);
        $command = new Create\Command($questionDto);

        try {
            $handler->handle($command);
            return $this->json(['success' => 1], 200);
        } catch (\Exception $exception) {
            $this->logger->warning($exception->getMessage());
            return $this->json(['success' => 0], 200);
        }
    }

    /**
     * @Route("/admin/tests/test/question/delete", name="admin_tests_test_question_delete")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param Delete\Handler $handler
     * @return Response
     */
    public function questionDelete(Request $request, Delete\Handler $handler): Response
    {
        $command = new Delete\Command($request->request->get('questionId'));

        try {
            $handler->handle($command);
            return $this->json(['success' => 1], 200);
        } catch (EntityNotFoundException $exception) {
            return $this->json(['success' => 0, 'error' => $exception->getMessage()], 200);
        }
    }

    /**
     * @Route("/admin/tests/test/delete", name="admin_tests_test_delete")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param TestDeleteHandler $handler
     * @return JsonResponse
     */
    public function testDelete(Request $request, TestDeleteHandler $handler): Response
    {
        $testId = $request->request->get('testId');

        $command = new TestDeleteCommand($testId);

        try {
            $handler->handle($command);
            return $this->json(['success' => 1], 200);
        } catch (EntityNotFoundException $e) {
            return $this->json(['success' => 0, 'exception' => $e->getMessage()], 200);
        }
    }

    /**
     * @Route("/api/admin/tests/test/preview/upload", name="admin_tests_test_preview_upload")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileSizeDependUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function preview(Request $request, FileSizeDependUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        try {
            $fileUploaded = $uploader->handle($file, self::TEST_FILE_PATH);
            return JsonResponse::create([
                'success' => 1,
                'file' => [
                    'url' => $fileUploaded->getFullName()
                ]
            ], Response::HTTP_OK);
        } catch (InvalidImageSizeException $exception) {
            return JsonResponse::create([
                'success' => 0,
                'error' => 'dimensions'
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/api/admin/tests/test/image/uploadFromUrl", name="admin_image_test_uploadFromUrl")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileViaUrlUploader $uploader
     * @return JsonResponse
     */
    public function imageViaUrl(Request $request, FileViaUrlUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $url = $request->request->get('link');
        $fileUploaded = $uploader->upload($url, self::TEST_FILE_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);

    }

    /**
     * @Route("/api/admin/tests/test/image/uploadFromFile", name="admin_image_test_uploadFromFile")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function imageViaFile(Request $request, FileUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');
        $fileUploaded = $uploader->upload($file, self::TEST_FILE_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'title' => [
                'required' => true, 'empty' => 'no-title',
                'min' => 30, 'minMessage' => 'too-short-title',
                'max' => 90, 'maxMessage' => 'too-long-title',
            ],
            'preview' => [
                'required' => true, 'empty' => 'no-preview',
                'min' => 110, 'minMessage' => 'too-short-preview',
                'max' => 180, 'maxMessage' => 'too-long-preview',
            ],
            'image' => [
                'required' => true, 'empty' => 'no-image',
            ],
            'questions' => [
                'required' => true, 'empty' => 'no-questions',
                'min' => 5, 'minMessage' => 'too-few-questions',
            ],
            'results' => [
                'required' => true, 'empty' => 'no-results'
            ],
            'tags' => [
                'required' => true, 'empty' => 'no-tags',
                'min' => 1, 'minMessage' => 'too-few-tags'
            ]
        ];
    }
}
