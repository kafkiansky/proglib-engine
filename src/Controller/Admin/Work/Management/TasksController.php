<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management;

use App\Application\Annotation\Permissions;
use App\Domain\User\Model\Entity\User;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\Label;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use App\Model\Management\Work\Service\Tasks\OwnerTasksService;
use DateTimeImmutable;
use DomainException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\Web\Core\Service\FlashMessageService;
use App\Model\Management\Work\UseCase\Tasks\Create;
use App\Model\Management\Work\UseCase\Tasks\Filter;
use App\Model\Management\Work\UseCase\Tasks\Assign;
use App\Model\Management\Work\UseCase\Tasks\Delete;
use App\Model\Management\Work\UseCase\Tasks\Edit;

class TasksController extends AbstractController
{
    /**
     * @var FlashMessageService
     */
    private $flashMessage;
    
    /**
     * @var MemberRepository
     */
    private $members;

    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @var OwnerTasksService
     */
    private $ownerService;

    public function __construct(
        FlashMessageService $flashMessage,
        MemberRepository $members,
        TaskRepository $tasks,
        OwnerTasksService $ownerService
    ) {
        $this->flashMessage = $flashMessage;
        $this->members = $members;
        $this->tasks = $tasks;
        $this->ownerService = $ownerService;
    }

    /**
     * @Route("/admin/management/tasks/all", name="admin_tasks_all")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function all(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->list($command, $request);

        return $this->render('admin/work/management/tasks/all.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/current", name="admin_tasks_current")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function current(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->listByStatus($command, $request, Label::TASK_CURRENT);

        return $this->render('admin/work/management/tasks/current.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/express", name="admin_tasks_express")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function express(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->listByStatus($command, $request, Label::TASK_EXPRESS);

        return $this->render('admin/work/management/tasks/express.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/inWork", name="admin_tasks_in_work")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function working(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->listByStatus($command, $request, Label::TASK_IN_WORK);

        return $this->render('admin/work/management/tasks/inWorking.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/closed", name="admin_tasks_closed")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function closed(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->listByStatus($command, $request, Label::TASK_CLOSED);

        return $this->render('admin/work/management/tasks/closed.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/onReview", name="admin_tasks_on_review")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function review(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->ownerService->listByStatus($command, $request, Label::TASK_ON_REVIEW);

        return $this->render('admin/work/management/tasks/onReview.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
            'count' => $this->tasks->countTasks()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/create", name="admin_tasks_create")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param Create\Handler $handler
     *
     * @return Response
     *
     * @throws Exception
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        /** @var User $user */
        /** @var Member $member */
        $user = $this->getUser();
        $member = $this->members->findByUser($user);

        $command = new Create\Command(
            $member,
            new DateTimeImmutable()
        );

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->flashMessage->success('Задача создана');
                return $this->redirectToRoute('admin_tasks_all');
            } catch (DomainException $exception) {
                $this->flashMessage->error($exception->getMessage());
                return $this->redirectToRoute('admin_tasks_all');
            }
        }

        return $this->render('admin/work/management/tasks/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/{id}/assign", name="admin_tasks_task_assign")
     * @Permissions(adminPermissions=true)
     *
     * @param Task $task
     * @param Assign\Handler $handler
     *
     * @return RedirectResponse
     */
    public function assign(Task $task, Assign\Handler $handler): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $command = new Assign\Command($user, $task);

        try {
            $handler->handle($command);
            $this->flashMessage->success('Задача успешно взята');
            return $this->redirectToRoute('admin_tasks_all');
        } catch (DomainException $exception) {
            $this->flashMessage->error($exception->getMessage());
            return $this->redirectToRoute('admin_tasks_all');
        }
    }

    /**
     * @Route("/admin/management/tasks/{id}/edit", name="admin_tasks_task_edit")
     * @Permissions(adminPermissions=true)
     *
     * @param Task $task
     * @param Request $request
     * @param Edit\Handler $handler
     *
     * @return RedirectResponse|Response
     */
    public function edit(Task $task, Request $request, Edit\Handler $handler): Response
    {
        $command = new Edit\Command($task);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->flashMessage->success('Задача успешно отредактирована');
                return $this->redirectToRoute('admin_tasks_all');
            } catch (DomainException $exception) {
                $this->flashMessage->error($exception->getMessage());
                return $this->redirectToRoute('admin_tasks_all');
            }
        }

        return $this->render('admin/work/management/tasks/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/tasks/{id}/delete", name="admin_tasks_task_delete")
     * @Permissions(adminPermissions=true)
     *
     * @param Task $task
     * @param Delete\Handler $handler
     *
     * @return RedirectResponse
     */
    public function delete(Task $task, Delete\Handler $handler): Response
    {
        $command = new Delete\Command($task);

        try {
            $handler->handle($command);
            $this->flashMessage->success('Задача удалена');
            return $this->redirectToRoute('admin_tasks_all');
        } catch (DomainException $exception) {
            $this->flashMessage->success($exception->getMessage());
            return $this->redirectToRoute('admin_tasks_all');
        }
    }
}
