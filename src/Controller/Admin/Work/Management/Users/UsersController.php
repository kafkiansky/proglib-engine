<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management\Users;

use App\Application\Annotation\Permissions;
use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\UseCase\Admin\Filter;
use App\Domain\User\UseCase\Admin\Role;
use App\Domain\User\UseCase\Admin\Block;
use App\Domain\User\UseCase\Admin\UnBlock;
use App\Model\Management\Work\UseCase\Members\Create;
use DomainException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends AbstractController
{
    private const PER_PAGE = 10;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(FlashMessageService $flashMessage, UserRepositoryInterface $userRepository)
    {
        $this->flashMessage = $flashMessage;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/admin/management/users/all", name="admin_users_all")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function all(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->userRepository->all(
            $command,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'email'),
            $request->query->get('direction', 'desc')

        );

        return $this->render('admin/work/management/users/all.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/management/users/{email}/changeRole", name="admin_users_user_role")
     * @Permissions(adminPermissions=true)
     *
     * @param User $user
     * @param Request $request
     * @param Role\Handler $handler
     *
     * @return RedirectResponse|Response
     */
    public function role(User $user, Request $request, Role\Handler $handler): Response
    {
        $command = new Role\Command($user, $this->getUser());

        $form = $this->createForm(Role\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->flashMessage->success('Роль успешно изменена');
                return $this->redirectToRoute('admin_users_user_show', [
                    'email' => $user->getEmail()]
                );
            } catch (DomainException $exception) {
                $this->flashMessage->error($exception->getMessage());
                return $this->redirectToRoute('admin_users_user_show', [
                    'email' => $user->getEmail()]
                );
            }
        }

        return $this->render('admin/work/management/users/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/users/{email}/block", name="admin_users_user_block")
     * @Permissions(adminPermissions=true)
     *
     * @param User $user
     * @param Request $request
     * @param Block\Handler $handler
     * @return RedirectResponse
     */
    public function block(User $user, Request $request, Block\Handler $handler)
    {
        if (!$this->isCsrfTokenValid('block', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_users_user_show', [
                'email' => $user->getEmail()
            ]);
        }

        $command = new Block\Command($user, $this->getUser());

        try {
            $handler->handle($command);
            $this->flashMessage->success('Пользователь заблокирован');
            return $this->redirectToRoute('admin_users_user_show', ['email' => $user->getEmail()]);
        } catch (DomainException $exception) {
            $this->flashMessage->error($exception->getMessage());
            return $this->redirectToRoute('admin_users_user_show', ['email' => $user->getEmail()]);
        }
    }

    /**
     * @Route("/admin/management/users/{email}/unblock", name="admin_users_user_unblock")
     * @Permissions(adminPermissions=true)
     *
     * @param User $user
     * @param Request $request
     * @param UnBlock\Handler $handler
     * @return RedirectResponse
     */
    public function unblock(User $user, Request $request, UnBlock\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('unblock', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_users_user_show', [
                'email' => $user->getEmail()
            ]);
        }

        $command = new UnBlock\Command($user);

        try {
            $handler->handle($command);
            $this->flashMessage->success('Пользователь разблокирован');
            return $this->redirectToRoute('admin_users_user_show', [
                'email' => $user->getEmail()
            ]);
        } catch (DomainException $exception) {
            $this->flashMessage->error($exception->getMessage());
            return $this->redirectToRoute('admin_users_user_show', [
                'email' => $user->getEmail()
            ]);
        }
    }

    /**
     * @Route("/admin/management/users/{email}/assign", name="admin_users_user_assign")
     * @Permissions(adminPermissions=true)
     *
     * @param User $user
     * @param Create\Handler $handler
     *
     * @return RedirectResponse
     */
    public function assign(User $user, Create\Handler $handler): RedirectResponse
    {
        $command = new Create\Command($user);

        try {
            $handler->handle($command);
            $this->flashMessage->success('Автор создан');
            return $this->redirectToRoute('admin_users_user_show', ['email' => $user->getEmail()]);
        } catch (DomainException $exception) {
            $this->flashMessage->error($exception->getMessage());
            return $this->redirectToRoute('admin_users_user_show', ['email' => $user->getEmail()]);
        }
    }

    /**
     * @Route("/admin/management/users/{email}", name="admin_users_user_show")
     * @Permissions(adminPermissions=true)
     *
     * @param User $user
     *
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('admin/work/management/users/show.html.twig', compact('user'));
    }
}
