<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management\Tags;

use App\Application\Annotation\Permissions;
use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Post\UseCase\Tag\Create;
use App\Domain\Post\UseCase\Tag\Edit;
use App\Domain\Post\UseCase\Tag\Delete;

class TagsController extends AbstractController
{
    private const PER_PAGE = 8;

    /**
     * @var FlashMessageService
     */
    private $flashMessageService;

    /**
     * @var TagsRepositoryInterface
     */
    private $tags;

    public function __construct(FlashMessageService $flashMessageService, TagsRepositoryInterface $tags)
    {
        $this->flashMessageService = $flashMessageService;
        $this->tags = $tags;
    }

    /**
     * @Route("/admin/management/tags/all", name="admin_tags_index")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $pagination = $this->tags->all(
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'createdAt'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/work/management/tags/index.html.twig', compact('pagination'));
    }

    /**
     * @Route("/admin/management/tags/create", name="admin_tags_create")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     * @throws \Exception
     */
    public function create(Request $request, Create\Handler $handler): Response
    {
        $command = new Create\Command();

        $form = $this->createForm(Create\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->flashMessageService->success('Тег создан');
                return $this->redirectToRoute('admin_tags_index');
            } catch (\DomainException $exception) {
                $this->flashMessageService->error($exception->getMessage());
                return $this->redirectToRoute('admin_tags_index');
            }
        }

        return $this->render('admin/work/management/tags/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/tags/{name}/edit", name="admin_tags_edit")
     * @Permissions(adminPermissions=true)
     *
     * @param Tag $tag
     * @param Request $request
     * @param Edit\Handler $handler
     *
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function edit(Tag $tag, Request $request, Edit\Handler $handler): Response
    {
        $command = new Edit\Command($tag);

        $form = $this->createForm(Edit\Form::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle($command);
                $this->flashMessageService->success('Тег изменен');
                return $this->redirectToRoute('admin_tags_index');
            } catch (\DomainException $exception) {
                $this->flashMessageService->error($exception->getMessage());
                return $this->redirectToRoute('admin_tags_edit', ['name' => $tag->getName()]);
            }
        }

        return $this->render('admin/work/management/tags/edit.html.twig', [
            'tag' => $tag,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/tags/{name}/delete", name="admin_tags_delete")
     * @Permissions(adminPermissions=true)
     *
     * @param Tag $tag
     * @param Request $request
     * @param Delete\Handler $handler
     * @return RedirectResponse
     */
    public function delete(Tag $tag, Request $request, Delete\Handler $handler): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('admin_tags_index');
        }

        $command = new Delete\Command($tag);

        try {
            $handler->handle($command);
            $this->flashMessageService->success('Тег удален');
            return $this->redirectToRoute('admin_tags_index');
        } catch (\DomainException $exception) {
            $this->flashMessageService->success($exception->getMessage());
            return $this->redirectToRoute('admin_tags_index');
        }
    }

    /**
     * @Route("/admin/management/tags/{name}", name="admin_tags_show")
     * @Permissions(adminPermissions=true)
     *
     * @param Tag $tag
     * @return Response
     */
    public function show(Tag $tag): Response
    {
        return $this->render('admin/work/management/tags/show.html.twig', compact('tag'));
    }
}
