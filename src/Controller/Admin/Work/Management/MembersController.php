<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management;

use App\Application\Annotation\Permissions;
use App\Application\Web\Core\Service\FlashMessageService;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Management\Work\UseCase\Members\Filter;

class MembersController extends AbstractController
{
    private const PER_PAGE = 10;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    /**
     * @var MemberRepository
     */
    private $members;

    /**
     * @var TaskRepository
     */
    private $tasks;

    public function __construct(
        FlashMessageService $flashMessage,
        MemberRepository $members,
        TaskRepository $tasks
    ) {
        $this->flashMessage = $flashMessage;
        $this->members = $members;
        $this->tasks = $tasks;
    }

    /**
     * @Route("/admin/management/members/all", name="admin_members_all")
     * @Permissions(adminPermissions=true)
     *
     * @param Request $request
     *
     * @return Response
     */
    public function all(Request $request): Response
    {
        $command = new Filter\Command();

        $form = $this->createForm(Filter\Form::class, $command);
        $form->handleRequest($request);

        $pagination = $this->members->all(
            $command,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'date'),
            $request->query->get('direction', 'desc')
        );

        return $this->render('admin/work/management/members/all.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/management/members/{id}/show", name="admin_members_member_show")
     * @Permissions(adminPermissions=true)
     *
     * @param Member $member
     *
     * @return Response
     */
    public function show(Member $member): Response
    {
        $tasks = $this->tasks->findForReviewers($member);

        return $this->render('admin/work/management/members/show.html.twig', compact('member', 'tasks'));
    }
}
