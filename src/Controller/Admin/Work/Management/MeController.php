<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MeController extends AbstractController
{
}