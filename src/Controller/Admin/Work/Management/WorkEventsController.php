<?php

declare(strict_types=1);

namespace App\Controller\Admin\Work\Management;

use App\Model\Management\WorkEvent\Entity\WorkEventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WorkEventsController extends AbstractController
{

    /**
     * @var WorkEventRepository
     */
    private $workEvents;

    public function __construct(WorkEventRepository $workEvents)
    {
        $this->workEvents = $workEvents;
    }

    /**
     * @Route("/admin/work/events/last", name="admin_work_events")
     */
    public function all()
    {
        $workEvents = $this->workEvents->last();

        return $this->render('admin/partials/adminAsideEvents.html.twig', compact('workEvents'));
    }
}
