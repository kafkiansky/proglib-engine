<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Application\Services\Files\FileUploader;
use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\Flusher;
use App\Domain\Post\ReadModel\PaginateView;
use App\Domain\User\Exception\InvalidEmailException;
use App\Domain\User\Model\Entity\Network;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\ValueObject\Email;
use App\Model\Comment\Entity\CommentRepository;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\Post\Repository\TagsRepositoryInterface;
use App\Domain\User\UseCase\Resume\Command;
use App\Domain\User\UseCase\Resume\Handler;
use App\Domain\User\UseCase\Profile\Edit;
use App\ReadModel\Comment\CommentPaginator;
use App\ReadModel\Post\PostPaginator;
use App\Security\Guard\Authenticators\GoogleAuthenticator;
use App\Model\Geographer\City\Entity\CityRepository;
use App\Model\Notification\Entity\Notification;
use App\Model\Notification\Entity\NotificationRepository;
use Doctrine\ORM\NonUniqueResultException;
use DomainException;
use Exception;
use League\Flysystem\FileExistsException;
use LengthException;
use PDOException;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Application\UseCase\User\UserSettings\UserUpdatedSettingsManager;
use App\Domain\User\Repository\UserRepository;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use App\Domain\User\UseCase\Profile\Nickname;

class CabinetController extends AbstractController
{
    private const AVATARS_DIR = 'users/avatars';
    private const RESUME_DIR = 'users/resume';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @param LoggerInterface $logger
     * @param CommentRepository $commentRepository
     * @param PostRepositoryInterface $postRepository
     * @param TagsRepositoryInterface $tagsRepository
     * @param FlashMessageService $flashMessage
     * @param Flusher $flusher
     * @param CacheInterface $cache
     */
    public function __construct(
        LoggerInterface $logger,
        CommentRepository $commentRepository,
        PostRepositoryInterface $postRepository,
        TagsRepositoryInterface $tagsRepository,
        FlashMessageService $flashMessage,
        Flusher $flusher,
        CacheInterface $cache
    ) {
        $this->logger = $logger;
        $this->commentRepository = $commentRepository;
        $this->postRepository = $postRepository;
        $this->tagsRepository = $tagsRepository;
        $this->flashMessage = $flashMessage;
        $this->flusher = $flusher;
        $this->cache = $cache;
    }

    /**
     * @Route("/me", name="cabinet")
     * @Security("is_granted('ROLE_USER')")
     *
     * @return Response
     */
    public function profile(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var Network[] $networks */
        $networks = $user->getNetworks();

        return $this->render('user/profile/main.html.twig', [
            'user' => $user,
            'networks' => $networks
        ]);
    }

    /**
     * @Route("/me/cabinet/edit", name="user_cabinet_edit")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param Edit\Handler $handler
     * @param Flusher $flusher
     * @return RedirectResponse
     * @throws InvalidEmailException
     * @throws Exception
     */
    public function profileSettings(Request $request, Edit\Handler $handler, Flusher $flusher)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $email = $request->request->get('user-mail');
        $city = $request->request->get('user-city');
        $name = $request->request->get('user-name');

        /** @var User $user */
        $user = $this->getUser();

        if ($email === $user->getEmail() &&
            $city === $user->getCity() &&
            $name === $user->getName()
        ) {
            return $this->redirectToRoute('cabinet');
        }

        if ($email !== $user->getEmail()) {
            $command = new Edit\Command($this->getUser(), new Email($email), $city, $name);

            $emailChangedMessage = sprintf('Проверьте почту <b>%s</b>, мы отправили туда письмо с подтверждением',
                $command->email->getValue()
            );

            try {
                $handler->handle($command);
                $this->flashMessage->success($emailChangedMessage);
                return $this->redirectToRoute('cabinet');
            } catch (DomainException $exception) {
                $this->logger->warning($exception->getMessage());
                $this->flashMessage->error($exception->getMessage());
                return $this->redirectToRoute('cabinet');
            }
        }

        if ($email === $user->getEmail() &&
            $city !== $user->getCity() ||
            $name !== $user->getName()
        ) {
            $user->changeProfileWithoutEmail($name, $city);
            $flusher->flush();
            $this->flashMessage->success('Данные успешно обновлены');
            return $this->redirectToRoute('cabinet');
        }

        return $this->redirectToRoute('cabinet');
    }

    /**
     * @Route("/api/user/username/nickname/change", name="api_user_nickname_change")
     *
     * @param Request $request
     * @param Nickname\Handler $handler
     * @return JsonResponse
     * @throws Exception
     */
    public function changeNickname(Request $request, Nickname\Handler $handler): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();
        $nickname = htmlspecialchars(trim($request->request->get('nickname')));

        $command = new Nickname\Command($nickname, $user);

        try {
            $handler->handle($command);
            return JsonResponse::create([
                'success' => 1,
                'error' => null,
                'nickname' => $user->getNickName()
            ]);
        } catch (DomainException $exception) {
            return JsonResponse::create(['success' => 0, 'error' => 'nonunique']);
        } catch (LengthException $exception) {
            return JsonResponse::create(['success' => 0, 'error' => 'length']);
        }
    }

    /**
     * @Route("/user/push/store", name="api_store_pushes")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param UserUpdatedSettingsManager $userUpdatedSettingsManager
     *
     * @return JsonResponse|Response
     */
    public function storePushes(Request $request, UserUpdatedSettingsManager $userUpdatedSettingsManager): Response
    {
        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('proglib_app');
        }

        /** @var User $user */
        $user = $this->getUser();

        $userUpdatedSettingsManager->storeUserPushes(
            $user,
            $request->request->get('push')
        );

        return $this->json([], 200);
    }

    /**
     * @Route("/api/user/subscribe/decide", name="api_store_subscribe")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return JsonResponse|Response
     * @throws Exception
     */
    public function subscribe(Request $request): Response
    {
        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('proglib_app');
        }

        /** @var User $user */
        $user = $this->getUser();

        $user->chooseSubscribeStatus($request->request->get('subscribed'));
        $this->flusher->flush();

        return JsonResponse::create(['success' => 1], 200);
    }

    /**
     * @Route("/user/email/confirm/{token}", name="confirm_new_email")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param string $token
     * @param GoogleAuthenticator $googleAuthenticator
     * @param Request $request
     * @param GuardAuthenticatorHandler $guardAuthenticationHandler
     *
     * @param UserRepository $userRepository
     * @return Response
     * @throws NonUniqueResultException
     */
    public function confirmNewEmail(
        string $token,
        GoogleAuthenticator $googleAuthenticator,
        Request $request,
        GuardAuthenticatorHandler $guardAuthenticationHandler,
        UserRepository $userRepository
    ): Response {
        /** @var User $user */
        if (!$user = $userRepository->findOneUserBy(['token.value' => $token])) {
            throw new NotFoundHttpException('Токен истек или никогда не существовал.');
        }

        $user->changeEmail($user->getChangedEmail());
        $user->destroyToken();
        $this->flusher->flush();

        return $guardAuthenticationHandler->authenticateUserAndHandleSuccess(
            $this->getUser(),
            $request,
            $googleAuthenticator,
            'main'
        );
    }

    /**
     * @Route("/api/user/cabinet/notifications/all/markAsRead", name="api_mark_all_notifications_as_read")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param NotificationRepository $notificationsRepo
     * @return RedirectResponse|JsonResponse
     */
    public function markAllNotificationAsRead(Request $request, NotificationRepository $notificationsRepo): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $actor */
        $actor = $this->getUser();

        /** @var Notification[] $notifications */
        $notifications = $notificationsRepo->findAllUnReadByActor($actor);

        /** @var Notification $notification */
        foreach ($notifications as $notification) {
            $notification->markAsRead();
        }

        $this->flusher->flush();

        return JsonResponse::create(['success' => 1], Response::HTTP_OK);
    }

    /**
     * @Route("/me/comments", name="user_comments")
     * @IsGranted("ROLE_USER")
     *
     * @param Request $request
     * @param CommentPaginator $paginator
     *
     * @return Response
     * @throws Exception
     */
    public function comments(Request $request, CommentPaginator $paginator): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $comments = $paginator->paginateCommentsOnCabinet($request, $user);

        return $this->render('user/profile/comments.html.twig', [
            'comments' => $comments->items,
            'remainingPages' => $comments->remainingPages,
            'location' => 'profile',
            'user' => $user
        ]);
    }

    /**
     * @Route("/api/user/cabinet/comments", name="api_user_comments_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param CommentPaginator $paginator
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function preLoadComments(Request $request, CommentPaginator $paginator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        /** @var PaginateView $posts */
        $posts = $paginator->paginateCommentsOnCabinet($request, $user);

        $view = [
            'html' => $this->renderView('components/feeds/commentsFeed.html.twig', [
                'comments' => $posts->items,
                'location' => 'profile'
            ]),
            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/me/publications", name="user_publications")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     */
    public function showPublishedPostsInCabinet(Request $request, PostPaginator $paginator): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var PaginateView $posts */
        $posts = $paginator->paginatePostsOnCabinet($request, $user);

        return $this->render('user/profile/publications.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'user' => $user
        ]);
    }

    /**
     * @Route("/api/user/cabinet/publications", name="api_user_publications_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     */
    public function showPostsByStatus(Request $request, PostPaginator $paginator): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        /** @var PaginateView $posts */
        $posts = $paginator->paginatePostsOnCabinet($request, $user);

        $view = [
            'html' => $this->renderView('components/feeds/feed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/user/avatar/change", name="api_change_avatar")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function changeAvatar(Request $request, FileUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $avatar = $request->files->get('image');

        $fileName = $uploader->upload($avatar, self::AVATARS_DIR);

        $response = [];

        try {
            $this->getUser()->changeAvatar($fileName->getFullName());
            $this->flusher->flush();

            $response = [
                'success' => 1,
                'file' => [
                    'url' => $fileName->getFullName()
                ]
            ];

            $status = Response::HTTP_OK;
        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new JsonResponse($response, $status);
    }

    /**
     * @Route("/user/cabinet/resume/upload", name="api_user_resume_upload")
     *
     * @param Request $request
     * @param Handler $handler
     * @param FileUploader $uploader
     *
     * @return JsonResponse|RedirectResponse
     *
     * @throws Exception
     */
    public function changeResume(Request $request, Handler $handler, FileUploader $uploader): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $resume */
        $resume = $request->files->get('file');

        if (!in_array($resume->guessExtension(), ['docx', 'doc', 'pdf'])) {
            return JsonResponse::create([
                'success' => 0,
                'error' => 'extension'
            ], 200);
        }

        $file = $uploader->upload($resume, self::RESUME_DIR);

        /** @var User $user */
        $user = $this->getUser();

        $handler->handle(new Command($file->getFullName()), $user);

        $response = [
            'success' => 1,
            'file' => [
                'url' => $file->getFullName()
            ]
        ];

        return JsonResponse::create($response, 200);
    }

    /**
     * @Route("/user/cabinet/resume/delete", name="api_user_resume_delete")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function deleteResume(Request $request): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        $user->removeResume();
        $this->flusher->flush();

        $this->cache->delete('user-profile' . $user->getId());

        return JsonResponse::create(null, 200);
    }

    /**
     * @Route("/api/user/city/search", name="api_user_city_search")
     *
     * @param Request $request
     * @param CityRepository $cities
     * @return JsonResponse
     */
    public function searchCity(Request $request, CityRepository $cities)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $similarCities = $cities->findByQuery($request->get('query'), 5);

        return $this->json(['cities' => $similarCities], 200);
    }
}
