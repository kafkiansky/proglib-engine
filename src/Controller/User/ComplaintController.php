<?php

declare(strict_types=1);

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Management\Complaint\UseCase\Create;
use Zend\EventManager\Exception\DomainException;

class ComplaintController extends AbstractController
{
    /**
     * @Route("/api/users/user/complaint/send", name="api_users_user_complaint_send")
     *
     * @param Request $request
     * @param Create\Handler $handler
     * @return JsonResponse
     * @throws \Exception
     */
    public function send(Request $request, Create\Handler $handler): JsonResponse
    {
        $reasons = $request->request->get('reasons');
        $complaintText = $request->request->get('complain-text');
        $complainType = $request->request->get('complain-type');
        $entityId = $request->request->get('complain-id');

        $command = new Create\Command(
            $this->getUser(),
            $complainType,
            $entityId,
            $complaintText,
            $reasons
        );

        try {
            $handler->handle($command);
            return $this->json(['success' => 1], 200);
        } catch (DomainException $e) {
            return $this->json(['success' => 0], 200);
        }
    }
}
