<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Flusher;
use App\Domain\User\Exception\InvalidEmailException;
use App\Domain\User\Model\Entity\SubscribedOnMailUser;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\ValueObject\Email;
use App\Domain\User\Repository\SubscribedOnMailUserRepository;
use App\Domain\User\Repository\UserRepository;
use App\Domain\User\UseCase\Mailing\Command;
use App\Domain\User\UseCase\Mailing\Handler;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MailingController
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var SubscribedOnMailUserRepository
     */
    private $subscribedOnMailUserRepository;

    /**
     * @param RouterInterface $router
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     * @param UserRepository $userRepository
     * @param TokenStorageInterface $tokenStorage
     * @param Flusher $flusher
     * @param SubscribedOnMailUserRepository $subscribedOnMailUserRepository
     */
    public function __construct(
        RouterInterface $router,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        UserRepository $userRepository,
        TokenStorageInterface $tokenStorage,
        Flusher $flusher,
        SubscribedOnMailUserRepository $subscribedOnMailUserRepository
    ) {
        $this->router = $router;
        $this->em = $em;
        $this->logger = $logger;
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
        $this->flusher = $flusher;
        $this->subscribedOnMailUserRepository = $subscribedOnMailUserRepository;
    }

    /**
     * @Route("/user/mailing/email/get", name="api_mailing_email_get")
     *
     * @param Request $request
     * @param Handler $handler
     *
     * @throws Exception
     * @throws InvalidEmailException
     *
     * @return JsonResponse
     */
    public function subscribeUserToMailing(Request $request, Handler $handler)
    {
        if ($request->isMethod('POST')) {

            $email = $request->request->get('mail');

            if (null === $email) {
                return JsonResponse::create(null, 500);
            }

            /** @var User $user */
            $user = $this->userRepository->findOneBy(['email' => $email]);

            /** @var SubscribedOnMailUser $userFromSubscription */
            $userFromSubscription = $this->subscribedOnMailUserRepository
                ->findOneBy(['email.email' => $email]);

            if ($user !== null || $userFromSubscription !== null) {
                return JsonResponse::create([], 500);
            }

            $handler->handle(
                new Command(
                    new Email($email)
                )
            );
        }

        return JsonResponse::create(null, 200);
    }
}
