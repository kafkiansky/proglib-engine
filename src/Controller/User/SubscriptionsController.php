<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepository;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\UseCase\Subscriptions\Command;
use App\Domain\User\UseCase\Subscriptions\Handler;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SubscriptionsController
{
    /**
     * @var Environment
     */
    private $twig;
    /**
     * @var TagsRepository
     */
    private $tagsRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $token;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        Environment $twig,
        TagsRepository $tagsRepository,
        LoggerInterface $logger,
        RouterInterface $router,
        TokenStorageInterface $token,
        EntityManagerInterface $em
    ) {
        $this->twig = $twig;
        $this->tagsRepository = $tagsRepository;
        $this->logger = $logger;
        $this->router = $router;
        $this->token = $token;
        $this->em = $em;
    }

    /**
     * @Route("/me/subscriptions", name="api_subscriptions")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param Handler $handler
     *
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function subscriptions(Request $request, Handler $handler): Response
    {
        /** @var User $user */
        $user = $this->token->getToken()->getUser();

        if ($request->isMethod('POST')) {

            if ($request->request->get('tags') === null) {
                /** @var User $currentUserTags */
                $currentUserTags = $user->getTags();

                /** @var Tag $tag */
                foreach ($currentUserTags as $tag) {
                    $user->removeTag($tag);
                }

                $this->em->flush();

            return new RedirectResponse($this->router->generate('proglib_app'));

            }

            $handler->handle(
                new Command($request->request->get('tags'))
            );

            return new RedirectResponse($this->router->generate('proglib_app'));
        }

        $tags = $this->tagsRepository->findAll();

        return new Response(
            $this->twig->render('user/profile/subscriptions.html.twig', [
                'tags' => $tags
            ])
        );
    }
}