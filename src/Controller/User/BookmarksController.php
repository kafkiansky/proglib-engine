<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Model\Comment\Entity\Comment;
use Symfony\Component\HttpFoundation\Request;
use App\Domain\User\Model\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class BookmarksController extends AbstractController
{
    /**
     * @Route("/p/bookmark/{id}", name="api_bookmark_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Request $request
     * @param Flusher $flusher
     *
     * @return JsonResponse
     */
    public function bookmarkPost(Post $post, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->bookmarkPost($user);
        $flusher->flush();

        return new JsonResponse(['bookmarksCount' => $post->getUserBookmarks()->count()]);
    }

    /**
     * @Route("/p/unbookmark/{id}", name="api_unbookmark_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Request $request
     * @param Flusher $flusher
     *
     * @return JsonResponse
     */
    public function unBookmarkPost(Post $post, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->unBookmark($user);
        $flusher->flush();

        return new JsonResponse(['bookmarksCount' => $post->getUserBookmarks()->count()]);
    }

    /**
     * @Route("c/bookmark/{id}", name="api_bookmark_comment")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Comment $comment
     *
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     */
    public function bookmarkComment(Comment $comment, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return JsonResponse::create([], Response::HTTP_UNAUTHORIZED);
        }

        $comment->bookmark($user);
        $flusher->flush();

        return JsonResponse::create(['bookmarksCount' => $comment->getUserBookmarks()->count()], 200);
    }

    /**
     * @Route("c/unbookmark/{id}", name="api_unbookmark_comment")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Comment $comment
     *
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     */
    public function unBookmarkComment(Comment $comment, Request $request, Flusher $flusher)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return JsonResponse::create([], Response::HTTP_UNAUTHORIZED);
        }

        $comment->unBookmark($user);
        $flusher->flush();

        return JsonResponse::create(['bookmarksCount' => $comment->getUserBookmarks()->count()]);
    }
}
