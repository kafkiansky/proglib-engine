<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\Request as RequestOAuth;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\Entity\UserOnConfirm;
use App\Domain\User\Repository\UserOnConfirmRepository;
use App\Domain\User\Service\ConfirmEmailTokenizer;
use App\Domain\User\Service\ConfirmEmailTokenSender;
use App\Security\Guard\Authenticators\GoogleAuthenticator;
use Exception;
use PDOException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class UserEmailController
{
    /**
     * @var ConfirmEmailTokenSender
     */
    private $confirmEmailSender;

    /**
     * @var UserOnConfirmRepository $userOnConfirmRepository
     */
    private $userOnConfirmRepository;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Flusher
     */
    private $flusher;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var GuardAuthenticatorHandler
     */
    private $guardAuthenticatorHandler;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param ConfirmEmailTokenSender $confirmEmailSender
     * @param UserOnConfirmRepository $userOnConfirmRepository
     * @param SessionInterface $session
     * @param Flusher $flusher
     * @param RouterInterface $router
     * @param GuardAuthenticatorHandler $guardAuthenticatorHandler
     * @param LoggerInterface $logger
     */
    public function __construct(
        ConfirmEmailTokenSender $confirmEmailSender,
        UserOnConfirmRepository $userOnConfirmRepository,
        SessionInterface $session,
        Flusher $flusher,
        RouterInterface $router,
        GuardAuthenticatorHandler $guardAuthenticatorHandler,
        LoggerInterface $logger
    ) {
        $this->confirmEmailSender = $confirmEmailSender;
        $this->userOnConfirmRepository = $userOnConfirmRepository;
        $this->session = $session;
        $this->flusher = $flusher;
        $this->router = $router;
        $this->guardAuthenticatorHandler = $guardAuthenticatorHandler;
        $this->logger = $logger;
    }

    /**
     * @Route("/user/email/confirm", name="api_confirm_email")
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function sendConfirmEmail(Request $request)
    {
        $clientId = $this->session->get('userClientId');

        /** @var UserOnConfirm $user */
        $user = $this->userOnConfirmRepository->findOneBy(['clientId' => $clientId]);

        if ($user) {
            $this->confirmEmailSender->send(
                $email = $request->request->get('mail'),
                $token = (new ConfirmEmailTokenizer())->generate()
            );

            $user->emailToConfirm($email, $token);
            $this->flusher->flush();

            return new JsonResponse([], 200);
        }

        return new JsonResponse([], 401);
    }

    /**
     * @Route("/user/email-token/confirm/{token}", name="confirm_email_token")
     *
     * @param string $token
     * @param Request $request
     * @param GoogleAuthenticator $googleAuthenticator
     *
     * @return Response|null
     *
     * @throws Exception
     */
    public function confirmEmail(
        string $token,
        Request $request,
        GoogleAuthenticator $googleAuthenticator
    ): ?Response {

        /** @var UserOnConfirm $userOnConfirm */
        $userOnConfirm = $this->userOnConfirmRepository->findOneBy(['token' => $token]);

        if (null === $userOnConfirm) {
            throw new NotFoundHttpException('Token was expired.');
        }

        try {
            $user = $this->createUserByOauthType($userOnConfirm);

            $this->flusher->persist($user);
            $this->flusher->remove($userOnConfirm);
            $this->flusher->flush();

        } catch (PDOException $e) {
            $this->logger->error($e->getMessage());
        }

        $this->session->remove('userClientId');

        return $this->guardAuthenticatorHandler
            ->authenticateUserAndHandleSuccess(
                /**@var User $user */
                $user,
                $request,
                $googleAuthenticator,
                'main'
            );
    }

    /**
     * @param UserOnConfirm $userOnConfirm
     * @return User
     *
     * @throws Exception
     */
    private function createUserByOauthType(UserOnConfirm $userOnConfirm): User
    {
        $user = null;

        if ($userOnConfirm->isOAuthType(RequestOAuth::GITHUB)) {
            $user = User::fromGithubRequest(
                $userOnConfirm->getClientId(),
                $userOnConfirm->getEmail(),
                $userOnConfirm->getAvatarUrl(),
                $userOnConfirm->getGithubHtmlUrl(),
                $userOnConfirm->getGithubApiUrl(),
                $userOnConfirm->getLocale(),
                $userOnConfirm->getName()
            );

            $user->attachNetwork(
                'github',
                $userOnConfirm->getClientId(),
                $userOnConfirm->getGithubHtmlUrl(),
                $userOnConfirm->getGithubApiUrl()
            );

        } elseif ($userOnConfirm->isOAuthType(RequestOAuth::GOOGLE)) {
            $user = User::fromGoogleRequest(
                $userOnConfirm->getClientId(),
                $userOnConfirm->getEmail(),
                $userOnConfirm->getName(),
                $userOnConfirm->getAvatarUrl(),
                $userOnConfirm->getLocale()
            );

            $user->attachNetwork('google', $userOnConfirm->getClientId());

        } elseif ($userOnConfirm->isOAuthType(RequestOAuth::FACEBOOK)) {
            $user = User::fromFacebookRequest(
                $userOnConfirm->getClientId(),
                $userOnConfirm->getEmail(),
                $userOnConfirm->getName(),
                $userOnConfirm->getAvatarUrl(),
                $userOnConfirm->getLocale()
            );

            $user->attachNetwork('facebook', $userOnConfirm->getClientId());

        } elseif ($userOnConfirm->isOAuthType(RequestOAuth::VK)) {
            $user = User::fromVkRequest(
                $userOnConfirm->getClientId(),
                $userOnConfirm->getEmail()
            );

            $user->attachNetwork('vk', $userOnConfirm->getClientId());
        }

        return $user;
    }
}
