<?php

declare(strict_types=1);

namespace App\Controller\User\Profile;

use App\ReadModel\Test\TestPaginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestsController extends AbstractController
{
    /**
     * @Route("/me/tests", name="user_tests")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param TestPaginator $paginator
     *
     * @return Response
     */
    public function all(Request $request, TestPaginator $paginator): Response
    {
        $tests = $paginator->paginateTestResultsForUser($request, $this->getUser(), 10);

        return $this->render('user/profile/tests.html.twig', [
            'tests' => $tests->items,
            'remainingPages' => $tests->remainingPages
        ]);
    }

    /**
     * @Route("/api/tests/fetch", name="api_tests_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param TestPaginator $paginator
     * @return JsonResponse
     */
    public function fetch(Request $request, TestPaginator $paginator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            $this->createNotFoundException();
        }

        $tests = $paginator->paginateTestResultsForUser($request, $this->getUser(), 10);

        $view = [
            'html' => $this->renderView('components/feeds/testsFeed.html.twig', [
                'tests' => $tests->items
            ]),
            'remainingPages' => $tests->remainingPages
        ];

        return new JsonResponse($view, 200);
    }
}
