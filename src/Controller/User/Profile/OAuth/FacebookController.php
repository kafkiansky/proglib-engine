<?php

declare(strict_types=1);

namespace App\Controller\User\Profile\OAuth;

use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\UseCase\Network\Attach;
use App\Domain\User\UseCase\Network\Detach;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use DomainException;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\FacebookUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class FacebookController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    public function __construct(
        LoggerInterface $logger,
        RouterInterface $router,
        FlashMessageService $flashMessage,
        TokenStorageInterface $tokenStorage
    ){
        $this->logger = $logger;
        $this->router = $router;
        $this->flashMessage = $flashMessage;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route("/user/cabinet/facebook/attach", name="facebook_attach")
     *
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function connect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook_attach')
            ->redirect([
                'public_profile', 'email'
            ]);
    }

    /**
     * @Route("/user/cabinet/facebook/attach/check", name="facebook_attach_check")
     *
     * @param ClientRegistry $clientRegistry
     * @param Attach\Handler $handler
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function attach(ClientRegistry $clientRegistry, Attach\Handler $handler): RedirectResponse
    {
        $client = $clientRegistry->getClient('facebook_attach');
        /** @var FacebookUser $facebookUser */
        $facebookUser = $client->fetchUser();

        $command = new Attach\Command(
            $this->tokenStorage->getToken()->getUser()->getId()->toString(),
            'facebook',
            $facebookUser->getId(),
            $facebookUser->getPictureUrl(),
            $facebookUser->getEmail() ?? null
        );

        try {
            $handler->handle($command);
            $this->flashMessage->success('Социальная сеть успешно привязана');
            return RedirectResponse::create($this->router->generate('cabinet'));
        } catch (DomainException $exception) {
            $this->logger->warning($exception->getMessage());
            $this->flashMessage->error($exception->getMessage());
            return RedirectResponse::create($this->router->generate('cabinet'));
        }
    }

    /**
     * @Route("/user/cabinet/facebook/detach", name="api_facebook_detach")
     *
     * @param Request $request
     * @param Detach\Handler $handler
     * @return JsonResponse
     */
    public function detach(Request $request, Detach\Handler $handler): JsonResponse
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $command = new Detach\Command(
            $user,
            'github',
            $request->request->get('clientId')
        );

        try {
            $handler->handle($command);
            return JsonResponse::create(['success' => 1], Response::HTTP_OK);
        } catch (DomainException $exception) {
            if ($exception->getCode() === Detach\Handler::NETWORK_IS_LAST) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            if ($exception->getCode() === Detach\Handler::NETWORK_IS_MAIN) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            return JsonResponse::create([
                'success' => 0,
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }
    }
}
