<?php

declare(strict_types=1);

namespace App\Controller\User\Profile\OAuth;

use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\UseCase\Network\Attach;
use App\Domain\User\UseCase\Network\Detach;
use Doctrine\ORM\NonUniqueResultException;
use DomainException;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\GoogleUser;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GoogleController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    public function __construct(
        LoggerInterface $logger,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router,
        FlashMessageService $flashMessage
    ) {
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->flashMessage = $flashMessage;
    }

    /**
     * @Route("/user/cabinet/google/attach", name="google_attach")
     *
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function connect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry->getClient('google_attach')
            ->redirect([
                'email', 'profile'
            ]);
    }

    /**
     * @Route("/user/cabinet/google/attach/check", name="google_attach_check")
     *
     * @param ClientRegistry $clientRegistry
     * @param Attach\Handler $handler
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function attach(ClientRegistry $clientRegistry, Attach\Handler $handler): RedirectResponse
    {
        $client = $clientRegistry->getClient('google_attach');

        /** @var GoogleUser $googleUser */
        $googleUser = $client->fetchUser();

        $command = new Attach\Command(
            $this->tokenStorage->getToken()->getUser()->getId()->toString(),
            'google',
            $googleUser->getId(),
            $googleUser->getAvatar(),
            $googleUser->getEmail()
        );

        try {
            $handler->handle($command);
            $this->flashMessage->success('Социальная сеть успешно привязана');
            return RedirectResponse::create($this->router->generate('cabinet'));
        } catch (DomainException $exception) {
            $this->logger->warning($exception->getMessage());
            $this->flashMessage->error($exception->getMessage());
            return RedirectResponse::create($this->router->generate('cabinet'));
        }
    }

    /**
     * @Route("/user/cabinet/google/detach", name="api_google_detach")
     *
     * @param Request $request
     * @param Detach\Handler $handler
     * @return JsonResponse
     */
    public function detach(Request $request, Detach\Handler $handler): JsonResponse
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $command = new Detach\Command(
            $user,
            'github',
            $request->request->get('clientId')
        );

        try {
            $handler->handle($command);
            return JsonResponse::create(['success' => 1], Response::HTTP_OK);
        } catch (DomainException $exception) {
            if ($exception->getCode() === Detach\Handler::NETWORK_IS_LAST) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            if ($exception->getCode() === Detach\Handler::NETWORK_IS_MAIN) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            return JsonResponse::create([
                'success' => 0,
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }
    }
}
