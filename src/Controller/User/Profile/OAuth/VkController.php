<?php

declare(strict_types=1);

namespace App\Controller\User\Profile\OAuth;

use App\Application\Web\Core\Service\FlashMessageService;
use App\Domain\User\Model\Entity\Network;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\UseCase\Network\Attach;
use App\Domain\User\UseCase\Network\Detach;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use DomainException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class VkController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $vkClientId;

    /**
     * @var string
     */
    private $vkClientSecret;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var Attach\Handler
     */
    private $attachHandler;

    /**
     * @var FlashMessageService
     */
    private $flashMessage;

    /**
     * @var Detach\Handler
     */
    private $detachHandler;

    public function __construct(
        string $vkClientId,
        string $vkClientSecret,
        UrlGeneratorInterface $urlGenerator,
        LoggerInterface $logger,
        TokenStorageInterface $tokenStorage,
        RouterInterface $router,
        FlashMessageService $flashMessage,
        Attach\Handler $attachHandler,
        Detach\Handler $detachHandler
    ) {
        $this->logger = $logger;
        $this->tokenStorage = $tokenStorage;
        $this->router = $router;
        $this->vkClientId = $vkClientId;
        $this->vkClientSecret = $vkClientSecret;
        $this->urlGenerator = $urlGenerator;
        $this->attachHandler = $attachHandler;
        $this->flashMessage = $flashMessage;
        $this->detachHandler = $detachHandler;
    }

    /**
     * @Route("/user/cabinet/vk/attach", name="vk_attach");
     *
     * @return RedirectResponse
     */
    public function connect()
    {
        $params = http_build_query([
            'client_id' => $this->vkClientId,
            'client_secret' => $this->vkClientSecret,
            'redirect_uri' => $this->urlGenerator->generate(
                'vk_attach_check',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            'scope' => 'email'
        ]);

        $oauthUrl = 'https://oauth.vk.com/authorize?' . $params;

        return new RedirectResponse($oauthUrl);
    }

    /**
     * @Route("/user/cabinet/vk/attach/check", name="vk_attach_check")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     *
     * @throws NonUniqueResultException
     * @throws EntityNotFoundException
     */
    public function attach(Request $request): RedirectResponse
    {
        $params = http_build_query([
            'client_id' => $this->vkClientId,
            'client_secret' => $this->vkClientSecret,
            'code' => $request->query->get('code'),
            'redirect_uri' => $this->router->generate('vk_attach_check', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);

        $url = 'https://oauth.vk.com/access_token?' . $params;

        $response = file_get_contents($url);

        $vkUser = json_decode($response);

        $clientId = $vkUser->user_id;

        $command = new Attach\Command(
            $this->tokenStorage->getToken()->getUser()->getId()->toString(),
            Network::VK,
            (string)$clientId,
            null,
            isset($vkUser->email) ? $vkUser->email : null
        );

        try {
            $this->attachHandler->handle($command);
            $this->flashMessage->success('Социальная сеть успешно привязана');
            return RedirectResponse::create($this->router->generate('cabinet'));
        } catch (DomainException $e) {
            $this->logger->warning($e->getMessage());
            $this->flashMessage->error($e->getMessage());
            return RedirectResponse::create($this->router->generate('cabinet'));
        }
    }

    /**
     * @Route("/user/cabinet/vk/detach", name="api_vk_detach")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function detach(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();

        $command = new Detach\Command(
            $user,
            Network::VK,
            $request->request->get('clientId')
        );

        try {
            $this->detachHandler->handle($command);
            return JsonResponse::create(['success' => 1], Response::HTTP_OK);
        } catch (DomainException $exception) {
            if ($exception->getCode() === Detach\Handler::NETWORK_IS_LAST) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            if ($exception->getCode() === Detach\Handler::NETWORK_IS_MAIN) {
                return JsonResponse::create([
                    'success' => 0,
                    'error' => $exception->getMessage()
                ], Response::HTTP_OK);
            }

            return JsonResponse::create([
                'success' => 0,
                'error' => $exception->getMessage()
            ]);
        }
    }
}
