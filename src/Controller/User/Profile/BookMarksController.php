<?php

declare(strict_types=1);

namespace App\Controller\User\Profile;

use App\Domain\Post\ReadModel\PaginateView;
use App\Domain\User\Model\Entity\User;
use App\ReadModel\Post\PostPaginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookMarksController extends AbstractController
{
    /**
     * @Route("/me/bookmarks", name="bookmarks")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     *
     * @return Response
     */
    public function bookmarks(Request $request, PostPaginator $paginator): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        /** @var PaginateView $bookMarks */
        $bookMarks = $paginator->paginateBookMarks($request, $user);

        return $this->render('user/profile/bookmarks.html.twig', [
            'posts' => $bookMarks->items,
            'remainingPages' => $bookMarks->remainingPages,
            'user' => $user
        ]);
    }

    /**
     * @Route("/api/user/cabinet/bookmarks", name="api_user_bookmarks_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     *
     * @return JsonResponse
     */
    public function preLoadBookMarks(Request $request, PostPaginator $paginator)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($request->request->get('type') === 'publications') {
            return $this->loadPost($request, $paginator, $user);
        } else {
            return $this->loadComments($request, $paginator, $user);
        }
    }

    /**
     * @param Request $request
     * @param PostPaginator $paginator
     * @param User $user
     * @return JsonResponse
     */
    private function loadComments(Request $request, PostPaginator $paginator, User $user): JsonResponse
    {
        $comments = $paginator->paginateBookMarksComment($request, $user);

        $view = [
            'html' => $this->renderView('components/feeds/commentsFeed.html.twig', [
                'comments' => $comments->items,
                'location' => 'profile'
            ]),
            'remainingPages' => $comments->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @param Request $request
     * @param PostPaginator $paginator
     * @param User $user
     * @return JsonResponse
     */
    private function loadPost(Request $request, PostPaginator $paginator, User $user): JsonResponse
    {
        /** @var PaginateView $posts */
        $posts = $paginator->paginateBookMarks($request, $user);

        $view = [
            'html' => $this->renderView('components/feeds/feed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }
}
