<?php

declare(strict_types=1);

namespace App\Controller\User\Profile;

use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Model\Entity\User;
use Symfony\Component\HttpFoundation\Response;

class SidebarController
{
    public const USER_FOREIGN = 'foreign';
    public const USER_OWN = 'own';

    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @param User $user
     * @param string $userType
     * @return Response
     */
    public function publicationsCount(User $user, string $userType)
    {
        $count = null;

        if ($userType === self::USER_OWN) {
            $count = $this->postRepository->findPublicationsCount($user->getId()->toString());
        } elseif ($userType === self::USER_FOREIGN) {
            $count = $this->postRepository->findForeignUserPublicationsCount($user->getId()->toString());
        }

        return Response::create($count);
    }
}
