<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Post\ReadModel\PaginateView;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Repository\UserRepository;
use App\ReadModel\Comment\CommentPaginator;
use App\ReadModel\Post\PostPaginator;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ForeignUserController extends AbstractController
{
    /**
     * @Route("/user/{nickname}", name="foreign_user_profile")
     * @param User $user
     * @return Response
     */
    public function user(User $user)
    {
        if ($user->getPosts()->count() > 0) {
            return $this->redirectToRoute('foreign_user_posts', [
                'nickname' => $user->getNickName()
            ]);
        }

        if ($user->getComments()->count() > 0) {
            return $this->redirectToRoute('foreign_user_comments', [
                'nickname' => $user->getNickName()
            ]);
        }

        return $this->render('user/profile/inactive.html.twig', compact('user'));
    }

    /**
     * @Route("/user/{nickname}/posts", name="foreign_user_posts")
     *
     * @param User $user
     * @param PostPaginator $paginator
     * @param Request $request
     * @return Response
     * @throws NonUniqueResultException
     */
    public function posts(User $user, PostPaginator $paginator, Request $request)
    {
        $posts = $paginator->paginatePostsOnCabinet($request, $user);

        return $this->render('user/profile/publications.html.twig', [
            'user' => $user,
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages
        ]);
    }

    /**
     * @Route("/user/{nickname}/comments", name="foreign_user_comments")
     *
     * @param User $user
     * @param Request $request
     * @param CommentPaginator $paginator
     * @return Response
     * @throws \Exception
     */
    public function comments(User $user, Request $request, CommentPaginator $paginator)
    {
        $comments = $paginator->paginateCommentsOnCabinet($request, $user);

        return $this->render('user/profile/comments.html.twig', [
            'comments' => $comments->items,
            'user' => $user,
            'location' => 'profile',
            'remainingPages' => $comments->remainingPages
        ]);
    }

    /**
     * @Route("/api/user/foreign/comments", name="api_user_foreign_comments_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param CommentPaginator $paginator
     *
     * @param UserRepository $users
     * @return JsonResponse
     * @throws \Exception
     */
    public function commentsFetch(Request $request, CommentPaginator $paginator, UserRepository $users): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        try {
            /** @var User $user */
            $user = $users->get($request->get('userId'));
        } catch (EntityNotFoundException $e) {
            return $this->json([], 200);
        }

        $posts = $paginator->paginateCommentsOnCabinet($request, $user);

        $view = [
            'html' => $this->renderView('components/feeds/commentsFeed.html.twig', [
                'comments' => $posts->items,
                'location' => 'profile'
            ]),
            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/api/user/foreign/publications", name="api_user_foreign_publications_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @param UserRepository $users
     * @return Response
     * @throws NonUniqueResultException
     */
    public function postsFetch(Request $request, PostPaginator $paginator, UserRepository $users): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        try {
            /** @var User $user */
            $user = $users->get($request->get('userId'));
        } catch (EntityNotFoundException $e) {
            return $this->json([], 200);
        }

        /** @var PaginateView $posts */
        $posts = $paginator->paginatePostsOnCabinet($request,  $user);

        $view = [
            'html' => $this->renderView('components/feeds/feed.html.twig', [
                'posts' => $posts->items
            ]),
            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }
}
