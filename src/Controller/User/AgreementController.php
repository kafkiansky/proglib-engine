<?php

declare(strict_types=1);

namespace App\Controller\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class AgreementController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @param Environment $twig
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/user-agreement", name="user_agreement")
     */
    public function agreement(): Response
    {
        return new Response(
            $this->twig->render('user/privacy/userAgreement.html.twig')
        );
    }

    /**
     * @Route("/privacy-policy", name="privacy_policy")
     */
    public function privacyPolicy(): Response
    {
        return new Response(
            $this->twig->render('user/privacy/privacyPolicy.html.twig')
        );
    }
}