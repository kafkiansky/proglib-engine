<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Model\Comment\Entity\Comment;
use App\Domain\User\Model\Entity\User;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Model\Notification\UseCase\Create\PostLike;
use App\Model\Notification\UseCase\Create\CommentLike;

class LikesController extends AbstractController
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param EventDispatcherInterface $dispatcher
     * @param LoggerInterface $logger
     */
    public function __construct(EventDispatcherInterface $dispatcher, LoggerInterface $logger)
    {
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    /**
     * @Route("/p/like/{id}", name="api_like_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param PostLike\Handler $handler
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function likePost(Post $post, PostLike\Handler $handler, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->likePost($post->getUser(), $user);

        $command = new PostLike\Command($post->getUser(), $user, $post);

        try {
            $handler->handle($command);
            $flusher->flush($post);
        } catch (\DomainException $exception) {
            $this->logger->warning($exception->getMessage());
        }

        return new JsonResponse(['likesCount' => $post->getUserLikes()->count()]);
    }

    /**
     * @Route("/p/unlike/{id}", name="api_unlike_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     *
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     */
    public function unlikePost(Post $post, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post->unlike($user);
        $flusher->flush();

        return new JsonResponse(['likesCount' => $post->getUserLikes()->count()]);
    }

    /**
     * @Route("/c/like/{id}", name="api_like_comment")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Comment $comment
     * @param CommentLike\Handler $handler
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function likeComment(Comment $comment, CommentLike\Handler $handler, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $comment->likeComment($comment->getUser(), $user);

        $command = new CommentLike\Command(
            $comment->getUser(),
            $user,
            $comment->getPost(),
            $comment
        );

        try {
            $handler->handle($command);
            $flusher->flush($comment);
        } catch (\DomainException $exception) {
            $this->logger->warning($exception->getMessage());
        }

        return new JsonResponse(['likesCount' => $comment->getUserLikes()->count()]);
    }

    /**
     * @Route("/c/unlike/{id}", name="api_unlike_comment")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Comment $comment
     *
     * @param Request $request
     * @param Flusher $flusher
     * @return JsonResponse
     */
    public function unlikeComment(Comment $comment, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $comment->unlike($user);
        $flusher->flush();

        return new JsonResponse(['likesCount' => $comment->getUserLikes()->count()]);
    }
}
