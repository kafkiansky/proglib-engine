<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Model\Notification\Entity\Id;
use App\Model\Notification\Entity\Notification;
use App\Model\Notification\Entity\NotificationPreferencesRepository;
use App\Model\Notification\Service\NotificationService;
use App\Model\Notification\Request\ChangeNotificationPreferencesRequest;
use App\ReadModel\Notification\NotificationFetcher;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use App\Model\Notification\UseCase\Preferences;

class NotificationController extends AbstractController
{
    public const PARTIALS_PER_PAGE = 5;
    public const ALL_PER_PAGE = 20;

    /**
     * @var NotificationFetcher
     */
    private $notifications;

    /**
     * @var Environment
     */
    private $view;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(NotificationFetcher $notifications, Environment $view, LoggerInterface $logger)
    {
        $this->notifications = $notifications;
        $this->view = $view;
        $this->logger = $logger;
    }

    /**
     * @Route("/user/notifications/fetch", name="api_notifications_fetch")
     *
     * @param Request $request
     * @return Response
     */
    public function partials(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('proglib_app');
        }

        $actor = $this->getUser();
        $view = null;

        $notifications = $this->notifications->notificationForUser(
            $actor,
            self::PARTIALS_PER_PAGE,
            Notification::NOT_READ
        );

        if (\count($notifications) < 0) {
            $view = [
                'html' => $this->renderView('components/feeds/notificationsFeed.html.twig', [
                    'notifications' => [],
                    'location' => 'header'
                ]),
                'success' => 0
            ];
        } else {
            $view = [
                'html' => $this->renderView('components/feeds/notificationsFeed.html.twig', [
                    'notifications' => $notifications,
                    'location' => 'header'
                ]),
                'success' => 1,
                'remainingCount' => $this->notifications->count($actor, Notification::NOT_READ) ?? 0
            ];
        }

        return JsonResponse::create($view, Response::HTTP_OK);
    }

    /**
     * @Route("/me/notifications", name="user_notifications")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param NotificationPreferencesRepository $repo
     * @return Response
     */
    public function all(Request $request, NotificationPreferencesRepository $repo): Response
    {
        $actor = $this->getUser();

        $notifications = $this->notifications->paginateNotifications($request, $actor, self::ALL_PER_PAGE);

        $existingPreferences = $repo->findAll($this->getUser()) ?
            explode(',', $repo->findAll($this->getUser())) :
            null
        ;

        return $this->render('user/profile/notifications.html.twig', [
            'notifications' => $notifications['notifications'],
            'remainingPages' => $notifications['remainingPages'],
            'preferences' => $existingPreferences,
            'remainingCount' => $this->notifications->count($actor, Notification::NOT_READ)
        ]);
    }

    /**
     * @Route("/user/notifications/fetch/all", name="api_user_notifications_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function allFetch(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('proglib_app');
        }

        $actor = $this->getUser();
        $notifications = $this->notifications->paginateNotifications($request, $actor, self::ALL_PER_PAGE);

        $view = [
            'html' => $this->renderView('components/feeds/notificationsFeed.html.twig',[
                'notifications' => $notifications['notifications']
            ]),
            'remainingPages' => $notifications['remainingPages'],
            'remainingCount' => $this->notifications->count($actor, Notification::NOT_READ)
        ];

        return JsonResponse::create($view, Response::HTTP_OK);
    }

    /**
     * @return Response
     */
    public function count(): Response
    {
        return Response::create(
            $this->notifications->count($this->getUser(), Notification::NOT_READ) ?? null
        );
    }

    /**
     * @Route("/api/user/notification/delete", name="api_user_notification_delete")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param NotificationService $notificationService
     * @return JsonResponse
     */
    public function delete(Request $request, NotificationService $notificationService): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $id = $request->get('id');

        try {
            $notificationService->delete(new Id($id));
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            return $this->json(['error' => $e->getMessage()], 200);
        }
    }

    /**
     * @Route("/user/notification/preferences/setUp", name="user_notification_preferences_setUp")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param ChangeNotificationPreferencesRequest $notificationRequest
     * @param Preferences\Handler $handler
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function preferences(
        ChangeNotificationPreferencesRequest $notificationRequest,
        Preferences\Handler $handler,
        Request $request
    ): Response {

        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('user_notifications');
        }

        $command = new Preferences\Command(
            $notificationRequest->notificationTypes(),
            $this->getUser()
        );

        $handler->handle($command);

        return $this->redirectToRoute('user_notifications');
    }
}
