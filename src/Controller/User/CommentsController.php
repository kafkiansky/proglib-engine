<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Application\Manager\Post\CommentManager;
use App\Application\Services\Converter\StrikeConverter;
use App\Application\Services\Files\FileUploader;
use App\Application\Services\Files\FileViaUrlUploader;
use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use App\Model\Comment\Entity\Comment;
use App\Model\Comment\Entity\CommentRepository;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\Service\UsernameFinder;
use App\Model\Comment\Entity\Id;
use App\Security\Voter\CommentVoter;
use Exception;
use League\Flysystem\FileExistsException;
use League\HTMLToMarkdown\HtmlConverter;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Twig\Environment;
use App\Model\Notification\UseCase\Create\PostComment;
use App\Model\Notification\UseCase\Create\CommentComment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class CommentsController extends AbstractController
{
    private const FILE_DIR = 'comments';

    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \Parsedown
     */
    private $parsedown;

    /**
     * @var Environment
     */
    private $view;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var UsernameFinder
     */
    private $usernameFinder;

    /**
     * @var UserRepositoryInterface
     */
    private $users;

    /**
     * @param TokenStorageInterface $token
     * @param CommentRepository $commentRepository
     * @param CommentManager $commentManager
     * @param LoggerInterface $logger
     * @param \Parsedown $parsedown
     * @param Environment $view
     * @param EventDispatcherInterface $dispatcher
     * @param Flusher $flusher
     * @param UsernameFinder $usernameFinder
     * @param UserRepositoryInterface $users
     */
    public function __construct(
        TokenStorageInterface $token,
        CommentRepository $commentRepository,
        CommentManager $commentManager,
        LoggerInterface $logger,
        \Parsedown $parsedown,
        Environment $view,
        EventDispatcherInterface $dispatcher,
        Flusher $flusher,
        UsernameFinder $usernameFinder,
        UserRepositoryInterface $users
    ) {
        $this->token = $token;
        $this->commentRepository = $commentRepository;
        $this->commentManager = $commentManager;
        $this->logger = $logger;
        $this->parsedown = $parsedown;
        $this->view = $view;
        $this->dispatcher = $dispatcher;
        $this->flusher = $flusher;
        $this->usernameFinder = $usernameFinder;
        $this->users = $users;
    }

    /**
     * @Route("/p/comment/{id}", name="api_comment_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Request $request
     * @param PostComment\Handler $handler
     * @param FileUploader $uploader
     * @return JsonResponse
     *
     * @throws FileExistsException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function commentAdd(Post $post, Request $request, PostComment\Handler $handler, FileUploader $uploader): JsonResponse
    {
        /** @var User $user */
        $user = $this->token->getToken()->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $commentContent = $request->request->get('comment-content');

        if (null === $commentContent) {
            return JsonResponse::create([]);
        }

        $uploadedImages = [];

        if ($images = $request->files->get('commentFiles')) {

            /** @var UploadedFile $image */
            foreach ($images as $image) {
                $uploadedImages[] = $uploader->upload($image, self::FILE_DIR)->getFullName();
            }
        }

        $imagesForSave = array_merge($uploadedImages, $request->request->get('commentImgUrls') ?? []);

        $comment = Comment::fromBlank(
            Id::next(),
            $this->parsedown->text($this->usernameFinder->find($commentContent)),
            $post,
            $user,
            Comment::CREATED,
            json_encode($imagesForSave)
        );

        $post->addComment($comment);

        try {
            $handler->handle($comment);
        } catch (\DomainException $exception) {
            $this->logger->warning($exception->getMessage());
        }

        $this->flusher->flush($post, $comment);

        $view = [
            'html' => $this->view->render('components/comments/comment.html.twig', [
                'comment' => $comment,
                'post' => $post,
                'location' => $request->get('location')
            ]),
            'count' => $post->getCommentsCount(),
            'success' => $comment->getId() ? true : false
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/p/commentAnswer/{id}", name="api_comment_answer_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Request $request
     * @param CommentComment\Handler $handler
     * @param FileUploader $uploader
     * @return JsonResponse
     *
     * @throws FileExistsException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function commentAnswerAdd(Post $post, Request $request, CommentComment\Handler $handler, FileUploader $uploader)
    {
        /** @var User $user */
        $user = $this->token->getToken()->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $commentContent = $request->request->get('comment-content');

        if (null === $commentContent) {
            return JsonResponse::create([]);
        }

        $parentId = $request->request->get('parentId');

        if (null === $parentId) {
            return JsonResponse::create([]);
        }

        /** @var Comment $parent */
        $parent = $this->commentRepository->find($parentId);

        $uploadedImages = [];

        if ($images = $request->files->get('commentFiles')) {

            /** @var UploadedFile $image */
            foreach ($images as $image) {
                $uploadedImages[] = $uploader->upload($image, self::FILE_DIR)->getFullName();
            }
        }

        $imagesForSave = array_merge($uploadedImages, $request->request->get('commentImgUrls') ?? []);

        $comment = Comment::fromAnswer(
            Id::next(),
            $this->parsedown->text($this->usernameFinder->find($commentContent)),
            $post,
            $user,
            Comment::CREATED,
            $parent,
            json_encode($imagesForSave)
        );

        $post->addComment($comment);

        try {
            $handler->handle($comment);
        } catch (\DomainException $exception) {
            $this->logger->warning($exception->getMessage());
        }

        $this->flusher->flush($post, $comment);

        $view = [
            'html' => $this->view->render('components/comments/comment.html.twig', [
                'comment' => $comment,
                'post' => $post,
                'location' => $request->get('location')
            ]),
            'count' => $post->getCommentsCount(),
            'success' => $comment->getId() ? true : false
        ];

        return JsonResponse::create($view, 200);

    }

    /**
     * @Route("/p/comment_remove/{id}", name="api_comment_remove")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Comment $comment
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function commentRemove(Comment $comment): JsonResponse
    {
        $this->denyAccessUnlessGranted(CommentVoter::DELETE, $comment);

        $user = $this->token->getToken()->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $post = $comment->getPost();

        $this->flusher->remove($comment);
        $this->flusher->flush();

        return new JsonResponse([
            'success' => true,
            'count' => $post->getCommentsCount()
        ]);
    }

    /**
     * @Route("/p/{post}/comment/{comment}/edit", name="api_comment_edit")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Comment $comment
     * @param HtmlConverter $converter
     * @param Request $request
     *
     * @param StrikeConverter $strikeConverter
     * @param FileUploader $uploader
     * @return JsonResponse
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws FileExistsException
     */
    public function edit(
        Post $post,
        Comment $comment,
        HtmlConverter $converter,
        Request $request,
        StrikeConverter $strikeConverter,
        FileUploader $uploader
    ): JsonResponse {

        $this->denyAccessUnlessGranted(CommentVoter::EDIT, $comment);

        if (!$request->isMethod('POST')) {
            return JsonResponse::create([
                'success' => 1,
                'id' => $comment->getId()->getValue(),
                'userName' => $comment->getUser()->getNickName() ?? $comment->getUser()->getUsername(),
                'userPic' => $comment->getUser()->getAvatar(),
                'time' => $comment->getCreatedAt()->format('d.m.Y'),
                'comment' => $strikeConverter->convert(
                    $this->usernameFinder->replace(
                        $converter->convert($comment->getComment())
                    )),
                'parentId' => $comment->getParentId() ?? null,
                'result' => $comment->getId()->getValue() ? true : false,
                'count' => $post->getCommentsCount(),
                'images' => $comment->getImages()
            ]);
        }

        $uploadedImages = [];

        if ($images = $request->files->get('commentFiles')) {

            /** @var UploadedFile $image */
            foreach ($images as $image) {
                $uploadedImages[] = $uploader->upload($image, self::FILE_DIR)->getFullName();
            }
        }

        $imagesForSave = array_merge($uploadedImages, $request->request->get('commentImgUrls') ?? []);

        $content = $request->request->get('comment-content');

        $comment->edit(
            $this->parsedown->text($this->usernameFinder->find($content)),
            json_encode($imagesForSave)
        );

        $this->flusher->flush();

        $view = [
            'html' => $this->view->render('components/comments/comment.html.twig', [
                'comment' => $comment,
                'post' => $post,
                'location' => $request->get('location')
            ]),
            'count' => $post->getCommentsCount(),
            'success' => $comment->getId()->getValue() ? true : false
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/comments/images/url/upload", name="api_comment_image_from_url_upload")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param FileViaUrlUploader $uploader
     *
     * @return JsonResponse
     */
    public function uploadImagesFromUrl(Request $request, FileViaUrlUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $link = $request->request->get('link');

        $downloadedFile = $uploader->upload($link, self::FILE_DIR);

        $response = [
            'success' => 1,
            'file' => [
                'url' => $downloadedFile->getFullName()
            ]
        ];

        $this->logger->info($downloadedFile->getFullName());

        return new JsonResponse($response);
    }

    /**
     * @Route("/user/username/fetch", name="api_user_username_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @return Response
     */
    public function mention(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return $this->redirectToRoute('proglib_app');
        }

        $username = $request->request->get('mention');

        if ($username === $this->getUser()->getEmail()) {
            return JsonResponse::create([
                'success' => 0,
                'users' => []
            ], Response::HTTP_OK);
        }

        $users = $this->users->allByUsername($username);

        return JsonResponse::create([
            'success' => 1,
            'users' => $users
        ], Response::HTTP_OK);
    }
}
