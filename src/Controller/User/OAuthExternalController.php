<?php

declare(strict_types=1);

namespace App\Controller\User;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class OAuthExternalController extends AbstractController
{
    /**
     * @var string
     */
    private $vkClientId;

    /**
     * @var string
     */
    private $vkClientSecret;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        string $vkClientId,
        string $vkClientSecret,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->vkClientId = $vkClientId;
        $this->vkClientSecret = $vkClientSecret;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     *
     * @Route("/connect/google", name="connect_google_start")
     */
    public function redirectToGoogleConnect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('google')
            ->redirect([
                'email', 'profile'
            ]);
    }

    /**
     * @Route("/auth/google", name="google_auth")
     *
     * @return RedirectResponse|Response
     */
    public function authenticateGoogleUser()
    {
        if (!$this->getUser()) {
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        return $this->redirectToRoute('proglib_app');
    }

    /**
     * @Route("/connect/github", name="connect_github_start")
     *
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function redirectToGithubConnect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('github')
            ->redirect([
                'user', 'public_repo'
            ]);
    }

    /**
     * @Route("/auth/github", name="github_auth")
     *
     * @return RedirectResponse|Response
     */
    public function authenticateGithubUser()
    {
        if (!$this->getUser()) {
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        return $this->redirectToRoute('proglib_app');
    }

    /**
     * @Route("/connect/facebook", name="connect_facebook_start")
     *
     * @param ClientRegistry $clientRegistry
     *
     * @return RedirectResponse
     */
    public function redirectToFacebookConnect(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook')
            ->redirect([
                'public_profile', 'email'
            ]);
    }

    /**
     * @Route("/auth/facebook", name="facebook_auth")
     *
     * @return RedirectResponse|Response
     */
    public function authenticateFacebookUser()
    {
        if (!$this->getUser()) {
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        return $this->redirectToRoute('proglib_app');
    }

    /**
     * @Route("/connect/vk", name="connect_vk_start")
     */
    public function redirectToVkConnect()
    {
        $params = http_build_query([
            'client_id' => $this->vkClientId,
            'client_secret' => $this->vkClientSecret,
            'redirect_uri' => $this->urlGenerator->generate('vk_auth', [], UrlGeneratorInterface::ABSOLUTE_URL),
            'scope' => 'email'
        ]);

        $oauthUrl = 'https://oauth.vk.com/authorize?' . $params;

        return new RedirectResponse($oauthUrl);
    }

    /**
     * @Route("/auth/vk", name="vk_auth")
     *
     * @return RedirectResponse|Response
     */
    public function authenticateVkUser()
    {
        if (!$this->getUser()) {
            return new Response('User not found', Response::HTTP_NOT_FOUND);
        }

        return $this->redirectToRoute('proglib_app');
    }

//    /**
//     * @Route("/login", name="login")
//     *
//     * @return Response
//     */
//    public function login(): Response
//    {
//        return $this->render('user/mainPage.html.twig');
//    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(): Response
    {
        return new RedirectResponse($this->generateUrl('proglib_app'));
    }
}