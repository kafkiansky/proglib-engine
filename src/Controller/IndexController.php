<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\Manager\User\UserManager;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\Post\Repository\TagsRepositoryInterface;
use App\Model\Comment\Entity\CommentRepository;
use App\Domain\User\Model\Entity\User;
use App\ReadModel\Post\PaginateView;
use App\ReadModel\Post\PostPaginator;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Routing\RouterInterface;

class IndexController extends AbstractController
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @var TagsRepositoryInterface
     */
    private $tagsRepository;

    /**
     * @var CommentRepository
     */
    private $commentRepository;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var FilesystemAdapter
     */
    private $fileSystem;

    /**
     * @param PostRepositoryInterface $postRepository
     * @param TagsRepositoryInterface $tagsRepository
     * @param CommentRepository $commentRepository
     * @param RouterInterface $router
     * @param UserManager $userManager
     * @param AdapterInterface $fileSystem
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        TagsRepositoryInterface $tagsRepository,
        CommentRepository $commentRepository,
        RouterInterface $router,
        UserManager $userManager,
        AdapterInterface $fileSystem
    ) {
        $this->postRepository = $postRepository;
        $this->tagsRepository = $tagsRepository;
        $this->commentRepository = $commentRepository;
        $this->userManager = $userManager;
        $this->router = $router;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @Route("/", name="proglib_app")
     *
     * @param Request $request
     *
     * @param PostPaginator $paginator
     * @return Response
     * @throws NonUniqueResultException
     */
    public function app(Request $request, PostPaginator $paginator): Response
    {
        $token = $this->getUser();

        /** @var User $user */
        $user = $token instanceof UserInterface ? $token : null;

        /** @var PaginateView $posts */
        $posts = $paginator->paginateOnMain($request, 10, $user);
        $tags = $this->tagsRepository->findAll();

        return $this->render('facade/index.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/api/posts/fetch", name="api_posts_fetch")
     *
     * @param Request $request
     *
     * @param PostPaginator $paginator
     * @return JsonResponse
     * @throws NonUniqueResultException
     */
    public function preLoadPosts(Request $request, PostPaginator $paginator): JsonResponse
    {
        $token = $this->getUser();

        /** @var User $user */
        $user = $token instanceof UserInterface ? $token : null;

        /** @var PaginateView $posts */
        $posts = $paginator->paginateOnMain($request, 10, $user);

        $view = [
            'html' => $this->renderView('components/feeds/advertFeed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @return Response
     */
    public function apiScript(): Response
    {
        $routes = $this->router->getRouteCollection()->all();

        $apiRoutes = [];

        foreach ($routes as $k => $route) {
            if (strpos($k, 'api_') === 0) {
                $defaults = $route->getDefaults();
                unset($defaults['_controller']);

                $apiRoutes[substr($k, 4)] = [
                    'path' => $route->getPath(),
                    'variables' => $route->compile()->getPathVariables(),
                    'defaults' => $defaults
                ];
            }
        }

        return $this->render('components/scripts/apiScript.html.twig', [
            'api_routes' => $apiRoutes,
            'routes' => $routes
        ]);
    }
}
