<?php

declare(strict_types=1);

namespace App\Controller\Company;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class AdvertisingController
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Environment
     */
    private $view;

    /**
     * @param RouterInterface $router
     * @param Environment $view
     */
    public function __construct(
        RouterInterface $router,
        Environment $view
    ) {
        $this->router = $router;
        $this->view = $view;
    }

    /**
     * @Route("/ads/new", name="ads_new")
     */
    public function new(Request $request)
    {
        return new Response(
            $this->view->render('')
        );
    }
}