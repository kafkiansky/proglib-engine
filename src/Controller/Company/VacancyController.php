<?php

declare(strict_types=1);

namespace App\Controller\Company;

use App\Application\Services\Files\FileUploader;
use App\Domain\Flusher;
use App\Domain\Post\Repository\TagsRepository;
use App\Model\Geographer\City\Entity\CityRepository;
use App\Model\Vacancy\Entity\Vacancy;
use GuzzleHttp\Client;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Vacancy\UseCase\Create;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Model\Vacancy\UseCase\Filter;

class VacancyController extends AbstractController
{
    public const COMPANY_FILES_PATH = 'companies';
    public const VACANCY_FILES_PATH = 'vacancies';

    /**
     * @Route("/vacancies/new", name="vacancy_new")
     *
     * @param TagsRepository $tagsRepo
     *
     * @return Response
     */
    public function new(TagsRepository $tagsRepo)
    {
        $tags = $tagsRepo->findAll();
        $rules = self::rules();

        return $this->render('advertising/vacancyForm.html.twig', compact('tags', 'rules'));
    }

    /**
     * @Route("/vacancies/{slug}/payment", name="vacancy_payment")
     *
     * @param Vacancy $vacancy
     * @return Response
     */
    public function payment(Vacancy $vacancy)
    {
        return $this->render('advertising/vacancyPayment.html.twig', [
            'type' => 'vacancy',
            'id' => '123',
            'slug' => $vacancy->getSlug()->getValue()
        ]);
    }

    /**
     * @Route("/vacancy/{slug}/payment/pay", name="vacancy_payment_pay")
     *
     * @param Request $request
     * @param Vacancy $vacancy
     *
     * @return Response
     */
    public function pay(Vacancy $vacancy, Request $request)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $client = new Client();
        $apiUrl = 'https://api.cloudpayments.ru/payments/cards/charge?';
        $params = [
            'Amount' => 100,
            'Currency' => $request->get('currency'),
            'Name' => $request->get('name'),
            'IpAddress' => $request->getClientIp(),
            'CardCryptogramPacket' => $request->get('cryptogram')
        ];

        $buildUrl = $apiUrl . http_build_query($params);

        $response = $client->post($buildUrl, [
            'auth' => [
                'pk_0a33dc2a393180714acb8d26f6bfe',
                '9f2f0e5f16fa1de79ac06b4195070be6'
            ]
        ]);

        return $this->json(['response' => $response->getBody()->getContents()], 200);
    }


    /**
     * @Route("/vacancies/all", name="vacancies_all")
     *
     * @param Filter\VacancyPaginator $paginator
     * @param Request $request
     * @param TagsRepository $tags
     * @return Response
     */
    public function all(Filter\VacancyPaginator $paginator, Request $request, TagsRepository $tags): Response
    {
        $filter = new Filter\Filter($request->request->all(), $request->query->all());

        $filteredVacancies = $paginator->paginate($filter, 10);

        return $this->render('advertising/vacanciesFeed.html.twig', [
            'vacancies' => $filteredVacancies['vacancies'],
            'remainingPages' => $filteredVacancies['remainingPages'],
            'filter' => $filter,
            'tags' => $tags->findAll()
        ]);
    }

    /**
     * @Route("/api/vacancies/more", name="api_vacancies_more")
     *
     * @param Request $request
     * @param Filter\VacancyPaginator $paginator
     * @param TagsRepository $tags
     * @return JsonResponse
     */
    public function more(Request $request, Filter\VacancyPaginator $paginator, TagsRepository $tags)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $filter = new Filter\Filter($request->request->all(), $request->query->all());

        $filteredVacancies = $paginator->paginate($filter, 10);

        $view = [
            'success' => 1,
            'html' => $this->renderView('components/feeds/vacanciesFeed.html.twig', [
                'vacancies' => $filteredVacancies['vacancies']
            ]),
            'remainingPages' => $filteredVacancies['remainingPages'],
            'filter' => $filter,
            'tags' => $tags->findAll()
        ];

        return JsonResponse::create($view, 200);
    }


    /**
     * @Route("/vacancies/{slug}", name="vacancy_show")
     *
     * @param Vacancy $vacancy
     * @param Flusher $flusher
     * @return Response
     */
    public function show(Vacancy $vacancy, Flusher $flusher): Response
    {
        $vacancy->increaseViewsCount();
        $flusher->flush();

        return $this->render('advertising/vacancy.html.twig', compact('vacancy'));
    }

    /**
     * @Route("/api/vacancies/create", name="api_vacancy_create")
     *
     * @param Request $request
     * @param Create\Handler $handler
     * @param ValidatorInterface $validator
     * @param Flusher $flusher
     * @param UrlGeneratorInterface $urlGenerator
     * @return Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function create(
        Request $request,
        Create\Handler $handler,
        ValidatorInterface $validator,
        Flusher $flusher,
        UrlGeneratorInterface $urlGenerator
    ): Response {

        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Create\Command($request->request->all());

        $violations = $validator->validate($command);

        if (\count($violations) > 0) {
            $errors = [];

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }

            return $this->json(['errors' => $errors], 200);
        }

        try {
            $slug = $handler->handle($command);
            $flusher->flush();
            return $this->json([
                'success' => 1,
                'redirectUrl' => $urlGenerator->generate('vacancy_payment', [
                    'slug' => $slug
                ])
            ], 200);
        } catch (\DomainException $e) {
            return $this->json(['success' => 0], 200);
        }
    }

    /**
     * @Route("/vacancies/{slug}/edit", name="vacancy_edit")
     *
     * @param Vacancy $vacancy
     * @param TagsRepository $tagsRepo
     * @return Response
     */
    public function edit(Vacancy $vacancy, TagsRepository $tagsRepo)
    {
        $tags = $tagsRepo->findAll();
        $rules = self::rules();

        return $this->render('advertising/vacancyForm.html.twig', compact('vacancy','tags', 'rules'));
    }

    /**
     * @Route("/api/vacancy/company-image/upload", name="api_vacancy_company-image_upload")
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function companyFilesUpload(Request $request, FileUploader $uploader)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        $fileUploaded = $uploader->upload($file, self::COMPANY_FILES_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/vacancy/vacancy-image/upload", name="api_vacancy_vacancy-image_upload")
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     * @throws FileExistsException
     */
    public function vacancyFilesUpload(Request $request, FileUploader $uploader)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        $fileUploaded = $uploader->upload($file, self::VACANCY_FILES_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/user/city/search", name="api_user_city_search")
     *
     * @param Request $request
     * @param CityRepository $cities
     * @return JsonResponse
     */
    public function searchCity(Request $request, CityRepository $cities)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $similarCities = $cities->findByQuery($request->get('query'), 5);

        return $this->json(['cities' => $similarCities], 200);
    }

    /**
     * @return array
     */
    private static function rules(): array
    {
        return [
            'vacancyTitle' => [
                'required' => true, 'empty' => 'no-title',
                'min' => 10, 'minMessage' => 'too-short-title',
                'max' => 100, 'maxMessage' => 'too-long-title',
            ],
            'salaryFrom' => [
                'required' => true, 'empty' => 'no-salary-from',
                'min' => 5000, 'minMessage' => 'too-little-salary-from',
            ],
            'salaryTo' => [
                'required' => true, 'empty' => 'no-salary-to',
                'min' => 5000, 'minMessage' => 'too-little-salary-to',
            ],
            'vacancyWorkType' => [
                'required' => true, 'empty' => 'no-work-type',
            ],
            'vacancyWorkPlace' => [
                'required' => true, 'empty' => 'no-work-place',
            ],
            'vacancyCity' => [
                'required' => true, 'empty' => 'no-city',
            ],
            'requirements' => [
                'required' => true, 'empty' => 'no-requirements',
                'min' => 1, 'minMessage' => 'too-few-requirements',
                'max' => 20, 'maxMessage' => 'too-many-requirements',
                'values' => 'requirements-values-are-empty',
            ],
            'tasks' => [
                'required' => true, 'empty' => 'no-tasks',
                'min' => 1, 'minMessage' => 'too-few-tasks',
                'max' => 20, 'maxMessage' => 'too-many-tasks',
                'values' => 'tasks-values-are-empty',
            ],
            'conditions' => [
                'required' => true, 'empty' => 'no-conditions',
                'min' => 1, 'minMessage' => 'too-few-conditions',
                'max' => 20, 'maxMessage' => 'too-many-conditions',
                'values' => 'conditions-values-are-empty',
            ],
            'tags' => [
                'required' => true, 'empty' => 'no-tags',
                'min' => 1, 'minMessage' => 'too-few-tags',
                'max' => 5, 'maxMessage' => 'too-many-tags',
            ],
            'candidateEmail' => [
                'required' => true, 'empty' => 'no-candidate-email',
            ],
            'notificationsEmail' => [
                'required' => true, 'empty' => 'no-notifications-email',
            ],
            'companyImage' => [
                'required' => true, 'empty' => 'no-company-image',
            ],
            'companyTitle' => [
                'required' => true, 'empty' => 'no-company-title',
                'min' => 10, 'minMessage' => 'too-short-company-title',
                'max' => 150, 'maxMessage' => 'too-long-company-title'
            ],
            'companySite' => [
                'required' => true, 'empty' => 'no-company-site',
            ],
            'companyDescription' => [
                'required' => true, 'empty' => 'no-company-description',
                'min' => 50, 'minMessage' => 'too-short-company-description',
                'max' => 400, 'maxMessage' => 'too-long-company-description',
            ],
        ];
    }
}
