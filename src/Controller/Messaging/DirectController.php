<?php

declare(strict_types=1);

namespace App\Controller\Messaging;

use App\Model\Comment\Entity\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Twig\Environment;

class DirectController extends AbstractController
{
    /**
     * @var CommentRepository
     */
    private $comments;

    /**
     * @var Environment
     */
    private $view;

    public function __construct(CommentRepository $comments, Environment $view)
    {
        $this->comments = $comments;
        $this->view = $view;
    }

    public function lastFiveComments()
    {
        $lastFiveComments = $this->comments->findLatestByCount();

        return $this->render('facade/desktop/lastComments.html.twig', [
            'comments' => $lastFiveComments
        ]);
    }
}
