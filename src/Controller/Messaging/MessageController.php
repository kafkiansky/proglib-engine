<?php

declare(strict_types=1);

namespace App\Controller\Messaging;

use App\Application\Ampq\Producer\ReportMessageProducer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MessageController
{
    /**
     * @var ReportMessageProducer
     */
    private $producer;

    public function __construct(ReportMessageProducer $producer)
    {
        $this->producer = $producer;
    }

    /**
     * @Route("/message/publish", name="messaging_publish")
     */
    public function index(): JsonResponse
    {
        $message = [
            'content' => 'Message is publish now'
        ];

        $rabbitMQMessage = json_encode($message);

        $this->producer->publish($rabbitMQMessage);

        return JsonResponse::create('Message was send', 200);
    }
}
