<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Application\Services\Files\FileSizeDependUploader;
use App\Application\Services\Files\FileUploader;
use App\Application\Services\Files\FileViaUrlUploader;
use App\Application\Services\Files\InvalidImageSizeException;
use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\TagsRepository;
use App\Domain\Post\Service\PostFetcher;
use App\Domain\Post\UseCase\Delete\Command;
use App\Domain\Post\UseCase\Delete\Handler;
use App\Domain\User\Model\Entity\User;
use App\ReadModel\Comment\CommentPaginator;
use App\ReadModel\Comment\PaginateView;
use App\ReadModel\Post\PostPaginator;
use App\Security\Voter\PostVoter;
use Exception;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Domain\Post\UseCase\Create;
use App\Domain\Post\UseCase\Publish;

class PostsController extends AbstractController
{
    private const POST_FILE_PATH = 'posts';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @Route("/p/new-post", name="new_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param TagsRepository $tags
     * @return Response
     */
    public function new(TagsRepository $tags): Response
    {
        $tags = $tags->findAll();
        $rules = self::rules();

        return $this->render('posts/create/new-post.html.twig', compact('tags', 'rules'));
    }

    /**
     * @Route("/p/{id}/edit", name="edit_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     *
     * @param TagsRepository $tags
     * @return Response
     */
    public function edit(Post $post, TagsRepository $tags): Response
    {
        $this->denyAccessUnlessGranted(PostVoter::EDIT, $post);

        $tags = $tags->findAll();
        $rules = self::rules();

        return $this->render('posts/create/new-post.html.twig', compact('post', 'tags', 'rules'));
    }

    /**
     * @Route("/p/publish", name="save_post")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param Publish\Handler $handler
     * @param ValidatorInterface $validator
     *
     * @return JsonResponse
     */
    public function publish(Request $request, Publish\Handler $handler, ValidatorInterface $validator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Publish\Command($request->request->all(), $this->getUser());

        $violations = $validator->validate($command);

        if (\count($violations) > 0) {
            $errors = [];

            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $errors[] = $violation->getMessage();
            }

            return $this->json(['errors' => $errors, 'success' => 0], 200);
        }

        try {
            $handler->handle($command);
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            $this->logger->warning($e->getMessage());
            return $this->json(['success' => 0], 500);
        }
    }

    /**
     * @Route("/p/draft", name="save_draft")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param Create\Handler $handler
     *
     * @param Flusher $flusher
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function keep(Request $request, Create\Handler $handler, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Create\Command($request->request->all(), $this->getUser());

        try {
            $handler->handle($command);
            $flusher->flush();
            return $this->json(['success' => 1], 200);
        } catch (\DomainException $e) {
            $this->logger->warning($e->getMessage());
            return $this->json(['success' => 0], 200);
        }
    }

    /**
     * @Route("/server/files/article-preview/upload", name="preview_image_upload")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param FileSizeDependUploader $uploader
     * @return JsonResponse
     *
     * @throws FileExistsException
     */
    public function upload(Request $request, FileSizeDependUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        try {
            $fileUploaded = $uploader->handle($file, self::POST_FILE_PATH);
            return JsonResponse::create([
                'success' => 1,
                'file' => [
                    'url' => $fileUploaded->getFullName()
                ]
            ], Response::HTTP_OK);
        } catch (InvalidImageSizeException $exception) {
            return JsonResponse::create([
                'success' => 0,
                'error' => 'dimensions'
            ], Response::HTTP_OK);
        }
    }

    /**
     * @Route("/server/files/article/upload", name="article_files_uploader")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Request $request
     * @param FileUploader $uploader
     * @return JsonResponse
     *
     * @throws FileExistsException
     */
    public function multiplyFiles(Request $request, FileUploader $uploader): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        $fileUploaded = $uploader->upload($file, self::POST_FILE_PATH);

        return JsonResponse::create([
            'success' => 1,
            'file' => [
                'url' => $fileUploaded->getFullName()
            ]
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/api/post/image/fetch", name="api_post_image_fetch")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param FileViaUrlUploader $uploader
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function imageFromUrlDownload(FileViaUrlUploader $uploader, Request $request): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $url = $request->request->get('link');
        $image = $uploader->upload($url, self::POST_FILE_PATH);

        $response = [
            'success' => 1,
            'file' => [
                'url' => $image->getFullName()
            ]
        ];

        return JsonResponse::create($response, 200);
    }

    /**
     * @Route("/p/{id}", name="single_post")
     *
     * @param Post $post
     * @param Flusher $flusher
     * @param PostFetcher $fetcher
     *
     * @return Response
     */
    public function showPost(Post $post, Flusher $flusher, PostFetcher $fetcher): Response
    {
        if ($post->getStatus() === Post::DRAFT &&
            $this->getUser() !== $post->getUser()
        ) {
            $this->createNotFoundException();
        }

        $post->increaseViewsCount();
        $flusher->flush();

        $recommendations = $fetcher->recommendationsForPost($post);

        return $this->render('posts/article.html.twig', compact('post', 'recommendations'));
    }

    /**
     * @Route("/p/tag/{tag}", name="tag_posts")
     *
     * @param Request $request
     * @param string $tag
     * @param PostPaginator $paginator
     *
     * @return Response
     */
    public function showTagPosts(Request $request, string $tag, PostPaginator $paginator): Response
    {
        $posts = $paginator->paginatePostsByTag($request, $tag);

        return $this->render('facade/tag.html.twig', [
            'tag' => $tag,
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages
        ]);
    }

    /**
     * @param Post $post
     * @param Request $request
     * @param CommentPaginator $paginator
     *
     * @return Response
     */
    public function postComments(Post $post, Request $request, CommentPaginator $paginator): Response
    {
        /** @var PaginateView $comments */
        $comments = $paginator->paginateCommentsForPost($request, $post);

        return $this->render('posts/partials/comments.html.twig', [
            'post' => $post,
            'comments' => $comments->items,
            'remainingPages' => $comments->remainingPages,
            'location' => 'post'
        ]);
    }

    /**
     * @Route("/api/p/comments/fetch", name="api_post_comments_fetch")
     *
     * @param Request $request
     * @param CommentPaginator $paginator $paginator
     *
     * @return JsonResponse
     */
    public function preLoadPostComments(Request $request, CommentPaginator $paginator)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $comments = $paginator->paginateCommentsForPost($request);

        $view = [
            'html' => $this->renderView('components/feeds/commentsFeed.html.twig', [
                'comments' => $comments->items,
                'location' => 'post'
            ]),
            'remainingPages' => $comments->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/api/p/{id}/delete", name="api_post_delete")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Post $post
     * @param Handler $handler
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function delete(Post $post, Handler $handler, Request $request)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $this->denyAccessUnlessGranted(PostVoter::DELETE, $post);

        /** @var User $user */
        $user = $this->getUser();

        $command = new Command($post, $user);

        try {
            $handler->handle($command);
            return JsonResponse::create(['success' => 1], Response::HTTP_OK);
        } catch (\DomainException $exception) {
            return JsonResponse::create(['success' => 0], Response::HTTP_OK);
        }
    }

    /**
     * @return array
     */
    public static function rules(): array
    {
        return [
            'id' => [
                'empty' => 'wrong-id'
            ],
            'title' => [
                'required' => true, 'empty' => 'no-title',
                'min' => 20, 'minMessage' => 'too-short-title',
                'max' => 130, 'maxMessage' => 'too-long-title',
            ],
            'preview' => [
                'required' => true, 'empty' => 'no-preview',
                'min' => 50, 'minMessage' => 'too-short-preview',
                'max' => 600, 'maxMessage' => 'too-long-preview',
            ],
            'body' => [
                'required' => true, 'empty' => 'no-content',
                'min' => 500, 'minMessage' => 'too-short-content',
            ],
            'tags' => [
                'required' => true, 'empty' => 'no-tags',
                'min' => 1, 'minMessage' => 'too-few-tags',
                'max' => 3, 'maxMessage' => 'too-many-tags',
            ]
        ];
    }
}
