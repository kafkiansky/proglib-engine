<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Domain\Post\ReadModel\PaginateView;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Model\Management\Search\Service\SearchService;
use App\ReadModel\Post\PostPaginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/search", name="search")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @param SearchService $searchService
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function search(Request $request, PostPaginator $paginator, SearchService $searchService): Response
    {
        /** @var PaginateView $posts */
        $posts = $paginator->paginateSearchPosts($request);

        $searchService->memorize($request->query->all(), $this->getUser() ?? null);

        return $this->render('facade/search.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'query' => $request->query->get('q')
        ]);
    }

    /**
     * @Route("/api/search/post", name="api_search_post_fetch")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     *
     * @return Response
     */
    public function searchOther(Request $request, PostPaginator $paginator)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var PaginateView $posts */
        $posts = $paginator->paginateSearchPosts($request);

        $view = [
            'html' => $this->renderView('components/feeds/feed.html.twig', [
                'posts' => $posts->items,
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }



    /**
     * @Route("/api/search/fetch", name="api_get_search_results")
     * @param Request $request
     * @param PostRepositoryInterface $posts
     * @return JsonResponse
     */
    public function asyncSearch(Request $request, PostRepositoryInterface $posts): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $query = $request->get('query');

        $posts = $posts->searchByQuery($query, 5, 0);

        $view = [
            'html' => $this->renderView('components/feeds/searchFeed.html.twig', [
                'posts' => $posts
            ]),
            'success' => 1
        ];

        return JsonResponse::create($view, 200);
    }
}
