<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\PostRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class StatsController
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    /**
     * @param RouterInterface $router
     * @param EntityManagerInterface $em
     * @param PostRepositoryInterface $postRepository
     */
    public function __construct(
        RouterInterface $router,
        EntityManagerInterface $em,
        PostRepositoryInterface $postRepository
    ) {
        $this->router = $router;
        $this->em = $em;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route("/posts/stats/bounce-rate", name="api_score_bounce_rate")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function scoreBounceRate(Request $request): JsonResponse
    {
        $postId = $request->request->get('postId');

        /** @var Post $post */
        $post = $this->postRepository->findOnePostBy(['id' => $postId]);

        if ($post) {
            $post->updateBounceRate($post->getBounceRate() + 1);
            $this->em->flush();

            return new JsonResponse([], 200);
        }

        return new JsonResponse([], 500);
    }
}