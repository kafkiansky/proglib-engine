<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Domain\Post\Repository\PostRepository;
use App\Domain\Post\Service\Order\Paginator;
use App\ReadModel\Post\PostPaginator;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class FavoriteController extends AbstractController
{
    /**
     * @var PostRepository
     */
    private $posts;

    public function __construct(PostRepository $posts)
    {
        $this->posts = $posts;
    }

    /**
     * @Route("/p/favorite/week", name="posts_favorite_by_week")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return Response
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function week(Request $request, PostPaginator $paginator): Response
    {
        $posts = $paginator->paginateFavoritePostsByWeek($request);

        return $this->render('facade/favorite.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'period' => 'week'
        ]);
    }

    /**
     * @Route("/api/p/favorite/week/fetch", name="api_posts_favorite_by_week_fetch")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return JsonResponse
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function fetchWeek(Request $request, PostPaginator $paginator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $posts = $paginator->paginateFavoritePostsByWeek($request);

        $view = [
            'html' => $this->renderView('components/feeds/advertFeed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/p/favorite/month", name="posts_favorite_by_month")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return Response
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function month(Request $request, PostPaginator $paginator): Response
    {
        $posts = $paginator->paginateFavoritePostsByMonth($request);

        return $this->render('facade/favorite.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'period' => 'month'
        ]);
    }

    /**
     * @Route("/api/p/favorite/month/fetch", name="api_posts_favorite_by_month_fetch")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return JsonResponse
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function fetchMonth(Request $request, PostPaginator $paginator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $posts = $paginator->paginateFavoritePostsByMonth($request);

        $view = [
            'html' => $this->renderView('components/feeds/advertFeed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }

    /**
     * @Route("/p/favorite/year", name="posts_favorite_by_year")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return Response
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function year(Request $request, PostPaginator $paginator): Response
    {
        $posts = $paginator->paginateFavoritePostsByYear($request);

        return $this->render('facade/favorite.html.twig', [
            'posts' => $posts->items,
            'remainingPages' => $posts->remainingPages,
            'period' => 'year'
        ]);
    }

    /**
     * @Route("/api/p/favorite/year/fetch", name="api_posts_favorite_by_year_fetch")
     *
     * @param Request $request
     * @param PostPaginator $paginator
     * @return JsonResponse
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function fetchYear(Request $request, PostPaginator $paginator): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $posts = $paginator->paginateFavoritePostsByYear($request);

        $view = [
            'html' => $this->renderView('components/feeds/advertFeed.html.twig', [
                'posts' => $posts->items
            ]),

            'remainingPages' => $posts->remainingPages
        ];

        return JsonResponse::create($view, 200);
    }
}
