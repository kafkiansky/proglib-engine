<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Model\Test\Entity\Test;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class BookmarksController extends AbstractController
{
    /**
     * @Route("/t/{slug}/bookmark", name="api_bookmark_test")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Test $test
     * @param Request $request
     * @param Flusher $flusher
     *
     * @return JsonResponse
     */
    public function bookmark(Test $test, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $test->bookmark($user);
        $flusher->flush();

        return new JsonResponse(['bookmarksCount' => $test->getUserBookmarks()->count()], 200);
    }

    /**
     * @Route("/t/{slug}/unbookmark", name="api_unbookmark_test")
     * @Security("is_granted('ROLE_USER')")
     *
     * @param Test $test
     * @param Request $request
     * @param Flusher $flusher
     *
     * @return JsonResponse
     */
    public function unBookmark(Test $test, Request $request, Flusher $flusher): JsonResponse
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return new JsonResponse([], Response::HTTP_UNAUTHORIZED);
        }

        $test->unBookmark($user);
        $flusher->flush();

        return new JsonResponse(['bookmarksCount' => $test->getUserBookmarks()->count()], 200);
    }
}
