<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Domain\Post\Repository\TagsRepository;
use App\ReadModel\Test\TestPaginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TestingController extends AbstractController
{
    /**
     * @Route("/tests/all", name="tests_all")
     *
     * @param Request $request
     * @param TestPaginator $paginator
     * @param TagsRepository $tags
     *
     * @return Response
     */
    public function all(Request $request, TestPaginator $paginator, TagsRepository $tags): Response
    {
        $pagination = $paginator->paginateOnMain($request);

        return $this->render('facade/tests.html.twig', [
            'tests' => $pagination->items,
            'remainingPages' => $pagination->remainingPages,
            'tags' => $tags->findAll()
        ]);
    }

    /**
     * @Route("/api/tests/all/fetch", name="api_tests_all_fetch")
     *
     * @param Request $request
     * @param TestPaginator $paginator
     *
     * @return JsonResponse
     */
    public function fetch(Request $request, TestPaginator $paginator)
    {
        if (!$request->isMethod('POST')) {
            $this->createNotFoundException();
        }

        $pagination = $paginator->paginateOnMain($request);

        $view = [
            'html' => $this->renderView('components/feeds/testsFeed.html.twig', [
                'tests' => $pagination->items
            ]),
            'remainingPages' => $pagination->remainingPages
        ];

        return new JsonResponse($view, 200);
    }
}
