<?php

declare(strict_types=1);

namespace App\Controller\Test;

use App\Domain\Flusher;
use App\Model\Test\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Model\Test\UseCase\Process\Check;
use App\Model\Test\UseCase\Process\Result;
use App\Model\Test\UseCase\Process\Next;
use App\Model\Test\UseCase\Process\Terminate;

class UserTestProcessController extends AbstractController
{
    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(Flusher $flusher)
    {
        $this->flusher = $flusher;
    }

    /**
     * @Route("/tests/{slug}", name="test_start")
     * @param Test $test
     * @return Response
     */
    public function start(Test $test): Response
    {
        return $this->render('posts/test.html.twig', compact('test'));
    }

    /**
     * @Route("/api/t/question/next", name="api_test_question_next")
     *
     * @param Request $request
     * @param Next\Handler $handler
     * @return JsonResponse
     */
    public function next(Request $request, Next\Handler $handler): Response
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Next\Command(
            $request->request->get('testId'),
            $request->request->get('currentQuestionId')
        );

        try {
            /** @var Next\QuestionView $nextQuestion */
            $nextQuestion = $handler->next($command);

            return JsonResponse::create([
                'success' => 1,
                'question' => [
                    'id' => $nextQuestion->id,
                    'text' => $this->renderView('posts/partials/newArticleContent.html.twig', [
                        'blocks' => json_decode($nextQuestion->text)
                    ]),
                    'variants' => $nextQuestion->variants,
                    'multiple' => $nextQuestion->multiple ? 1 : 0,
                ],
                'current' => $nextQuestion->current,
                'total' => $nextQuestion->total
            ], 200);
        } catch (\DomainException $e) {
            return $this->json(['exception' => $e->getMessage()], 200);
        }
    }

    /**
     * @Route("/api/t/question/checkAnswer", name="api_test_question_checkAnswer")
     *
     * @param Request $request
     * @param Check\Handler $handler
     * @param Result\Handler $resultHandler
     * @return Response
     * @throws \Exception
     */
    public function check(Request $request, Check\Handler $handler, Result\Handler $resultHandler)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Check\Command(
            $request->request->get('questionId'),
            $request->request->get('answers')
        );

        try {
            /** @var Check\QuestionResultDto $resultDto */
            $resultDto = $handler->check($command);

            $commandResult = new Result\Command(
                $request->request->get('testId'),
                $request->request->get('questionId'),
                $this->getUser(),
                $resultDto->result
            );

            $resultHandler->handle($commandResult);
        } catch (\DomainException $e) {
            return $this->json(['exception' => $e->getMessage()], 500);
        }

        $this->flusher->flush();

        return JsonResponse::create([
            'success' => 1,
            'explanation' => $this->renderView('posts/partials/newArticleContent.html.twig', [
                    'blocks' => json_decode($resultDto->explanation)
                ]),
            'result' => $resultDto->result,
            'correctAnswers' => $resultDto->correctAnswers
        ], 200);
    }

    /**
     * @Route("/api/test/terminate", name="api_test_terminate")
     *
     * @param Request $request
     * @param Terminate\Handler $handler
     * @return JsonResponse
     */
    public function terminate(Request $request, Terminate\Handler $handler)
    {
        if (!$request->isMethod('POST')) {
            throw new NotFoundHttpException();
        }

        $command = new Terminate\Command(
            $request->request->get('testId'),
            $this->getUser()
        );

        try {
            $result = $handler->terminate($command);

            return JsonResponse::create([
                'success' => 1,
                'result' => $this->renderView('posts/partials/newArticleContent.html.twig', [
                    'blocks' => $result->resultText
                ]),
                'total' => $result->answersCount,
                'correct' => $result->correctAnswersNumber
            ]);
        } catch (\DomainException $e) {
            return $this->json([], 500);
        }
    }
}
