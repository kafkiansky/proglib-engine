<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class PostVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';
    public const DELETE = 'delete';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Post) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        /** @var User $user */
        if (!$user instanceof UserInterface) {
            return false;
        }

        $post = $subject;

        if ($attribute === self::VIEW) {
            return $this->canView($post, $user);
        }

        if ($attribute === self::EDIT) {
            return $this->canEdit($post, $user);
        }

        if ($attribute === self::DELETE) {
            return $this->canDelete($post, $user);
        }

        return false;
    }

    /**
     * @param Post $post
     * @param User $user
     * @return bool
     */
    public function canView(Post $post, User $user): bool
    {
        if ($post->getStatus() === Post::DRAFT && $post->getUser() !== $user) {
            return false;
        }

        return true;
    }

    /**
     * @param Post $post
     * @param User $user
     * @return bool
     */
    public function canEdit(Post $post, User $user): bool
    {
        return $user === $post->getUser() ||
            in_array($user->getRole(), Role::highRoles());
    }

    /**
     * @param Post $post
     * @param User $user
     * @return bool
     */
    public function canDelete(Post $post, User $user): bool
    {
        return $user === $post->getUser();
    }
}
