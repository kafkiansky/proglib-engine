<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Domain\Post\Model\Entity\Comment;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class CommentVoter extends Voter
{
    public const VIEW = 'view';
    public const EDIT = 'edit';
    public const DELETE = 'delete';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject): bool
    {
        if (!in_array($attribute, [self::VIEW, self::EDIT, self::DELETE])) {
            return false;
        }

        if (!$subject instanceof Comment) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        /**@var User $user */
        if (!$user instanceof UserInterface) {
            return false;
        }

        $comment = $subject;

        if ($attribute === self::VIEW) {
            return $this->canView($comment);
        }

        if ($attribute === self::EDIT) {
            return $this->canEdit($comment, $user);
        }

        if ($attribute === self::DELETE) {
            return $this->canDelete($comment, $user);
        }

        return false;
    }

    /**
     * @param Comment $comment
     * @param User $user
     * @return bool
     */
    public function canEdit(Comment $comment, User $user): bool
    {
        return $user === $comment->getUser();
    }

    /**
     * @param Comment $comment
     * @param User $user
     * @return bool
     */
    public function canDelete(Comment $comment, User $user): bool
    {
        return $user === $comment->getUser() ||
            in_array($user->getRoles(), Role::premiumRoles());
    }

    /**
     * @param Comment $comment
     * @return bool
     */
    public function canView(Comment $comment): bool
    {
        if ($comment->getStatus() === Comment::DELETED) {
            return false;
        }

        return true;
    }
}
