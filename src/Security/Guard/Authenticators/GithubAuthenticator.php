<?php

declare(strict_types=1);

namespace App\Security\Guard\Authenticators;

use App\Application\Services\User\Avatar;
use App\Domain\User\Model\Entity\Network;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\Entity\UserOnConfirm;
use App\Domain\User\Repository\UserOnConfirmRepository;
use App\Domain\User\Repository\UserRepository;
use App\Domain\User\Repository\UserSocialRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2Client;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\GithubResourceOwner;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GithubAuthenticator extends SocialAuthenticator
{
    /**
     * @var ClientRegistry
     */
    private $clientRegistry;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var UserOnConfirmRepository
     */
    private $userOnConfirmRepository;

    /**
     * @var UserSocialRepository
     */
    private $userSocialRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Avatar
     */
    private $avatar;

    /**
     * @param ClientRegistry $clientRegistry
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param RouterInterface $router
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface $session
     * @param UserOnConfirmRepository $userOnConfirmRepository
     * @param UserSocialRepository $userSocialRepository
     * @param TokenStorageInterface $tokenStorage
     * @param FlashBagInterface $flashBag
     * @param LoggerInterface $logger
     * @param Avatar $avatar
     */
    public function __construct(
        ClientRegistry $clientRegistry,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        RouterInterface $router,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        UserOnConfirmRepository $userOnConfirmRepository,
        UserSocialRepository $userSocialRepository,
        TokenStorageInterface $tokenStorage,
        FlashBagInterface $flashBag,
        LoggerInterface $logger,
        Avatar $avatar
    ) {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->router = $router;
        $this->eventDispatcher = $eventDispatcher;
        $this->session = $session;
        $this->userOnConfirmRepository = $userOnConfirmRepository;
        $this->userSocialRepository = $userSocialRepository;
        $this->tokenStorage = $tokenStorage;
        $this->flashBag = $flashBag;
        $this->logger = $logger;
        $this->avatar = $avatar;
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     *
     * @return RedirectResponse|Response
     */
    public function start(
        Request $request,
        AuthenticationException $authException = null
    ): Response
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->attributes->get('_route') === 'github_auth';
    }

    /**
     * @param Request $request
     *
     * @return AccessToken|mixed
     */
    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getGithubClient());
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return User|UserInterface|void|null
     *
     * @throws Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var GithubResourceOwner $githubUser */
        $githubUser = $this->getGithubClient()
            ->fetchUserFromToken($credentials);

        $githubUserData = $githubUser->toArray();

        $clientId = $githubUser->getId();

        if ($this->userSocialRepository->findOneBy(['clientId' => $clientId]) &&
            !$this->userRepository->findOneUserBy(['clientId' => $clientId]) ||
            $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {

            /** @var Network $userByNetwork */
            $userByNetwork = $this->userSocialRepository->findOneBy(['clientId' => $clientId]);

            return $userByNetwork->getUser();
        }

        if (null === $githubUserData['email'] &&
            null === $this->userRepository->findOneUserBy(['clientId' => $clientId])
        ) {

            $this->session->set('userClientId', $githubUser->getId());

            if ($this->userOnConfirmRepository->findOneBy(['clientId' => $githubUser->getId()])) {
                return;
            }

            $userOnConfirm = UserOnConfirm::fromGithub($githubUser);

            $this->em->persist($userOnConfirm);
            $this->em->flush();

            return;
        }

        if (null !== $githubUserData['email'] &&
            null === $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            null === $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {
            $user = User::fromGithubRequest(
                (string) $clientId,
                $githubUserData['email'],
                $githubUserData['avatar_url'] ?? $this->avatar->getDefaultAvatar(),
                $githubUserData['html_url'],
                $githubUserData['url'],
                $githubUserData['location'],
                $githubUserData['name']
            );

            $user->attachNetwork(
                'github',
                $clientId,
                $this->avatar->getDefaultAvatar(),
                $githubUserData['email'] ?? null,
                $githubUserData['html_url'],
                $githubUserData['url']
            );

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        }
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return null|RedirectResponse
     */
    public function onAuthenticationFailure(
        Request $request,
        AuthenticationException $exception
    ): ?RedirectResponse {

        $this->session->set('failure', 'email');

        return new RedirectResponse(
            $this->router->generate('proglib_app'),
            Response::HTTP_MOVED_PERMANENTLY
        );
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     *
     * @return null|Response
     */
    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        $providerKey
    ): ?Response
    {
        return new RedirectResponse($this->router->generate('proglib_app'));
    }

    /**
     * @return OAuth2Client
     */
    private function getGithubClient(): OAuth2Client
    {
        return $this->clientRegistry->getClient('github');
    }
}
