<?php

declare(strict_types=1);

namespace App\Security\Guard\Authenticators;

use App\Application\Services\User\Avatar;
use App\Domain\User\Model\Entity\Network;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\Entity\UserOnConfirm;
use App\Domain\User\Repository\UserOnConfirmRepository;
use App\Domain\User\Repository\UserRepository;
use App\Domain\User\Repository\UserSocialRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class VKAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var string
     */
    private $vkClientId;

    /**
     * @var string
     */
    private $vkClientSecret;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var UserOnConfirmRepository
     */
    private $userOnConfirmRepository;

    /**
     * @var UserSocialRepository
     */
    private $userSocialRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var Avatar
     */
    private $avatar;

    /**
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param RouterInterface $router
     * @param string $vkClientId
     * @param string $vkClientSecret
     * @param SessionInterface $session
     * @param UserOnConfirmRepository $userOnConfirmRepository
     * @param UserSocialRepository $userSocialRepository
     * @param TokenStorageInterface $tokenStorage
     * @param FlashBagInterface $flashBag
     * @param Avatar $avatar
     */
    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepository,
        RouterInterface $router,
        string $vkClientId,
        string $vkClientSecret,
        SessionInterface $session,
        UserOnConfirmRepository $userOnConfirmRepository,
        UserSocialRepository $userSocialRepository,
        TokenStorageInterface $tokenStorage,
        FlashBagInterface $flashBag,
        Avatar $avatar
    ) {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->router = $router;
        $this->vkClientId = $vkClientId;
        $this->vkClientSecret = $vkClientSecret;
        $this->session = $session;
        $this->userOnConfirmRepository = $userOnConfirmRepository;
        $this->userSocialRepository = $userSocialRepository;
        $this->tokenStorage = $tokenStorage;
        $this->flashBag = $flashBag;
        $this->avatar = $avatar;
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     *
     * @return RedirectResponse|Response
     */
    public function start(
        Request $request,
        AuthenticationException $authException = null
    ): Response {
        throw new AccessDeniedHttpException();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'vk_auth';
    }

    /**
     * @param Request $request
     *
     * @return array|mixed
     */
    public function getCredentials(Request $request): array
    {
        return [
            'code' => $request->query->get('code')
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface|null|void
     *
     * @throws Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $params = http_build_query([
            'client_id' => $this->vkClientId,
            'client_secret' => $this->vkClientSecret,
            'code' => $credentials['code'],
            'redirect_uri' => $this->router->generate('vk_auth', [], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);

        $url = 'https://oauth.vk.com/access_token?' . $params;

        $response = file_get_contents($url);

        $vkUser = json_decode($response);

        $clientId = $vkUser->user_id;

        if ($this->userSocialRepository->findOneBy(['clientId' => $clientId]) &&
            !$this->userRepository->findOneUserBy(['clientId' => $clientId]) ||
            $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {

            /** @var Network $userByNetwork */
            $userByNetwork = $this->userSocialRepository->findOneBy(['clientId' => $clientId]);

            return $userByNetwork->getUser();
        }

        if (!isset($vkUser->email) &&
            null === $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            null === $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {
            $this->session->set('userClientId', $clientId);

            if ($this->userOnConfirmRepository->findOneBy(['clientId' => $clientId])) {
                return;
            }

            $userOnConfirm = UserOnConfirm::fromVk($clientId);

            $this->em->persist($userOnConfirm);
            $this->em->flush();

            return;
        }

        if (null !== $vkUser->email &&
            null === $this->userSocialRepository->findOneBy(['clientId' => $clientId]) &&
            null === $this->userRepository->findOneUserBy(['clientId' => $clientId])
        ) {
            $user = User::fromVkRequest(
                (string)$clientId,
                $vkUser->email,
                $this->avatar->getDefaultAvatar()
            );

            $user->attachNetwork(
                Network::VK,
                (string)$clientId,
                $vkUser->email,
                $this->avatar->getDefaultAvatar()
            );

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        }
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return RedirectResponse|null
     */
    public function onAuthenticationFailure(
        Request $request,
        AuthenticationException $exception
    ): ?RedirectResponse {

        $this->session->set('failure', 'email');

        return RedirectResponse::create($this->router->generate('proglib_app'));
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     *
     * @return RedirectResponse|Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return new RedirectResponse($this->router->generate('proglib_app'));
    }

    /**
     * @return bool
     */
    public function supportsRememberMe(): bool
    {
        return true;
    }
}
