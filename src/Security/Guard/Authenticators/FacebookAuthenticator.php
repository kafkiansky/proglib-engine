<?php

declare(strict_types=1);

namespace App\Security\Guard\Authenticators;

use App\Application\Services\User\Avatar;
use App\Domain\User\Model\Entity\Network;
use App\Domain\User\Model\Entity\User;
use App\Domain\User\Model\Entity\UserOnConfirm;
use App\Domain\User\Repository\UserOnConfirmRepository;
use App\Domain\User\Repository\UserRepository;
use App\Domain\User\Repository\UserSocialRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2Client;
use KnpU\OAuth2ClientBundle\Security\Authenticator\SocialAuthenticator;
use League\OAuth2\Client\Provider\FacebookUser;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

final class FacebookAuthenticator extends SocialAuthenticator
{
    /**
     * @var ClientRegistry
     */
    private $clientRegistry;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var UserOnConfirmRepository
     */
    private $userOnConfirmRepository;
    /**
     * @var UserSocialRepository
     */
    private $userSocialRepository;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    /**
     * @var Avatar
     */
    private $avatar;

    /**
     * @param ClientRegistry $clientRegistry
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     * @param RouterInterface $router
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface $session
     * @param UserOnConfirmRepository $userOnConfirmRepository
     * @param UserSocialRepository $userSocialRepository
     * @param TokenStorageInterface $tokenStorage
     * @param FlashBagInterface $flashBag
     * @param Avatar $avatar
     */
    public function __construct(
        ClientRegistry $clientRegistry,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        RouterInterface $router,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        UserOnConfirmRepository $userOnConfirmRepository,
        UserSocialRepository $userSocialRepository,
        TokenStorageInterface $tokenStorage,
        FlashBagInterface $flashBag,
        Avatar $avatar
    ) {
        $this->clientRegistry = $clientRegistry;
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->router = $router;
        $this->eventDispatcher = $eventDispatcher;
        $this->session = $session;
        $this->userOnConfirmRepository = $userOnConfirmRepository;
        $this->userSocialRepository = $userSocialRepository;
        $this->tokenStorage = $tokenStorage;
        $this->flashBag = $flashBag;
        $this->avatar = $avatar;
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     *
     * @return RedirectResponse|Response
     */
    public function start(
        Request $request,
        AuthenticationException $authException = null
    ): Response
    {
        throw new AccessDeniedHttpException();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request): bool
    {
        return $request->attributes->get('_route') === 'facebook_auth';
    }

    /**
     * @param Request $request
     *
     * @return AccessToken|mixed
     */
    public function getCredentials(Request $request)
    {
        return $this->fetchAccessToken($this->getFacebookClient());
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return User|null|UserInterface|void
     *
     * @throws Exception
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        /** @var FacebookUser $facebookUser */
        $facebookUser = $this->getFacebookClient()
            ->fetchUserFromToken($credentials);

        $clientId = $facebookUser->getId();
        $email = $facebookUser->getEmail();

        if ($this->userSocialRepository->findOneBy(['clientId' => $clientId]) &&
            !$this->userRepository->findOneUserBy(['clientId' => $clientId]) ||
            $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {

            /** @var Network $userByNetwork */
            $userByNetwork = $this->userSocialRepository->findOneBy(['clientId' => $clientId]);

            return $userByNetwork->getUser();
        }

        if (null === $email &&
            null === $this->userRepository->findOneUserBy(['clientId' => $facebookUser->getId()]) &&
            null === $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {

            $this->session->set('userClientId', $facebookUser->getId());

            if ($this->userOnConfirmRepository->findOneBy(['clientId' => $facebookUser->getId()])) {
                return;
            }

            $userOnConfirm = UserOnConfirm::fromFacebook($facebookUser);

            $this->em->persist($userOnConfirm);
            $this->em->flush();

            return;
        }

        if (null !== $email &&
            null === $this->userRepository->findOneUserBy(['clientId' => $clientId]) &&
            null === $this->userSocialRepository->findOneBy(['clientId' => $clientId])
        ) {
            $user = User::fromFacebookRequest(
                $clientId,
                $facebookUser->getEmail(),
                $facebookUser->getName(),
                $facebookUser->getPictureUrl() ?? $this->avatar->getDefaultAvatar(),
                $facebookUser->getLocale()
            );

            $user->attachNetwork(
                'facebook',
                $clientId,
                $this->avatar->getDefaultAvatar(),
                $facebookUser->getEmail() ?? null
            );

            $this->em->persist($user);
            $this->em->flush();

            return $user;
        }
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return null|RedirectResponse
     */
    public function onAuthenticationFailure(
        Request $request,
        AuthenticationException $exception
    ): ?RedirectResponse {

        $this->session->set('failure', 'email');

        return RedirectResponse::create($this->router->generate('proglib_app'));
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     *
     * @return null|Response
     */
    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,$providerKey
    ): ?Response
    {
        return new RedirectResponse($this->router->generate('proglib_app'));
    }

    /**
     * @return OAuth2Client
     */
    private function getFacebookClient(): OAuth2Client
    {
        return $this->clientRegistry->getClient('facebook');
    }
}
