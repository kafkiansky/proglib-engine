<?php

declare(strict_types=1);

namespace App\Security\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Twig\Environment;

class AccessDeniedListener
{
    /**
     * @var Environment
     */
    private $view;

    public function __construct(Environment $view)
    {
        $this->view = $view;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof AccessDeniedHttpException) {
            return;
        }

        $errorTemplate = $this->view->render('error/400.html.twig', [
            'errorCode' => 403
        ]);

        $response = new Response($errorTemplate);
        $event->setResponse($response);
    }
}
