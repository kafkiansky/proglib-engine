<?php

declare(strict_types=1);

namespace App\Security\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class ContentNotFoundListener
{
    /**
     * @var Environment
     */
    private $view;

    public function __construct(Environment $view)
    {
        $this->view = $view;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof NotFoundHttpException) {
            return;
        }

        $response = new Response();
        $errorTemplate = $this->view->render('error/400.html.twig', [
            'errorCode' => 404,
        ]);

        $response->setContent($errorTemplate);
        $event->setResponse($response);
    }
}
