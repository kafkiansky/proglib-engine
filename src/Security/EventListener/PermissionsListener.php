<?php

declare(strict_types=1);

namespace App\Security\EventListener;

use App\Application\Annotation\Permissions;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class PermissionsListener
{
    /**
     * @var Reader
     */
    private $reader;

    /**
     * @var TokenStorageInterface
     */
    private $token;

    public function __construct(Reader $reader, TokenStorageInterface $token)
    {
        $this->reader = $reader;
        $this->token = $token;
    }

    /**
     * @param FilterControllerEvent $event
     * @throws \ReflectionException
     */
    public function onKernelController(FilterControllerEvent $event): void
    {
        if (!is_array($controller = $event->getController())) {
            return;
        }

        $object = new \ReflectionObject($controller[0]);
        $method = $object->getMethod($controller[1]);

        /** @var Permissions $annotation */
        foreach ($this->reader->getMethodAnnotations($method) as $annotation) {
            if (isset($annotation->adminPermissions)) {
                /** @var User $user */
                $user = $this->token->getToken()->getUser();

                if (!$user instanceof UserInterface) {
                    throw new NotFoundHttpException();
                }

                if (!in_array($user->getRoles()[0], Role::premiumRoles())) {
                    throw new NotFoundHttpException();
                }
            }
        }
    }
}
