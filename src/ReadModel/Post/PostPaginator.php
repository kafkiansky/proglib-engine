<?php

declare(strict_types=1);

namespace App\ReadModel\Post;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\PostRepository;
use App\Domain\Post\Repository\TagsRepository;
use App\Domain\User\Model\Entity\User;
use App\Model\Comment\Entity\CommentRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class PostPaginator
{
    /**
     * @var PostRepository
     */
    private $posts;

    /**
     * @var CommentRepository
     */
    private $comments;
    /**
     * @var TagsRepository
     */
    private $tags;

    public function __construct(PostRepository $posts, CommentRepository $comments, TagsRepository $tags)
    {
        $this->posts = $posts;
        $this->comments = $comments;
        $this->tags = $tags;
    }

    public function paginatePostsByTag(Request $request, string $tagName)
    {
        try {
            /** @var Tag $tag */
            $tag = $this->tags->findByName($tagName);
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        dump($tag);

        $count = $this->posts->getCountPostsForTag($tag->getId()->toString());

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->findPostByTags($tag->getId()->toString(), 10, $offset, true);

        return new PaginateView($posts, $pages - $page);
    }

    /**
     * @param Request $request
     * @param int $limit
     * @param User|null $user
     * @return PaginateView
     * @throws NonUniqueResultException
     */
    public function paginateOnMain(Request $request, int $limit, ?User $user): PaginateView
    {
        $tag = $request->request->get('postsType');

        if ($tag !== null && preg_match('/\w{8}-\w{4}-\w{4}-\w{4}-\w{12}/', $tag, $matches)) {

            $page = $request->get('page') ?? 1;
            $count = $this->posts->getCountPostsForTag($tag);
            $pages = (int)ceil($count / 10);
            $offset = ($page - 1) * 10;

            $posts = $this->posts->findPostByTags($tag, $limit, $offset, true);

            return new PaginateView($posts, $pages - $page);
        }

        if ($user instanceof UserInterface && $user->getTags()->count() !== 0) {
            $page = $request->get('page') ?? 1;
            $count = $this->posts->findPublishedSubscriptionsCount($user);
            $pages = (int)ceil($count / 10);
            $offset = ($page - 1) * 10;

            $posts = $this->posts->findPublishedSubscriptions($user, $offset);
        } else {

            $page = $request->get('page') ?? 1;
            $count = $this->posts->getCountOfAllPosts();
            $pages = (int)ceil($count / 10);
            $offset = ($page - 1) * 10;

            $posts = $this->posts->findPublished($offset);
        }

        return new PaginateView($posts,$pages - $page);
    }

    /**
     * @param Request $request
     *
     * @return PaginateView
     */
    public function paginateSearchPosts(Request $request): PaginateView
    {
        $query = empty($request->query->get('q')) ?
            $request->request->get('q') :
            $request->query->get('q');

        $count = $this->posts->searchByQueryCount($query);
        $page = $request->get('page') ?? 1;

        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->searchByQuery($query, 10, $offset);

        return new PaginateView(
            $posts,
            $pages - $page
        );
    }

    /**
     * @param Request $request
     * @return PaginateView
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function paginateFavoritePostsByWeek(Request $request): PaginateView
    {
        $count = $this->posts->countFavoritePostsByWeek();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->favoriteByWeek($offset, 10);

        return new PaginateView($posts,$pages - $page);
    }

    /**
     * @param Request $request
     * @return PaginateView
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function paginateFavoritePostsByMonth(Request $request): PaginateView
    {
        $count = $this->posts->countFavoritePostsByMonth();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->favoriteByMonth($offset, 10);

        return new PaginateView($posts, $pages - $page);
    }

    /**
     * @param Request $request
     * @return PaginateView
     * @throws DBALException
     * @throws EntityNotFoundException
     */
    public function paginateFavoritePostsByYear(Request $request): PaginateView
    {
        $count = $this->posts->countFavoritePostsByYear();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->favoriteByYear($offset, 10);

        return new PaginateView($posts, $pages - $page);
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @return PaginateView
     */
    public function paginateBookMarks(Request $request, User $user)
    {
        $count = $user->getPostsBookmarked()->count();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $bookMarks = $this->posts->findBookMarks($user, $offset);

        return new PaginateView($bookMarks, $pages - $page);
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @return PaginateView
     */
    public function paginateBookMarksComment(Request $request, User $user): PaginateView
    {
        $count = $user->getCommentsBookmarked()->count();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $bookMarksComment = $this->comments->findBookmarkedComments($user, $offset);

        return new PaginateView($bookMarksComment, $pages - $page);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return PaginateView
     * @throws NonUniqueResultException
     */
    public function paginatePostsOnCabinet(Request $request, User $user): PaginateView
    {
        $count = $this->posts->getCount($status = $request->get('status') ?? Post::PUBLISHED, $user);

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $posts = $this->posts->findUserPostByStatus($user, $status, $offset);

        return new PaginateView($posts, $pages - $page);
    }
}
