<?php

declare(strict_types=1);

namespace App\ReadModel\Comment;

class PaginateView
{
    public $items = [];

    /**
     * @var int
     */
    public $remainingPages;

    /**
     * @param array|null $items
     * @param int $remainingPages
     */
    public function __construct(?array $items, int $remainingPages)
    {
        $this->items = $items;
        $this->remainingPages = $remainingPages;
    }
}
