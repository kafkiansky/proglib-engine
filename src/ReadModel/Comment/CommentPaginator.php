<?php

declare(strict_types=1);

namespace App\ReadModel\Comment;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use App\Model\Comment\Entity\CommentRepository;
use Symfony\Component\HttpFoundation\Request;

class CommentPaginator
{
    /**
     * @var CommentRepository
     */
    private $comments;

    public function __construct(CommentRepository $comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param Request $request
     * @param Post|null $post
     *
     * @return PaginateView
     */
    public function paginateCommentsForPost(Request $request, Post $post = null): PaginateView
    {
        $postId = \is_null($post) ? $request->get('post_id') :
            $post->getId()->toString()
        ;

        $count = $this->comments->findLastCommentsForPostCount($postId);

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $comments = $this->comments
            ->findLastCommentsForPost($postId, $offset);

        return new PaginateView($comments, $pages - $page);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return PaginateView
     * @throws \Exception
     */
    public function paginateCommentsOnCabinet(Request $request, User $user): PaginateView
    {
        $count = $this->comments->findUserCommentsByOrder($user, $request);

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count['count'] / 10);
        $offset = ($page - 1) * 10;

        $comments = $this->comments->findUserCommentsByOrder($user, $request, $offset);

        return new PaginateView($comments['comments'], $pages - $page);
    }
}
