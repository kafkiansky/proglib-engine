<?php

declare(strict_types=1);

namespace App\ReadModel\Test;

use App\Domain\User\Model\Entity\User;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\Entity\TestResultRepository;
use Symfony\Component\HttpFoundation\Request;

class TestPaginator
{
    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var TestResultRepository
     */
    private $testsResults;

    public function __construct(TestRepository $tests, TestResultRepository $testsResults)
    {
        $this->tests = $tests;
        $this->testsResults = $testsResults;
    }

    /**
     * @param Request $request
     * @return PaginateView
     */
    public function paginateOnMain(Request $request): PaginateView
    {
        $count = (int)$this->tests->getTestsCount();

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $tests = $this->tests->findAll($offset, 10);

        return new PaginateView($tests, $pages - $page);
    }

    /**
     * @param Request $request
     * @param User $user
     * @param int $limit
     * @return PaginateView
     */
    public function paginateTestResultsForUser(Request $request, User $user, int $limit): PaginateView
    {
        $count = (int)$this->testsResults->getUserTestResultsCount($user);

        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / $limit);
        $offset = ($page - 1) * $limit;

        $tests = $this->testsResults->getResultForUser($user, $offset, $limit);

        return new PaginateView($tests, $pages - $page);
    }
}
