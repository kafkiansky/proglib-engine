<?php

declare(strict_types=1);

namespace App\ReadModel\Notification;

use App\Domain\User\Model\Entity\User;
use App\Model\Notification\Entity\Notification;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use Symfony\Component\HttpFoundation\Request;

class NotificationFetcher
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param User $actor
     * @param int $limit
     * @param int $status
     * @param int $offset
     * @return array|PartialsNotificationView[]
     */
    public function notificationForUser(User $actor, int $limit, $status, int $offset = 0): array
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('n.id',
                'n.executor_id AS executorId',
                'executor.email AS executor',
                'n.comment_id AS commentId',
                'p.title AS postTitle',
                'n.post_id AS postId',
                'n.executed_at AS executedAt',
                'n.notification_type AS notificationType'
            )
            ->from('notifications', 'n')
            ->leftJoin('n', 'users', 'executor', 'n.executor_id = executor.id')
            ->leftJoin('n', 'posts', 'p', 'n.post_id = p.id');

        if ($status === 'all' || $status === null) {
            $qb
                ->where('n.actor_id = :id')
                ->setParameter('id', $actor->getId());
        } elseif ($status === 'not_read' || $status === Notification::NOT_READ) {
            $qb
                ->where('n.is_read = :read')
                ->andWhere('n.actor_id = :id')
                ->setParameter('read', Notification::NOT_READ)
                ->setParameter('id', $actor->getId());
        }

        $notifications = $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('n.executed_at', 'DESC')
            ->execute()
            ->fetchAll(FetchMode::CUSTOM_OBJECT, PartialsNotificationView::class);

        $qb = $this->connection->createQueryBuilder();
        $marked = $qb
            ->update('notifications', 'n')
            ->set('n.is_read', $qb->expr()->literal(Notification::READ))
            ->where('n.id = :id');

        /** @var PartialsNotificationView $notification */
        foreach ($notifications as $notification) {
            $marked
                ->setParameter('id', $notification->id)
                ->execute();
        }

        return $notifications;
    }

    /**
     * @param User $actor
     * @param int $status
     * @return mixed
     */
    public function count(User $actor, $status)
    {
        $qb = $this->connection->createQueryBuilder()
            ->select('COUNT(*)id')
            ->from('notifications', 'n');
        if ($status === 'all' || $status === null) {
            $qb
                ->where('n.actor_id = :id')
                ->setParameter('id', $actor->getId());
        } elseif ($status === 'not_read' || $status === Notification::NOT_READ) {
            $qb
                ->where('n.actor_id = :id')
                ->andWhere('n.is_read = :read')
                ->setParameter('id', $actor->getId())
                ->setParameter('read', Notification::NOT_READ);
        }

            $count = $qb
            ->execute()
            ->fetch(\PDO::FETCH_OBJ)
            ->id;

        return $count;
    }

    /**
     * @param Request $request
     * @param User $actor
     * @param int $limit
     * @return array
     */
    public function paginateNotifications(Request $request, User $actor, int $limit): array
    {
        $status = $request->request->get('status') ?? null;

        $count = $this->count($actor, $status);
        $page = $request->get('page') ?? 1;
        $pages = (int)ceil($count / 10);
        $offset = ($page - 1) * 10;

        $notifications = $this->notificationForUser($actor, $limit, $status, $offset);

        return [
            'notifications' => $notifications,
            'remainingPages' => $pages - $page
        ];
    }
}
