<?php

declare(strict_types=1);

namespace App\ReadModel\Notification;

use App\Model\Notification\Entity\Id;
use App\Model\Notification\Entity\NotificationType;
use Ramsey\Uuid\Uuid;

class PartialsNotificationView
{
    /**
     * @var Id
     */
    public $id;

    /**
     * @var Uuid
     */
    public $executorId;

    /**
     * @var string
     */
    public $executor;

    /**
     * @var Uuid
     */
    public $commentId;

    /**
     * @var Uuid
     */
    public $postId;

    /**
     * @var string
     */
    public $postTitle;

    /**
     * @var \DateTime
     */
    public $executedAt;

    /**
     * @var NotificationType
     */
    public $notificationType;
}
