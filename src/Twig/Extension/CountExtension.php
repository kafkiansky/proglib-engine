<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CountExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('count', [$this, 'count'])
        ];
    }

    /**
     * @param string|null $elements
     * @return int
     */
    public function count(?string $elements)
    {
        if (null === $elements) {
            return 0;
        }

        if (\is_string($elements)) {
            return \count(\explode(',', $elements));
        }

        if (\is_array($elements)) {
            return \count($elements);
        }

        return 0;
    }
}
