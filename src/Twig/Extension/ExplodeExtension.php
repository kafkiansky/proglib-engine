<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ExplodeExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('explode_string', [$this, 'explodeString'])
        ];
    }

    /**
     * @param string $elements
     * @param string $delimiter
     * @return array
     */
    public function explodeString(string $delimiter, string $elements): array
    {
        return \explode($delimiter, $elements);
    }
}
