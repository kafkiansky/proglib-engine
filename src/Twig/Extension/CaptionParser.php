<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CaptionParser extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('captionParse', [$this, 'captionParser'])
        ];
    }

    /**
     * @param array $matches
     *
     * @return string
     */
    public function parse(array $matches): string
    {
        $img = $matches[2];
        $caption = $matches[3];

        $parsedContent = '<figure>'. $img .'<figcaption>'. $caption .'</figcaption></figure>';

        return $parsedContent;
    }

    /**
     * @param string $content
     *
     * @return string|string[]|null
     */
    public function captionParser(string $content)
    {
        $pattern = '/\[caption\s?(.*?)](<img[^>]+>)(.*?)\[\/caption\]/';

        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);

        if (!$matches) {
            return $content;
        }

        return preg_replace_callback($pattern, [CaptionParser::class, 'parse'], $content);
    }
}
