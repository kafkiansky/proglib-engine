<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class ContainsExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('contains', [$this, 'contains'])
        ];
    }

    /**
     * @param $needed
     * @param $elements
     * @return bool
     */
    public function contains($needed, $elements): bool
    {
        if (null === $elements) {
            return false;
        }

        if (\is_array($elements)) {
            if (in_array($needed, $elements)) {
                return true;
            }
        }

        if (\is_string($elements)) {
            if (in_array($needed, \explode(',', $elements))) {
                return true;
            }
        }

        return false;
    }
}
