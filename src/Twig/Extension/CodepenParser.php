<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CodepenParser extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('codepenParse', [$this, 'codepenParser'])
        ];
    }

    public function parse(array $matches)
    {
        if (count($matches) < 3) {
            return (new ShortCodeParser())->parse($matches);
        }

        $url = $matches[1];
        $urlPath = parse_url($url, PHP_URL_PATH);

        $params = explode('/', $urlPath);

        $parsedContent = '
            <p class="codepen" data-height="265" data-theme-id="0" 
                data-default-tab="result" data-user="' . $params[1] . '" data-slug-hash="' . $params[3] .'" 
                style="height: 265px; box-sizing: border-box; display: flex; align-items: center; 
                justify-content: center; border: 2px solid; margin: 1em 0; padding: 1em;" 
                data-pen-title="Audio Element"
            >
                <span>See the Pen <a href="'. $url .'">
                    Audio Element</a> by <a href=\"https://codepen.io/'. $params[1] .'">@'. $params[1] .'</a>
                    on <a href=\"https://codepen.io">CodePen</a>.
                </span>
             </p>
            <script async src="https://static.codepen.io/assets/embed/ei.js"></script>'
        ;

        return $parsedContent;
    }

    /**
     * @param string $content
     * @return string|string[]|null
     */
    public function codepenParser(string $content)
    {
        $pattern = '/\[embed\](.*?)\[\/embed\]/';

        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);

        if (!$matches) {
            return $content;
        }

        return preg_replace_callback($pattern, [CodepenParser::class, "parse"], $content);
    }
}
