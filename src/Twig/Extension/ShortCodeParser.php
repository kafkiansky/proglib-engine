<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ShortCodeParser extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('parseShortCode', [$this, 'parseShortCode'])
        ];
    }

    public function parse(array $matches)
    {
        $url = $matches[1];
        $queryParameter = parse_url($url, PHP_URL_QUERY);

        if ($queryParameter === null) {
            $urlForEmbed = ltrim(parse_url($url, PHP_URL_PATH), '/');
            $src = "https://www.youtube.com/embed/" . $urlForEmbed . "?feature=oembed";

            $parsedContent = '
                <div class="frame-wrapper">
                    <iframe width="560" height="315"
                        src="'. $src .'"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                    </iframe>
                </div>';

            return $parsedContent;
        }

        $videoType = explode('=', $queryParameter);

        $src = null;

        switch ($videoType[0]) {
            case "list":
                $src = "https://www.youtube.com/embed/videoseries?list=" . $videoType[1];
                break;
            case "v":
                $src = "https://www.youtube.com/embed/" . $videoType[1] . "?feature=oembed";
                break;
        }

        $parsedContent = '
        <iframe width="480" height="270"
            src='. $src .'.
            frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope;
            picture-in-picture" allowfullscreen>
        </iframe>';

        return $parsedContent;
    }

    /**
     * @param string $content
     *
     * @return mixed|string
     */
    public function parseShortCode(string $content)
    {
        $pattern = '/\[embed\](.*?)\[\/embed\]/';

        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);

        if (!$matches) {
            return $content;
        }

        return preg_replace_callback($pattern, [ShortCodeParser::class, "parse"], $content);
    }
}
