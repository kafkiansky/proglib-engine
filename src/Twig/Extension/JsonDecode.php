<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class JsonDecode extends AbstractExtension
{
    public function getFilters() {
        return [
            new TwigFilter('json_decode', 'json_decode'),
        ];
    }
}
