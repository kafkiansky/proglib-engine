<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use App\Domain\User\Model\Entity\User;
use phpcent\Client;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CentrifugoExtension extends AbstractExtension
{
    private $centrifugo;
    private $security;

    public function __construct(Client $centrifugo, Security $security)
    {
        $this->centrifugo = $centrifugo;
        $this->security = $security;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('centrifugo_token', [$this, 'token'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return string
     */
    public function token(): string
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $userId = $user instanceof User ? $user->getId()->toString() : '';

        return $this->centrifugo->generateConnectionToken($userId, time() + 3600 * 12);
    }
}
