<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class WordsFormatter extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('format_word', [$this, 'formatWord'])
        ];
    }

    /**
     * @param int $number
     * @param array $words
     *
     * @return mixed|string
     */
    public function formatWord(int $number, array $words)
    {
        $cases = [2, 0, 1, 1, 1, 2];

        if ($number % 100 > 4 && $number % 100 < 20) {
            $formatterWords = $words[2];
        } elseif ($number % 10 < 5) {
            $formatterWords = $words[$cases[$number % 10]];
        } else {
            $formatterWords = $words[$cases[5]];
        }

        return $formatterWords;
    }
}
