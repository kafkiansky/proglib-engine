<?php

declare(strict_types=1);

namespace App\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use HTMLPurifier as HTML;

class HtmlPurifier extends AbstractExtension
{
    /**
     * @var HTML
     */
    private $html;

    public function __construct(HTML $html)
    {
        $this->html = $html;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('html_purify', [$this, 'htmlPurify'])
        ];
    }

    /**
     * @param string $text
     * @return string
     */
    public function htmlPurify(string $text): string
    {
        return $this->html->purify($text);
    }
}
