<?php

declare(strict_types=1);

namespace App\Twig\Widget\Tasks;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class StatusWidget extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('label_status', [$this, 'status'], ['needs_environment' => true, 'is_safe' => ['html']])
        ];
    }

    public function status(Environment $twig, $label)
    {
        return $twig->render('admin/widget/tasks/status.html.twig', [
            'label' => $label
        ]);
    }
}
