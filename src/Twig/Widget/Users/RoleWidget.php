<?php

declare(strict_types=1);

namespace App\Twig\Widget\Users;

use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RoleWidget extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('user_role', [$this, 'role'], ['needs_environment' => true, 'is_safe' => ['html']]),
        ];
    }

    public function role(Environment $view, string $role)
    {
        return $view->render('admin/widget/users/role.html.twig', [
            'role' => $role
        ]);
    }
}