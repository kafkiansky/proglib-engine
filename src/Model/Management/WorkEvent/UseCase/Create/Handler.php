<?php

declare(strict_types=1);

namespace App\Model\Management\WorkEvent\UseCase\Create;

use App\Domain\Flusher;
use App\Model\Management\WorkEvent\Entity\Id;
use App\Model\Management\WorkEvent\Entity\WorkEvent;
use App\Model\Management\WorkEvent\Entity\WorkEventRepository;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var WorkEventRepository
     */
    private $workEvents;

    public function __construct(Flusher $flusher, WorkEventRepository $workEvents)
    {
        $this->flusher = $flusher;
        $this->workEvents = $workEvents;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $workEvent = new WorkEvent(
            $command->id,
            $command->content,
            $command->task
        );

        $this->workEvents->add($workEvent);
        $this->flusher->flush();
    }
}
