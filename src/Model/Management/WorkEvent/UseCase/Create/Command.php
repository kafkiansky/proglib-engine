<?php

declare(strict_types=1);

namespace App\Model\Management\WorkEvent\UseCase\Create;

use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\WorkEvent\Entity\Id;

class Command
{
    /**
     * @var Id
     */
    public $id;

    /**
     * @var string
     */
    public $content;

    /**
     * @var Task|null
     */
    public $task;

    /**
     * @var Member|null
     */
    private $member;

    public function __construct(Id $id, string $content, ?Task $task, ?Member $member = null)
    {
        $this->id = $id;
        $this->content = $content;
        $this->task = $task;
        $this->member = $member;
    }
}
