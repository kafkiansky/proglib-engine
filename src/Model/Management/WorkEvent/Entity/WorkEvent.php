<?php

declare(strict_types=1);

namespace App\Model\Management\WorkEvent\Entity;

use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Tasks\Task;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="work_events")
 */
class WorkEvent
{
    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="management_notification_id_type")
     */
    private $id;

    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $executor;

    /**
     * @var Task|null
     * @ORM\ManyToOne(targetEntity=Task::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $task;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $content;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $executedAt;

    public function __construct(Id $id, string $content, ?Task $task = null, ?Member $executor = null)
    {
        $this->id = $id;
        $this->executor = $executor;
        $this->content = $content;
        $this->task = $task;
        $this->executedAt = new \DateTime('now');
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return Task|null
     */
    public function getTask(): ?Task
    {
        return $this->task;
    }

    /**
     * @return Member
     */
    public function getExecutor(): Member
    {
        return $this->executor;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return \DateTime
     */
    public function getExecutedAt(): \DateTime
    {
        return $this->executedAt;
    }
}
