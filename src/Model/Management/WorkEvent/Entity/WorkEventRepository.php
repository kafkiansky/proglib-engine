<?php

declare(strict_types=1);

namespace App\Model\Management\WorkEvent\Entity;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

class WorkEventRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->em = $em;
        $this->connection = $connection;
        $this->repository = $em->getRepository(WorkEvent::class);
    }

    /**
     * @param WorkEvent $workEvent
     */
    public function add(WorkEvent $workEvent): void
    {
        $this->em->persist($workEvent);
    }

    /**
     * @return mixed[]
     */
    public function last()
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'we.executor_id',
                'we.task_id',
                'we.content',
                'we.executed_at',
                't.resume',
                'm.name',
                'm.avatar'
            )
            ->from('work_events', 'we')
            ->innerJoin('we', 'tasks', 't', 'we.task_id = t.id')
            ->leftJoin('we', 'members', 'm', 'we.executor_id = m.id')
            ->orderBy('we.executed_at', 'DESC')
            ->setMaxResults(20)
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }
}
