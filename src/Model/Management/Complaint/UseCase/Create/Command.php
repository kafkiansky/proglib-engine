<?php

declare(strict_types=1);

namespace App\Model\Management\Complaint\UseCase\Create;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var User
     */
    public $sender;

    /**
     * @var string
     */
    public $complaintType;

    /**
     * @var string
     */
    public $reasons;

    /**
     * @var string
     */
    public $complaintText;

    /**
     * @var string
     */
    public $entityId;

    public function __construct(
        User $sender,
        string $complaintType,
        string $entityId,
        string $complaintText,
        string $reasons
    ) {
        $this->sender = $sender;
        $this->complaintType = $complaintType;
        $this->complaintText = $complaintText;
        $this->reasons = $reasons;
        $this->entityId = $entityId;
    }
}
