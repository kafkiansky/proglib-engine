<?php

declare(strict_types=1);

namespace App\Model\Management\Complaint\UseCase\Create;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Comment;
use App\Domain\Post\Model\Entity\CommentId;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Repository\CommentRepositoryInterface;
use App\Domain\Post\Repository\PostRepositoryInterface;
use App\Domain\User\Model\Entity\User;
use App\Model\Management\Complaint\Entity\Complaint;
use App\Model\Management\Complaint\Entity\ComplaintRepository;
use App\Model\Management\Complaint\Entity\Id;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Test\Entity\TestRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var ComplaintRepository
     */
    private $complaints;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var PostRepositoryInterface
     */
    private $posts;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var CommentRepositoryInterface
     */
    private $comments;

    public function __construct(
        ComplaintRepository $complaints,
        Flusher $flusher,
        PostRepositoryInterface $posts,
        TestRepository $tests,
        CommentRepositoryInterface $comments
    ) {
        $this->complaints = $complaints;
        $this->flusher = $flusher;
        $this->posts = $posts;
        $this->tests = $tests;
        $this->comments = $comments;
    }

    /**
     * @param Command $command
     * @throws \DomainException
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        $recipient = $this->getRecipient($command->complaintType, $command->entityId);

        if ($command->sender->getId() === $recipient) {
            throw new \DomainException('Unable to send complaint to yourself.');
        }

        $complaint = new Complaint(
            Id::next(),
            $command->reasons,
            $command->complaintType,
            new DateTimeImmutable(),
            $command->sender,
            $recipient,
            $command->entityId
        );

        $this->complaints->add($complaint);
        $this->flusher->flush();
    }
    /**
     * @param $complaintType
     * @param $entityId
     * @return User|Member
     * @throws EntityNotFoundException
     */
    private function getRecipient($complaintType, $entityId)
    {
        if ($complaintType === Complaint::COMPLAINT_POST) {
            /** @var Post $post */
            $post = $this->posts->findOnePostBy(['id' => $entityId]);
            return $post->getUser();
        } elseif ($complaintType === Complaint::COMPLAINT_COMMENT) {
            /** @var Comment $comment */
            $comment = $this->comments->find(new CommentId($entityId));
            return $comment->getUser();
        } else {
            $test = $this->tests->get(new \App\Model\Test\Entity\Id($entityId));
            return $test->getAuthor()->getUser();
        }
    }
}
