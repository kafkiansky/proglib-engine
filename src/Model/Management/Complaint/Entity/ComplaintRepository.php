<?php

declare(strict_types=1);

namespace App\Model\Management\Complaint\Entity;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;

class ComplaintRepository
{
    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Complaint::class);
    }

    /**
     * @param Complaint $complaint
     */
    public function add(Complaint $complaint): void
    {
        $this->em->persist($complaint);
    }
}
