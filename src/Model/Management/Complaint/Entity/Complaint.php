<?php

declare(strict_types=1);

namespace App\Model\Management\Complaint\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user_complaints")
 */
class Complaint
{
    public const COMPLAINT_POST = 'post';
    public const COMPLAINT_COMMENT = 'comment';
    public const COMPLAINT_TEST = 'test';

    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="complaint_id_type")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $reasons;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $complaintText;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $complaintType;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $sender;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $recipient;

    /**
     * @var Uuid
     * @ORM\Column(type="uuid", nullable=false)
     */
    private $entityId;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="date_immutable")
     */
    private $sendedAt;

    public function __construct(
        Id $id,
        string $reasons,
        string $complaintType,
        \DateTimeImmutable $sendedAt,
        User $sender,
        User $recipient,
        string $entityId,
        ?string $complaintText = null
    ) {
        $this->id = $id;
        $this->reasons = $reasons;
        $this->complaintType = $complaintType;
        $this->sendedAt = $sendedAt;
        $this->complaintText = $complaintText;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->entityId = $entityId;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getComplaintType(): string
    {
        return $this->complaintType;
    }

    /**
     * @return string
     */
    public function getComplaintText(): string
    {
        return $this->complaintText;
    }

    /**
     * @return string
     */
    public function getReasons(): string
    {
        return $this->reasons;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getSendedAt(): \DateTimeImmutable
    {
        return $this->sendedAt;
    }
}
