<?php

declare(strict_types=1);

namespace App\Model\Management\Search\Entity;

use Doctrine\ORM\EntityManagerInterface;

class SearchRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Search $search
     */
    public function add(Search $search): void
    {
        $this->em->persist($search);
    }
}
