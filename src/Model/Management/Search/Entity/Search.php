<?php

declare(strict_types=1);

namespace App\Model\Management\Search\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 * @ORM\Table(name="searches", indexes={
 *          @Index(name="user_query_idx", columns={"user_id"})
 *     })
 */
class Search
{
    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="search_id_type")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $query;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $user;

    /**
     * @var array|null
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private $keyWords;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $searchedAt;

    /**
     * @param Id $id
     * @param string $query
     * @param User|null $user
     * @throws \Exception
     */
    public function __construct(Id $id, string $query, ?User $user = null)
    {
        $this->id = $id;
        $this->query = $query;
        $this->user = $user;
        $this->searchedAt = new \DateTimeImmutable();
    }

    /**
     * @return string
     */
    public function getQuery(): string
    {
        return $this->query;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return array|null
     */
    public function getKeyWords(): ?array
    {
        return $this->keyWords;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getSearchedAt(): \DateTimeImmutable
    {
        return $this->searchedAt;
    }
}
