<?php

declare(strict_types=1);

namespace App\Model\Management\Search\Service;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Model\Management\Search\Entity\Id;
use App\Model\Management\Search\Entity\Search;
use App\Model\Management\Search\Entity\SearchRepository;

class SearchService
{
    /**
     * @var SearchRepository
     */
    private $searches;

    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(SearchRepository $searches, Flusher $flusher)
    {
        $this->searches = $searches;
        $this->flusher = $flusher;
    }

    /**
     * @param array $args
     * @param User|null $user
     * @throws \Exception
     */
    public function memorize(array $args, ?User $user = null)
    {
        $search = new Search(Id::next(), $args['q'], $user);

        $this->searches->add($search);
        $this->flusher->flush();
    }
}
