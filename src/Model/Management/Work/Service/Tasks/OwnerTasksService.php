<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Service\Tasks;

use App\Model\Management\Work\Entity\Tasks\Label;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use App\Model\Management\Work\UseCase\Tasks\Filter;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\HttpFoundation\Request;

class OwnerTasksService
{
    public const PER_PAGE = 10;

    /**
     * @var TaskRepository
     */
    private $tasks;

    public function __construct(TaskRepository $tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @param Filter\Command $command
     * @param Request $request
     * @return PaginationInterface
     */
    public function list(Filter\Command $command, Request $request)
    {
        return $this->tasks->all(
            $command,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'resume'),
            $request->query->get('direction', 'desc')
        );
    }

    /**
     * @param Filter\Command $command
     * @param Request $request
     * @param string $status
     * @return PaginationInterface
     */
    public function listByStatus(Filter\Command $command, Request $request, string $status)
    {
        return $this->tasks->allByStatus(
            $command,
            $request->query->getInt('page', 1),
            self::PER_PAGE,
            $request->query->get('sort', 'resume'),
            $request->query->get('direction', 'desc'),
            $status
        );
    }
}
