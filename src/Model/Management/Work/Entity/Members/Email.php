<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Members;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable()
 */
class Email
{
    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, name="email")
     */
    private $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value);

        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new \DomainException('Неправильный электронный адрес');
        }

        $this->value = mb_strtolower($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}
