<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Members;

use App\Domain\User\Model\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use App\Model\Management\Work\UseCase\Members\Filter\Command;
use Knp\Component\Pager\PaginatorInterface;

class MemberRepository
{
    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(EntityManagerInterface $em, Connection $connection, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Member::class);
        $this->connection = $connection;
        $this->paginator = $paginator;
    }

    public function all(Command $command, int $page, int $size, string $sort, string $direction)
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'm.id',
                'm.name',
                'm.date',
                'u.role'
            )
            ->from('members', 'm')
            ->innerJoin('m', 'users', 'u', 'm.user_id = u.id');

        if ($command->name) {
            $qb->andWhere($qb->expr()->like('m.name', ':name'));
            $qb->setParameter(':name', '%' . mb_strtolower($command->name) . '%');
        }

        if ($command->role) {
            $qb->andWhere('u.role = :role');
            $qb->setParameter('role', $command->role);
        }

        if (!\in_array($sort, ['name', 'role', 'date'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $stmt = $qb->execute()->fetchAll();
        $members = [];

        foreach ($stmt as $member) {
            $members[] = $this->repository->findOneBy(['id' => $member['id']]);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($members, $page, $size);
    }

    /**
     * @param Member $member
     */
    public function add(Member $member): void
    {
        $this->em->persist($member);
    }

    /**
     * @param User $user
     *
     * @return Member|null
     */
    public function findByUser(User $user): ?Member
    {
        /** @var Member $member */
        $member = $this->repository->findOneBy(['user' => $user->getId()->toString()]);

        return $member ?: null;
    }

    public function get(string $id)
    {
        return $this->repository->find($id);
    }

    public function findAuthors()
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select('id', 'name')
            ->from('members', 'm')
            ->execute();

        return $stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}