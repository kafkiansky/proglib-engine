<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Members;

use App\Domain\User\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="members")
 */
class Member
{
    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var Email
     *
     * @ORM\Embedded(class="Email", columnPrefix=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Embedded(class="Status", columnPrefix=false)
     */
    private $status;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="App\Domain\User\Model\Entity\User")
     */
    private $user;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $avatar;

    /**
     * @var \DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $date;

    private function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @param Email $email
     * @param string $name
     * @param User $user
     *
     * @return Member
     *
     * @throws \Exception
     */
    public static function create(Email $email, string $name, User $user): Member
    {
        $member = new self();
        $member->user = $user;
        $member->email = $email;
        $member->name = $name;
        $member->date = new \DateTimeImmutable();
        $member->status = Status::wait();
        $member->avatar = $user->getAvatar();

        return $member;
    }

    /**
     * @param Status $status
     */
    public function changeStatus(Status $status): void
    {
        if ($this->getStatus() === $status->getName()) {
            throw new \DomainException('Такой статус уже стоит');
        }

        $this->status = $status->getName();
    }

    public function isEqual(Member $member)
    {
        return $this->getId() === $member->getId();
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getDate()
    {
        return $this->date;
    }
}
