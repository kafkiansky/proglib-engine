<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Members;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable()
 */
class Status
{
    public const WORKING = 'working';
    public const NOT_AVAILABLE = 'not_available';
    public const WAIT = 'wait';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, name="status")
     */
    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::WAIT,
            self::NOT_AVAILABLE,
            self::WORKING
        ]);

        $this->name = $name;
    }

    /**
     * @return Status
     */
    public static function working(): Status
    {
        return new self(self::WORKING);
    }

    /**
     * @return Status
     */
    public static function wait(): Status
    {
        return new self(self::WAIT);
    }

    /**
     * @return Status
     */
    public static function notAvailable(): Status
    {
        return new self(self::NOT_AVAILABLE);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
