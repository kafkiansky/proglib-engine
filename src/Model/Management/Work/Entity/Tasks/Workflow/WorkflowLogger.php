<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Tasks\Workflow;

use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\WorkEvent\Entity\Id;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;
use App\Model\Management\WorkEvent\UseCase\Create;

class WorkflowLogger implements EventSubscriberInterface
{
    /**
     * @var Create\Handler
     */
    private $handler;

    public function __construct(Create\Handler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onWork(Event $event): void
    {
        /** @var Task $task */
        $task = $event->getSubject();

        $command = new Create\Command(
            Id::next(),
            'Задача перешла в статус "В работе"',
            $task
        );

        $this->handler->handle($command);
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onEdits(Event $event): void
    {
        /** @var Task $task */
        /** @var Member $executor */
        $task = $event->getSubject();
        $executor = $task->getAuthor();

        $command = new Create\Command(
            Id::next(),
            sprintf('Пользователь %s отправил задачу на редактирование', $executor->getName()),
            $task,
            $executor
        );

        $this->handler->handle($command);
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onReview(Event $event): void
    {
        /** @var Task $task */
        /** @var Member $executor */
        $task = $event->getSubject();
        $executor = $task->getExecutor();

        $command = new Create\Command(
            Id::next(),
            sprintf('Пользователь %s перевел задачу на ревью', $executor->getName()),
            $task,
            $executor
        );

        $this->handler->handle($command);
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onRejected(Event $event): void
    {
        /** @var Task $task */
        /** @var Member $executor */
        $task = $event->getSubject();
        $executor = $task->getAuthor();

        $command = new Create\Command(
            Id::next(),
            sprintf('Пользователь %s отказал в публикации', $executor->getName()),
            $task,
            $executor
        );

        $this->handler->handle($command);
    }

    /**
     * @param Event $event
     * @throws \Exception
     */
    public function onPublished(Event $event): void
    {
        /** @var Task $task */
        /** @var Member $executor */
        $task = $event->getSubject();
        $executor = $task->getExecutor();

        $command = new Create\Command(
            Id::next(),
            sprintf('Пользователь %s опубликовал задачу', $executor->getName()),
            $task,
            $executor
        );

        $this->handler->handle($command);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.task_publishing.entered.work' => 'onWork',
            'workflow.task_publishing.entered.edits' => 'onEdits',
            'workflow.task_publishing.entered.review' => 'onReview',
            'workflow.task_publishing.entered.rejected' => 'onRejected',
            'workflow.task_publishing.entered.published' => 'onPublished',
        ];
    }
}
