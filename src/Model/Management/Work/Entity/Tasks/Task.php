<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Tasks;

use App\Model\Management\Work\Entity\Members\Member;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tasks")
 */
class Task
{
    public const NEW = 'new';

    /**
     * @var UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id;

    /**
     * @var Member
     *
     * @ORM\ManyToOne(targetEntity=Member::class)
     */
    private $author;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $startDate;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $resume;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $label;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, options={"default": "new"})
     */
    public $currentState;

    /**
     * @var Member
     *
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $executor;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $createdAt;

    /**
     * @var DateTimeImmutable
     *
     * @ORM\Column(type="date_immutable")
     */
    private $updatedAt;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param Member $author
     * @param DateTimeImmutable $startDate
     * @param \DateTime $endDate
     * @param string $label
     * @param string $resume
     * @param string|null $content
     * @param Member|null $executor
     *
     * @return Task
     *
     * @throws Exception
     */
    public static function create(
        Member $author,
        DateTimeImmutable $startDate,
        \DateTime $endDate,
        string $label,
        string $resume,
        ?string $content = null,
        ?Member $executor = null
    ): Task {
        $task = new self();
        $task->author = $author;
        $task->startDate = $startDate;
        $task->endDate = $endDate;
        $task->label = $label;
        $task->resume = $resume;
        $task->content = $content;
        $task->executor = $executor;
        $task->currentState = self::NEW;

        return $task;
    }

    /**
     * @param string $resume
     * @param string $content
     * @param string $label
     * @param \DateTime $endDate
     */
    public function edit(string $resume, string $content, string $label, \DateTime $endDate)
    {
        $this->resume = $resume;
        $this->content = $content;
        $this->label = $label;
        $this->label = $label;
        $this->endDate = $endDate;
    }

    /**
     * @param Member $member
     */
    public function assignExecutor(Member $member)
    {
        $this->executor = $member;
    }

    /**
     * @return bool
     */
    public function hasExecutor(): bool
    {
        return $this->executor instanceof Member;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Member|null
     */
    public function getAuthor(): ?Member
    {
        return $this->author;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getStartDate(): DateTimeImmutable
    {
        return $this->startDate;
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return string
     */
    public function getResume(): string
    {
        return $this->resume;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return Label
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return Member|null
     */
    public function getExecutor(): ?Member
    {
        return $this->executor;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getUpdatedAt(): DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
