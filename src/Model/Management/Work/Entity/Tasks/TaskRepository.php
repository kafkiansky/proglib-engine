<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Tasks;

use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\UseCase\Tasks\Filter\Command;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;

class TaskRepository
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ObjectRepository
     */
    private $repository;
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(EntityManagerInterface $em, Connection $connection, PaginatorInterface $paginator)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Task::class);
        $this->connection = $connection;
        $this->paginator = $paginator;
    }

    /**
     * @param Task $task
     */
    public function add(Task $task)
    {
        $this->em->persist($task);
    }

    /**
     * @param Task $task
     */
    public function remove(Task $task): void
    {
        $this->em->remove($task);
    }

    /**
     * @param Command $command
     * @param int $page
     * @param int $size
     * @param string $sort
     * @param string $direction
     * @return PaginationInterface
     */
    public function all(Command $command, int $page, int $size, string $sort, string $direction): PaginationInterface
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                't.id',
                't.resume',
                't.label',
                't.executor_id AS executor',
                't.end_date AS endDate'
            )
            ->from('tasks', 't');

        if ($command->resume) {
            $qb->andWhere($qb->expr()->like('t.resume', ':resume'));
            $qb->setParameter(':resume', '%' . mb_strtolower($command->resume) . '%');
        }

        if ($command->label) {
            $qb->andWhere('t.label = :label');
            $qb->setParameter('label', $command->label);
        }

        if ($command->executor) {
            $qb->andWhere('t.executor_id = :executor');
            $qb->setParameter('executor', $command->executor);
        }

        if (!\in_array($sort, ['resume', 'label', 'executor', 'endDate'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $stmt = $qb->execute()->fetchAll();
        $tasks = [];

        foreach ($stmt as $task) {
            $tasks[] = $this->repository->findOneBy(['id' => $task['id']]);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($tasks, $page, $size);
    }

    /**
     * @param Command $command
     * @param int $page
     * @param int $size
     * @param string $sort
     * @param string $direction
     *
     * @return PaginationInterface
     */
    public function allByStatus(Command $command, int $page, int $size, string $sort, string $direction, string $status)
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                't.id',
                't.resume',
                't.label',
                't.executor_id AS executor',
                't.end_date AS endDate'
            )
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', $status);

        if ($command->resume) {
            $qb->andWhere($qb->expr()->like('t.resume', ':resume'));
            $qb->setParameter(':resume', '%' . mb_strtolower($command->resume) . '%');
        }

        if ($command->label) {
            $qb->andWhere('t.label = :label');
            $qb->setParameter('label', $command->label);
        }

        if ($command->executor) {
            $qb->andWhere('t.executor_id = :executor');
            $qb->setParameter('executor', $command->executor);
        }

        if (!\in_array($sort, ['resume', 'label', 'executor'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $sort);
        }

        $stmt = $qb->execute()->fetchAll();
        $tasks = [];

        foreach ($stmt as $task) {
            $tasks[] = $this->repository->findOneBy(['id' => $task['id']]);
        }

        $qb->orderBy($sort, $direction === 'desc' ? 'desc' : 'asc');

        return $this->paginator->paginate($tasks, $page, $size);

    }

    /**
     * @return array
     */
    public function countTasks(): array
    {
        $counts = [];

        $all = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks')
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['all'] = $all->id;

        $express = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', Label::TASK_EXPRESS)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['express'] = $express->id;

        $work = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', Label::TASK_IN_WORK)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['work'] = $work->id;

        $closed = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', Label::TASK_CLOSED)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['closed'] = $closed->id;

        $current = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', Label::TASK_CURRENT)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['current'] = $current->id;

        $onReview = $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('tasks', 't')
            ->where('t.label = :label')
            ->setParameter('label', Label::TASK_ON_REVIEW)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);

        $counts['onReview'] = $onReview->id;

        return $counts;
    }

    /**
     * @param Member $member
     * @return object|null
     */
    public function findByExecutor(Member $member)
    {
        return $this->repository->findOneBy(['executor' => $member]);
    }

    /**
     * @param string $id
     * @return Task|null|object
     */
    public function findById(string $id): ?Task
    {
        return $this->repository->find($id);
    }

    /**
     * @param Member $executor
     *
     * @return mixed[]
     */
    public function findForReviewers(Member $executor)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                't.id',
                't.resume',
                'COUNT(*) AS count'
            )
            ->from('tasks', 't')
            ->where('t.executor_id = :executor')
            ->andWhere('t.label = :label')
            ->setParameter('label', Label::TASK_CLOSED)
            ->setParameter('executor', $executor->getId()->toString())
            ->groupBy('t.id')
            ->execute()
            ->fetch(\PDO::FETCH_OBJ);
    }
}

