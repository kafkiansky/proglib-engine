<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Tasks;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable()
 */
class Label
{
    public const TASK_EXPRESS = 'express';
    public const TASK_CURRENT = 'current';
    public const TASK_CLOSED = 'closed';
    public const TASK_IN_WORK = 'working';
    public const TASK_ON_REVIEW = 'on-review';

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, name="label")
     */
    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::TASK_EXPRESS,
            self::TASK_CURRENT,
            self::TASK_IN_WORK,
            self::TASK_CLOSED,
            self::TASK_ON_REVIEW
        ]);

        $this->name = $name;
    }

    /**
     * @return Label
     */
    public static function express(): Label
    {
        return new self(self::TASK_EXPRESS);
    }

    /**
     * @return Label
     */
    public static function work(): Label
    {
        return new self(self::TASK_IN_WORK);
    }

    /**
     * @return Label
     */
    public static function current(): Label
    {
        return new self(self::TASK_CURRENT);
    }

    /**
     * @return Label
     */
    public static function closed(): Label
    {
        return new self(self::TASK_CLOSED);
    }

    /**
     * @return Label
     */
    public static function review(): Label
    {
        return new self(self::TASK_ON_REVIEW);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public static function labels()
    {
        return [
            'Обычная' => self::TASK_CURRENT,
            'Срочная' => self::TASK_EXPRESS
        ];
    }
}
