<?php

declare(strict_types=1);

namespace App\Model\Management\Work\Entity\Tasks\Event;

use App\Model\Management\Work\Entity\Members\Member;
use Symfony\Component\EventDispatcher\Event;

class TaskEdited extends Event
{
    public const NAME = 'task.edited';

    /**
     * @var Member
     */
    public $executor;

    /**
     * @var Member
     */
    public $actor;

    /**
     * @var string
     */
    public $taskId;

    public function __construct(Member $executor, Member $actor, string $taskId)
    {
        $this->executor = $executor;
        $this->actor = $actor;
        $this->taskId = $taskId;
    }
}
