<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Assign;

use App\Domain\Flusher;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\Registry;
use Zend\EventManager\Exception\DomainException;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @var MemberRepository
     */
    private $members;

    /**
     * @var Registry
     */
    private $registry;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Flusher $flusher,
        TaskRepository $tasks,
        MemberRepository $members,
        Registry $registry,
        LoggerInterface $logger
    ) {
        $this->flusher = $flusher;
        $this->tasks = $tasks;
        $this->members = $members;
        $this->registry = $registry;
        $this->logger = $logger;
    }

    /**
     * @param Command $command
     * @throws LogicException
     */
    public function handle(Command $command): void
    {
        /** @var Member $executor */
        $executor = $this->members->findByUser($command->user);

//        if ($command->task->getExecutor() === $executor) {
//            throw new \DomainException('Вы уже взяли эту задачу');
//        }
//
//        if ($command->task->hasExecutor()) {
//            throw new \DomainException('Задачу уже взял другой автор');
//        }

        $workflow = $this->registry->get($command->task);

        try {
            $workflow->apply($command->task, 'publish');

            $command->task->assignExecutor($executor);
            $this->flusher->flush();
        } catch (LogicException $exception) {
            throw new DomainException($exception->getMessage());
        }
    }
}
