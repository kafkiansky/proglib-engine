<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Assign;

use App\Domain\User\Model\Entity\User;
use App\Model\Management\Work\Entity\Tasks\Task;

class Command
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var Task
     */
    public $task;

    public function __construct(User $user, Task $task)
    {
        $this->user = $user;
        $this->task = $task;
    }
}