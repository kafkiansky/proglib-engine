<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Edit;

use App\Domain\Flusher;
use App\Model\Management\Work\Entity\Tasks\Event\TaskEdited;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    public function __construct(Flusher $flusher, TaskRepository $tasks, EventDispatcherInterface $dispatcher)
    {
        $this->flusher = $flusher;
        $this->tasks = $tasks;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command)
    {
        if (!$this->tasks->findById($command->id)) {
            throw new \DomainException('Такой задачи не существует');
        }

        /** @var Task $task */
        $task = $this->tasks->findById($command->id);

        $task->edit(
            $command->resume,
            $command->content,
            $command->label,
            $command->endDate
        );

        $event = new TaskEdited($task->getAuthor(), $task->getExecutor(), $task->getId()->toString());
        $this->dispatcher->dispatch(TaskEdited::NAME, $event);

        $this->flusher->flush();
    }
}