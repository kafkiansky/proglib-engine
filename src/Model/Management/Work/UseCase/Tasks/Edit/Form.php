<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Edit;

use App\Model\Management\Work\Entity\Tasks\Label;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resume', TextType::class, [
                'label' => 'Резюме'
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Описание',
                'attr' => ['rows' => '10'],
            ])
            ->add('label', ChoiceType::class, [
                'choices' => [
                    'Текущая' => Label::TASK_CURRENT,
                    'Срочная' => Label::TASK_EXPRESS
                ],
                'label' => 'Тип задачи',
            ])
            ->add('endDate', DateType::class, [
                'label' => 'Крайний срок',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker', 'autocomplete' => 'off'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class
        ]);
    }
}