<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Edit;

use App\Model\Management\Work\Entity\Tasks\Task;

class Command
{
    public $id;
    public $resume;
    public $content;
    public $label;
    public $startDate;
    public $endDate;
    private $author;

    public function __construct(Task $task)
    {
        $this->id = $task->getId()->toString();
        $this->author = $task->getAuthor();
        $this->resume = $task->getResume();
        $this->content = $task->getContent();
        $this->label = $task->getLabel();
        $this->startDate = $task->getStartDate();
        $this->endDate = $task->getEndDate();
    }
}
