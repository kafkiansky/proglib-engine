<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Delete;

use App\Model\Management\Work\Entity\Tasks\Task;

class Command
{
    /**
     * @var Task
     */
    public $task;

    public function __construct(Task $task)
    {
        $this->task = $task;
    }
}
