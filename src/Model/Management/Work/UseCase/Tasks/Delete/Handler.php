<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Delete;

use App\Domain\Flusher;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TaskRepository
     */
    private $tasks;

    public function __construct(Flusher $flusher, TaskRepository $tasks)
    {
        $this->flusher = $flusher;
        $this->tasks = $tasks;
    }

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        if (!$this->tasks->findById($command->task->getId()->toString())) {
            throw new \DomainException('Такой задачи не существует');
        }

        $this->tasks->remove($command->task);
        $this->flusher->flush();
    }
}
