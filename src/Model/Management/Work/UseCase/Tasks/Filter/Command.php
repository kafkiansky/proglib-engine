<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Filter;

class Command
{
    public $resume;
    public $label;
    public $executor;
}
