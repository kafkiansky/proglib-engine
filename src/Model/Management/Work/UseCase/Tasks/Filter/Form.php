<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Filter;

use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\Label;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Form extends AbstractType
{
    /**
     * @var MemberRepository
     */
    private $members;

    public function __construct(MemberRepository $members)
    {
        $this->members = $members;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('resume', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Резюме',
                    'onchange' => 'this.form.submit()',
                ]
            ])
            ->add('label', ChoiceType::class, [
                'choices' => [
                    'Срочные' => Label::TASK_EXPRESS,
                    'Текущие' => Label::TASK_CURRENT,
                    'В работе' => Label::TASK_IN_WORK,
                    'Завершенные' => Label::TASK_CLOSED
                ],
                'required' => false,
                'placeholder' => 'Статус',
                'attr' => ['onchange' => 'this.form.submit()']
            ])
            ->add('executor', ChoiceType::class, [
                'choices' => array_flip($this->members->findAuthors()),
                'required' => false,
                'placeholder' => 'Исполнитель',
                'attr' => ['onchange' => 'this.form.submit()']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
            'method' => 'GET',
            'csrf_protection' => false,
        ]);
    }
}