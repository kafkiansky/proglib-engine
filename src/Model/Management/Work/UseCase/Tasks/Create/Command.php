<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Create;

use App\Model\Management\Work\Entity\Members\Member;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var Member
     *
     * @Assert\NotBlank()
     */
    public $author;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $label;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="20")
     */
    public $resume;

    /**
     * @var string
     */
    public $content;

    /**
     * @var \DateTimeImmutable
     *
     */
    public $startDate;

    /**
     * @var \DateTime
     */
    public $endDate;

    /**
     * @var string
     */
    public $executor;

    public function __construct(Member $author, \DateTimeImmutable $startDate)
    {
        $this->author = $author;
        $this->startDate = $startDate;
    }
}
