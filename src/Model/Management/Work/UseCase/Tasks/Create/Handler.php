<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Tasks\Create;

use App\Domain\Flusher;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;
use App\Model\Management\Work\Entity\Tasks\Task;
use App\Model\Management\Work\Entity\Tasks\TaskRepository;
use Exception;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @var MemberRepository
     */
    private $members;

    public function __construct(Flusher $flusher, TaskRepository $tasks, MemberRepository $members)
    {
        $this->flusher = $flusher;
        $this->tasks = $tasks;
        $this->members = $members;
    }

    /**
     * @param Command $command
     *
     * @throws Exception
     */
    public function handle(Command $command): void
    {
        /** @var Member $executor */
        $executor = !empty($command->executor) ?
            $this->members->get($command->executor) :
            null;

        $task = Task::create(
            $command->author,
            $command->startDate,
            $command->endDate,
            $command->label,
            $command->resume,
            $command->content,
            $executor
        );

        $this->tasks->add($task);

        $this->flusher->flush();
    }
}