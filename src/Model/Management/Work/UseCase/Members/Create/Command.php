<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Members\Create;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var User
     */
    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
