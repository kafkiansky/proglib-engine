<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Members\Create;

use App\Domain\Flusher;
use App\Model\Management\Work\Entity\Members\Email;
use App\Model\Management\Work\Entity\Members\Member;
use App\Model\Management\Work\Entity\Members\MemberRepository;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var MemberRepository
     */
    private $members;

    public function __construct(Flusher $flusher, MemberRepository $members)
    {
        $this->flusher = $flusher;
        $this->members = $members;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        $isMember = $this->members->findByUser($command->user);

        if ($isMember) {
            throw new \DomainException('Пользователь уже является менеджером');
        }

        $member = Member::create(
            new Email($command->user->getEmail()),
            $command->user->getEmail(),
            $command->user
        );

        $this->members->add($member);

        $this->flusher->flush();
    }
}
