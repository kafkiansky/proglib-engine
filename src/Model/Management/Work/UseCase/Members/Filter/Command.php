<?php

declare(strict_types=1);

namespace App\Model\Management\Work\UseCase\Members\Filter;

class Command
{
    public $name;
    public $role;
}
