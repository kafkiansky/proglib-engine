<?php

declare(strict_types=1);

namespace App\Model\Notification\Request;

use App\Model\RequestDtoInterface;
use Symfony\Component\HttpFoundation\Request;

class ChangeNotificationPreferencesRequest implements RequestDtoInterface
{
    /**
     * @var array
     */
    private $notificationTypes;

    public function __construct(Request $request)
    {
        $this->notificationTypes = $request->get('notifications-type');
    }

    /**
     * @return array|null
     */
    public function notificationTypes(): ?array
    {
        return $this->notificationTypes;
    }
}
