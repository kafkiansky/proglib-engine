<?php

declare(strict_types=1);

namespace App\Model\Notification\Service;

use App\Domain\Flusher;
use App\Model\Notification\Entity\Id;
use App\Model\Notification\Entity\NotificationRepository;
use Doctrine\ORM\EntityNotFoundException;

class NotificationService
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var NotificationRepository
     */
    private $notifications;

    public function __construct(Flusher $flusher, NotificationRepository $notifications)
    {
        $this->flusher = $flusher;
        $this->notifications = $notifications;
    }

    /**
     * @param Id $id
     */
    public function delete(Id $id)
    {
        try {
            $notification = $this->notifications->get($id);
            $this->flusher->remove($notification);
            $this->flusher->flush();
        } catch (EntityNotFoundException $e) {
            throw new \DomainException('Такого уведомления не существует.');
        }
    }
}
