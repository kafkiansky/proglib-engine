<?php

declare(strict_types=1);

namespace App\Model\Notification\Service;

use App\Domain\User\Model\Entity\User;
use App\Model\Notification\Entity\NotificationPreferences;
use App\Model\Notification\Entity\NotificationPreferencesRepository;

class NotificationPreferencesService
{
    /**
     * @var NotificationPreferencesRepository
     */
    private $notificationsPreferences;

    public function __construct(NotificationPreferencesRepository $notificationsPreferences)
    {
        $this->notificationsPreferences = $notificationsPreferences;
    }

    /**
     * @param User $user
     * @param string $notificationType
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isUserDisableNotification(User $user, string $notificationType)
    {
        $noOne = 'no-one';

        /** @var NotificationPreferences $userPreferences */
        $userPreferences = $this->notificationsPreferences->findByUser($user);

        if ($userPreferences === null) {
            return false;
        }

        if (!in_array($notificationType, $userPreferences->getPreferences()) || $userPreferences->getPreferences()[0] === $noOne) {
            return true;
        }

        return false;
    }
}
