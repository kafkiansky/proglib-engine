<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Preferences;

use App\Domain\Flusher;
use App\Model\Notification\Entity\NotificationPreferences;
use App\Model\Notification\Entity\NotificationPreferencesId;
use App\Model\Notification\Entity\NotificationPreferencesRepository;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var NotificationPreferencesRepository
     */
    private $notificationsPreferences;

    public function __construct(Flusher $flusher, NotificationPreferencesRepository $notificationsPreferences)
    {
        $this->flusher = $flusher;
        $this->notificationsPreferences = $notificationsPreferences;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        /** @var NotificationPreferences $existsPreferences */
        if ($existsPreferences = $this->notificationsPreferences->findByUser($command->user)) {
            $existsPreferences->edit($command->notificationTypes ?? ['no-one']);
        } else {
            $notificationPreferences = new NotificationPreferences(
                NotificationPreferencesId::next(),
                $command->user,
                $command->notificationTypes ?? ['no-one']
            );

            $this->notificationsPreferences->add($notificationPreferences);
        }

        $this->flusher->flush();
    }
}
