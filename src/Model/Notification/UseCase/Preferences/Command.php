<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Preferences;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var array|null
     */
    public $notificationTypes;

    /**
     * @var User
     */
    public $user;

    public function __construct(?array $notificationTypes, User $user)
    {
        $this->notificationTypes = $notificationTypes;
        $this->user = $user;
    }
}
