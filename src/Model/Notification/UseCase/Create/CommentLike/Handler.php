<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Create\CommentLike;

use App\Domain\Flusher;
use App\Domain\Post\Event\CommentLiked;
use App\Model\Notification\Entity\Notification;
use App\Model\Notification\Service\NotificationPreferencesService;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var NotificationPreferencesService
     */
    private $service;

    public function __construct(Flusher $flusher, NotificationPreferencesService $service)
    {
        $this->flusher = $flusher;
        $this->service = $service;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        if ($command->actor === $command->executor) {
            throw new \DomainException('Cannot create notify with equal actor and executor');
        }

        if ($this->service->isUserDisableNotification($command->actor, CommentLiked::NAME)) {
            return;
        }

        $notification = Notification::likeForComment(
            $command->id,
            $command->actor,
            $command->executor,
            $command->comment,
            $command->post
        );

        $this->flusher->persist($notification);
    }
}
