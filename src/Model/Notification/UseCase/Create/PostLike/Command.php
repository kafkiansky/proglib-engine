<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Create\PostLike;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use App\Model\Notification\Entity\Id;

class Command
{
    /**
     * @var Id
     */
    public $id;

    /**
     * @var User
     */
    public $actor;

    /**
     * @var User
     */
    public $executor;

    /**
     * @var Post
     */
    public $post;

    public function __construct(User $actor, User $executor, Post $post)
    {
        $this->id = Id::next();
        $this->actor = $actor;
        $this->executor = $executor;
        $this->post = $post;
    }
}
