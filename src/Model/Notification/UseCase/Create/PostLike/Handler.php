<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Create\PostLike;

use App\Domain\Flusher;
use App\Domain\Post\Event\PostLiked;
use App\Model\Notification\Entity\Notification;
use App\Model\Notification\Service\NotificationPreferencesService;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var NotificationPreferencesService
     */
    private $service;

    public function __construct(Flusher $flusher, NotificationPreferencesService $service)
    {
        $this->flusher = $flusher;
        $this->service = $service;
    }

    /**
     * @param Command $command
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function handle(Command $command): void
    {
        if ($command->actor === $command->executor) {
            throw new \DomainException('Cannot create notify with equal actor and executor');
        }

        if ($this->service->isUserDisableNotification($command->actor, PostLiked::NAME)) {
            return;
        }

        $notification = Notification::likeForPost(
            $command->id,
            $command->actor,
            $command->executor,
            $command->post
        );

        $this->flusher->persist($notification);
    }
}
