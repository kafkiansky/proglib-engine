<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Create\CommentComment;

use App\Domain\Post\Model\Entity\Comment;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use App\Model\Notification\Entity\Id;
use DateTimeImmutable;

class Command
{
    /**
     * @var Id
     */
    public $id;

    /**
     * @var DateTimeImmutable
     */
    public $executedAt;

    /**
     * @var User
     */
    public $actor;

    /**
     * @var User
     */
    public $executor;

    /**
     * @var Comment
     */
    public $comment;

    /**
     * @var Post
     */
    public $post;

    public function __construct(User $actor, User $executor, Comment $comment, Post $post)
    {
        $this->id = Id::next();
        $this->actor = $actor;
        $this->executor = $executor;
        $this->comment = $comment;
        $this->post = $post;
        $this->executedAt = new DateTimeImmutable();
    }
}
