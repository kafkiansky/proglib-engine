<?php

declare(strict_types=1);

namespace App\Model\Notification\UseCase\Create\CommentComment;

use App\Domain\Flusher;
use App\Domain\Post\Event\CommentCommented;
use App\Model\Comment\Entity\Comment;
use App\Model\Notification\Entity\Id;
use App\Model\Notification\Entity\Notification;
use App\Model\Notification\Entity\NotificationRepository;
use App\Model\Notification\Service\NotificationPreferencesService;

class Handler
{
    /**
     * @var NotificationRepository
     */
    private $notifications;

    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var NotificationPreferencesService
     */
    private $service;

    public function __construct(
        NotificationRepository $notifications,
        Flusher $flusher,
        NotificationPreferencesService $service
    ) {
        $this->notifications = $notifications;
        $this->flusher = $flusher;
        $this->service = $service;
    }

    /**
     * @param Comment $comment
     * @throws \Exception
     */
    public function handle(Comment $comment)
    {
        if ($comment->getParent()->getUser() === $comment->getUser()) {
            throw new \DomainException('Cannot create notify with equal actor and executor');
        }

        if ($this->service->isUserDisableNotification($comment->getParent()->getUser(), CommentCommented::NAME)) {
            return;
        }

        $notification = Notification::commentForComment(
            Id::next(),
            $comment->getParent()->getUser(),
            $comment->getUser(),
            $comment,
            $comment->getPost()
        );

        $this->notifications->add($notification);
        $this->flusher->flush();
    }
}
