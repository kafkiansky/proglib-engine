<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use App\Domain\User\Model\Entity\User;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use App\Domain\Post\Model\Entity\Post;
use App\Model\Comment\Entity\Comment;

/**
 * @ORM\Entity()
 * @ORM\Table(name="notifications",
 *     indexes={
 *          @ORM\Index(name="idx_actor", columns={"actor_id"}),
 *          @ORM\Index(name="idx_executor", columns={"executor_id"}),
 *     }
 * )
 */
class Notification
{
    public const READ = 1;
    public const NOT_READ = 0;

    /**
     * @var Id
     * @ORM\Column(type="notification_id")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="notifications")
     */
    private $actor;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $executor;

    /**
     * @var Comment
     * @ORM\ManyToOne(targetEntity=Comment::class, inversedBy="notifications")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $comment;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="notifications")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $post;

    /**
     * @var NotificationType
     * @ORM\Column(type="notification_type")
     */
    private $notificationType;

    /**
     * @var DateTimeImmutable
     * @ORM\Column(type="string")
     */
    private $executedAt;

    /**
     * @var int
     * @ORM\Column(type="smallint", nullable=false, options={"default": 0})
     */
    private $isRead;

    private function __construct(Id $id)
    {
        $this->id = $id;
        $this->executedAt = (new \DateTime())->format('Y-m-d H:i:s');
        $this->isRead = 0;
    }

    /**
     * @param Id $id
     * @param User $actor
     * @param User $executor
     * @param Post $post
     *
     * @return Notification
     */
    public static function likeForPost(
        Id $id,
        User $actor,
        User $executor,
        Post $post
    ): ?Notification {

        $notification = new self($id);
        $notification->actor = $actor;
        $notification->executor = $executor;
        $notification->post = $post;
        $notification->notificationType = NotificationType::likeOnPost();

        return $notification;
    }

    /**
     * @param Id $id
     * @param User $actor
     * @param User $executor
     * @param Comment $comment
     * @param Post $post
     *
     * @return Notification
     */
    public static function likeForComment(
        Id $id,
        User $actor,
        User $executor,
        Comment $comment,
        Post $post
    ): Notification {

        $notification = new self($id);
        $notification->actor = $actor;
        $notification->executor = $executor;
        $notification->comment = $comment;
        $notification->post = $post;
        $notification->notificationType = NotificationType::likeOnComment();

        return $notification;
    }

    /**
     * @param Id $id
     * @param User $actor
     * @param User $executor
     * @param Comment $comment
     * @param Post $post
     *
     * @return Notification
     */
    public static function commentForPost(
        Id $id,
        User $actor,
        User $executor,
        Comment $comment,
        Post $post
    ): Notification {

        $notification = new self($id);
        $notification->actor = $actor;
        $notification->executor = $executor;
        $notification->comment = $comment;
        $notification->post = $post;
        $notification->notificationType = NotificationType::commentOnPost();

        return $notification;
    }

    /**
     * @param Id $id
     * @param User $actor
     * @param User $executor
     * @param Comment $comment
     * @param Post $post
     *
     * @return Notification
     */
    public static function commentForComment(
        Id $id,
        User $actor,
        User $executor,
        Comment $comment,
        Post $post
    ): Notification {

        $notification = new self($id);
        $notification->actor = $actor;
        $notification->executor = $executor;
        $notification->comment = $comment;
        $notification->post = $post;
        $notification->notificationType = NotificationType::commentOnComment();

        return $notification;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getActor(): User
    {
        return $this->actor;
    }

    /**
     * @return User
     */
    public function getExecutor(): User
    {
        return $this->executor;
    }

    /**
     * @return Comment
     */
    public function getComment(): Comment
    {
        return $this->comment;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @return NotificationType
     */
    public function getNotificationType(): NotificationType
    {
        return $this->notificationType;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getExecutedAt(): DateTimeImmutable
    {
        return $this->executedAt;
    }

    public function markAsRead()
    {
        $this->isRead = self::READ;
    }

    /**
     * @return bool
     */
    public function isRead(): bool
    {
        return $this->isRead === self::READ;
    }

    /**
     * @return bool
     */
    public function isNotRead(): bool
    {
        return $this->isRead === self::NOT_READ;
    }
}
