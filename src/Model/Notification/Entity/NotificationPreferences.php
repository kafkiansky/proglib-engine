<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 * @ORM\Table(name="notification_preferences", indexes={
 *           @Index(name="user_preferences_idx", columns={"user_id"})
 *     })
 */
class NotificationPreferences
{
    /**
     * @var NotificationPreferencesId
     * @ORM\Id()
     * @ORM\Column(type="notification_preferences_id_type")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=false)
     */
    private $preferences;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $approvedAt;

    public function __construct(NotificationPreferencesId $id, User $user, array $preferences)
    {
        $this->id = $id;
        $this->user = $user;
        $this->preferences = $preferences;
        $this->approvedAt = new \DateTime('now');
    }

    /**
     * @param array $preferences
     * @throws \Exception
     */
    public function edit(array $preferences): void
    {
        $this->preferences = $preferences;
        $this->approvedAt = new \DateTime('now');
    }

    /**
     * @return NotificationPreferencesId
     */
    public function getId(): NotificationPreferencesId
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getPreferences(): array
    {
        return $this->preferences;
    }

    /**
     * @return \DateTime
     */
    public function getApprovedAt(): \DateTime
    {
        return $this->approvedAt;
    }
}
