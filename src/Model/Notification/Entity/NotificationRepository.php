<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

class NotificationRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->em = $em;
        $this->connection = $connection;
        $this->repository = $em->getRepository(Notification::class);
    }

    public function actualNotificationCount(User $actor)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(*) id')
            ->from('notifications', 'n')
            ->where('n.actor_id = :id')
            ->andWhere('n.is_read = :status')
            ->setParameter('id', $actor->getId())
            ->setParameter('status', Notification::NOT_READ)
            ->execute()
            ->fetch(\PDO::FETCH_OBJ)
            ->id;
    }

    public function add(Notification $notification): void
    {
        $this->em->persist($notification);
    }

    /**
     * @param User $actor
     * @return object|null
     */
    public function findByActor(User $actor)
    {
        return $this->repository->findOneBy(['actor' => $actor]);
    }

    /**
     * @param User $actor
     * @return array
     */
    public function findAllUnReadByActor(User $actor): array
    {
        $ids = $this->connection->createQueryBuilder()
            ->select('n.id')
            ->from('notifications', 'n')
            ->where('n.is_read = :read')
            ->andWhere('n.actor_id = :actor')
            ->setParameter('read', Notification::NOT_READ)
            ->setParameter('actor', $actor->getId())
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);

        $notifications = [];

        foreach ($ids as $id) {
            $notifications[] = $this->repository->findOneBy(['id' => $id->id]);
        }

        return $notifications;
    }

    /**
     * @param Id $id
     * @return object|null
     * @throws EntityNotFoundException
     */
    public function get(Id $id)
    {
        $notification = $this->repository->find($id);

        if (!$notification) {
            throw new EntityNotFoundException(sprintf('Notification with id %s not found', $id));
        }

        return $notification;
    }
}
