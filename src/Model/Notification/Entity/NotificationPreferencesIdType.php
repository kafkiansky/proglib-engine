<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class NotificationPreferencesIdType extends GuidType
{
    public const NAME = 'notification_preferences_id_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof NotificationPreferencesId ? $value->getValue() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new NotificationPreferencesId($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
