<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class NotificationTypeType extends StringType
{
    public const NAME = 'notification_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof NotificationType ? $value->getName() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new NotificationType($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
