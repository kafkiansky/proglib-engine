<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use Webmozart\Assert\Assert;

class NotificationType
{
    public const COMMENT_ON_COMMENT = 'comment_on_comment';
    public const COMMENT_ON_POST = 'comment_on_post';
    public const LIKE_ON_COMMENT = 'like_on_comment';
    public const LIKE_ON_POST = 'like_on_post';

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::COMMENT_ON_COMMENT,
            self::COMMENT_ON_POST,
            self::LIKE_ON_COMMENT,
            self::LIKE_ON_POST
        ]);

        $this->name = $name;
    }

    /**
     * @return NotificationType
     */
    public static function commentOnComment(): NotificationType
    {
        return new self(self::COMMENT_ON_COMMENT);
    }

    /**
     * @return NotificationType
     */
    public static function commentOnPost(): NotificationType
    {
        return new self(self::COMMENT_ON_POST);
    }

    /**
     * @return NotificationType
     */
    public static function likeOnComment(): NotificationType
    {
        return new self(self::LIKE_ON_COMMENT);
    }

    /**
     * @return NotificationType
     */
    public static function likeOnPost(): NotificationType
    {
        return new self(self::LIKE_ON_POST);
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }
}
