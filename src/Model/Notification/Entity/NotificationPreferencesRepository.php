<?php

declare(strict_types=1);

namespace App\Model\Notification\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class NotificationPreferencesRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(NotificationPreferences::class);
        $this->connection = $connection;
    }

    /**
     * @param NotificationPreferences $notificationPreferences
     */
    public function add(NotificationPreferences $notificationPreferences): void
    {
        $this->em->persist($notificationPreferences);
    }

    /**
     * @param User $user
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function findByUser(User $user)
    {
        $stmt =  $this->repository->createQueryBuilder('np')
            ->where('np.user = :user')
            ->setParameter('user', $user)
            ->getQuery();

        try {
            return $stmt->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     * @return false|mixed
     */
    public function findAll(User $user)
    {
        return $this->connection->createQueryBuilder()
            ->select('np.preferences')
            ->from('notification_preferences', 'np')
            ->where('np.user_id = :userId')
            ->setParameter('userId', $user->getId())
            ->execute()
            ->fetchColumn();
    }
}
