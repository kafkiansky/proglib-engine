<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Delete;

use App\Domain\Flusher;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(TestRepository $tests, Flusher $flusher)
    {
        $this->tests = $tests;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        /** @var Test $test */
        $test = $this->tests->get(new Id($command->testId));
        $this->tests->remove($test);
        $this->flusher->flush();
    }
}
