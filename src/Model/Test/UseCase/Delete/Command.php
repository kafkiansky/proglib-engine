<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Delete;

class Command
{
    public $testId;

    public function __construct(string $testId)
    {
        $this->testId = $testId;
    }
}
