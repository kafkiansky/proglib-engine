<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Create;

use App\Model\Test\Entity\Slug;
use App\Model\Test\Entity\Test;

class Command
{
    /**
     * @var string
     */
    public $testId;

    /**
     * @var string
     */
    public $title;

    /**
     * @var Slug
     */
    public $slug;

    /**
     * @var string
     */
    public $preview;

    /**
     * @var string
     */
    public $image;

    /**
     * @var array
     */
    public $tags;

    /**
     * @var array
     */
    public $questions;

    /**
     * @var string
     */
    public $results;

    /**
     * @var int
     */
    public $total;

    /**
     * @var string
     */
    public $status;

    public function __construct(array $args)
    {
        $this->testId = $args['id'];
        $this->title = $args['title'];
        $this->preview = $args['preview'];
        $this->questions = json_decode($args['questions']);
        $this->results = $args['results'];
        $this->slug = new Slug($args['title']);
        $this->total = \count(json_decode($args['questions']));
        $this->status = Test::DRAFT;
        $this->image = $args['image'];
        $this->tags = $args['tags'] ?? null;
    }
}
