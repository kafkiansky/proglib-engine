<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Create;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepository;
use App\Model\Test\Entity\Answer;
use App\Model\Test\Entity\Explanation;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\Entity\Variant;
use App\Model\Test\UseCase\Question\Normalize\JsonNormalizer;
use DateTimeImmutable;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var JsonNormalizer
     */
    private $jsonNormalizer;

    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var TagsRepository
     */
    private $tags;

    public function __construct(
        Flusher $flusher,
        TestRepository $tests,
        ValidatorInterface $validator,
        JsonNormalizer $jsonNormalizer,
        QuestionRepository $questions,
        TagsRepository $tags
    ) {
        $this->flusher = $flusher;
        $this->tests = $tests;
        $this->validator = $validator;
        $this->jsonNormalizer = $jsonNormalizer;
        $this->questions = $questions;
        $this->tags = $tags;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        try {
            /** @var Test $existsTest */
            $existsTest = $this->tests->get(new Id($command->testId));

            $existsTest->edit(
                $command->title,
                $command->slug,
                $command->preview,
                $command->results,
                $command->total,
                $command->image,
                $command->status
            );

            $this->createQuestion($command->questions, $existsTest);
            $this->addTags($command->tags, $existsTest);

        } catch (EntityNotFoundException $exception) {
            $test = Test::create(
                new Id($command->testId),
                $command->status,
                $command->title,
                $command->total,
                $command->slug,
                $command->preview,
                $command->results,
                $command->image
            );

            $this->tests->add($test);
            $this->addTags($command->tags, $test);

            $this->createQuestion($command->questions, $test);
        }
    }

    /**
     * @param array $tags
     * @param Test $test
     * @throws NonUniqueResultException
     */
    private function addTags(array $tags, Test $test)
    {
        $savedTags = [];

        /** @var Tag $testTag */
        foreach ($test->getTags() as $testTag) {
            $savedTags[] = $testTag->getId()->toString();
        }

        $arraysAreEqual = ($savedTags === $tags);

        if ($arraysAreEqual) {
            return;
        }

        /** @var Tag $existingTag */
        foreach ($test->getTags() as $existingTag) {
            $test->removeTag($existingTag);
        }

        if (null !== $tags) {
            $existingTags = [];

            foreach ($tags as $tagId) {
                $existingTags[] = $this->tags->findOneBy(['id' => $tagId]);
            }

            /** @var Tag $existingTag */
            foreach ($existingTags as $existingTag) {
                $test->addTag($existingTag);
            }
        }
    }

    /**
     * @param array $questions
     * @param Test $test
     * @throws \Exception
     */
    private function createQuestion(array $questions, Test $test): void
    {
        foreach ($questions as $question) {
            $questionDto = $this->jsonNormalizer->normalize($question, $test->getId()->getValue());
            $this->questions->update(
                new Question(
                    new QuestionId($questionDto->questionId),
                    $questionDto->question,
                    $test,
                    new Answer($questionDto->answers),
                    new Explanation($questionDto->explanation),
                    new DateTimeImmutable(),
                    new Variant($questionDto->variants),
                    $questionDto->multiple,
                    $questionDto->order
                )
            );
        }
    }
}
