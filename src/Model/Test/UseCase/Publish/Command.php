<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Publish;

use App\Model\Test\Entity\Slug;
use Symfony\Component\Validator\Constraints as Assert;
use App\Model\Test\Entity\Test;

class Command
{
    /**
     * @Assert\Uuid(message="no-uuid")
     */
    public $testId;

    /**
     * @Assert\NotBlank(message="no-title")
     * @Assert\Length(min="46", minMessage="too-short-title", max="65", maxMessage="too-long-title")
     */
    public $title;

    /**
     * @var Slug
     */
    public $slug;

    /**
     * @Assert\NotBlank(message="no-preview")
     * @Assert\Length(min="121", minMessage="too-short-preview", max="156", maxMessage="too-long-preview")
     */
    public $preview;

    /**
     * @Assert\NotBlank(message="no-image")
     */
    public $image;

    /**
     * @Assert\NotBlank(message="no-questions")
     * @Assert\Count(min="5", minMessage="too-few-questions")
     */
    public $questions;

    /**
     * @Assert\NotBlank(message="no-results")
     *
     * @var string
     */
    public $results;

    /**
     * @var int
     */
    public $total;

    /**
     * @Assert\NotBlank(message="no-tags")
     * @Assert\Count(min="1", minMessage="too-few-tags")
     */
    public $tags;

    /**
     * @@Assert\EqualTo(Test::PUBLISHED, message="wrong-status")
     */
    public $status;

    public function __construct(array $args)
    {
        $this->testId = $args['id'];
        $this->title = $args['title'];
        $this->preview = $args['preview'];
        $this->questions = json_decode($args['questions']);
        $this->results = $args['results'];
        $this->slug = new Slug($args['title']);
        $this->total = \count(json_decode($args['questions']));
        $this->status = Test::PUBLISHED;
        $this->tags = $args['tags'];
        $this->image = $args['image'];
    }
}
