<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Publish;

use App\Domain\Flusher;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepository;
use App\Model\Test\Entity\Answer;
use App\Model\Test\Entity\Explanation;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\Entity\Variant;
use App\Model\Test\UseCase\Question\Normalize\JsonNormalizer;
use DateTimeImmutable;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var JsonNormalizer
     */
    private $jsonNormalizer;

    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var TagsRepository
     */
    private $tags;

    public function __construct(
        Flusher $flusher,
        JsonNormalizer $jsonNormalizer,
        QuestionRepository $questions,
        TestRepository $tests,
        TagsRepository $tags
    ) {
        $this->flusher = $flusher;
        $this->jsonNormalizer = $jsonNormalizer;
        $this->questions = $questions;
        $this->tests = $tests;
        $this->tags = $tags;
    }

    public function handle(Command $command)
    {
        try {
            /** @var Test $existsTest */
            $existsTest = $this->tests->get(new Id($command->testId));

            $existsTest->edit(
                $command->title,
                $command->slug,
                $command->preview,
                $command->results,
                $command->total,
                $command->image,
                $command->status
            );

            $this->createQuestion($command->questions, $existsTest);
            $this->refreshTags($command->tags, $existsTest);
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }
    }

    /**
     * @param array $tags
     * @param Test $test
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function refreshTags(array $tags, Test $test)
    {
        $savedTags = [];

        /** @var Tag $testTag */
        foreach ($test->getTags() as $testTag) {
            $savedTags[] = $testTag->getId()->toString();
        }

        $arraysAreEqual = ($savedTags === $tags);

        if ($arraysAreEqual) {
            return;
        }

        /** @var Tag $existingTag */
        foreach ($test->getTags() as $existingTag) {
            $test->removeTag($existingTag);
        }

        $existingTags = [];

        foreach ($tags as $tagId) {
            $existingTags[] = $this->tags->findOneBy(['id' => $tagId]);
        }

        /** @var Tag $existingTag */
        foreach ($existingTags as $existingTag) {
            $test->addTag($existingTag);
        }
    }

    /**
     * @param array $questions
     * @param Test $test
     * @throws \Exception
     */
    private function createQuestion(array $questions, Test $test): void
    {
        foreach ($questions as $question) {
            $questionDto = $this->jsonNormalizer->normalize($question, $test->getId()->getValue());
            $this->questions->update(
                new Question(
                    new QuestionId($questionDto->questionId),
                    $questionDto->question,
                    $test,
                    new Answer($questionDto->answers),
                    new Explanation($questionDto->explanation),
                    new DateTimeImmutable(),
                    new Variant($questionDto->variants),
                    $questionDto->multiple,
                    $questionDto->order
                )
            );
        }
    }
}
