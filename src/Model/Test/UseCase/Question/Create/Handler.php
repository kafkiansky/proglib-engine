<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Create;

use App\Domain\Flusher;
use App\Model\Test\Entity\Answer;
use App\Model\Test\Entity\Explanation;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\Entity\Variant;
use DateTimeImmutable;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(QuestionRepository $questions, TestRepository $tests, Flusher $flusher)
    {
        $this->questions = $questions;
        $this->tests = $tests;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        try {
            $existsTest = $this->tests->get(new Id($command->testId));
            $question = $this->createQuestion($command, $existsTest);

            $this->questions->update($question);
        } catch (EntityNotFoundException $e) {
            $test = Test::create(new Id($command->testId), Test::DRAFT);

            $this->tests->add($test);
            $this->flusher->flush();

            $question = $this->createQuestion($command, $test);
            $this->questions->update($question);
        }
    }

    /**
     * @param Command $command
     * @param Test $test
     * @return Question
     * @throws \Exception
     */
    private function createQuestion(Command $command, Test $test): Question
    {
        return new Question(
            new QuestionId($command->id),
            $command->question,
            $test,
            new Answer($command->answer),
            new Explanation($command->explanation),
            new DateTimeImmutable(),
            new Variant($command->variant),
            $command->isMultiple,
            $command->orderQuestion
        );
    }
}
