<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Create;

use App\Model\Test\UseCase\Question\Normalize\QuestionDto;

class Command
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $testId;

    /**
     * @var string
     */
    public $question;

    /**
     * @var string
     */
    public $answer;

    /**
     * @var string
     */
    public $explanation;

    /**
     * @var string
     */
    public $variant;

    /**
     * @var bool
     */
    public $isMultiple;

    /**
     * @var int|null
     */
    public $orderQuestion;

    public function __construct(QuestionDto $question)
    {
        $this->id = $question->questionId;
        $this->testId = $question->testId;
        $this->question = $question->question;
        $this->answer = $question->answers;
        $this->explanation = $question->explanation;
        $this->variant = $question->variants;
        $this->isMultiple = $question->multiple;
        $this->orderQuestion = $question->order;
    }
}
