<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Delete;

use App\Domain\Flusher;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var Flusher
     */
    private $flusher;

    public function __construct(QuestionRepository $questions, Flusher $flusher)
    {
        $this->questions = $questions;
        $this->flusher = $flusher;
    }

    /**
     * @param Command $command
     * @throws EntityNotFoundException
     */
    public function handle(Command $command): void
    {
        /** @var Question $question */
        $question = $this->questions->get(new QuestionId($command->questionId));
        $this->questions->remove($question);
        $this->flusher->flush();
    }
}
