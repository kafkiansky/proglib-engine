<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Delete;

class Command
{
    public $questionId;

    public function __construct(string $questionId)
    {
        $this->questionId = $questionId;
    }
}
