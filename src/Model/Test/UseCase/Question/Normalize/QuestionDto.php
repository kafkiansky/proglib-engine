<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Normalize;

class QuestionDto
{
    public $testId;
    public $questionId;
    public $answers;
    public $question;
    public $variants;
    public $multiple;
    public $order;
    public $explanation;

    public function __construct(
        string $testId,
        string $questionId,
        string $answers,
        string $question,
        string $variants,
        string $explanation,
        $multiple,
        int $order
    ) {
        $this->testId = $testId;
        $this->questionId = $questionId;
        $this->answers = $answers;
        $this->question = $question;
        $this->variants = $variants;
        $this->multiple = $multiple;
        $this->order = $order;
        $this->explanation = $explanation;
    }
}
