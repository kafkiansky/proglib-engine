<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Question\Normalize;

class JsonNormalizer
{
    /**
     * @param $question
     * @param string $testId
     * @return QuestionDto
     */
    public function normalize($question, string $testId): QuestionDto
    {
        return new QuestionDto(
            $testId,
            $question->id,
            json_encode($question->answers, JSON_UNESCAPED_UNICODE),
            json_encode($question->text, JSON_UNESCAPED_UNICODE),
            json_encode($question->variants, JSON_UNESCAPED_UNICODE),
            json_encode($question->description, JSON_UNESCAPED_UNICODE),
            $question->multiple,
            $question->order
        );
    }
}
