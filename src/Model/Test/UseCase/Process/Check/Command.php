<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Check;

class Command
{
    /**
     * @var string
     */
    public $questionId;

    /**
     * @var array
     */
    public $answers;

    /**
     * @param string $questionId
     * @param array $answers
     */
    public function __construct(string $questionId, array $answers)
    {
        $this->questionId = $questionId;
        $this->answers = $answers;
    }
}
