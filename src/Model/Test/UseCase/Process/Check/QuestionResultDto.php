<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Check;

class QuestionResultDto
{
    public $result;
    public $correctAnswers;
    public $explanation;

    public function __construct(int $result, array $correctAnswers, $explanation)
    {
        $this->result = $result;
        $this->correctAnswers = $correctAnswers;
        $this->explanation = $explanation;
    }
}
