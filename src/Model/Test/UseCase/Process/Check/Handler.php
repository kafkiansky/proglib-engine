<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Check;

use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var QuestionRepository
     */
    private $questions;

    public function __construct(QuestionRepository $questions)
    {
        $this->questions = $questions;
    }

    /**
     * @param Command $command
     * @return QuestionResultDto
     * @throws \DomainException
     */
    public function check(Command $command): QuestionResultDto
    {
        try {
            /** @var Question $question */
            $question = $this->questions->get(new QuestionId($command->questionId));
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        $correctAnswers = [];
        $result = null;
        $questionAnswers = json_decode($question->getAnswer()->getValue() ,true);

        foreach ($command->answers as $answer) {
            if (in_array($answer, $questionAnswers)) {
                $correctAnswers[] = $answer;
            }
        }

        $result = (\count($correctAnswers) === \count($questionAnswers)) ? 1 : 0;

        return new QuestionResultDto(
            $result,
            $questionAnswers,
            $question->getExplanation()->getValue()
        );
    }
}
