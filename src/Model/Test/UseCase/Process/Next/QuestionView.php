<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Next;

class QuestionView
{
    public $id;
    public $text;
    public $variants;
    public $multiple;
    public $current;
    public $total;
}
