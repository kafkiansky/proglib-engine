<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Next;

use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var TestRepository
     */
    private $tests;

    public function __construct(QuestionRepository $questions, TestRepository $tests)
    {
        $this->questions = $questions;
        $this->tests = $tests;
    }

    /**
     * @param Command $command
     * @return QuestionView|null
     */
    public function next(Command $command): ?QuestionView
    {
        try {
            /** @var Test $test */
            $test = $this->tests->get(new Id($command->testId));
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        if ($command->currentQuestionId === '') {
            return $this->questions->next($test, 1);
        }

        try {
            /** @var Question $currentQuestion */
            $currentQuestion = $this->questions->get(new QuestionId($command->currentQuestionId));
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        $nextOrder = $currentQuestion->getOrderQuestion() + 1;

        return $this->questions->next($test, $nextOrder);
    }
}
