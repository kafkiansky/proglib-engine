<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Next;

class Command
{
    /**
     * @var string
     */
    public $currentQuestionId;

    /**
     * @var string
     */
    public $testId;

    public function __construct(string $testId, string $currentQuestionId)
    {
        $this->testId = $testId;
        $this->currentQuestionId = $currentQuestionId;
    }
}
