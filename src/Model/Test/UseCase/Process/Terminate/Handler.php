<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Terminate;

use App\Domain\Flusher;
use App\Domain\User\Model\Entity\User;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestProcessRepository;
use App\Model\Test\Entity\TestRepository;
use App\Model\Test\Entity\TestResult;
use App\Model\Test\Entity\TestResultId;
use App\Model\Test\Entity\TestResultRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TestRepository
     */
    private $tests;

    /**
     * @var TestProcessRepository
     */
    private $testProcess;

    /**
     * @var TestResultRepository
     */
    private $testResult;

    public function __construct(
        Flusher $flusher,
        TestRepository $tests,
        TestProcessRepository $testProcess,
        TestResultRepository $testResult
    ) {
        $this->flusher = $flusher;
        $this->tests = $tests;
        $this->testProcess = $testProcess;
        $this->testResult = $testResult;
    }

    public function terminate(Command $command)
    {
        try {
            /** @var Test $test */
            $test = $this->tests->get(new Id($command->testId));
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        $userCorrectResultsCount = (int)$this->testProcess->calculateResultForUser($command->user, $test);
        $userScore = ($userCorrectResultsCount * 100) / $test->getTotal();
        $final = $this->between(json_decode($test->getResults()), $userScore);

        $this->refreshUserProcess($test, $command->user);
        $this->memorizeTestResult($test, $command->user, sprintf('%s/%s', $userCorrectResultsCount, $test->getTotal()));

        $this->flusher->flush();

        return new ResultView(
            $final->text,
            $userCorrectResultsCount,
            $test->getTotal()
        );
    }

    /**
     * @param Test $test
     * @param User $user
     */
    private function refreshUserProcess(Test $test, User $user): void
    {
        $this->testProcess->deleteProcess($test, $user);
    }

    /**
     * @param Test $test
     * @param User $user
     * @param $result
     * @throws \Exception
     */
    private function memorizeTestResult(Test $test, User $user, $result)
    {
        if ($existsTestResult = $this->testResult->get($user, $test)) {
            /** @var TestResult $existsTestResult */
            $existsTestResult = $this->testResult->get($user, $test);
            $existsTestResult->changeResult($result);
        } else {
            $testResult = new TestResult(
                TestResultId::next(),
                $user,
                $test,
                $result
            );

            $this->testResult->add($testResult);
        }
    }

    /**
     * @param array $ranges
     * @param $userScore
     * @return mixed
     */
    private function between(array $ranges, $userScore)
    {
        foreach ($ranges as $result) {
            if ($this->inRange($userScore, $result->range->from, $result->range->to)) {
                return $result;
            }
        }
    }

    /**
     * @param $needed
     * @param $min
     * @param $max
     * @return bool
     */
    private function inRange($needed, $min, $max): bool
    {
        return ($needed >= $min && $needed <= $max);
    }
}
