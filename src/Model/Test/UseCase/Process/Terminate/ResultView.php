<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Terminate;

class ResultView
{
    public $resultText;
    public $correctAnswersNumber;
    public $answersCount;

    public function __construct($resultText, $correctAnswersNumber, $answersCount)
    {
        $this->resultText = $resultText;
        $this->correctAnswersNumber = $correctAnswersNumber;
        $this->answersCount = $answersCount;
    }
}
