<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Terminate;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $testId;

    /**
     * @var User
     */
    public $user;

    public function __construct(string $testId, User $user)
    {
        $this->testId = $testId;
        $this->user = $user;
    }
}
