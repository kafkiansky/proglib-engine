<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Result;

use App\Domain\User\Model\Entity\User;

class Command
{
    /**
     * @var string
     */
    public $testId;

    /**
     * @var string
     */
    public $questionId;

    /**
     * @var User
     */
    public $user;

    /**
     * @var int
     */
    public $result;

    /**
     * @param string $testId
     * @param string $questionId
     * @param User $user
     * @param int $result
     */
    public function __construct(string $testId, string $questionId, User $user, int $result)
    {
        $this->testId = $testId;
        $this->questionId = $questionId;
        $this->user = $user;
        $this->result = $result;
    }
}
