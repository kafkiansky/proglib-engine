<?php

declare(strict_types=1);

namespace App\Model\Test\UseCase\Process\Result;

use App\Domain\Flusher;
use App\Model\Test\Entity\Id;
use App\Model\Test\Entity\Question;
use App\Model\Test\Entity\QuestionId;
use App\Model\Test\Entity\QuestionRepository;
use App\Model\Test\Entity\Test;
use App\Model\Test\Entity\TestProcess;
use App\Model\Test\Entity\TestProcessId;
use App\Model\Test\Entity\TestProcessRepository;
use App\Model\Test\Entity\TestRepository;
use Doctrine\ORM\EntityNotFoundException;

class Handler
{
    /**
     * @var Flusher
     */
    private $flusher;

    /**
     * @var TestProcessRepository
     */
    private $testsProcess;

    /**
     * @var QuestionRepository
     */
    private $questions;

    /**
     * @var TestRepository
     */
    private $tests;

    public function __construct(
        Flusher $flusher,
        TestProcessRepository $testsProcess,
        QuestionRepository $questions,
        TestRepository $tests
    ) {
        $this->flusher = $flusher;
        $this->testsProcess = $testsProcess;
        $this->questions = $questions;
        $this->tests = $tests;
    }

    /**
     * @param Command $command
     * @throws \DomainException
     * @throws \Exception
     */
    public function handle(Command $command): void
    {
        try {
            /** @var Test $test */
            /** @var Question $question */
            $test = $this->tests->get(new Id($command->testId));
            $question = $this->questions->get(new QuestionId($command->questionId));
        } catch (EntityNotFoundException $e) {
            throw new \DomainException($e->getMessage());
        }

        $testProcess = new TestProcess(
            TestProcessId::next(),
            $test,
            $question,
            $command->user,
            new \DateTime('now'),
            $command->result
        );

        $this->testsProcess->add($testProcess);
    }
}
