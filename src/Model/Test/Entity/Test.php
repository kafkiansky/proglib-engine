<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Model\Management\Work\Entity\Members\Member;
use App\Domain\Post\Model\Entity\Tag;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tests")
 */
class Test
{
    public const DRAFT = 'draft';
    public const PUBLISHED = 'published';

    /**
     * @var Id
     * @ORM\Column(type="test_id_type")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $passing;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $views;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $results;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $previewImage;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity=Question::class, mappedBy="test")
     */
    private $questions;

    /**
     * @var Member
     * @ORM\ManyToOne(targetEntity=Member::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $author;

    /**
     * @var Slug
     * @ORM\Column(type="test_slug_type", nullable=true)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $total;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="test", cascade={"persist"})
     * @ORM\JoinTable(name="test_tags")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tags;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="testsLiked")
     * @ORM\JoinTable(name="tests_likes",
     *     joinColumns={@ORM\JoinColumn(name="test_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *     )
     */
    private $userLikes;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="testsBookmarked")
     * @ORM\JoinTable(name="tests_bookmarks",
     *     joinColumns={@ORM\JoinColumn(name="test_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *     )
     */
    private $userBookmarks;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $publishedAt;

    private function __construct(Id $id)
    {
        $this->id = $id;
        $this->questions = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->userBookmarks = new ArrayCollection();
        $this->userLikes = new ArrayCollection();
        $this->publishedAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
        $this->views = 0;
        $this->passing = 0;
    }

    /**
     * @param Id $id
     * @param string $status
     * @param string|null $title
     * @param int|null $total
     * @param Slug|null $slug
     * @param string|null $preview
     * @param null $results
     * @param string|null $image
     * @return Test
     * @throws \Exception
     */
    public static function create(
        Id $id,
        string $status,
        string $title = null,
        int $total = null,
        Slug $slug = null,
        string $preview = null,
        $results = null,
        string $image = null
    ): Test {

        $test = new self($id);
        $test->title = $title;
        $test->total = $total;
        $test->slug = $slug;
        $test->status = $status;
        $test->preview = $preview;
        $test->results = $results;
        $test->previewImage = $image;
        $test->createdAt = new \DateTimeImmutable();
        $test->updatedAt = new \DateTimeImmutable();

        return $test;
    }

    /**
     * @param string|null $title
     * @param Slug|null $slug
     * @param string|null $preview
     * @param array|null $results
     * @param int|null $total
     * @param string|null $image
     * @param string|null $status
     */
    public function edit(
        $title = null,
        Slug $slug = null,
        $preview = null,
        $results = null,
        int $total = null,
        string $image = null,
        string $status = null
    ): void {
        $this->title = $title;
        $this->slug = $slug;
        $this->preview = $preview;
        $this->results = $results;
        $this->total = $total;
        $this->previewImage = $image;
        $this->status = $status;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return string|null
     */
    public function getPreview(): ?string
    {
        return $this->preview;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getResults(): ?string
    {
        return $this->results;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getPublishedAt(): ?\DateTimeImmutable
    {
        return $this->publishedAt;
    }

    /**
     * @return Member
     */
    public function getAuthor(): Member
    {
        return $this->author;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getPreviewImage(): ?string
    {
        return $this->previewImage;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getPassing(): int
    {
        return $this->passing;
    }

    /**
     * @return Slug
     */
    public function getSlug(): Slug
    {
        return $this->slug;
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    public function getUserLikes()
    {
        return $this->userLikes;
    }

    public function getUserBookmarks()
    {
        return $this->userBookmarks;
    }

    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag): void
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;

            $tag->addTest($this);
        }
    }

    public function removeTag(Tag $tag)
    {
        if (!$this->tags->contains($tag)) {
            return;
        }

        $this->tags->removeElement($tag);
        $tag->removeTest($this);
    }

    /**
     * @param User $user
     */
    public function like(User $user): void
    {
        if ($this->userLikes->contains($user)) {
            return;
        }

        $this->userLikes->add($user);
    }

    /**
     * @param User $user
     */
    public function unlike(User $user): void
    {
        if (!$this->userLikes->contains($user)) {
            return;
        }

        $this->userLikes->removeElement($user);
    }

    /**
     * @param User $user
     */
    public function bookmark(User $user): void
    {
        if ($this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->add($user);
    }

    /**
     * @param User $user
     */
    public function unBookmark(User $user): void
    {
        if (!$this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->removeElement($user);
    }

    public function getUser()
    {
        return $this->author->getUser();
    }
}
