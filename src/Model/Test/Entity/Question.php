<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 * @ORM\Table(name="test_questions", indexes={
 *      @Index(name="test_question_idx", columns={"test_id"}),
 *      @Index(name="order_question_idx", columns={"order_question"})
 * })
 */
class Question
{
    /**
     * @var QuestionId
     * @ORM\Id()
     * @ORM\Column(type="question_id_type")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderQuestion;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $question;

    /**
     * @var Answer
     * @ORM\Column(type="answer_type", nullable=false)
     */
    private $answer;

    /**
     * @var Explanation
     * @ORM\Column(type="explanation_type", nullable=true)
     */
    private $explanation;

    /**
     * @var Test
     * @ORM\ManyToOne(targetEntity=Test::class, inversedBy="questions")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $test;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $isMultiple;

    /**
     * @var Variant
     * @ORM\Column(type="variant_type", nullable=false)
     */
    private $variant;

    /**
     * @var DateTimeImmutable
     * @ORM\Column(type="date_immutable")
     */
    private $createdAt;

    /**
     * @param QuestionId $id
     * @param string $question
     * @param Test $test
     * @param Answer $answer
     * @param Explanation $explanation
     * @param DateTimeImmutable $createdAt
     * @param Variant $variant
     * @param bool $isMultiple
     * @param int|null $orderQuestion
     */
    public function __construct(
        QuestionId $id,
        string $question,
        Test $test,
        Answer $answer,
        Explanation $explanation,
        DateTimeImmutable $createdAt,
        Variant $variant,
        $isMultiple,
        ?int $orderQuestion = null
    ) {
        $this->id = $id;
        $this->question = $question;
        $this->test = $test;
        $this->answer = $answer;
        $this->explanation = $explanation;
        $this->createdAt = $createdAt;
        $this->orderQuestion = $orderQuestion;
        $this->variant = $variant;
        $this->isMultiple = $isMultiple === true ? 1 : 0;
    }

    /**
     * @param string $question
     * @param Answer $answer
     * @param Explanation $explanation
     */
    public function edit(string $question, Answer $answer, Explanation $explanation): void
    {
        $this->question = $question;
        $this->answer = $answer;
        $this->explanation = $explanation;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @return Explanation
     */
    public function getExplanation(): Explanation
    {
        return $this->explanation;
    }

    /**
     * @return Answer
     */
    public function getAnswer(): Answer
    {
        return $this->answer;
    }

    /**
     * @return QuestionId
     */
    public function getId(): QuestionId
    {
        return $this->id;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getOrderQuestion(): int
    {
        return $this->orderQuestion;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @return Variant
     */
    public function getVariant(): Variant
    {
        return $this->variant;
    }

    /**
     * @return bool
     */
    public function isMultiple()
    {
        return $this->isMultiple === true;
    }

    public function getIsMultiple()
    {
        return $this->isMultiple;
    }
}
