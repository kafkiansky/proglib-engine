<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

class TestProcessRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(TestProcess::class);
        $this->connection = $connection;
    }

    /**
     * @param TestProcess $testProcess
     */
    public function add(TestProcess $testProcess): void
    {
        $this->em->persist($testProcess);
    }

    /**
     * @param User $user
     * @param Test $test
     * @return mixed
     */
    public function calculateResultForUser(User $user, Test $test)
    {
        return $this->connection->createQueryBuilder()
            ->select('SUM(tp.result)')
            ->from('tests_process', 'tp')
            ->where('tp.user_id = :userId')
            ->andWhere('tp.test_id = :testId')
            ->setParameter('userId', $user->getId())
            ->setParameter('testId', $test->getId()->getValue())
            ->execute()
            ->fetch(\PDO::FETCH_COLUMN);
    }

    /**
     * @param Test $test
     * @param User $user
     */
    public function deleteProcess(Test $test, User $user): void
    {
        $this->repository->createQueryBuilder('tp')
            ->delete()
            ->where('tp.user = :userId')
            ->andWhere('tp.test = :testId')
            ->setParameter('userId', $user->getId())
            ->setParameter('testId', $test->getId()->getValue())
            ->getQuery()->getResult();
    }

    public function findByUserAndTest(Test $test, User $user)
    {
        $result = $this->connection->createQueryBuilder()
            ->select('tp.id')
            ->from('tests_process', 'tp')
            ->where('tp.test_id = :testId')
            ->andWhere('tp.user_id = :userId')
            ->setParameter('testId', $test->getId()->getValue())
            ->setParameter('userId', $user->getId())
            ->execute()
            ->fetch(\PDO::FETCH_OBJ) > 1;
    }

    /**
     * @param TestResultId $id
     * @return object|null
     * @throws EntityNotFoundException
     */
    public function get(TestResultId $id): ?object
    {
        $entity = $this->repository->find($id);

        if (!$entity) {
            throw new EntityNotFoundException(sprintf('TestResult with id %s not found', $id));
        }

        return $entity;
    }
}
