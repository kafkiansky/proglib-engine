<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use phpDocumentor\Reflection\Types\Null_;

class TestResultRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(TestResult::class);
        $this->connection = $connection;
    }

    /**
     * @param TestResult $testResult
     */
    public function add(TestResult $testResult): void
    {
        $this->em->persist($testResult);
    }

    /**
     * @param User $user
     * @param Test $test
     * @return mixed|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get(User $user, Test $test)
    {
        $query =  $this->repository->createQueryBuilder('tr')
            ->where('tr.user = :user')
            ->andWhere('tr.test = :test')
            ->setParameter('user', $user)
            ->setParameter('test', $test)
            ->getQuery();

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param User $user
     * @param int $offset
     * @param int $limit
     * @return mixed[]
     */
    public function getResultForUser(User $user, int $offset, int $limit)
    {
        return $this->connection->createQueryBuilder()
            ->select(
                'tr.id',
                't.title',
                't.preview',
                't.slug',
                't.preview_image AS previewImage',
                'tr.result',
                'tr.passes_number AS passesNumber',
                'GROUP_CONCAT(DISTINCT tg.name) AS testTags',
                'GROUP_CONCAT(DISTINCT tl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT tb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount'
            )
            ->from('test_results', 'tr')
            ->leftJoin('tr', 'tests', 't', 'tr.test_id = t.id')
            ->leftJoin('tr', 'users', 'u', 'tr.user_id = u.id')
            ->leftJoin('tr', 'test_tags', 'tt', 't.id = tt.test_id')
            ->leftJoin('tr', 'tags', 'tg', 'tt.tag_id = tg.id')
            ->leftJoin('tr', 'tests_likes', 'tl', 'tl.test_id = t.id')
            ->leftJoin('tr', 'comments', 'c', 'c.test_id = t.id')
            ->leftJoin('tr', 'tests_bookmarks', 'tb', 'tb.test_id = t.id')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId()->toString())
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy('tr.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * @param User $user
     * @return false|mixed
     */
    public function getUserTestResultsCount(User $user)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(tr.id) count')
            ->from('test_results', 'tr')
            ->leftJoin('tr', 'users', 'u', 'tr.user_id = u.id')
            ->where('u.id = :id')
            ->setParameter('id', $user->getId()->toString())
            ->execute()
            ->fetchColumn();
    }
}
