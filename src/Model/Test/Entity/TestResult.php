<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\User\Model\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="test_results")
 */
class TestResult
{
    /**
     * @var TestResultId
     * @ORM\Id()
     * @ORM\Column(type="test_result_id_type")
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var Test
     * @ORM\ManyToOne(targetEntity=Test::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $test;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    private $passesNumber;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $result;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $passedAt;

    public function __construct(
        TestResultId $id,
        User $user,
        Test $test,
        string $result
    ) {
        $this->id = $id;
        $this->user = $user;
        $this->test = $test;
        $this->passesNumber = 1;
        $this->result = $result;
        $this->passedAt = new \DateTime('now');
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getPassedAt(): \DateTime
    {
        return $this->passedAt;
    }

    /**
     * @return int
     */
    public function getResult(): int
    {
        return $this->result;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @return int
     */
    public function getPassesNumber(): int
    {
        return $this->passesNumber;
    }

    /**
     * @return TestResultId
     */
    public function getId(): TestResultId
    {
        return $this->id;
    }

    public function increasePasses()
    {
        $this->passesNumber += 1;
    }

    public function changeResult(string $result)
    {
        $this->result = $result;
        $this->increasePasses();
    }
}
