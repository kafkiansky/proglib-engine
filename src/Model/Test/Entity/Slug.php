<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use Cocur\Slugify\Slugify;
use Webmozart\Assert\Assert;

class Slug
{
    /**
     * @var string
     */
    private $value;

    public function __construct(string $value)
    {
        Assert::notEmpty($value);
        $this->value = (Slugify::create())->slugify($value);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
