<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class TestResultIdType extends GuidType
{
    public const NAME = 'test_result_id_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof TestResultId ? $value->getValue() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new TestResultId($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
