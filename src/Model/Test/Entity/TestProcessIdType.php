<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

class TestProcessIdType extends GuidType
{
    public const NAME = 'test_process_id_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof TestProcessId ? $value->getValue() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new TestProcessId($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
