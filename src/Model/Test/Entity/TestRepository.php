<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\Post\Model\Entity\Post;
use App\Domain\Post\Model\Entity\Tag;
use App\Domain\User\Model\Entity\User;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;

class TestRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, Connection $connection)
    {
        $this->connection = $connection;
        $this->repository = $em->getRepository(Test::class);
        $this->em = $em;
    }

    public function add(Test $test): void
    {
        $this->em->persist($test);
    }

    /**
     * @param Id $id
     * @return Test
     * @throws EntityNotFoundException
     */
    public function get(Id $id): Test
    {
        /** @var Test $test */
        $test = $this->repository->find($id);

        if (!$test) {
            throw new EntityNotFoundException('Test not found');
        }

        return $test;
    }

    /**
     * @param Test $test
     */
    public function remove(Test $test): void
    {
        $this->em->remove($test);
    }

    public function testsForAuthenticatedUser(User $user, int $offset, int $limit)
    {
        $qb = $this->repository->createQueryBuilder('p');
        $subscriptions = $user->getTags();

        if ($subscriptions->count() === 0) {
            return $this->findAll($offset, $limit);
        }

        $subscriptionsIds = [];
        foreach ($subscriptions as $tag) {
            $subscriptionsIds[] = $tag->getId();
        }

        $posts = $qb
            ->innerJoin('p.tags', 't')
            ->where('p.status = :status')
            ->setParameter('status', Post::PUBLISHED)
            ->andWhere($qb->expr()->in('t.id', $subscriptionsIds))
            ->orderBy('p.createdAt', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();

        return $posts;
    }

    /**
     * @return int
     */
    public function getTestsCount()
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(t.id) count')
            ->from('tests', 't')
            ->where('t.status = :status')
            ->setParameter('status', Test::PUBLISHED)
            ->execute()
            ->fetchColumn();
    }

    /**
     * @param User $user
     * @return int
     */
    public function getSubscriptionsCount(User $user)
    {
        $subscriptions = $user->getTags();

        if ($subscriptions->count() === 0) {
            return $this->getTestsCount();
        }

        $count = 0;

        /** @var Tag $tag */
        foreach ($subscriptions as $tag) {
            $count += $tag->getTest()->count();
        }

        return $count;
    }

    /**
     * @param int $offset
     * @param int $limit
     * @param string|null $tag
     * @return array
     */
    public function findAll(int $offset, int $limit, string $tag = null)
    {
        $qb =  $this->connection->createQueryBuilder()
            ->select(
                't.id',
                't.title',
                't.preview',
                't.preview_image AS previewImage',
                't.slug',
                't.status',
                't.author_id AS user',
                'GROUP_CONCAT(DISTINCT tl.user_id) AS userLikes',
                'GROUP_CONCAT(DISTINCT tg.name) AS testTags',
                'GROUP_CONCAT(DISTINCT tb.user_id) AS userBookmarks',
                'COUNT(DISTINCT c.id) commentsCount',
                't.published_at AS createdAt',
                't.updated_at AS updatedAt'
            )
            ->from('tests', 't')
            ->leftJoin('t', 'test_tags', 'tt', 't.id = tt.test_id')
            ->leftJoin('t', 'users', 'u', 't.author_id = u.id')
            ->leftJoin('t', 'tags', 'tg', 'tt.tag_id = tg.id')
            ->leftJoin('t', 'tests_likes', 'tl', 'tl.test_id = t.id')
            ->leftJoin('t', 'comments', 'c', 'c.test_id = t.id')
            ->leftJoin('t', 'tests_bookmarks', 'tb', 'tb.test_id = t.id')
            ->where('t.status = :status')
            ->setParameter('status', Test::PUBLISHED);

        if ($tag !== null) {
            $qb->andWhere('tg.id = :tagId')
                ->setParameter('tagId', $tag);
        }

       return $qb
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->groupBy('t.id')
            ->execute()
            ->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getCountByTag(string $tag)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(t.id) count')
            ->from('tests', 't')
            ->leftJoin('t', 'test_tags', 'tt', 't.id = tt.test_id')
            ->leftJoin('t', 'tags', 'tg', 'tt.tag_id = tg.id')
            ->where('t.status = :status')
            ->andWhere('tg.id = :id')
            ->setParameter('status', Test::PUBLISHED)
            ->setParameter('id', $tag)
            ->groupBy('t.id')
            ->execute()
            ->fetchColumn();
    }
}
