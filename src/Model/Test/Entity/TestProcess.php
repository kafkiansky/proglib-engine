<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Domain\User\Model\Entity\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 * @ORM\Table(name="tests_process", indexes={
 *      @Index(name="test_process_test_idx", columns={"test_id"}),
 *      @Index(name="test_process_question_idx", columns={"question_id"})
 * })
 */
class TestProcess
{
    /**
     * @var TestProcessId
     * @ORM\Id()
     * @ORM\Column(type="test_process_id_type")
     */
    private $id;

    /**
     * @var Test
     * @ORM\ManyToOne(targetEntity=Test::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $test;

    /**
     * @var Question
     * @ORM\ManyToOne(targetEntity=Question::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $question;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var int|null
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $result;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $passedAt;

    public function __construct(
        TestProcessId $id,
        Test $test,
        Question $question,
        User $user,
        DateTime $passedAt,
        ?int $result = null
    ) {
        $this->id = $id;
        $this->test = $test;
        $this->question = $question;
        $this->passedAt = $passedAt;
        $this->result = $result;
        $this->user = $user;
    }

    /**
     * @return TestProcessId
     */
    public function getId(): TestProcessId
    {
        return $this->id;
    }

    /**
     * @return Question
     */
    public function getQuestion(): Question
    {
        return $this->question;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @return int|null
     */
    public function getResult(): ?int
    {
        return $this->result;
    }

    /**
     * @return DateTime
     */
    public function getPassedAt(): DateTime
    {
        return $this->passedAt;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
