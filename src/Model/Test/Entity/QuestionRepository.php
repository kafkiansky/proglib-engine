<?php

declare(strict_types=1);

namespace App\Model\Test\Entity;

use App\Application\Manager\AbstractManager;
use App\Model\Test\UseCase\Process\Next\QuestionView;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Log\LoggerInterface;

class QuestionRepository extends AbstractManager
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ObjectRepository
     */
    private $repository;

    public function __construct(Connection $connection, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->connection = $connection;
        $this->em = $em;
        $this->repository = $em->getRepository(Question::class);
        parent::__construct($em, $logger);
    }

    public function add(Question $question): void
    {
        $this->em->persist($question);
    }

    public function update(Question $question)
    {
        $now = date('Y-m-d H:i:s');

        $params[] = [
            'id' => $question->getId(),
            'test_id' => $question->getTest()->getId(),
            'order_question' => $question->getOrderQuestion(),
            'question' => $question->getQuestion(),
            'answer' => $question->getAnswer()->getValue(),
            'explanation' => $question->getExplanation()->getValue(),
            'variant' => $question->getVariant()->getValue(),
            'is_multiple' => $question->getIsMultiple(),
            'created_at' => $now,
        ];

        try {
            $this->upsertToTable('test_questions', $params, [
                'order_question',
                'question',
                'answer',
                'explanation',
                'variant',
                'is_multiple'
            ]);
        } catch (DBALException $e) {
            $this->getLogger()->error($e->getMessage());
        }
    }

    /**
     * @param QuestionId $questionId
     * @return Question|null
     * @throws EntityNotFoundException
     */
    public function get(QuestionId $questionId): ?Question
    {
        /** @var Question $question */
        $question = $this->repository->find($questionId);

        if (!$question) {
            throw new EntityNotFoundException(sprintf('Question with id %s not found', $questionId));
        }

        return $question;
    }

    /**
     * @param Test $test
     * @param int $order
     * @return QuestionView|null
     */
    public function next(Test $test, int $order): ?QuestionView
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'q.id',
                'q.test_id',
                'q.order_question AS current',
                'q.question AS text',
                'q.is_multiple AS multiple',
                'q.variant AS variants',
                't.total'
            )
            ->from('test_questions', 'q')
            ->innerJoin('q', 'tests', 't', 'q.test_id = t.id')
            ->where('q.order_question = :order')
            ->andWhere('q.test_id = :id')
            ->setParameter('order', $order)
            ->setParameter('id', $test->getId()->getValue())
            ->execute();

        $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, QuestionView::class);

        return $stmt->fetch() ?: null;
    }

    public function first()
    {

    }

    public function remove(Question $question): void
    {
        $this->em->remove($question);
    }
}
