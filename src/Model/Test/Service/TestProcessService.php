<?php

declare(strict_types=1);

namespace App\Model\Test\Service;

use App\Model\Test\Entity\TestProcessRepository;

class TestProcessService
{
    /**
     * @var TestProcessRepository
     */
    private $repository;

    public function __construct(TestProcessRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getNext()
    {

    }
}