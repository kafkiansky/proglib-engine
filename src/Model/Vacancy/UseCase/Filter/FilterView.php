<?php

declare(strict_types=1);

namespace App\Model\Vacancy\UseCase\Filter;

class FilterView
{
    public $id;
    public $title;
    public $workPlace;
    public $workType;
    public $city;
    public $companyName;
    public $salaryTo;
    public $salaryFrom;
    public $slug;
    public $count;
    public $tags;
}
