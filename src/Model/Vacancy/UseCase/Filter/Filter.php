<?php

declare(strict_types=1);

namespace App\Model\Vacancy\UseCase\Filter;

class Filter
{
    public $workType;
    public $workPlace;
    public $city;
    public $tags;
    public $salaryFrom;
    public $page;

    public function __construct(array $request, array $query)
    {
        $this->workType = $request['workType'] ?? $query['workType'] ?? null;
        $this->workPlace = $request['workPlace'] ?? $query['workPlace'] ?? null;
        $this->city = $request['city'] ?? $query['city'] ?? null;
        $this->salaryFrom = $request['salaryFrom'] ?? $query['salaryFrom'] ?? null;
        $this->tags = $request['tag'] ?? $query['tag'] ?? null;
        $this->page = $request['page'] ?? null;
    }
}
