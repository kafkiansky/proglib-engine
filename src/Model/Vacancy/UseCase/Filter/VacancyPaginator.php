<?php

declare(strict_types=1);

namespace App\Model\Vacancy\UseCase\Filter;

use App\Domain\Post\Repository\TagsRepository;
use App\Model\Vacancy\Entity\Vacancy;
use App\Model\Vacancy\Entity\WorkPlace;
use App\Model\Vacancy\Entity\WorkType;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class VacancyPaginator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;
    /**
     * @var TagsRepository
     */
    private $tags;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em, Connection $connection, TagsRepository $tags)
    {
        $this->em = $em;
        $this->connection = $connection;
        $this->tags = $tags;
        $this->repository = $em->getRepository(Vacancy::class);
    }

    /**
     * @param Filter $filter
     * @param int $limit
     * @return array
     */
    public function paginate(Filter $filter, int $limit): array
    {
        $vacanciesCount = $this->count($filter);
        $metaData = $this->calculate($filter, $vacanciesCount, $limit);

        $vacancies = $this->all($filter, $metaData['offset'], $limit);

        return [
            'vacancies' => $vacancies,
            'remainingPages' => $metaData['remainingPages']
        ];
    }

    /**
     * @param Filter $filter
     * @return false|mixed
     */
    private function count(Filter $filter)
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'COUNT(*)count'
            )
            ->from('vacancy_tags', 'vt')
            ->innerJoin('vt', 'vacancies', 'v', 'vt.vacancy_id = v.id')
            ->innerJoin('v', 'companies', 'c', 'v.company_id = c.id')
            ->innerJoin('vt', 'tags', 't', 'vt.tag_id = t.id');

        if ($filter->tags !== '' && $filter->tags !== null) {
            $names = [];
            $explodedTagsName = explode(',', $filter->tags);

            foreach ($explodedTagsName as $name) {
                $names[] = $name;
            }

            $qb->andWhere('t.name IN (:names)');
            $qb->setParameter('names', $names, Connection::PARAM_STR_ARRAY);
        }

        if ($filter->workPlace === WorkPlace::OFFICE) {
            $qb->andWhere('v.work_place = :workPlace');
            $qb->setParameter('workPlace', WorkPlace::OFFICE);
        }

        if ($filter->workPlace === WorkPlace::REMOTE) {
            $qb->andWhere('v.work_place = :workPlace');
            $qb->setParameter('workPlace', WorkPlace::REMOTE);
        }

        if ($filter->workType === WorkType::FULLTIME) {
            $qb->andWhere('v.work_type = :workType');
            $qb->setParameter('workType', WorkType::FULLTIME);
        }

        if ($filter->workType === WorkType::PARTTIME) {
            $qb->andWhere('v.work_type = :workType');
            $qb->setParameter('workType', WorkType::PARTTIME);
        }

        if ($filter->salaryFrom !== '' && $filter->salaryFrom !== null) {
            $qb->andWhere('v.salary_from >= :salaryFrom');
            $qb->setParameter('salaryFrom', (int)$filter->salaryFrom);
        }

        if ($filter->city) {
            $qb->andWhere('v.city = :city');
            $qb->setParameter('city', $filter->city);
        }

        $qb->orderBy('v.created_at', 'DESC');

        $stmt = $qb->execute();

        return $stmt->fetchColumn();
    }

    /**
     * @param Filter $filter
     * @param int $offset
     * @param int $limit
     * @return mixed[]
     */
    private function all(Filter $filter, int $offset, int $limit)
    {
        $qb = $this->connection->createQueryBuilder()
            ->select(
                'v.id'
            )
            ->from('vacancy_tags', 'vt')
            ->innerJoin('v', 'companies', 'c', 'v.company_id = c.id')
            ->innerJoin('vt', 'vacancies', 'v', 'vt.vacancy_id = v.id')
            ->innerJoin('vt', 'tags', 't', 'vt.tag_id = t.id');

        if ($filter->tags !== '' && $filter->tags !== null) {
            $names = [];
            $explodedTagsName = explode(',', $filter->tags);

            foreach ($explodedTagsName as $name) {
                $names[] = $name;
            }

            $qb->andWhere('t.name IN (:names)');
            $qb->setParameter('names', $names, Connection::PARAM_STR_ARRAY);
        }

        if ($filter->workPlace === WorkPlace::OFFICE) {
            $qb->andWhere('v.work_place = :workPlace');
            $qb->setParameter('workPlace', WorkPlace::OFFICE);
        }

        if ($filter->workPlace === WorkPlace::REMOTE) {
            $qb->andWhere('v.work_place = :workPlace');
            $qb->setParameter('workPlace', WorkPlace::REMOTE);
        }

        if ($filter->workType === WorkType::FULLTIME) {
            $qb->andWhere('v.work_type = :workType');
            $qb->setParameter('workType', WorkType::FULLTIME);
        }

        if ($filter->workType === WorkType::PARTTIME) {
            $qb->andWhere('v.work_type = :workType');
            $qb->setParameter('workType', WorkType::PARTTIME);
        }

        if ($filter->salaryFrom !== '' && $filter->salaryFrom !== null) {
            $qb->andWhere('v.salary_from >= :salaryFrom');
            $qb->setParameter('salaryFrom', (int)$filter->salaryFrom);
        }

        if ($filter->city) {
            $qb->andWhere('v.city = :city');
            $qb->setParameter('city', $filter->city);
        }

        $qb->setFirstResult($offset)->setMaxResults($limit);

        $qb->orderBy('v.created_at', 'DESC');

        $stmt = $qb->execute();
        $ids = array_unique($stmt->fetchAll(\PDO::FETCH_COLUMN));

        $vacancies = [];

        foreach ($ids as $id) {
            $vacancies[] = $this->repository->find($id);
        }

        return $vacancies;
    }

    /**
     * @param Filter $filter
     * @param $count
     * @param int $limit
     * @return array
     */
    private function calculate(Filter $filter, $count, int $limit): array
    {
        $remainingPages = null;

        $pagesNumber = (int)ceil($count / $limit);

        if ($pagesNumber <= 1) {
            $remainingPages = 0;
        } else {
            $remainingPages = $pagesNumber - $filter->page;
        }

        $offset = ($result = ($filter->page - 1) * $limit) < 0 ?
            0 :
            $result;

        return [
            'offset' => $offset,
            'remainingPages' => $remainingPages
        ];
    }
}
