<?php

declare(strict_types=1);

namespace App\Model\Vacancy\UseCase\Create;

use App\Domain\Post\Model\Entity\Tag;
use App\Domain\Post\Repository\TagsRepository;
use App\Model\Company\Entity\Company;
use App\Model\Company\Entity\CompanyRepository;
use App\Model\Company\Entity\Id as CompanyId;
use App\Model\Vacancy\Entity\Id as VacancyId;
use App\Model\Vacancy\Entity\Slug;
use App\Model\Vacancy\Entity\Vacancy;
use App\Model\Vacancy\Entity\VacancyRepository;
use App\Model\Vacancy\Entity\WorkPlace;
use App\Model\Vacancy\Entity\WorkType;
use Doctrine\ORM\NonUniqueResultException;

class Handler
{
    /**
     * @var VacancyRepository
     */
    private $vacancies;

    /**
     * @var CompanyRepository
     */
    private $companies;

    /**
     * @var TagsRepository
     */
    private $tags;

    public function __construct(
        VacancyRepository $vacancies,
        CompanyRepository $companies,
        TagsRepository $tags
    ) {
        $this->vacancies = $vacancies;
        $this->companies = $companies;
        $this->tags = $tags;
    }

    /**
     * @param Command $command
     * @return string
     * @throws NonUniqueResultException
     */
    public function handle(Command $command)
    {
        $company = new Company(
            CompanyId::next(),
            $command->companyTitle,
            $command->companySite,
            $command->companyImage,
            $command->companyDescription
        );

        $vacancy = new Vacancy(
            VacancyId::next(),
            $command->vacancyTitle,
            new WorkType($command->vacancyWorkType),
            new WorkPlace($command->vacancyWorkPlace),
            $company,
            $this->cleanUpArray($command->requirements),
            $this->cleanUpArray($command->tasks),
            $this->cleanUpArray($command->conditions),
            $command->candidateEmail,
            new Slug($command->vacancyTitle . '-' . $command->companyTitle),
            $command->vacancyCity,
            $command->vacancyImage,
            (int)$command->salaryFrom,
            (int)$command->salaryTo
        );

        $this->commitTags($command->tags, $vacancy);

        $this->companies->add($company);
        $this->vacancies->add($vacancy);

        return $vacancy->getSlug()->getValue();
    }

    /**
     * @param array $tags
     * @param Vacancy $vacancy
     * @throws NonUniqueResultException
     */
    private function commitTags(array $tags, Vacancy $vacancy): void
    {
        $existsTags = [];

        foreach ($tags as $tagId) {
            $existsTags[] = $this->tags->findOneBy(['id' => $tagId]);
        }

        /** @var Tag $existsTag */
        foreach ($existsTags as $existsTag) {
            $vacancy->addTag($existsTag);
        }
    }

    /**
     * @param array $props
     * @return array
     */
    private function cleanUpArray(array $props): array
    {
        $nonEmptyValues = [];

        foreach ($props as $prop) {
            if (null === $prop || '' === $prop) {
                continue;
            }

            $nonEmptyValues[] = $prop;
        }

        return $nonEmptyValues;
    }
}
