<?php

declare(strict_types=1);

namespace App\Model\Vacancy\UseCase\Create;

use App\Application\Validation\Constraint\NotEmptyArrayValues;
use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @var string
     * @Assert\NotBlank(message="no-title")
     * @Assert\Length(min="10", min="too-short-title", max="100", maxMessage="too-long-title")
     */
    public $vacancyTitle;

    /**
     * @var int
     * @Assert\NotBlank(message="no-salary-from")
     * @Assert\GreaterThan(value="5000", message="too-little-salary-from")
     */
    public $salaryFrom;

    /**
     * @var int
     * @Assert\NotBlank(message="no-salary-to")
     * @Assert\GreaterThan(value="5000", message="too-little-salary-to")
     */
    public $salaryTo;

    /**
     * @var string
     * @Assert\NotBlank(message="no-work-type")
     */
    public $vacancyWorkType;

    /**
     * @var string
     * @Assert\NotBlank(message="no-work-place")
     */
    public $vacancyWorkPlace;

    /**
     * @var string
     */
    public $vacancyCity;

    /**
     * @var array
     * @Assert\NotBlank(message="no-requirements")
     * @NotEmptyArrayValues(message="requirements-values-are-empty")
     * @Assert\Count(min="1", minMessage="too-few-requirements", max="20", maxMessage="too-many-requirements")
     */
    public $requirements;

    /**
     * @var array
     * @Assert\NotBlank(message="no-tasks")
     * @NotEmptyArrayValues(message="tasks-values-are-empty")
     * @Assert\Count(min="1", minMessage="too-few-tasks", max="20", maxMessage="too-many-tasks")
     */
    public $tasks;

    /**
     * @var array
     * @Assert\NotBlank(message="no-conditions")
     * @NotEmptyArrayValues(message="conditions-values-are-empty")
     * @Assert\Count(min="1", minMessage="too-few-conditions", max="20", maxMessage="too-many-conditions")
     */
    public $conditions;

    /**
     * @var string
     * @Assert\NotBlank(message="no-candidate-email")
     */
    public $candidateEmail;

    /**
     * @var string
     */
    public $vacancyImage;

    /**
     * @var array
     * @Assert\NotBlank(message="no-tags")
     * @Assert\Count(min="1", minMessage="too-few-tags", max="5", maxMessage="too-many-tags")
     */
    public $tags;

    /**
     * @var string
     * @Assert\NotBlank(message="no-title")
     * @Assert\Length(min="10", min="too-short-company-title", max="150", maxMessage="too-long-company-title")
     */
    public $companyTitle;

    /**
     * @var string
     * @Assert\NotBlank(message="no-company-site")
     */
    public $companySite;

    /**
     * @var string
     * @Assert\NotBlank(message="no-company-description")
     * @Assert\Length(min="50", minMessage="too-short-description", max="400", maxMessage="too-long-description")
     */
    public $companyDescription;

    /**
     * @var string
     */
    public $companyImage;

    /**
     * @var string
     * @Assert\NotBlank(message="no-notifications-email")
     */
    public $notificationEmail;

    public function __construct(array $args)
    {
        $this->vacancyTitle = $args['vacancyTitle'] ?? null;
        $this->salaryFrom = $args['salaryFrom'] ?? null;
        $this->salaryTo = $args['salaryTo'] ?? null;
        $this->vacancyWorkType = $args['vacancyWorkType'] ?? null;
        $this->vacancyWorkPlace = $args['vacancyWorkPlace'] ?? null;
        $this->vacancyCity = $args['vacancyCity'] ?? null;
        $this->tags = $args['tags'] ?? null;
        $this->requirements = $args['requirements'] ?? null;
        $this->tasks = $args['tasks'] ?? null;
        $this->conditions = $args['conditions'] ?? null;
        $this->vacancyImage = $args['vacancyImage'] ?? null;
        $this->companyImage = $args['companyImage'] ?? null;
        $this->companyTitle = $args['companyTitle'] ?? null;
        $this->companySite = $args['companySite'] ?? null;
        $this->companyDescription = $args['companyDescription'] ?? null;
        $this->candidateEmail = $args['candidateEmail'] ?? null;
        $this->notificationEmail = $args['notificationEmail'] ?? null;
    }
}
