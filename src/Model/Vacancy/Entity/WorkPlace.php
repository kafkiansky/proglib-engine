<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Webmozart\Assert\Assert;

class WorkPlace
{
    public const REMOTE = 'remote';
    public const OFFICE = 'office';

    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::REMOTE,
            self::OFFICE
        ]);

        $this->name = $name;
    }

    /**
     * @return WorkPlace
     */
    public static function remote(): WorkPlace
    {
        return new self(self::REMOTE);
    }

    /**
     * @return WorkPlace
     */
    public static function office(): WorkPlace
    {
        return new self(self::OFFICE);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
