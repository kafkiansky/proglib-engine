<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Webmozart\Assert\Assert;

class WorkType
{
    public const FULLTIME = 'fulltime';
    public const PARTTIME = 'parttime';

    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        Assert::oneOf($name, [
            self::FULLTIME,
            self::PARTTIME
        ]);

        $this->name = $name;
    }

    /**
     * @return WorkType
     */
    public static function fulltime()
    {
        return new self(self::FULLTIME);
    }

    /**
     * @return WorkType
     */
    public static function parttime()
    {
        return new self(self::PARTTIME);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
