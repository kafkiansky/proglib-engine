<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class WorkTypeType extends StringType
{
    public const NAME = 'work_type_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof WorkType ? $value->getName() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new WorkType($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
