<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Cocur\Slugify\Slugify;
use Webmozart\Assert\Assert;

class Slug
{
    /**
     * @var string
     */
    private $value;

    public function __construct(string $slugable)
    {
        Assert::notEmpty($slugable);

        $this->value = (Slugify::create())->slugify($slugable);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->value;
    }
}
