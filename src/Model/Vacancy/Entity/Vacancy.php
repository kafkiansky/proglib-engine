<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use App\Model\Company\Entity\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use App\Domain\Post\Model\Entity\Tag;

/**
 * @ORM\Entity()
 * @ORM\Table(name="vacancies", indexes={
 *         @Index(name="company_vacancy_idx", columns={"company_id"}),
 *         @Index(name="company_city_idx", columns={"city"}),
 *         @Index(name="company_work_type_idx", columns={"work_type"}),
 *         @Index(name="company_work_place_idx", columns={"work_place"}),
 *     })
 */
class Vacancy
{
    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="vacancy_id_type")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $title;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $salaryFrom;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $salaryTo;

    /**
     * @var WorkType
     * @ORM\Column(type="work_type_type", nullable=false)
     */
    private $workType;

    /**
     * @var WorkPlace
     * @ORM\Column(type="work_place_type", nullable=false)
     */
    private $workPlace;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $city;

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=false)
     */
    private $requirements;

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=false)
     */
    private $tasks;

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=false)
     */
    private $conditions;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $candidateEmail;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var Company
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="vacancies")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $company;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity=Tag::class, inversedBy="vacancy")
     * @ORM\JoinTable(name="vacancy_tags")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $tags;

    /**
     * @var Slug
     * @ORM\Column(type="vacancy_slug_type")
     */
    private $slug;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $views;

    /**
     * @param Id $id
     * @param string $title
     * @param WorkType $workType
     * @param WorkPlace $workPlace
     * @param Company $company
     * @param array $requirements
     * @param array $tasks
     * @param array $conditions
     * @param string $candidateEmail
     * @param Slug $slug
     * @param string|null $city
     * @param string|null $image
     * @param int|null $salaryFrom
     * @param int|null $salaryTo
     * @throws \Exception
     */
    public function __construct(
        Id $id,
        string $title,
        WorkType $workType,
        WorkPlace $workPlace,
        Company $company,
        array $requirements,
        array $tasks,
        array $conditions,
        string $candidateEmail,
        Slug $slug,
        ?string $city,
        ?string $image,
        ?int $salaryFrom,
        ?int $salaryTo
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->workType = $workType;
        $this->workPlace = $workPlace;
        $this->requirements = $requirements;
        $this->tasks = $tasks;
        $this->slug = $slug;
        $this->conditions = $conditions;
        $this->candidateEmail = $candidateEmail;
        $this->city = $city;
        $this->image = $image;
        $this->salaryFrom = $salaryFrom;
        $this->salaryTo = $salaryTo;
        $this->company = $company;
        $this->views = 0;
        $this->tags = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
        $this->updatedAt = new \DateTimeImmutable();
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return array
     */
    public function getRequirements(): array
    {
        return $this->requirements;
    }

    /**
     * @return array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        return $this->conditions;
    }

    /**
     * @return int|null
     */
    public function getSalaryFrom(): ?int
    {
        return $this->salaryFrom;
    }

    /**
     * @return int|null
     */
    public function getSalaryTo(): ?int
    {
        return $this->salaryTo;
    }

    /**
     * @return WorkType
     */
    public function getWorkType(): WorkType
    {
        return $this->workType;
    }

    /**
     * @return WorkPlace
     */
    public function getWorkPlace(): WorkPlace
    {
        return $this->workPlace;
    }

    /**
     * @return string
     */
    public function getCandidateEmail(): string
    {
        return $this->candidateEmail;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getViews(): int
    {
        return $this->views;
    }

    public function increaseViewsCount(): void
    {
        $this->views = $this->getViews() + 1;
    }


    /**
     * @param Tag $tag
     */
    public function addTag(Tag $tag): void
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;

            $tag->addVacancy($this);
        }
    }

    /**
     * @param Tag $tag
     */
    public function removeTag(Tag $tag): void
    {
        if (!$this->tags->contains($tag)) {
            return;
        }

        $this->tags->removeElement($tag);
        $tag->removeVacancy($this);
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @return Slug
     */
    public function getSlug(): Slug
    {
        return $this->slug;
    }
}
