<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class VacancyRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Vacancy::class);
    }

    /**
     * @param Vacancy $vacancy
     */
    public function add(Vacancy $vacancy): void
    {
        $this->em->persist($vacancy);
    }

    public function paginate()
    {
        return $this->repository->findAll();
    }
}
