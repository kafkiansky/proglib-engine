<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Entity;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class WorkPlaceType extends StringType
{
    public const NAME = 'work_place_type';

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof WorkPlace ? $value->getName() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return !empty($value) ? new WorkPlace($value) : null;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform) : bool
    {
        return true;
    }
}
