<?php

declare(strict_types=1);

namespace App\Model\Vacancy\Service\Search;

use App\Model\Vacancy\Entity\Vacancy;
use Elasticsearch\Client;

class VacancyIndexer
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Vacancy $vacancy
     */
    public function index(Vacancy $vacancy)
    {
        $this->client->index([
            'index' => 'vacancies',
            'type' => 'vacancy',
            'id' => $vacancy->getId()->getValue(),
            'body' => [
                'id' => $vacancy->getId()->getValue(),
                'title' => $vacancy->getTitle(),
                'city' => $vacancy->getCity(),
                'salaryFrom' => $vacancy->getSalaryFrom(),
                'salaryTo' => $vacancy->getSalaryTo(),
                'image' => $vacancy->getImage(),
                'requirements' => $vacancy->getRequirements(),
                'tasks' => $vacancy->getTasks(),
                'conditions' => $vacancy->getConditions()
            ]
        ]);
    }

    /**
     * @param Vacancy $vacancy
     */
    public function remove(Vacancy $vacancy)
    {
        $this->client->delete([
            'index' => 'vacancies',
            'type' => 'vacancy',
            'id' => $vacancy->getId()->getValue()
        ]);
    }
}
