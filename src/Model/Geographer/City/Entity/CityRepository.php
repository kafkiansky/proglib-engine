<?php

declare(strict_types=1);

namespace App\Model\Geographer\City\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class CityRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(City::class);
    }

    /**
     * @param City $city
     */
    public function add(City $city): void
    {
        $this->em->persist($city);
    }

    /**
     * @param string $query
     * @param int $limit
     * @param int $offset
     * @return mixed|null
     */
    public function findByQuery(string $query, int $limit, int $offset = 0)
    {
        $cities = $this->repository->createQueryBuilder('c')
            ->where('c.name LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if (!$cities) {
            return null;
        }

        return $cities;
    }
}
