<?php

declare(strict_types=1);

namespace App\Model\Geographer\City\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity()
 * @ORM\Table(name="cities", indexes={@Index(name="city_name_idx", columns={"name"})})
 */
class City
{
    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="city_id_type")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;

    public function __construct(Id $id, int $code, string $name)
    {
        $this->id = $id;
        $this->code = $code;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
