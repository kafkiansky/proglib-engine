<?php

declare(strict_types=1);

namespace App\Model\Company\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class CompanyRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $em->getRepository(Company::class);
    }

    /**
     * @param Company $company
     */
    public function add(Company $company): void
    {
        $this->em->persist($company);
    }
}
