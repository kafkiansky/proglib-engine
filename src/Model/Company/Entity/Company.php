<?php

declare(strict_types=1);

namespace App\Model\Company\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use App\Model\Vacancy\Entity\Vacancy;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="companies")
 */
class Company
{
    /**
     * @var Id
     * @ORM\Id()
     * @ORM\Column(type="company_id_type")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $site;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    private $image;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity=Vacancy::class, mappedBy="company", cascade={"remove"}))
     */
    private $vacancies;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $updatedAt;

    public function __construct(
        Id $id,
        string $name,
        string $site,
        string $image,
        string $description
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->site = $site;
        $this->image = $image;
        $this->description = $description;
        $this->createdAt = new \DateTimeImmutable('now');
        $this->updatedAt = new \DateTimeImmutable('now');
    }

    /**
     * @return ArrayCollection
     */
    public function getVacancies(): ArrayCollection
    {
        return $this->vacancies;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getUpdatedAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
}
