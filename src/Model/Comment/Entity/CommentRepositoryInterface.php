<?php

declare(strict_types=1);

namespace App\Model\Comment\Entity;

use App\Domain\User\Model\Entity\User;
use App\Model\Comment\Entity\Comment;
use App\Domain\Post\Model\Entity\Post;
use Symfony\Component\HttpFoundation\Request;

interface CommentRepositoryInterface
{
    public function find($id);

    public function findLatest(Post $post);

    public function findUserComments(User $user);

    public function findUserCommentsCount(User $user);

    public function findUserCommentsByOrder(User $user, Request $request, int $offset = 0);

    public function findBookmarkedComments(User $user, int $offset = 0): ?array;

    public function findLastCommentsForPost(string $postId, int $offset = 0): ?array;

    public function findLastCommentsForPostCount(string $postId);

    public function findCountComments(string $postId);

    public function add(Comment $comment): void;

    public function findLatestByCount(int $count = 5);
}
