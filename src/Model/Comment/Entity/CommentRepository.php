<?php

declare(strict_types=1);

namespace App\Model\Comment\Entity;

use App\Domain\Post\ReadModel\BookMarksView;
use App\Domain\User\Model\Entity\User;
use App\Model\Comment\Entity\Comment;
use App\Domain\Post\Model\Entity\Post;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class CommentRepository implements CommentRepositoryInterface
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(Connection $connection, EntityManagerInterface $em)
    {
        $this->connection = $connection;
        $this->em = $em;
        $this->repository = $em->getRepository(Comment::class);
    }

    /**
     * @param Post $post
     * @return mixed
     */
    public function findLatest(Post $post)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $comments = $qb
            ->where('c.post = :post')
            ->andWhere('c.parent IS NULL')
            ->setParameter('post', $post)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return $comments;
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function findUserComments(User $user)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $comments = $qb
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->orderBy('c.createdAt', 'ASC')
            ->getQuery()
            ->getResult();

        return $comments;
    }

    /**
     * @param User $user
     * @return mixed
     *
     * @throws NonUniqueResultException
     */
    public function findUserCommentsCount(User $user)
    {
        $qb = $this->repository->createQueryBuilder('c');

        $comments = $qb
            ->select('count(c.id)')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();

        return $comments;
    }

    /**
     * @param User $user
     * @param Request $request
     * @param int $offset
     *
     * @return array
     *
     * @throws Exception
     */
    public function findUserCommentsByOrder(
        User $user,
        Request $request,
        int $offset = 0
    ): array {

        $orderByDate = $request->request->get('orderByDate');
        $orderByStatus = $request->request->get('orderByStatus');

        $comments = [];

        if (!isset($orderByDate) && !isset($orderByStatus)) {
            $comments = $this->findAllLatestCommentsAndCountForAllTime($offset, $user);
        } elseif ($orderByStatus === 'latest') {
            $comments = $this->findLatestPostsByTime($offset, $user, $orderByDate ?? 'last_week');
        } elseif ($orderByStatus === 'popular') {
            $comments = $this->decideForQuery($offset, $user->getId()->toString(), $orderByDate);
        }

        return $comments;
    }

    /**
     * @param int $offset
     * @param User $user
     * @param string $time
     *
     * @return array|mixed
     *
     * @throws Exception
     */
    private function findLatestPostsByTime(int $offset, User $user, string $time): array
    {
        $from = new \DateTime('now');

        $qb = $this->repository->createQueryBuilder('c')
            ->where('c.user = :user')
            ->andWhere('c.createdAt BETWEEN :from AND :to')
            ->setParameter('user', $user)
            ->setMaxResults(10)
            ->setFirstResult($offset);

        $comments = [];

        if ($time === 'last_week') {
            $comments['comments'] = $qb->setParameter('from', new \DateTime('-1 week'))
                ->setParameter('to', $from)
                ->orderBy('c.createdAt', 'DESC')
                ->getQuery()
                ->getResult();
        } elseif ($time === 'last_month') {
            $comments['comments'] = $qb->setParameter('from', new \DateTime('-1 month'))
                ->setParameter('to', $from)
                ->orderBy('c.createdAt', 'DESC')
                ->getQuery()
                ->getResult();
        } elseif ($time === 'last_year') {
            $comments['comments'] = $qb->setParameter('from', new \DateTime('-1 year'))
                ->setParameter('to', $from)
                ->orderBy('c.createdAt', 'DESC')
                ->getQuery()
                ->getResult();
        } elseif ($time === 'for_all_the_time') {
            $comments['comments'] = $this->findAllLatestCommentsForAllTime($offset, $user);
        }

        $comments['count'] = $user->getComments()->count();

        return $comments;
    }

    /**
     * @param int $offset
     * @param string $user
     * @param string $time
     * @return array
     * @throws DBALException
     */
    private function decideForQuery(int $offset, string $user, string $time): array
    {
        $comments = [];

        if ($time === 'last_week') {
            $comments['comments'] = $this->findPopularPostsByWeek($offset, $user);
            $comments['count'] = $this->findPopularPostsByWeekCount($user);
        } elseif ($time === 'last_month') {
            $comments['comments'] = $this->findPopularPostsByMonth($offset, $user);
            $comments['count'] = $this->findPopularPostsByMonthCount($user);
        } elseif ($time === 'last_year') {
            $comments['comments'] = $this->findPopularPostsByYear($offset, $user);
            $comments['count'] = $this->findPopularPostsByYearCount($user);
        } elseif ($time === 'for_all_the_time') {
            $comments['comments'] = $this->findAllPopularCommentsForAllTime($offset, $user);
            $comments['count'] = $this->findAllPopularCommentsCount($user);
        }

        return $comments;
    }

    /**
     * @param int $offset
     * @param string $user
     * @return array
     * @throws DBALException
     */
    private function findPopularPostsByWeek(int $offset, string $user): array
    {
        $sql = '
            SELECT
	            cl.comment_id AS commentId
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 WEEK 
	        AND 
	            NOW()
	            )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        DESC
	        LIMIT :offset, 10
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('id', $user);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        $popularCommentsByWeek = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $comment) {
            $popularCommentsByWeek[] = $this->find(['id' => $comment->commentId]);
        }

        return $popularCommentsByWeek;
    }

    /**
     * @param string $user
     * @return int
     * @throws DBALException
     */
    private function findPopularPostsByWeekCount(string $user): int
    {
        $sql = '
            SELECT 
	            COUNT(*) AS cc
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments 
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 WEEK
	        AND 
	            NOW()
	            )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id' => $user]);

        $cc = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return \count(compact($cc));
    }

    /**
     * @param int $offset
     * @param string $user
     * @return array
     * @throws DBALException
     */
    private function findPopularPostsByMonth(int $offset, string $user): array
    {
        $sql = '
            SELECT 
	            cl.comment_id AS commentId
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 MONTH
	        AND 
	            NOW()
	            )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        DESC
	        LIMIT :offset, 10
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('id', $user);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        $popularCommentsByMonth = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $comment) {
            $popularCommentsByMonth[] = $this->find(['id' => $comment->commentId]);
        }

        return $popularCommentsByMonth;
    }

    /**
     * @param string $user
     * @return int
     * @throws DBALException
     */
    private function findPopularPostsByMonthCount(string $user): int
    {
        $sql = '
            SELECT 
	            COUNT(*) AS cc
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments 
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 MONTH
	        AND 
	            NOW()
	            )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id' => $user]);

        $cc = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return \count($cc);
    }

    /**
     * @param int $offset
     * @param string $user
     * @return array
     * @throws DBALException
     */
    private function findPopularPostsByYear(int $offset, string $user): array
    {
        $sql = '
            SELECT 
	            cl.comment_id AS commentId
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments 
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 YEAR
	        AND 
	            NOW()
	            )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        DESC
	        LIMIT :offset, 10
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('id', $user);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        $popularCommentsByYear = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $comment) {
            $popularCommentsByYear[] = $this->find(['id' => $comment->commentId]);
        }

        return $popularCommentsByYear;
    }

    /**
     * @param string $user
     * @return int
     * @throws DBALException
     */
    private function findPopularPostsByYearCount(string $user): int
    {
        $sql = '
            SELECT 
	            COUNT(*) AS cc
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments 
	        WHERE 
	            user_id = :id
	        AND 
	            created_at 
	        BETWEEN 
	            CURDATE()-INTERVAL 1 YEAR
	        AND 
	            NOW()
	        )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id)
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id' => $user]);

        $cc = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        return \count($cc);
    }

    /**
     * @param string $user
     * @return int
     * @throws DBALException
     */
    private function findAllPopularCommentsCount(string $user): int
    {
        $sql = '
            SELECT 
	            COUNT(*) AS cc
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments
	        WHERE 
	            user_id = :id
	        )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id)
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->execute(['id' => $user]);

        $cc = $stmt->fetchAll(\PDO::FETCH_OBJ);

        return \count($cc);
    }

    /**
     * @param int $offset
     * @param string $user
     * @return array
     * @throws DBALException
     */
    private function findAllPopularCommentsForAllTime(int $offset, string $user): array
    {
        $sql = '
            SELECT
	            cl.comment_id AS commentId
	        FROM 
	            comments_likes cl
	        WHERE 
	            cl.comment_id 
	        IN 
	        (SELECT 
	            id 
	        FROM 
	            comments 
	        WHERE 
	            user_id = :id
	        )
	        GROUP BY cl.comment_id 
	        ORDER BY COUNT(comment_id) 
	        DESC
	        LIMIT :offset, 10
	        '
        ;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('id', $user);
        $stmt->bindValue('offset', $offset, \PDO::PARAM_INT);
        $stmt->execute();

        $popularPostsForAllTime = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_OBJ) as $comment) {
            $popularPostsForAllTime[] = $this->find(['id' => $comment->commentId]);
        }

        return $popularPostsForAllTime;
    }

    /**
     * @param int $offset
     * @param User $user
     *
     * @return array
     */
    private function findAllLatestCommentsForAllTime(int $offset, User $user): array
    {
         return $this->repository->createQueryBuilder('c')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $offset
     * @param User $user
     *
     * @return array
     */
    private function findAllLatestCommentsAndCountForAllTime(int $offset, User $user): array
    {
        $comments = [];

        $comments['comments'] = $this->repository->createQueryBuilder('c')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->setMaxResults(10)
            ->setFirstResult($offset)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        $comments['count'] = $user->getComments()->count();

        return $comments;
    }

    /**
     * @param User $user
     * @param int $offset
     * @return array|null
     */
    public function findBookmarkedComments(User $user, int $offset = 0): ?array
    {
        $stmt = $this->connection->createQueryBuilder()
            ->select(
                'c.id',
                'c.comment',
                'c.user_id AS userId',
                'c.post_id as postId',
                'c.images',
                'c.updated_at as updatedAt'
            )
            ->from('comments', 'c')
            ->innerJoin('c', 'comments_bookmarks', 'cb', 'cb.comment_id = c.id')
            ->where('cb.user_id = :user')
            ->setParameter('user', $user->getId())
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->execute();

        $stmt->setFetchMode(FetchMode::CUSTOM_OBJECT, BookMarksView::class);

        $bookMarksComment = $stmt->fetchAll();
        $comments = [];

        /** @var BookMarksView $bookMarkComment */
        foreach ($bookMarksComment as $bookMarkComment) {
            $comments[] = $this->find(['id' => $bookMarkComment->id]);
        }

        return $comments ?: null;
    }

    /**
     * @param string $postId
     * @param int $offset
     *
     * @return array|null
     */
    public function findLastCommentsForPost(string $postId, int $offset = 0): ?array
    {
        $comments = $this->repository->createQueryBuilder('c')
            ->where('c.post = :id')
            ->andWhere('c.parent IS NULL')
            ->setParameter('id', $postId)
            ->setFirstResult($offset)
            ->setMaxResults(10)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();

        return $comments ?: null;
    }

    public function findLastCommentsForPostCount(string $postId)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(*)count')
            ->from('comments', 'c')
            ->where('c.post_id = :id')
//            ->andWhere('c.parent_id IS NULL')
            ->setParameter('id', $postId)
            ->execute()
            ->fetchColumn();
    }

    public function findCountComments(string $postId)
    {
        return $this->connection->createQueryBuilder()
            ->select('COUNT(*)count')
            ->from('comments', 'c')
            ->where('c.post_id = :id')
            ->setParameter('id', $postId)
            ->execute()
            ->fetchColumn();
    }

    public function add(Comment $comment): void
    {
        $this->em->persist($comment);
    }

    /**
     * @param int $count
     * @return mixed
     */
    public function findLatestByCount(int $count = 5)
    {
        return $this->repository->createQueryBuilder('c')
            ->setMaxResults($count)
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }
}
