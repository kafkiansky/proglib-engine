<?php

declare(strict_types=1);

namespace App\Model\Comment\Entity;

use App\Domain\Post\Event\CommentLiked;
use App\Domain\Post\Event\Direct\CommentCreated;
use App\Domain\Post\Model\Entity\Post;
use App\Domain\User\Model\Entity\User;
use App\Model\AggregateRoot;
use App\Model\EventsTrait;
use App\Model\Test\Entity\Test;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use App\Model\Notification\Entity\Notification;

/**
 * @ORM\Entity()
 * @ORM\Table(name="comments")
 */
class Comment implements AggregateRoot
{
    use EventsTrait;

    public const CREATED = 'created';
    public const EDITED = 'edited';
    public const DELETED = 'deleted';

    /**
     * @var Id
     *
     * @ORM\Column(type="comment_id")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $comment;

    /**
     * @ORM\OneToMany(targetEntity=Notification::class, mappedBy="comment", cascade={"remove"})
     */
    private $notifications;

    /**
     * @var Post
     *
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="comments")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $post;

    /**
     * @var Test
     *
     * @ORM\ManyToOne(targetEntity=Test::class)
     * @ORM\JoinColumn(referencedColumnName="id", nullable=true)
     */
    private $test;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="comments")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $children;

    /**
     * @var Comment
     * @ORM\ManyToOne(targetEntity=Comment::class, inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Domain\User\Model\Entity\User", inversedBy="commentsLiked")
     * @ORM\JoinTable(name="comments_likes",
     *     joinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *     )
     */
    private $userLikes;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Domain\User\Model\Entity\User", inversedBy="commentsBookmarked")
     * @ORM\JoinTable(name="comments_bookmarks",
     *     joinColumns={@ORM\JoinColumn(name="comment_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $userBookmarks;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $images;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->userLikes = new ArrayCollection();
        $this->userBookmarks = new ArrayCollection();
        $this->createdAt = new DateTime('now');
        $this->updatedAt = new DateTime('now');
    }

    /**
     * @param Id $id
     * @param string $content
     * @param Post $post
     * @param User $user
     * @param string $status
     * @param null $images
     *
     * @return Comment
     *
     * @throws Exception
     */
    public static function fromBlank(
        Id $id,
        string $content,
        Post $post,
        User $user,
        string $status,
        $images = null
    ): Comment {
        $comment = new self();
        $comment->id = $id;
        $comment->comment = $content;
        $comment->post = $post;
        $comment->user = $user;
        $comment->status = $status;
        $comment->images = $images;

        $comment->recordEvent(new CommentCreated($comment));

        return $comment;
    }

    /**
     * @param Id $id
     * @param string $content
     * @param Post $post
     * @param User $user
     * @param string $status
     * @param Comment $parent
     * @param null $images
     *
     * @return Comment
     */
    public static function fromAnswer(
        Id $id,
        string $content,
        Post $post,
        User $user,
        string $status,
        Comment $parent,
        $images = null
    ): Comment {
        $comment = new self();
        $comment->id = $id;
        $comment->comment = $content;
        $comment->post = $post;
        $comment->user = $user;
        $comment->status = $status;
        $comment->parent = $parent;
        $comment->images = $images;

        $comment->recordEvent(new CommentCreated($comment));

        return $comment;
    }

    /**
     * @param string $content
     * @param null $images
     *
     * @throws Exception
     */
    public function edit(string $content, $images = null)
    {
        $this->comment = $content;
        $this->images = $images;
        $this->status = self::EDITED;
        $this->updatedAt = new DateTime();
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Post|null
     */
    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function getUserLikes()
    {
        return $this->userLikes;
    }

    /**
     * @param User $actor
     * @param User $executor
     */
    public function likeComment(User $actor, User $executor): void
    {
        if ($this->userLikes->contains($executor)) {
            return;
        }

        $this->userLikes->add($executor);
        $this->recordEvent(new CommentLiked($actor, $executor));
    }

    /**
     * @param User $user
     */
    public function unlike(User $user): void
    {
        if (!$this->userLikes->contains($user)) {
            return;
        }

        $this->userLikes->removeElement($user);
    }

    /**
     * @return string|null
     */
    public function getImages(): ?string
    {
        return $this->images;
    }

    /**
     * @param User $user
     */
    public function bookmark(User $user)
    {
        if ($this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->add($user);
    }

    /**
     * @param User $user
     */
    public function unBookmark(User $user): void
    {
        if (!$this->userBookmarks->contains($user)) {
            return;
        }

        $this->userBookmarks->removeElement($user);
    }

    public function getUserBookmarks()
    {
        return $this->userBookmarks;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }
}
