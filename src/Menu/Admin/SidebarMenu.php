<?php

declare(strict_types=1);

namespace App\Menu\Admin;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SidebarMenu
{
    /**
     * @var FactoryInterface
     */
    private $factory;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $auth;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        FactoryInterface $factory,
        AuthorizationCheckerInterface $auth,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->factory = $factory;
        $this->auth = $auth;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return ItemInterface
     */
    public function build(): ItemInterface
    {
        $menu = $this->factory->createItem('root')
            ->setChildrenAttributes(['class' => 'nav']);

        $menu->addChild('Главная', ['route' => 'admin_profile'])
            ->setExtra('icon', 'nav-icon icon-speedometer')
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Публикации')->setAttribute('class', 'nav-title');

        $menu->addChild('Опубликованные', ['route' => 'admin_profile'])
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setExtra('icon', 'nav-icon icon-check')
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('На проверке', ['route' => 'admin_profile'])
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setExtra('icon', 'nav-icon icon-energy')
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $dropdown = $menu->addChild('first.stat', [
            'route' => 'admin_profile',
            'label' => ' Статистика'
        ])
            ->setExtra('icon', 'nav-icon icon-graph')
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setAttribute('class', 'nav-item')
            ->setAttribute('class', 'nav-dropdown')
            ->setChildrenAttribute('class', 'nav-dropdown-items')
            ->setLinkAttribute('class', 'nav-link nav-dropdown-toggle');

        $link = $this->urlGenerator->generate('links_show');


        $dropdown
            ->addChild('Битые ссылки', ['uri' => $link])
            ->setAttribute('class','nav-item')
            ->setLinkAttribute('class', 'nav-link')
            ->setExtra('icon', 'nav-icon icon-link')
        ;

        $dropdown
            ->addChild('Bounce rate', ['uri' => $link])
            ->setAttribute('class','nav-item')
            ->setLinkAttribute('class', 'nav-link')
            ->setExtra('icon', 'nav-icon icon-eye');


        $menu->addChild('Пользователи')->setAttribute('class', 'nav-title');

        $menu->addChild('Активные', ['route' => 'admin_profile'])
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setExtra('icon', 'nav-icon icon-user')
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $menu->addChild('Черный список', ['route' => 'admin_profile'])
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setExtra('icon', 'nav-icon icon-ban')
            ->setAttribute('class', 'nav-item')
            ->setLinkAttribute('class', 'nav-link');

        $dropdown = $menu->addChild('second.stat', [
            'route' => 'admin_profile',
            'label' => 'Статистика'
        ])
            ->setExtra('icon', 'nav-icon icon-graph')
            ->setExtra('routes', [
                ['route' => 'admin_profile'],
                ['pattern' => '/^work.projects\..+/']
            ])
            ->setAttribute('class', 'nav-item')
            ->setAttribute('class', 'nav-dropdown')
            ->setChildrenAttribute('class', 'nav-dropdown-items')
            ->setLinkAttribute('class', 'nav-link nav-dropdown-toggle');

        $link = $this->urlGenerator->generate('links_show');

        $dropdown
            ->addChild('Жалобы', ['uri' => $link])
            ->setAttribute('class','nav-item')
            ->setLinkAttribute('class', 'nav-link')
            ->setExtra('icon', 'nav-icon icon-fire')
        ;

        $dropdown
            ->addChild('Регистрации', ['uri' => $link])
            ->setAttribute('class','nav-item')
            ->setLinkAttribute('class', 'nav-link')
            ->setExtra('icon', 'nav-icon icon-user-following');

        return $menu;
    }
}
