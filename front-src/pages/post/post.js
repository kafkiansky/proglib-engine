import "./post.scss";

import initElevator from "./modules/elevator";
import strikeLinks from "./modules/strike-links";
import bounceRate from "./modules/bounce-rate";
import initShareButton from "./modules/share-button";

window.strikeLinks = strikeLinks;
window.ready('strike-links');

window.onReady('dom', () => {
    initElevator();
    
    let article = document.querySelector('.post');
    let articleContent = article.querySelector('.post__content');
    let postId = article.dataset.id;

    if (!postId) return;

    bounceRate(articleContent, "score_bounce_rate", postId);
    initShareButton(articleContent);
});