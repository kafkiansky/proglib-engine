/* share button */
import addHandler from "../../../services/events-handlers";

export default (content) => {
  let fixShare = document.querySelector('.fix-share');
  if (!fixShare) return;
  let checkShare = () => {
      if (content.getBoundingClientRect().top < 10) {
          fixShare.classList.add('shown');
      } else {
          fixShare.classList.remove('shown');
      }
  }
  checkShare();
  addHandler('scroll', checkShare);
}
