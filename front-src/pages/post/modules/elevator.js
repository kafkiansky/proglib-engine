
/* кнопка Наверх */

import "./elevator.scss";
import addHandler from "../../../services/events-handlers";
import getScrollTop from "../../../helpers/get-scroll-top";


export default () => {
  let elevator = document.getElementById('elevator');
  let showElevatorPosition = 300;

  elevator.addEventListener("click", function() {
    if (elevator.lastPosition) {
      elevator.direction = "bottom";
      window.scroll({
        top: elevator.lastPosition,
        behavior: 'smooth'
      })
      elevator.lastPosition = null;
      elevator.setAttribute('data-direction', 'top');
    } else {
      elevator.direction = "top";
      elevator.lastPosition = getScrollTop();
      window.scroll({
        top: 0,
        behavior: 'smooth'
      });
      elevator.setAttribute('data-direction', 'bottom');
    }
  });

  addHandler("scroll", (e) => {
      if (getScrollTop() > showElevatorPosition) {
          elevator.classList.remove("hidden");
          elevator.direction = null;
      } else if (!elevator.lastPosition && elevator.direction !== "bottom") {
          elevator.classList.add("hidden");
      }
  });
}


