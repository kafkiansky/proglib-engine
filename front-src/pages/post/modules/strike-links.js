export default function strikeLinks(container, brokenLinks) {
  let strikeLink = function(link) {
      let strike = document.createElement('strike');
      link.parentElement.replaceChild(strike, link);
      strike.appendChild(link);
  }

  if (brokenLinks && brokenLinks.length) {
      let links = container.querySelectorAll('a');

      setTimeout(function() {
          links.forEach(function(link) {
              let href = link.getAttribute('href');
              if (brokenLinks.indexOf(href) !== -1) {
                  strikeLink(link);
              }
          })
      }, 0);
  }
}