import addHandler from "../../../services/events-handlers";

/* bounce rate */
let statTime = 60 * 1000;
    
let isReadUp = false;
let isTime = false;

let checkReadUp = function(content) {
  let bottom = content.getBoundingClientRect().bottom;
  return bottom < window.innerHeight;
};

let sendStat = function(method, postId) {
  let data = new FormData();
  data.append('postId', postId);
  API(method, {}, data)
      .catch(function() {});
}


export default (content, method, postId) => {
  let timer = setTimeout(function() {
    clearTimeout(timer);
    isTime = true;
    if (isReadUp) sendStat(method, postId);
  }, statTime);

  addHandler('scroll', function(e, stop) {
    if (checkReadUp(content)) {
      stop();
      isReadUp = true;
      if (isTime) sendStat(method, postId);
    }
  });
}