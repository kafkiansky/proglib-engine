import "./commercial-feed.scss";

import Feed from "../../components/feed/modules/feed";
import GroupFilters from "../../components/feed/modules/group-filters";
import InputFilters from "../../components/feed/modules/input-filters";
import CSelect from "../../components/c-form/c-select/c-select";

window.onReady('dom', function() {
  CSelect.init();

  let filters = [];

  let $typeFilters = document.querySelector('.filters_type');
  if ($typeFilters)  filters.push(new GroupFilters($typeFilters, "workType"));

  let $placeFilters = document.querySelector('.filters_place');
  if ($placeFilters) filters.push(new GroupFilters($placeFilters, "workPlace"));

  let $tagFilters = document.querySelector('.tags-select');
  if ($tagFilters) filters.push(new GroupFilters($tagFilters, "tag"));

  let $salaryFromFilter = document.getElementById('vacancy-salary-from-filter');
  if ($salaryFromFilter) filters.push(new InputFilters($salaryFromFilter, 'salaryFrom'));
  
  let $cityFilter = document.getElementById('vacancy-city-filter');
  if ($cityFilter) filters.push(new InputFilters($cityFilter, "city"));

  new Feed({
      filters: filters,
      method: "vacancies_more",
      reset: document.querySelector('[data-reset-filters]')
  });
});