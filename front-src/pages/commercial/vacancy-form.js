import CForm from "../../components/c-form/c-form";
import formsInit from "../../components/c-form/forms-init";

import vacancyFormGetMessage from "./vacancy-form-get-message";

import CustomFileLoader from "../../components/loader/modules/loader";
import "./vacancy-form.scss";

window.vacancyFormInit = (rules) => {
  
  formsInit();

  let vacancyImageLoaderElement = document.querySelector('.vacancy-image');
  new CustomFileLoader(vacancyImageLoaderElement, {
    progressDelay: 400,
    previewClass: "x-editor-preview",
    initialPreviews: [],
    getStatusMessage: status => {
      if (status == "dimensions") return "Изображение не соответствует требуемым параметрам";
      return false;
    }
  });

  let companyImageLoaderElement = document.querySelector('.company-image');
  new CustomFileLoader(companyImageLoaderElement, {
    progressDelay: 400,
    previewClass: "x-editor-preview",
    initialPreviews: [],
    getStatusMessage: status => {
      if (status == "dimensions") return "Изображение не соответствует требуемым параметрам";
      return false;
    }
  });

  let form = document.querySelector('.vacancy-form');

  window.vacancyForm = new CForm(form, {
    validationRules: rules,
    method: "vacancy_create",
    getValidationErrorMessage: vacancyFormGetMessage(rules),
    onSuccess: (result) => {
      if (result.redirectUrl) location = result.redirectUrl;
    }
  });
   
};
window.ready('vacancy-form-init');

