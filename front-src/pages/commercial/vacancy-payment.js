import paymentForm from "../../components/payment-form/payment-form";

window.onReady('dom', () => {
  let submitted = false;


  let paymentPeriodForm = document.querySelector('.payment__period-form');
  let paymentPeriodFormSubmit = paymentPeriodForm.querySelector('[type="submit"]');

  let total = paymentPeriodForm.dataset.total;
  let currency = paymentPeriodForm.dataset.currency;
  

  paymentPeriodForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (submitted) return;
    paymentForm.setSum({
      total, currency
    });
    paymentForm.show();
  });

  paymentForm.init(data => {
    paymentPeriodForm.setAttribute('data-submiting', '');
    submitted = true;
    paymentPeriodForm.querySelector('[type="submit"]').disabled = true;
    paymentForm.hide();

    
    let orderData = new FormData(paymentPeriodForm);
    orderData.append('cryptogram', data.cryptogram);
    orderData.append('name', data.name.toUpperCase());
    orderData.append('mail', data.mail);
    orderData.append('total', total);
    orderData.append('currency', currency);

    fetch(paymentPeriodForm.action, {
      method: 'POST',
      body: orderData
    })
      // .then(result => result.json())
      .then(result => {
        paymentPeriodForm.removeAttribute('data-submiting');
      })
      .catch(error => {
        console.error(error);
        paymentPeriodForm.removeAttribute('data-submiting');
      })
  });
})