import getWordEnding from "../../helpers/get-word-ending";

let forms = {
  requirements: {
    dat: ['требование', 'требования', 'требований'],
    gen: ['требования', 'требований', 'требований']
  },
  conditions: {
    dat: ['условие', 'условия', 'условий'],
    gen: ['условия', 'условий', 'условий']
  },
  tasks: {
    dat: ['задачу', 'задачи', 'задачи'],
    gen: ['задачи', 'задач', 'задач']
  }
};

export default rules => error => {
  let min, max;

  switch (error) {
    case 'no-tags':
      return 'Нет ни одного тега';
    case 'too-few-tags':
      min = rules['tags'].min;
      return `Выберите хотя бы ${min} ${getWordEnding(min, getWordEnding.TAGS.dat)}`;
    case 'too-few-tags':
      max =  rules['tags'].max;
      return `Не больше ${max}  ${getWordEnding(max, getWordEnding.TAGS.gen)}`;  

    case 'no-title':
      return 'У вакансии нет названия';
    case 'too-short-title':
      min = rules['vacancyTitle'].min;
      return `Минимальная длина названия вакансии: ${min} ${getWordEnding(min, getWordEnding.SYMBOLS.dat)}`;
    case 'too-long-title':
      max = rules['vacancyTitle'].max;
      return `Максимальная длина названия вакансии: ${max} ${getWordEnding(max, getWordEnding.SYMBOLS.gen)}`;

    case 'no-salary-from':
      return 'Не установлен нижний предел оклада';
    case 'too-little-salary-from':
      return 'Нижний предел оклада должен быть больше ' + rules['salaryFrom'].min;

    case 'no-salary-to':
      return 'Не установлен верхний предел оклада';
    case 'too-little-salary-to':
      return 'Верхний предел оклада должен быть больше ' + rules['salaryTo'].min;

    case 'no-work-type':
      return 'Не выбран тип занятости';
    case 'no-work-place':
      return 'Не указано место работы';

    case 'no-city':
      return 'Не указан город';

    case 'no-requirements':
      return 'Не указано ни одного требования';
    case 'too-few-requirements':
      min = rules['requirements'].min;
      return `Добавьте хотя бы ${min} ${getWordEnding(min, forms.requirements.dat)}`;
    case 'too-many-requirements':
      max = rules['requirements'].max
      return `Не больше ${min} ${getWordEnding(min, forms.requirements.gen)}`;

    case 'no-tasks':
      return 'Не указано ни одной задачи';
    case 'too-few-tasks':
      min = rules['tasks'].min;
      return `Добавьте хотя бы ${min} ${getWordEnding(min, forms.tasks.dat)}`;
    case 'too-many-tasks':
      max = rules['tasks'].max
      return `Не больше ${min} ${getWordEnding(min, forms.tasks.gen)}`;

    case 'no-conditions':
      return 'Не указано ни одного условия работы';
    case 'too-few-conditions':
      min = rules['conditions'].min;
      return `Добавьте хотя бы ${min} ${getWordEnding(min, forms.conditions.dat)} работы`;
    case 'too-many-conditions':
      max = rules['conditions'].max
      return `Не больше ${min} ${getWordEnding(min, forms.conditions.gen)} работы`;
    
    case 'no-candidate-email':
      return 'Не указана почта для кандидатов';
    case 'no-notifications-email':
      return 'Не указана почта для отправки оповещений';
    
    case 'no-company-title':
      return 'У компании нет названия';
    case 'too-short-company-title':
      min = rules['companyTitle'].min;
      return `Минимальная длина названия компании: ${min} ${getWordEnding(min, getWordEnding.SYMBOLS.dat)}`;
    case 'too-long-company-title':
      max = rules['companyTitle'].max;
      return `Максимальная длина названия компании: ${max} ${getWordEnding(max, getWordEnding.SYMBOLS.gen)}`;

    case 'no-company-description':
      return 'У компании нет описания';
    case 'too-short-company-description':
      min = rules['companyDescription'].min;
      return `Минимальная длина описания компании: ${min} ${getWordEnding(min, getWordEnding.SYMBOLS.dat)}`;
    case 'too-long-company-description':
      max = rules['companyDescription'].max;
      return `Максимальная длина описания компании: ${max} ${getWordEnding(max, getWordEnding.SYMBOLS.gen)}`;

    case 'no-company-image':
      return 'У компании нет логотипа';

    case 'no-company-site':
      return 'Укажите сайт компании';

    case 'mail-re':
      return 'Некорректный адрес электронной почты';
    case 'url-re':
      return 'Некорректный url';

    default:
      return error;
  }
}