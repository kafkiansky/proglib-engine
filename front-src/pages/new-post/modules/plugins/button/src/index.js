require('./index.css').toString();

class Button {

  constructor({data, config, api}) {
    this.api = api;
    this._CSS = {
      block: this.api.styles.block,
      button: this.api.styles.button,
      input: this.api.styles.input,
      settingsButton: this.api.styles.settingsButton,
      settingsButtonActive: 'ce-button__settings-button--active',
      customSettingsButton: 'ce-button__settings-button',
      wrapper: 'ce-button',
      customButton: 'button',
      customInput: 'ce-button__input'
    };
    this._settings = config;
    this.classes = this.getClasses();
    
    this.defaultButtonText = config.defaultButtonText || 'Call To Action';

    this.settingsButtons = [];
    this._data = this.normalizeData(data);

    this._element = this.getTag();
  }

  normalizeData(data) {
    if (typeof data !== 'object') {
      data = {};
    }

    data.text = data.text || this.defaultButtonText;
    data.link = data.link || '';
    data.className = data.className || 'default';

    this.className = data.className;

    return data;
  }

  render() {
    return this._element;
  }

  setClassName(className) {
    this.data = {
      text: this.data.text,
      link: this.data.link,
      className: className
    };

    this.className = className;

    this.settingsButtons.forEach(button => {
      button.classList.toggle(this._CSS.settingsButtonActive, button.dataset.className === className);
    });
  }

  renderSettings() {
    let holder = document.createElement('DIV');
    this.classes.forEach(className => {
      let selectTypeButton = document.createElement('SPAN');
      selectTypeButton.classList.add(this._CSS.settingsButton);
      selectTypeButton.classList.add(this._CSS.customSettingsButton);

      if (this.className === className.name) {
        selectTypeButton.classList.add(this._CSS.settingsButtonActive);
      }

      selectTypeButton.title = className.title;
      selectTypeButton.dataset.className = className.name;
      selectTypeButton.addEventListener('click', () => {
        this.setClassName(className.name);
      });
      selectTypeButton.style.backgroundColor = className.color;
      holder.appendChild(selectTypeButton);
      this.settingsButtons.push(selectTypeButton);
    });
    return holder;
  }

  validate(blockData) {
    return blockData.text.trim() !== '' && blockData.link.trim() !== '';
  }

  save(toolsContent) {
    let $link = toolsContent.querySelector('.' + this._CSS.customInput);
    let link = $link.innerHTML;
    if (link && !link.match(/^https?:\/\//)) link = "https://" + link;
    $link.innerHTML = link;

    return {
      text: toolsContent.querySelector('.' + this._CSS.customButton).innerHTML,
      link: link,
      className: this.className
    };
  }

  getClasses() {
    return [
      {
        name: "warning",
        color: "orange",
        title: "warning",
      },
      {
        name: "error",
        color: "red",
        title: "error",
      },
      {
        name: "success",
        color: "green",
        title: "success",
      },
      {
        name: "info",
        color: "blue",
        title: "info",
      },
      {
        name: "default",
        color: "white",
        title: "default",
      },
    ];
  }

  get data() {
    this._data.text = this._element.querySelector('.' + this._CSS.customButton).innerHTML;
    this._data.link = this._element.querySelector('.' + this._CSS.customInput).innerHTML;
    this._data.className = this._data.className;

    return this._data;
  }

  set data(data) {
    this._data = this.normalizeData(data);
    if (this._element.parentNode) {
      let newButton = this.getTag();
      this._element.parentNode.replaceChild(newButton, this._element);
      this._element = newButton;
    }
  }

  getTag() {
    let tag = document.createElement('div');
    tag.classList.add(this._CSS.wrapper);
    tag.classList.add(this._CSS.block);

    let btn = document.createElement('button');
    btn.innerHTML = this._data.text || '';
    btn.contentEditable = 'true';
    btn.classList.add(this._CSS.customButton);
    btn.classList.add('button_' + this.className);

    let link = document.createElement('div');
    link.innerHTML = this._data.link || '';
    link.contentEditable = 'true';
    link.classList.add(this._CSS.customInput);
    link.classList.add(this._CSS.input);
    link.dataset.placeholder = "Введите ссылку";

    tag.appendChild(btn);
    tag.appendChild(link);

    return tag;
  }

  static get toolbox() {
    return {
      icon: `<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24"><path d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z"/></svg>`,
      title: 'Button'
    };
  }
}

export default Button;
