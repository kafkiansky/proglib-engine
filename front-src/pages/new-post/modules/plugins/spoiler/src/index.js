import './index.css';
import sslide from "../../../../../../modules/libs/sslide";

class Spoiler {

  constructor({data, config, api}) {
    this.api = api;
    this._CSS = {
      block: this.api.styles.block,
      input: this.api.styles.input,
      wrapper: 'ce-spoiler',
      shown: 'ce-spoiler__shown',
      hide: 'ce-spoiler__hide',
      toggler: 'ce-spoiler__toggler',
      opened: 'opened'
    };
    this.isOpen = true;
    this._settings = config;

    this._data = this.normalizeData(data);

    this._element = this.getTag();
  }

  normalizeData(data) {
    if (typeof data !== 'object') {
      data = {};
    }

    data.shown = data.shown || '';
    data.hidden = data.hidden || '';

    return data;
  }

  render() {
    return this._element;
  }

  validate(blockData) {
    return blockData.shown.trim() !== '' && blockData.hidden.trim() !== '';
  }

  save(toolsContent) {
    return {
      shown: toolsContent.querySelector('.' + this._CSS.shown).innerHTML,
      hidden: toolsContent.querySelector('.' + this._CSS.hidden).innerHTML,
    };
  }

  get data() {
    this._data.shown = this._element.querySelector('.' + this._CSS.shown).innerHTML;
    this._data.hidden = this._element.querySelector('.' + this._CSS.hidden).innerHTML;

    return this._data;
  }

  set data(data) {
    this._data = this.normalizeData(data);
    if (this._element.parentNode) {
      let tag = this.getTag();
      this._element.parentNode.replaceChild(tag, this._element);
      this._element = newButton;
    }
  }

  getTag() {
    let tag = document.createElement('div');
    tag.classList.add(this._CSS.wrapper);
    tag.classList.add(this._CSS.block);
    tag.classList.toggle(this._CSS.opened, this.isOpen);

    let shown = document.createElement('div');
    shown.innerHTML = this._data.shown || '';
    shown.contentEditable = 'true';
    shown.classList.add(this._CSS.shown);
    shown.classList.add(this._CSS.input);
    shown.dataset.placeholder = "Видимая часть";

    let hidden = document.createElement('div');
    hidden.innerHTML = this._data.hidden || '';
    hidden.contentEditable = 'true';
    hidden.classList.add(this._CSS.hidden);
    hidden.classList.add(this._CSS.input);
    hidden.dataset.placeholder = "Скрытая часть";
    hidden = sslide(hidden);

    let toggler = document.createElement('div');
    toggler.classList.add(this._CSS.toggler);
    toggler.addEventListener('click', () => {
      this.isOpen = !this.isOpen;
      tag.classList.toggle(this._CSS.opened, this.isOpen);
      if (this.isOpen) hidden.slideDown(400);
      else hidden.slideUp(400);
    });

    tag.appendChild(shown);
    tag.appendChild(toggler);
    tag.appendChild(hidden);
    
    return tag;
  }

  static get toolbox() {
    return {
      icon: `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M22 3H7c-.69 0-1.23.35-1.59.88L0 12l5.41 8.11c.36.53.97.89 1.66.89H22c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zM9 13.5c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm5 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zm5 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"/></svg>`,
      title: 'Spoiler'
    };
  }
}

export default Spoiler;
