import "./index.css";

class Gist {

  constructor({data, api}) {
    this.api = api;
    this.element = null;
    this._data = {};
    this.data = data;
  }

  get CSS() {
    return {
      baseClass: this.api.styles.block,
      container: 'gist-tool',
      label: 'gist-tool__label',
      caption: this.api.styles.input,
      urlWrapper: 'gist-tool__url',
      url: 'url'
    }
  }

  render() {
    let container = document.createElement('div');
    container.classList.add(this.CSS.container);
    container.classList.add(this.CSS.baseClass);

    let label = document.createElement('div');
    label.classList.add(this.CSS.label);
    label.innerHTML = "Github Gist";
       
    let url = document.createElement('span');
    url.classList.add(this.CSS.url);
    url.innerHTML = this.data.url;

    let urlWrapper = document.createElement('div');
    urlWrapper.classList.add(this.CSS.urlWrapper);
    urlWrapper.innerHTML = "gist: ";
    urlWrapper.appendChild(url);

    let caption = document.createElement('div');
    caption.classList.add(this.CSS.caption);
    caption.innerHTML = this.data.caption || '';
    caption.dataset.placeholder = "Caption";
    caption.contentEditable = true;

    container.appendChild(label);
    container.appendChild(urlWrapper);
    container.appendChild(caption);
    
    this.element = container;

    return container;
  }

  save() {
    return this.data;
  }

  get data() {
    if (this.element) {
      const caption = this.element.querySelector(`.${this.api.styles.input}`);
      this._data.caption = caption ? caption.innerHTML : '';
    }

    return this._data;
  }

  set data(data) {
    if (!(data instanceof Object)) {
      throw Error('Gist Tool data should be object');
    }
    this._data = {
      url: data.url || this.data.url,
      script: data.script || this.data.script,
      caption: data.caption || this.data.caption || '',
    };

    const oldView = this.element;

    if (oldView) {
      oldView.parentNode.replaceChild(this.render(), oldView);
    }
  }


  onPaste(event) {
    let service = event.detail.key;
    let url = event.detail.data;
    
    if (service === "gist") {
      let script = url;
      let hashIndex = url.indexOf('#');
      if (hashIndex !== -1) script = script.slice(0, hashIndex);

      this.data = {
        url: url,
        script: script + ".js",
        caption: this.data.caption || ''
      }
    }
  } 

  static get pasteConfig() {
    return {
      patterns: {
        gist: /https?:\/\/gist\.github\.com\/([^\/\?\&]+)\/([^\/\?\&]+)/
      }
    };
  }
}

export default Gist; 


