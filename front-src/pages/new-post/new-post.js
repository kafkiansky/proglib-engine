import "./new-post.scss";

import PostEditor from "../../components/editor/post-editor/post-editor";

window.PostEditor = PostEditor;

if (window.ready)
  window.ready('post-editor');