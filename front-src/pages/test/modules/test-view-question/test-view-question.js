import testViewVariantsFactory from "../test-view-variant/test-view-variants-factory";
import testViewExplanationFactory from "./test-view-explanation-factory";
import Dispatcher from "../../../../services/dispatcher";
class TestViewQuestion {
  constructor(questionData) {
    new Dispatcher(this);
    this.data = questionData;

    this.variants = [];

    this.waiting = true;
    this.selected = [];
  }

  get id() {
    return this.data.question.id;
  }

  set correct(value) {
    if (value === null) {
      TestViewQuestion.elements.container.classList.remove('correct');
      TestViewQuestion.elements.container.classList.remove('incorrect');
    } else {
      TestViewQuestion.elements.container.classList.toggle('correct', !!value);
      TestViewQuestion.elements.container.classList.toggle('incorrect', !value);
    }
  }

  select(variant) {
    let index = this.selected.indexOf(variant);
    
    if (index === -1) {
      this.selected.push(variant);
      variant.checked = true;

      if (this.selected.length === 1) {
        TestViewQuestion.elements.check.disabled = false;
      }
    } else {
      this.selected.splice(index, 1);
      variant.checked = false;

      if (this.selected.length === 0) {
        TestViewQuestion.elements.check.disabled = true;
      }
    }
  }

  render() {

    TestViewQuestion.elements.current.textContent = this.data.current;
    TestViewQuestion.elements.total.textContent = this.data.total;

    TestViewQuestion.elements.description.innerHTML = "";

    let question = this.data.question;
    TestViewQuestion.elements.text.innerHTML = question.text;
    TestViewQuestion.elements.variants.innerHTML = "";
    let variants = JSON.parse(question.variants);
    this.variants = variants.map(variantData => {
      let variant = testViewVariantsFactory.create(variantData);
      if (!variant) return false;
      TestViewQuestion.elements.variants.appendChild(variant.element);
      variant.on('test.variant.events.click', () => this._onVariantClick(variant));
      return variant;
    }).filter(variant => variant);

    TestViewQuestion.elements.check.disabled = true;

    if (question.multiple) {
      TestViewQuestion.elements.check.classList.remove('hidden');
      let submit = () => {
        if (this.selected.length) {
          this.submit();
          TestViewQuestion.elements.check.removeEventListener('click', submit);
        }
      }
      TestViewQuestion.elements.check.addEventListener('click', submit);
    } else {
      TestViewQuestion.elements.check.classList.add('hidden');
    }

    if (window.highlight) {
      setTimeout(() => {
        window.highlight.initBlock(TestViewQuestion.elements.container)
      }, 0);
      
    }
  }

  _onVariantClick(clicked) {
    if (!this.waiting) return;

    this.select(clicked);

    if (!this.data.question.multiple) {
      this.submit();
    }
  }

  submit() {
    this.waiting = false;
    this.trigger(TestViewQuestion.events.select, this.selected.map(variant => variant.value));
    this.variants.forEach(variant => variant.disabled = true);
  }

  setResult(result) {
    this.correct = result.result;
    
    this.variants.forEach(variant => {
      variant.correct = result.correctAnswers.indexOf(variant.id) !== -1;
    }); 
    
    let explanation = testViewExplanationFactory.create(result.explanation, result.result);

    if (this.data.question.multiple) {
      TestViewQuestion.elements.description.appendChild(explanation);
    } else {
      this.selected[0].element.appendChild(explanation);
    }

    TestViewQuestion.elements.check.disabled = true;

    if (window.highlight) {
      setTimeout(() => {
        window.highlight.initBlock(explanation);
      }, 0);
    }
  }

  _getDescriptionTitle(correct) {
    if (correct) return "<h3 class='title'>Верно!</h3>";
    else return "<h3 class='title'>Неверно!</h3>";
  }
}

TestViewQuestion.events = {
  select: 'test.question.select.answer'
}

TestViewQuestion.elements = {
  container: null,
  total: null,
  current: null,
  text: null,
  variants: null,
  description: null,
  check: null,
};

Object.defineProperty(TestViewQuestion, "container", {
  set: function(element) {
    this.elements.container = element;
    
    this.elements.current = element.querySelector('.current');
    this.elements.total = element.querySelector('.total');
    this.elements.text = element.querySelector('.test-view__question');
    this.elements.variants = element.querySelector('.test-view__variants');
    this.elements.description = element.querySelector('.test-view__question-description');
    this.elements.check = element.querySelector('[data-test-check]');
  }
});

export default TestViewQuestion;