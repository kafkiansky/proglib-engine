class TestViewExplanationFactory {
  constructor() {
    this.templateId = "test-explanation-template";
    this.template = null;

    this.selectors = {
      text: '.test-view-explanation__text',
    };
  }

  _init() {
    this.template = document.getElementById(this.templateId).content.firstElementChild;
  }

  create(text, status) {
    if (!this.template) this._init();

    let element = this.template.cloneNode(true);

    element.querySelector(this.selectors.text).innerHTML = text;
    element.classList.add(status ? "correct" : "incorrect");

    return element;
  }
}

export default new TestViewExplanationFactory();
