class TestViewResult {
  constructor(element) {
    this.element = element;
  }

  render(data) {
    this.element.querySelector(TestViewResult.selectors.correct).innerHTML = data.correct;
    this.element.querySelector(TestViewResult.selectors.total).innerHTML = data.total;

    this.element.querySelector(TestViewResult.selectors.text).innerHTML = data.result;
  }
}

TestViewResult.selectors = {
  correct: ".test-view-result__correct",
  total: ".test-view-result__total",
  text: ".test-view-result__text",
}

export default TestViewResult;