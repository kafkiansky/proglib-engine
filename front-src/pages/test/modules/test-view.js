import TestViewQuestion from "./test-view-question/test-view-question";
import TestViewResult from "./test-view-result/test-view-result";
import loadImages from "../../../modules/dom/lazyload";

/**
 * @typedef TestViewConfig
 * @description Объект конфигурации для представления теста
 * @property {string} method Метод в апи для запроса данных
 * @property {string} id Идентификатор теста
 * @property {HTMLElement} next Кнопка Начать тест/Следующий вопрос
 * @property {HTMLElement} question Контейнер для вывода вопроса
 */

/**
 * TestView
 * @class
 * @description Представление теста
 * @property {string} nextMethod Метод в апи для запроса данных
 * @property {string} checkMethod Метод в апи для проверки ответа
 * @property {string} resultMethod Метод в апи для получения результата теста
 * @property {TestViewQuestion|null} currentQuestion Активный вопрос
 * 
 * @property {Object} elements
 * @property {HTMLElement} elements.next Кнопка Начать тест/Следующий вопрос
 * @property {HTMLElement} elements.question Контейнер для вывода вопроса
 * 
 * @property {Boolean} waiting Ожидание ответа
 */

class TestView {
  /**
   * @constructor
   * @param 
   * @param {TestViewConfig} config 
   */
  constructor(config) {
    this.elements = {
      container: config.container,
      next: config.next,
      question: config.question,
      result: config.result,
    };

    TestViewQuestion.container = config.container;

    this.id = config.id;
    this.nextMethod = config.nextMethod;
    this.checkMethod = config.checkMethod;
    this.resultMethod = config.resultMethod;

    this.currentQuestion = null;

    this._waiting = false;
    this._loading = false;

    this.total = 0;
    this.current = 0;

    this.elements.next.addEventListener('click', () => this.next());
  }

  get loading() {
    return this._loading;
  }

  set loading(value) {
    this._loading = Boolean(value);
    if (this._loading) this.elements.next.setAttribute('data-loading', '');
    else this.elements.next.removeAttribute('data-loading');
  }

  get waiting() {
    return this._waiting;
  }

  set waiting(value) {
    value = Boolean(value);

    this._waiting = value;
    this.elements.next.disabled = this._waiting;
  }

  /**
   * Клик на кнопку Следующий/закончить
   */
  next() {
    if (this._loading) return;

    if (this.total > 0 && this.total === this.current) {
      this.getResult()
    } else {
      this.getNextQuestion();
    }
  }

  getNextQuestion() {
    this.loading = true;
    let data = new FormData();
    data.append('testId', this.id);

    let questionData = this.currentQuestion ? this.currentQuestion.id : "";
      
    data.append('currentQuestionId', questionData);
    data.append('currentQuestionIndex', this.current);

    API(this.nextMethod, null, data)
      .then(res => {
        if (res.success) {
          this.renderQuestion(res);
          this.loading = false;
        } else if (res.error === "no-question") {
          this.loading = false;
          this.getResult();
        } else {
          throw new Error(res.error);
        }
      })
      .catch(err => {
        this.loading = false;
        console.error(err);
      });
  }

  getResult() {
    let data = new FormData();
    data.append('testId', this.id);

    API(this.resultMethod, null, data)
      .then(res => {
        if (res.success) {
          this.renderResult(res);
          this.loading = false;
        } else throw new Error(res.error);
      })
      .catch(err => {
        this.loading = false;
        console.error(err);
      });
  }

  /**
   * Вывод нового вопроса на страницу
   * @param {Object} data 
   */
  renderQuestion(data) {
    this.total = data.total;
    this.current = data.current;
  
    this.elements.next.textContent = TestView.texts[this.total === this.current ? 'finish' : 'toNext'];
    this.elements.next.disabled = true;

    this.currentQuestion = new TestViewQuestion(data);
    this.currentQuestion.render();
    this.currentQuestion.on('test.question.select.answer', (variants) => {
      this._checkAnswer(variants);
    })

    this.elements.question.classList.remove('hidden');
    
    this._scrollIntoView();

    loadImages();
  }

  /**
   * Вывод результата на страницу
   * @param {Object} data 
   */
  renderResult(data) {
    
    let result = new TestViewResult(this.elements.result);
    result.render(data);
    this.elements.question.classList.add('hidden');
    this.elements.next.classList.add('hidden');
    this.elements.result.classList.remove('hidden');

    this._scrollIntoView();
    loadImages();
  }

  _scrollIntoView() {
    this.elements.container.scrollIntoView({
      behavior: "smooth",
      block: "start"
    });
  }

  _checkAnswer(variants) {
    this.loading = true;
    // проверить ответ
    let data = new FormData();
    data.append('testId', this.id);
    data.append('questionId', this.currentQuestion.id);

    variants.forEach(variant => {
      data.append('answers[]', variant);
    });
    

    API(this.checkMethod, null, data)
      .then(res => {
        this.currentQuestion.setResult(res);
        loadImages();
        this.loading = false;
      })
      .catch(e => {
        this.loading = false;
        console.error(e);
      });
    // разблокировать кнопку Дальше
    this.elements.next.disabled = false;
  }
}

TestView.texts = {
  'toNext': 'Следующий вопрос',
  'finish': 'Закончить тест'
};

export default TestView;