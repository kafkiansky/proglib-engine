import TestViewVariant from "./test-view-variant";

class TestViewVariantText extends TestViewVariant {

  get templateSelector() {
    return "#test-variant-text-template";
  }

  _setData() {
    let text = this.element.querySelector(TestViewVariantText.selectors.text);
    text.innerHTML = this.data.text;
  }
}

TestViewVariantText.selectors = {
  text: '[data-variant-text]',
};

export default TestViewVariantText;