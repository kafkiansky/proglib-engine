import Dispatcher from "../../../../services/dispatcher";
class TestViewVariant {
  constructor(variantData, questionId) {
    new Dispatcher(this);

    this.data = variantData;
    this._render();
    this._checked = false;
    
    this._isCorrect = false;
    this._isIncorrect = false;

    let buttons = this.element.querySelectorAll(TestViewVariant.selectors.button);
    buttons.forEach(btn => {
      btn.addEventListener('click', () => this._onClick());
    });
  }

  get id() {
    return this.data.id;
  }

  get value() {
    return this.id;
  }

  get checked() {
    return this._checked;
  }

  set checked(value) {
    this._checked = Boolean(value);
    this.element.classList.toggle('checked', this._checked);
  }

  set correct(value) {
    value = Boolean(value);
    this._isCorrect = value;
    this._isIncorrect = !value;
    this.element.classList.toggle('correct', this._isCorrect);
    this.element.classList.toggle('incorrect', this._isIncorrect);
  }

  set disabled(value) {
    value = Boolean(value);
    this.element.classList.toggle('disabled', value);
  }

  get templateSelector() {
    return "";
  }


  _render() {
    let tmp;
    try {
      tmp = document.querySelector(this.templateSelector);
      this.element = tmp.content.firstElementChild.cloneNode(true);
    } catch(e) {
      throw new Error('No template for test variant' + e);
    }
    
    this._setData();
  }

  _setData() {

  }

  _onClick() {
    this.trigger(TestViewVariant.events.click);
  }
}

TestViewVariant.selectors = {
  button: '[data-variant-button]'
};

TestViewVariant.events = {
  click: 'test.variant.events.click'
}

export default TestViewVariant;