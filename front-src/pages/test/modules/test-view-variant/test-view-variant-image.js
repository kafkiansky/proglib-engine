import TestViewVariant from "./test-view-variant";

class TestViewVariantImage extends TestViewVariant {
  get templateSelector() {
    return "#test-variant-image-template";
  }

  _setData() {
    let image = this.element.querySelector('img');
    image.src = this.data.url;
  }
}

export default TestViewVariantImage;