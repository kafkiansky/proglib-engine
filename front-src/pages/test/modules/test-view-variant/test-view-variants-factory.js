import TestViewVariantText from "./test-view-variant-text";
import TestViewVariantImage from "./test-view-variant-image";
import TestViewVariantCode from "./test-view-variant-code";

class TestViewVariantsFactory {
  create(data) {
    switch(data.type) {
      case "text":
        return new TestViewVariantText(data);
      case "image":
        return new TestViewVariantImage(data);
      case "code":
        return new TestViewVariantCode(data);
      default:
        throw new Error("Unknown test variant type: " + data.type);
    }
  }
}

export default new TestViewVariantsFactory();