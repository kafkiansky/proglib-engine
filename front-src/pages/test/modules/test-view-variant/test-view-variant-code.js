import TestViewVariant from "./test-view-variant";

class TestViewVariantCode extends TestViewVariant {
  get templateSelector() {
    return "#test-variant-code-template";
  }

  _setData() {
    let text = this.element.querySelector(TestViewVariantCode.selectors.text);
    text.innerHTML = this.data.text;
  }
}

TestViewVariantCode.selectors = {
  text: '[data-variant-code]',
};

export default TestViewVariantCode;