/**
 * account sidebar
 */
export default function initSidebar() {
    const sidebar = document.querySelector('[data-account-sidebar]');
    const content = document.querySelector('[data-account-content]');
    const container = document.querySelector('[data-account-container]'); 

    const avatarLoader = document.querySelector('.user-avatar');

    const toSidebar = document.querySelector('[data-account-back]');
    const toContent = document.querySelector('[data-account-forward]');

    let canSwitch = window.getComputedStyle(sidebar).position == "absolute";

    const bodyOpenedAccountClass = "account-opened";
    const switchDurationMs = 800;

    const check = () => {
        canSwitch = window.getComputedStyle(sidebar).position == "absolute";
        if (!canSwitch) {
            container.style.minHeight = "";
            sidebar.style.transition = "";
            sidebar.style.transform = "";
            content.style.transition = "";
            content.style.opacity = "";
            content.style.display = "";
            document.body.classList.remove(bodyOpenedAccountClass);
            return false;
        }
        return true;
    };

    addHandler("resize", check);

    const openSidebar = () => {
        if (!check()) return;
        document.body.classList.add(bodyOpenedAccountClass);

        let height = sidebar.offsetHeight;
        container.style.minHeight = height + "px";

        sidebar.style.transition = "transform " + switchDurationMs + "ms";
        content.style.transition = "opacity " + switchDurationMs / 2 + "ms";

        sidebar.style.transform = "translateX(0%)";
        content.style.opacity = 0;

        setTimeout(() => {
            content.style.display = "none";
            
        }, switchDurationMs);
    };

    const openContent = () => {
        if (!check()) return;

        sidebar.style.transition = "transform " + switchDurationMs / 2 + "ms";
        content.style.transition = "opacity " + switchDurationMs * 2 + "ms";

        sidebar.style.transform = "translateX(-100%)";
        content.style.opacity = 0;
        content.style.display = "";
        content.style.opacity = 1;

        setTimeout(() => {
            document.body.classList.remove(bodyOpenedAccountClass);
            container.style.minHeight = "";
        }, switchDurationMs);
    };

    toSidebar.addEventListener('click', openSidebar);
    toContent.addEventListener('click', (e) => {
        if (avatarLoader.compareDocumentPosition(e.target) & 16) return;
        openContent();
    });
};
