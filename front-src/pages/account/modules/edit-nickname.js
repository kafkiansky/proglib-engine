const selectors = {
  nickname: '.nickname',
  nicknameView: '.nickname__view',
  nicknameEdit: '.nickname__edit',
  nicknameText: '.nickname__text',
  nicknameInput: 'input',
  nicknameError: '.error',
  nicknameLoad: '[data-load]',
};

const apiMethod = "user_nickname_change";
const nicknameMinLength = 3;

const errors = {
  emptyNickname: 'Имя не может быть пустым',
  shortNickname: 'Минимум 3 символа',
  nonunique: 'Неуникальный ник',
}

let nicknameView, nicknameEdit, nicknameText, nicknameInput, nicknameError, nicknameLoad;

const showError = (html) => {
  nicknameError.innerHTML = html;
};

const showEditBlock = () => {
  nicknameView.classList.add('hidden');
  nicknameEdit.classList.remove('hidden');
};

const hideEditBlock = () => {
  nicknameView.classList.remove('hidden');
  nicknameEdit.classList.add('hidden');
};

const startLoading = () => {
  nicknameLoad.setAttribute('data-loading', '');
};

const stopLoading = () => {
  nicknameLoad.removeAttribute('data-loading');
};

const setOrigin = () => {
  nicknameInput.value = nicknameInput.dataset.origin;
};

const setNickname = nickname => {
  nicknameText.textContent = nickname;
  nicknameInput.value = nickname;
  nicknameInput.dataset.origin = nickname;
};

const submit = () => {
  let value = nicknameInput.value;

  if (value == nicknameInput.dataset.origin) {
    hideEditBlock();
    return;
  }

  if (!value) {
      showError(errors.emptyNickname);
      return;
  }

  if (value.length < nicknameMinLength) {
      showError(errors.shortNickname);
      return;
  }

  let data = new FormData();
  data.append('nickname', value);

  startLoading();

  API(apiMethod, null, data)
    .then((res) => {
        stopLoading();
        if (res.success) {
            showError("");
            setNickname(res.nickname);
            hideEditBlock();
        } else {
            if (res.error == "nonunique")
                showError(errors.nonunique);
            else if (res.error == "length") 
                showError(errors.shortNickname);
            else showError(res.error);
        }
    })
    .catch((e) => {
        stopLoading();
        hideEditBlock();
        console.error(e);
    });
};

const initElements = () => {
  let nickname = document.querySelector(selectors.nickname);

  nicknameView = nickname.querySelector(selectors.nicknameView);
  nicknameEdit = nickname.querySelector(selectors.nicknameEdit);

  nicknameText = nicknameView.querySelector(selectors.nicknameText);
  nicknameInput = nicknameEdit.querySelector(selectors.nicknameInput);
  nicknameError = nicknameEdit.querySelector(selectors.nicknameError);
  nicknameLoad = nicknameEdit.querySelector(selectors.nicknameLoad);

  nickname.addEventListener('click', (e) => {
    e.stopPropagation();
  });

  nicknameView.addEventListener('click', () => {
    showEditBlock();
  });

  nicknameEdit.addEventListener('submit', function(e) {
    e.preventDefault();
    submit();
  });

  window.addHandler('click', function(e) {
    if (!e.target.closest(selectors.nickname)) {
        hideEditBlock();
        setOrigin();
    }
  });
};

export default initElements;