import CustomFileLoader from "../../../components/loader/modules/loader";

const loaderSelector = ".user-avatar";
const userpicSelector = ".user-avatar__image";

export default () => {
  /* загрузчик аватара */
  let $avatarLoader = document.querySelector(loaderSelector);
  let userpic = document.querySelector(userpicSelector);
  new CustomFileLoader($avatarLoader, {
      onlyImages: true,
      onChange: src => {
          userpic.style.backgroundImage = "url(" + src + ")";
      }
  });
}