import loadUserpick from "./modules/load-userpick";
import editNickname from "./modules/edit-nickname";

window.onReady('dom', () => {
  loadUserpick();
  editNickname();
});

