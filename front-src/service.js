import "./service.scss";

import readyObj from "./services/on-ready";
window.ready = readyObj.ready;
window.onReady = readyObj.onReady;

window.checkAuth = () => {
  return window.authorized;
}

document.addEventListener('DOMContentLoaded', () => window.ready('dom'));

// добавление обработчиков к событиями scroll, resize, document.click
import addHandler from "./services/events-handlers";
window.addHandler = addHandler;

window.helpers = {};

// определение правильного окончания слов с числительными
import getWordEnding from "./helpers/get-word-ending";
window.helpers.getWordEnding = getWordEnding;