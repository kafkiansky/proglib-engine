/**
 * обработка событий документа
 * scroll, resize - с задержкой
 * click
 */

import throttler from "../helpers/throttle";

const handlers = Object.create(null);

const handler = (eventName, holdTime) => throttler(e => {
    const callbacks = handlers[eventName];
    if (!callbacks || callbacks.length == 0) return;
    callbacks.forEach((cb, index) => {
        let stop = () => {
            callbacks.splice(index, 1);
        }
        cb(e, stop);
    });
}, holdTime);

window.addEventListener('scroll', handler('scroll', 200));
window.addEventListener('resize', handler('resize', 200));

const addHandler = (eventName, cb) => {
    if (!handlers[eventName]) handlers[eventName] = [];
    handlers[eventName].push(cb);
};

document.addEventListener('click', e => {
    const callbacks = handlers['click'];
    if (!callbacks || callbacks.length == 0) return;
    callbacks.forEach(cb => cb(e));
});

export default addHandler;