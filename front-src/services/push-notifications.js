/** toggle push notifications */
;(function() {
    let pushTogglers = Array.from(document.querySelectorAll('input[name="push-notifications-toggle"]'));
    let block = false;
    pushTogglers.forEach(function(toggler) {
      toggler.addEventListener('change', function() {
        if (block) return;

        block = true;
        let checked = this.checked;

        pushTogglers.forEach(function(toggler) {
          toggler.checked = checked;
        });

        let postData = new FormData();
        postData.append("push", checked);

        API('store_pushes', null, postData)
          .catch(e => {});

        block = false;
      });
    });
})();