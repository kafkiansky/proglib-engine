/**
 * Функционал генерации событий и регистрации обработчиков
 * @class
 * @property {Object} obj объект, который подключает диспетчер с помощью композиции
 * @property {Function[]} callbacks массив коллбэков для событий
 */
class Dispatcher {
  /**
   * @constructor
   * @param {Object} obj 
   */
  constructor(obj) {
    this.obj = obj;
    this.callbacks = {};

    this.obj.on = this.on.bind(this);
    this.obj.once = this.once.bind(this);
    this.obj.off = this.off.bind(this);
    this.obj.trigger = this.trigger.bind(this);
  }

  /**
   * Подписка на событие
   * @param {string} eventName название события
   * @param {Function} callback обработчик события
   */
  on(eventName, callback) {
    if (!this.callbacks[eventName]) this.callbacks[eventName] = [];
    let cb = (...data) => {
      callback(...data);
    };
    this.callbacks[eventName].push(cb);
  }

  /**
   * Разовая подписка на событие
   * @param {string} eventName название события
   * @param {Function} callback обработчик события
   */
  once(eventName, callback) {
    if (!this.callbacks[eventName]) this.callbacks[eventName] = [];
    let cb = (...data) => {
      callback(...data);
      this.off(eventName, cb);
    }
    this.callbacks[eventName].push(cb);
  }

  /**
   * Отписка от события
   * @param {string} eventName название события
   * @param {Function} callback обработчик события
   */
  off(eventName, callback) {
    if (this.callbacks[eventName]) {
      let index = this.callbacks[eventName].indexOf(callback);
      if (index !== -1) {
        this.callbacks[eventName].splice(index, 1);
      }
    }
  }

  /**
   * Вызов события на объекте - вызов всех обработчиков
   * @param {string} eventName название события
   * @param {*} data данные для коллбэков
   */
  trigger(eventName, data) {
    if (this.callbacks[eventName]) {
      this.callbacks[eventName].forEach(cb => cb(data));
    }
  }
}

export default Dispatcher;