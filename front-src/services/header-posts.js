let postsCounters = [];
let count = 0;

const updateCounters = () => {
  postsCounters.forEach(counter => {
    if (count) counter.textContent = count;
    
    counter.classList.toggle('hidden', count <= 0);
  });
}

window.onReady('dom', () => {
  postsCounters = Array.from(document.querySelectorAll('[data-posts-count]'));
  if (postsCounters.length) {
    count = parseInt(postsCounters[0].textContent.trim()) || 0;
  }
  updateCounters();
});

export default () => {
  count++;
  updateCounters();
};
