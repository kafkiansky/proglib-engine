import addHandler from "../events-handlers";

let deletes = {
  "post": {
    method: "post_delete",
    error: "Не удалось удалить статью"
  },
  "test": {
    method: "post_delete",
    error: "Не удалось удалить тест"
  }
}

addHandler('click', e => {
  let deleter = e.target.closest('[data-delete]');
  if (deleter) {
    let deleteType = deleter.dataset.delete;
    if (!deleteType || !deletes[deleteType]) return;

    let method = deletes[deleteType].method;
    let objectId = deleter.dataset.deleteId;

    if (!method || !objectId) return;

    let reloadPath = deleter.dataset.deleteReload;

    API(method, { id: objectId })
      .then(res => {
        if (res.success) {
          if (reloadPath) window.location.href = reloadPath;
          else {
            let deleteItem = deleter.closest('[data-delete-item]');
            if (deleteItem) deleteItem.remove();
          }
        } else {
          window.showAlert(deletes[deleteType].error, "top-right", "error");
        }
      })
      .catch(e => {
        console.error(e);
        window.showAlert(deletes[deleteType].error, "top-right", "error");
      });
  }
});