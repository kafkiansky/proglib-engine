import centrifuge from "./centrifuge";
import notifications from "./header-notifications";
import addPost from "./header-posts";
import { addComment } from "../components/comments-feed/script";

centrifuge.subscribePrivate("alerts", data => {
  if (data.notificationsCount) {
    notifications.updateCounters(data.notificationsCount);
  }
  
});

centrifuge.subscribePublic("comments", data => {
  if (data.comment) {
    addComment(data.comment);
  }
});


centrifuge.subscribePublic("wall", data => {
  if (data.newPost) {
    addPost();
  }
});