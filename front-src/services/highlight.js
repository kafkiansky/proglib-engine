/**
 * Подсветка синтаксиса в блоках кода
 * 
 * Библиотека highlight.js https://highlightjs.org/
 * Тема a11y-dark
 * Файлы тем: https://github.com/highlightjs/highlight.js/tree/master/src/styles
 * Демо https://highlightjs.org/static/demo/
 */

import "../scss/libs/highlight/a11y-dark.min.scss";
import hljs from "highlight.js";

const defaultClassName = "code-wrapper";

/**
 * Находит указание на язык в классах и атрибутах блока
 * @param {HTMLElement} codeBlock 
 * @returns {string|null}
 */
const getLangFromClasses = (сodeBlock) => {
    let classes = сodeBlock.classList;
    let lang;
    classes.forEach(function(blockClass) {
        if (blockClass.indexOf('lang:') == 0) {
            lang = blockClass.slice(5);
        }
    });
    return lang;
}

/**
 * Оборачивает содержимое блока в тег code
 * @param {HTMLElement} codeBlock 
 * @param {HTMLCodeElement} 
 */
const createCodeWrapper = (codeBlock) => {
    let code = document.createElement('code');
    code.innerHTML = codeBlock.innerHTML;
    codeBlock.innerHTML = "";
    codeBlock.appendChild(code);

    return code;
}

/**
 * Форматирует блок кода в вид pre>code
 * @param {HTMLElement} codeBlock 
 */
const formatCodeBlock = (codeBlock, className) => {
    let lang = getLangFromClasses(codeBlock);
    
    let child = codeBlock.firstElementChild;
    if (!child || child.tagName.toUpperCase() !== "CODE") {
        child = createCodeWrapper(codeBlock);
    }

    if (className) {
        codeBlock.classList.add(className);
    }
    
    if (lang) {
        child.classList.add(lang);
    }

    return child;
}

/**
 * Форматирует найденные блоки кода
 * @param {HTMLElement} content Родительский контейнер
 * @param {string} [className] Имя класса для обработанного блока
 */
const format = (content, className = defaultClassName) => {
  content = content || document.body;

  let $blocks = Array.from(content.querySelectorAll('pre'));
  return $blocks.map(block => formatCodeBlock(block, className));
};

/**
 * Подсвечивает синтаксис во всех найденных блоках
 */
const init = () => {
    hljs.initHighlighting();
}

const initBlock = (block) => {
    let blocks = format(block);
    blocks.forEach(block => hljs.highlightBlock(block));
}

window.highlight = { format, init, initBlock };

window.ready('highlight');

