export default (popup) => () => {
    if (!window.authorized) {
        popup.open();
        return false;
    }
    return true;
}