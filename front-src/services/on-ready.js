const onReady = function(scripts, callback) {
  if (!Array.isArray(scripts)) scripts = [scripts];
  scripts = scripts.filter(function(script) {
    return onReady.ready.indexOf(script) == -1;
  });

  if (scripts.length) {
      onReady.callbacks.push({
        cb: callback,
        scripts: scripts
      });
  } else {
    callback();
  }
};
onReady.callbacks = [];
onReady.ready = [];

const ready = function(script) {
  onReady.ready.push(script);
  for (let i = onReady.callbacks.length - 1; i >= 0; i--) {
    let cb = onReady.callbacks[i];
    if (!cb) break;
    let scriptIndex = cb.scripts.indexOf(script);
    if (scriptIndex !== -1) cb.scripts.splice(scriptIndex, 1);
    if (cb.scripts.length === 0) {
      onReady.callbacks.splice(i, 1);
      cb.cb();
    }
  }
};

let readyObj = { ready, onReady };

export default readyObj;