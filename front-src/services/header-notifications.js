import ToggleBlock from "../modules/dom/toggle-block";

let authorizationMessage = "<div class='empty'><span class='link' onclick='window.authorizationPopup.open()'>Авторизуйтесь</span>, чтобы получать оповещения</div>";

let selectors = {
    notifications: '.notifications', // кнопка в меню
    notificationsIcon: '.ico, .count', // иконка, клик по которой открывает попап
    //notificationsButton: "[data-show-notifications]",
    notificationsPopup: ".notifications-list", // попап со списком уведомлений
    notificationsList: ".notifications-list__content", // сам список
    notificationsCounter: "[data-notifications-count]", // счетчики
    notificationsMarkButton: "[data-notifications-mark]", // кнопки Отметить все уведомления прочитанными 
};

let notificationsCounters = []; // счетчики уведомлений
let markNotificationsButtons = []; // кнопки Отметить все уведомления прочитанными
let originTitle = document.title;

let updateCounters = (count = 0) => {
    count = +count;
    notificationsCounters.forEach(counter => {
        if (count && count > 0) {
            counter.textContent = count;
            counter.classList.remove("hidden");
            document.title = `(${count}) ${originTitle}`;
        } else {
            counter.classList.add("hidden");
            document.title = originTitle;
        }
    });
    markNotificationsButtons.forEach(btn => {
        btn.classList.toggle("hidden", !count || count == 0)  
    });
}

window.updateNotificationsCounters = updateCounters;

window.onReady('dom', () => {
    notificationsCounters = Array.from(document.querySelectorAll(selectors.notificationsCounter));
    markNotificationsButtons = Array.from(document.querySelectorAll(selectors.notificationsMarkButton));

    let notifications = Array.from(document.querySelectorAll(selectors.notifications)); // кнопка в меню

    let emptyData = new FormData();
    
    notifications.forEach(el => {
        let notificationsList = el.querySelector(selectors.notificationsList); // список уведомлений
        let notificationsBlock = new ToggleBlock(el, {
            name: "notifications-list",
            opener: selectors.notificationsIcon,
            hidden: selectors.notificationsPopup,
            toggler: false,
            beforeOpen: function(cb) {
                loadNotification(cb);
            }
        });
        let loadNotification = (openPopup, closePopup) => {
            if (!window.userId) {
                notificationsList.innerHTML = authorizationMessage;
                openPopup();
                return;
            }

            notificationsList.innerHTML = "";
            notificationsList.classList.add('loading');
            // показать попап
            openPopup();

            // получить первые 3-5 непрочитанных уведомлений
            
            API("notifications_fetch", {}, emptyData)
                .then(res => {
                    if (res.success) {
                        notificationsList.classList.remove('loading');
                        
                        notificationsList.insertAdjacentHTML("afterBegin", res.html);

                        // уменьшить счетчики
                        updateCounters(res.remainingCount);
                    }
                })
                .catch(error => {
                    console.error(error);
                    notificationsList.classList.remove('loading');
                    
                    if (error.status === 403) {
                        notificationsList.innerHTML = authorizationMessage;
                    }
                });
        }
        
    });

    markNotificationsButtons.forEach(btn => {
        btn.addEventListener('click', () => {

            API('mark_all_notifications_as_read', null, emptyData)
                .then(res => {
                    if (res.success) {
                        updateCounters(0);
                    }
                })
                .catch(e => console.error(e));
        });
    })
});

export default {
    updateCounters: updateCounters
}