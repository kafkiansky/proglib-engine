import addHandler from "../../events-handlers";

/**
 * Reaction
 * @class
 * @description Реакция пользователя
 * @property {string} type Тип реакции (лайк, добавление в закладки)
 */
class Reaction {
  constructor(config) {
    this.type = config.type;
    this.needAuthorization = config.needAuthorization;

    addHandler("click", (e) => {
      let reactionButton = e.target.closest(`[${this.reactionButtonAttribute}]`);
      if (!reactionButton) return;

      if (this.needAuthorization) {
        if (!window.authorized) {
          window.authorizationPopup.open();
          return;
        }
      }

      let reactionContainer = e.target.closest(`[${this.reactionContainerAttribute}]`);
      if (!reactionContainer) return;

      let reactionObjectType = reactionContainer.dataset[this.reactionObjectTypeProperty];
      let reactionObjectId = reactionContainer.dataset[this.reactionObjectIdProperty];
      let reactionObjectSlug = reactionContainer.dataset[this.reactionObjectSlugProperty];
      if (!reactionObjectType || !(reactionObjectId || reactionObjectSlug)) return;

      reactionButton.setAttribute('data-loading', '');

      let marks = this.getMarks(reactionContainer);
      let counters = this.getCounters(reactionContainer);
     
      let isReactionDone = reactionContainer.hasAttribute(this.reactionDoneAttribute);
      let action = isReactionDone ? this.undoActionName : this.doActionName;

      let method = action + reactionObjectType;

      this.send(method, reactionObjectId, reactionObjectSlug)
        .then(res => {
          if (isReactionDone) {
            reactionContainer.removeAttribute(this.reactionDoneAttribute);
            marks.forEach(mark => mark.classList.remove(this.markClass));
          } else {
            reactionContainer.setAttribute(this.reactionDoneAttribute, "");
            marks.forEach(mark => mark.classList.add(this.markClass));
          }
          reactionButton.removeAttribute('data-loading');
          counters.forEach(counter => counter.textContent = res[this.resultCountFieldName]);
        })
        .catch(e => {
          reactionButton.removeAttribute('data-loading');
          console.error(e);
        });
    })
  }

  /**
   * Возвращает имя атрибута кнопки, осуществляющей реакцию
   * @returns {string}
   */
  get reactionButtonAttribute() {
    return "data-" + this.type;
  }

  /**
   * Возвращает имя атрибута маркируемых элементов реакции
   * @returns {string}
   */
  get reactionMarkAttribute() {
    return "data-" + this.type + "-mark";
  }

  /**
   * Возвращает имя атрибута счетчика реакции
   * @returns {string}
   */
  get reactionCounterAttribute() {
    return "data-" + this.type + "s";
  }

  /**
   * Возвращает имя класса-маркера
   * @returns {string}
   */
  get markClass() {
    return "marked";
  }

  /**
   * Возвращает имя контейнера реакции
   * @returns {string}
   */
  get reactionContainerAttribute() {
    return "data-" + this.type + "-wrapper";
  }

  /**
   * Возвращает имя data-свойства контейнера реакции, содержащего тип реакции
   */
  get reactionObjectTypeProperty() {
    return this.type + "Wrapper";
  }

  /**
   * Возвращает имя data-свойства контейнера реакции, содержащего id объекта реакции
   */
  get reactionObjectIdProperty() {
    return "id";
  }

  get reactionObjectSlugProperty() {
    return "slug";
  }

  get reactionDoneAttribute() {
    return "data-" + this.type + "-done";
  }

  get doActionName() {
    return this.type + "_";
  }

  get undoActionName() {
    return "un" + this.type + "_";
  }

  get resultCountFieldName() {
    return "count";
  }

  getMarks(reactionContainer) {
    return Array.from(reactionContainer.querySelectorAll(`[${this.reactionMarkAttribute}]`));
  }

  getCounters(reactionContainer) {
    return Array.from(reactionContainer.querySelectorAll(`[${this.reactionCounterAttribute}]`));
  }

  send(method, reactionObjectId, reactionObjectSlug) {
    let data = new FormData();
    return API(method, { id: reactionObjectId, slug: reactionObjectSlug }, data);
  }
}

export default Reaction;