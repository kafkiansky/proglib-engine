import Reaction from "./reaction";

class ReactionBookmark extends Reaction {
  constructor() {
    super({
      type: "bookmark",
      needAuthorization: true
    });
  }

  get reactionDoneAttribute() {
    return "data-bookmarked";
  }

  get resultCountFieldName() {
    return "bookmarksCount";
  }
}

export default ReactionBookmark;