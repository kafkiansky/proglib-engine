import Reaction from "./reaction";

class ReactionLike extends Reaction {
  constructor() {
    super({
      type: "like",
      needAuthorization: true
    });
  }

  get reactionDoneAttribute() {
    return "data-liked";
  }

  get resultCountFieldName() {
    return "likesCount";
  }
}

export default ReactionLike;