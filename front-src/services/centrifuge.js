import Centrifuge from "centrifuge";

let centrifuge, user;

const connect = () => {
  let url = document.querySelector('meta[name=centrifugo-url]').getAttribute('content');
  let token = document.querySelector('meta[name=centrifugo-token]').getAttribute('content');
  centrifuge = new Centrifuge(url);

  centrifuge.setToken(token);
  centrifuge.connect();
}

const subscribe = (channel, callback) => {
  if (!centrifuge) connect();
  centrifuge.subscribe(channel, message => callback(message.data));
}

const subscribePrivate = (channel, callback) => {
  if (!user) user = document.querySelector('meta[name=centrifugo-user]').getAttribute('content');
  subscribe(channel + '#' + user, callback);
}

const subscribePublic = (channel, callback) => {
  subscribe(`public:${channel}`, callback);
}

const publish = (channel, data) => {
  if (!centrifuge) connect();
  return centrifuge.publish(channel + "#" + user, data);
}

export default {
  connect, subscribePrivate, subscribePublic, subscribe, publish
};