import "./app.scss";

import sslide from "./modules/libs/sslide";
window.sslide = sslide;
window.ready('sslide');

// функция вывода всплывающих сообщений
import showAlert from "./components/popup/show-alert";
window.showAlert = showAlert("float-alert");
window.ready('show-alert');

// всплывающие окна
import Popup from "./components/popup/popup";
window.Popup = Popup;
window.onReady('dom', () => {
  Popup.init();
  window.ready('popup');
});

// вызов окна подтверждения
import confirm from "./components/popup/confirm";
window.confirm = confirm;
window.ready('confirm-popup');

// проверка авторизации
import checkAuth from "./services/check-auth";
// вызов окна авторизации
import authPopupInit from "./components/popup/auth-popup";

window.checkAuth = (function() {
  let checkFunction = null;
  window.onReady('auth-popup', () => {
    checkFunction = checkAuth(window.authorizationPopup);
    window.ready('auth-check');
  });
  let checker = () => {  
    if (checkFunction) return checkFunction();
    else window.onReady('auth-check', () => checker());
  }
  return checker;
})();

authPopupInit
  .then(authPopup => {
    window.authorizationPopup = authPopup;
    window.ready('auth-popup');
    window.checkAuth = () => {
      if (!window.authorized) {
        authPopup.open();
        return false;
      }
      return true;
    }
  });

// раскрывающиеся блоки
import ToggleBlock from "./modules/dom/toggle-block";
window.ToggleBlock = ToggleBlock;
window.ready('toggle-block');
window.onReady('dom', () => ToggleBlock.init());

// плавная прокрутка якорей
import "./modules/dom/anchors";

// закладки и лайки
import "./services/reactions/reactions";

// увеличение изображений по клику
import "./modules/dom/increase-images";

// удаление статьи
import "./services/actions/delete";

// слайдер
import Slider from "./modules/dom/slider";
window.ready('slider');
window.onReady('dom', () => Slider.init());
document.body.addEventListener('feed.events.update', () => Slider.init());

// view bindings
import "./modules/dom/view-bindings";

// переключение тумблера push-уведомлений
import "./services/push-notifications";

// ленивая загрузка изображений
import lazyload from "./modules/dom/lazyload";
window.onReady('dom', () => lazyload());
document.body.addEventListener('feed.events.update', () => lazyload());

// валидатор
import "./components/c-form/validator/validator";

// socket messages
import "./services/user-alerts";

// уведомления в шапке
import "./services/header-notifications";

// Пожаловаться
import "./components/popup/complain";

import "./modules/dom/sidebar";

import "./components/search-form/search-form";

import cityField from "./components/city-field/city-field";
window.onReady('dom', () => {
  let cityInputs = Array.from(document.querySelectorAll('[data-city-field]'));
  cityInputs.forEach(input => cityField(input));
})