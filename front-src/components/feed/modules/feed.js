import Dispatcher from "../../../services/dispatcher";

class Feed {
    constructor(config) {
        this.config = config || {};

        this.page = 1;
        this.filters = this.config.filters || [];

        this.el = config.el || document.body;

        this.container = this.el.querySelector('[data-load-container]');
        this._method = config.method;

        this.loadButton = this.el.querySelector('.load-more');

        this._pause = false;

        this.filters.forEach(filter => {

            filter.on("filter.events.change", data => {
                if (!this._pause && !this.config.explicitUpdate) {
                    this.update(filter);
                }
            });
        });
        this.loadButton.addEventListener('click', () => this.loadMore());

        this.getEmptyMessage = config.getEmptyMessage;

        if (config.reset) {
            config.reset.addEventListener('click', () => {
                
                this._pause = true;
                this.filters.forEach(filter => {
                    filter.reset();
                });
                
                this.update();
                this._pause = false;
            })
        }


        new Dispatcher(this);
    }

    get method() {
        if (typeof this._method === "function") return this._method();
        else return this._method;
    }

    updateFeed(html) {
        html = html.trim();
        
        if (html.length) {
            this.container.innerHTML = html;
        } else {
            let message;
            if (this.getEmptyMessage) {
                let filters = this.getFilters();
                message = this.getEmptyMessage(filters);
            }
            if (!message) message = this.getDefaultEmptyMessage();
            this.container.innerHTML = `<div class="empty">${message}</div>`;
        }
    }

    getDefaultEmptyMessage() {
        return "Нет результатов";
    }

    addCards(html) {
        this.container.insertAdjacentHTML("beforeEnd", html);
    }

    getFilters() {
        return this.filters.reduce((filtersData, filter) => Object.assign(filtersData, filter.getData()), {});
    }

    getParams() {
        let filterParams = this.getFilters();
        let params = new FormData();
        for (let param in filterParams) {
            params.append(param, filterParams[param]);
        }
        params.append('page', this.page);

        if (this.config.request) {
            for (let param in this.config.request) {
                params.append(param, this.config.request[param]);
            }
        }

        return params;
    }

    /* обновился фильтр */
    update(filter) {
        if (filter) {
            filter.el.setAttribute('data-progress', '');
        }
        this.page = 1;
        let params = this.getParams();
        
        this.getCards(params)
            .then(res => {
                this.updateFeed(res.html);
                this.toggleLoadButton(res.remainingPages);
                this.onUpdate(res);
                if (filter) {
                    filter.el.removeAttribute('data-progress');
                }
                
            })
            .catch(err => {
                console.error(err);
                if (filter) {
                    filter.el.removeAttribute('data-progress');
                }
                
                this.toggleLoadButton(0);
            });
    }

    /* подгрузить еще */
    loadMore() {
        this.page++;
        let params = this.getParams();
        this.loadButton.setAttribute('data-progress', '');
        this.getCards(params)
            .then(res => {
                this.addCards(res.html);
                this.toggleLoadButton(res.remainingPages, res.remainingItems);
                this.onUpdate(res);
                this.loadButton.removeAttribute('data-progress');
            })
            .catch(err => {
                console.error(err);
                this.loadButton.removeAttribute('data-progress');
                this.toggleLoadButton(0);
            });
    }

    toggleLoadButton(remainingPages, remainingItems) {
        this.loadButton.classList.toggle("hidden", remainingPages <= 0); 
        if (this.config.getMoreText) {
            this.loadButton.innerHTML = this.config.getMoreText(remainingItems);
        }
    }

    getCards(params) {
        return API(this.method, {}, params)
            .then(res => {
                if (!res)  throw new Error('Empty response')
                return res;
            })
            .catch(error => {
                if (this.config.onError) this.config.onError(error);
            });
    }

    onUpdate(res) {
        document.body.dispatchEvent(new Event(Feed.events.update));
        this.trigger(Feed.events.update, res);

        if (window.highlight) {
            window.highlight.initBlock(this.container);
        }
    }
}

Feed.events = {
    update: 'feed.events.update'
}

export default Feed;