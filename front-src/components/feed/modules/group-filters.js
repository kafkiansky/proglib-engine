import Filters from "./filters";

class GroupFilters extends Filters {
  constructor(element, field) {
    super(element, field);

    this.selected = this.checked;
  }

  _handleItem(item) {
    let input = item.el.querySelector('input');
    item.input = input;

    input.addEventListener('change', () => this._onChange());
  }

  get checked() {
    return this.items.filter(item => item.input.checked)
  }

  get selected() {
    return this._selectedItems || [];
  }

  set selected(items = []) {
    this._selectedItems = items;
    this._setActive(items);
  }

  get value() {
    return this.selected.map(item => item.input.value).join(',');
  }

  _onChange() {
    this.selected = this.checked;
    let data = this.getData();
    this.trigger(Filters.events.change, data);
    this.setUrlParams();
  }

  reset() {
    this.items.forEach((item) => {
      item.input.checked = item.el.hasAttribute('data-initial');
      item.input.dispatchEvent(new Event('change', { bubbles: true }));
    })
  }
}

export default GroupFilters;