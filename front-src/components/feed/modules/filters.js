import "./filters.scss";

import Dispatcher from "../../../services/dispatcher";

class Filters {
  constructor(el, field) {
    new Dispatcher(this);

    this.el = el;
    if (!el) throw new Error('No filters element');

    this.field = field;

    this.items = Array.from(this.el.querySelectorAll('[data-filters-item]')).map(el => ({ el }));
    this._selectedItem = null;

    this.activeClass = "active";

    this.items.forEach(item => this._handleItem(item));
  }

  get selected() {
    return this._selectedItem;
  }

  set selected(item) {
    this._selectedItem = item;
    this._setActive([item]);
  }

  get value() {
    let selectedItem = this.selected;
    return selectedItem ? selectedItem.el.dataset.value : null;
  }

  get urlValue() {
    return this.value;
  }

  _handleItem(item) {
    if (item.el.classList.contains(this.activeClass)) this.selected = item;

    item.el.addEventListener('click', (e) => {
      e.preventDefault();
      this._onClick(item);
    });
  }

  getData() {
    let data = {};
    data[this.field] = this.value;
    return data;
  }

  _onClick(item) {
    this.selected = item;
    let data = this.getData();
    this.trigger(Filters.events.change, data);
  }

  _setActive(items = []) {
    this.items.forEach(filterItem => {
      if (items.indexOf(filterItem) !== -1) {
        filterItem.el.classList.add('active');
      } else {
        filterItem.el.classList.remove('active');
      }
    })
  }

  reset() {
    
  }

  setUrlParams() {
    let searchParams = new URLSearchParams(window.location.search);
    let value = this.urlValue;
    if (value) {
      searchParams.set(this.field, value);
    } else {
      searchParams.delete(this.field);
    }
    let searchString = searchParams.toString().replace('%2C', ',')
    history.pushState(null, null, location.origin + location.pathname + "?" + searchString);
  }
}

Filters.events = {
  "change": "filter.events.change",
};

export default Filters;
