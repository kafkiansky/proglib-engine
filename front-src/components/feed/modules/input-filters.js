import Dispatcher from "../../../services/dispatcher";

class InputFilters {
  constructor(element, field) {
    new Dispatcher(this);
    this.el = element;
    let withSuggestions = this.el.classList.contains('with-suggestions');
    
    this.input = this.el.querySelector('input');
    this.input.addEventListener('keydown', e => {
      let code = e.keyCode || e.which;
      if (code == 38 || code == 40) e.preventDefault();
    })

    let eventName = withSuggestions ? 'selected' : 'change';

    this.input.addEventListener(eventName, () => {
      this._onChange()
    });
    this.field = field;
  }

  get value() {
    return this.input.value;
  }

  get urlValue() {
    return this.value;
  }

  getData() {
    let data = {};
    data[this.field] = this.value;
    return data;
  }

  _onChange() {
    let data = this.getData();
    this.trigger("filter.events.change", data);
    this.setUrlParams();
  }

  setUrlParams() {
    let searchParams = new URLSearchParams(window.location.search);
    let value = this.urlValue;
    if (value) {
      searchParams.set(this.field, value);
    } else {
      searchParams.delete(this.field);
    }

    let searchString = searchParams.toString().replace('%2C', ',')
    history.pushState(null, null, location.origin + location.pathname + "?" + searchString);
  }

  reset() {
    this.input.value = '';
    this.input.dispatchEvent(new Event('change', { bubbles: true }));
  }
}

export default InputFilters;