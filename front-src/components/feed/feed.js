import Feed from "./modules/feed";
import Filters from "./modules/filters";

window.Feed = Feed;
window.Filters = Filters;

window.ready('feed');