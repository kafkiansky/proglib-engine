import addHandler from "../../services/events-handlers";

let cities = ["Новосибирск", "Новороссийск", "Новгород"]


export default (input) => {
  let dropdown = document.createElement('div');
  dropdown.className = "suggestions-list";

  let silent = false;
  let items = [];

  input.addEventListener('focus', () => {
    if (items.length) input.parentElement.appendChild(dropdown);
  });

  input.addEventListener('input', () => {
    if (silent) return;

    let term = input.value;

    if (!term) {
      dropdown.innerHTML = '';
      items.length = 0;
      input.dispatchEvent(new Event('selected'));
      return;
    }

    let suggestions = cities.filter(city => city.toLowerCase().indexOf(term.toLowerCase()) !== -1);

    dropdown.innerHTML = '';
    items.length = 0;

    suggestions.forEach(city => {
      let el = document.createElement('div');
      el.className = "suggestions-list__item";
      el.textContent = city;
      items.push(el);

      let setValue = () => {
        silent = true;
        dropdown.remove();
        items.length = 0;
        input.value = city;
        silent = false;

        input.dispatchEvent(new Event('selected'));
      };

      el.addEventListener('click', setValue, {
        once: true
      });

      dropdown.appendChild(el);
    })

    input.parentElement.appendChild(dropdown);
  });

  addHandler('click', e => {
    let inside = input.parentElement.contains(e.target);
    if (!inside) {
      dropdown.remove();
    }
  })
}