import Dispatcher from "../../services/dispatcher";

import Popup from "../popup/popup";
import "./payment-form.scss";

import IMask from 'imask';

import Validator from "../../components/validator/validator";

import paymentSystems from "./payment-systems";

class PaymentForm {
  constructor(form, publicId) {
    new Dispatcher(this);

    this.form = form;
    this.form.addEventListener('submit', (e) => this.onSubmit(e));

    this.fields = this.getFields();

    this.checkout = new cp.Checkout(
      publicId,
      this.form
    );

    this.validator = new Validator();
  }

  set sum(value) {
    let sum = this.form.querySelector('.sum');
    if (sum) {
      sum.innerHTML = `${value.total} ${value.currency}`;
    }
  }

  getFields() {
    let fields = {};

    PaymentForm.fields.forEach(field => {
      let element = this.form.querySelector(`[data-cp="${field.name}"]`);
      let mask = field.mask;

      let formField = {};
      formField.el = element;
      formField.parent = element.parentElement;
      formField.error = document.createElement('div');
      formField.error.classList.add('error');
      if (mask) formField.mask = IMask(element, mask);

      fields[field.name] = formField;
    });

    this._initCardNumberField(fields.cardNumber);


    return fields;
  }

  _initCardNumberField(field) {
    if (!field || !field.el) return;

    field.el.addEventListener('input', (e) => {
      let system = paymentSystems.get(field.mask.unmaskedValue);
      field.parent.setAttribute('data-system', system ? system.label : '');
    })
  }

  validateMail() {
    let value = this.fields.mail.el.value;
    if (!value) return true;
    return this.validator.validateRegex(value, 'mail');
  }

  focus() {
    let firstFieldName = PaymentForm.fields[0].name;
    let field = this.fields[firstFieldName];
    
    if (!field || !field.el) return;

    setTimeout(() => {
      field.el.focus();
    }, 300)
  }

  createCryptogram() {
    return this.checkout.createCryptogramPacket();
  }

  saveData() {
    for (let field in this.fields) {
      this.fields[field].value = this.fields[field].el.value;
    }
  }

  onSubmit(event) {
    event.preventDefault();
    this.saveData();

    if (!this.validateMail()) {
      this.setErrors({ 'mail': 'Некорректный адрес'});
      return;
    }

    const cryptogram = this.createCryptogram();
    if (cryptogram.success) {
      this.setErrors({});
      let data = {};
      data.cryptogram = cryptogram.packet;

      PaymentForm.fields.forEach(fieldObject => {
        let fieldName = fieldObject.name;
        let field = this.fields[fieldName];
        
        if (field)
          data[fieldName] = field.value;
      });

      this.trigger('generated', data)
    }
    else {
      this.setErrors(cryptogram.messages);
    }
  }

  setErrors(errors) {
    for (let fieldName in this.fields) {
      this.setFieldError(fieldName, errors[fieldName]);
    }
  }

  setFieldError(fieldName, error) {
    let field = this.fields[fieldName];

    if (error) {
      field.error.textContent = error;
      field.parent.appendChild(field.error);
      field.el.classList.add('invalid');
    } else {
      field.error.remove();
      field.el.classList.remove('invalid');
    }
  }
}

PaymentForm.fields = [
  {
    name: 'cardNumber',
    mask: {
      mask: '0000 0000 0000 0000[ 000]',
      placeholderChar: '_',
      lazy: false,
    }
  }, 
  {
    name: 'expDateMonth',
    mask: {
      mask: IMask.MaskedRange,
      from: 1,
      to: 12,
      maxLength: 2,
      lazy: false,
      autofix: true
    } 
  }, 
  {
    name: 'expDateYear',
    mask: {
      mask: IMask.MaskedRange,
      from: 0,
      to: 99,
      maxLength: 2,
      lazy: false,
      autofix: true
    } 
  },
  {
    name: 'name'
  },
  {
    name: 'cvv',
    mask: {
      mask: IMask.MaskedRange,
      from: 0,
      to: 999,
      maxLength: 3,
    } 
  },{
    name: 'mail',
  }
];

let paymentPopup, paymentForm;

const init = (cb) => {
  paymentForm = new PaymentForm(
    document.getElementById('paymentForm'),
    "test_api_00000000000000000000001",
  );

  let paymentPopupElement = document.getElementById('payment');
  paymentPopup = new Popup(paymentPopupElement);
  paymentForm.focus();

  paymentForm.on('generated', data => {
    if (cb)
      cb(data);
  })
};

const show = () => {
  paymentPopup.open();
};

const hide = () => {
  paymentPopup.close();
};

const setSum = (sum) => {
  paymentForm.sum = sum;
}


export default {
  init, show, hide, setSum
};