const mir = {
  label: 'mir',
  title: 'MIR'
};

const ae = {
  label: 'ae',
  title: 'American Express'
};

const visa = {
  label: 'visa',
  title: 'VISA'
};

const mc = {
  label: 'mc',
  title: 'MasterCard'
};

const maestro = {
  label: 'maestro',
  title: 'Maestro'
};

const up = {
  label: 'up',
  title: 'UnionPay'
};

const discover = {
  label: 'discover',
  title: 'Discover'
};

const paymentSystemsTree = {
  nodes: {
    '2': { value: mir },
    '3': {
      nodes: {
        '4': { value: ae },
        '7': { value: ae }
      }
    },
    '4': { value: visa },
    '5': {
      nodes: {
        '0': { value: maestro },
        '1': { value: mc },
        '2': { value: mc },
        '3': { value: mc },
        '4': { value: mc },
        '5': { value: mc },
        '6': { value: maestro },
        '7': { value: maestro },
        '8': { value: maestro },
      }
    },
    '6': {
      nodes: {
        '0': { value: discover },
        '2': { value: up },
        '3': { value: maestro },
        '7': { value: maestro },
      }
    }
  }
};

const get = (str) => {
  let index = 0;
  let max = str.length;
  let tree = paymentSystemsTree;

  while(index <= max) {
    if (!tree) return false;
    if (tree.value) return tree.value;
    if (!tree.nodes) return false;
    let char = str[index];
    if (!char) return false;
    tree = tree.nodes[char];
    index++;
  }
}

const paymentSystems = {
  get
};

export default paymentSystems;