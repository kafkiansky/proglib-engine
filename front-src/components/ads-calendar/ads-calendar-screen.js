import Dispatcher from "../../services/dispatcher";

/**
 * @class AdsCalendarScreen
 * @description Экран календаря
 * 
 * @property {Object} elements 
 * @property {HTMLElement} elements.container Контейнер экрана
 * @property {HTMLElement} elements.monthTable HTML-элемент таблицы месяца
 * @property {HTMLElement} elements.month HTML-элемент с названием отображаемого месяца
 * @property {HTMLElement} elements.year HTML-элемент с номером отображаемого года
 * @property {HTMLElement} elements.prev HTML-элемент стрелки Назад
 * @property {HTMLElement} elements.next HTML-элемент стрелки Вперед
 * 
 * @property {Number} year Отображаемый в календаре год
 * @property {Number} month Отображаемый в календаре месяц
 * 
 * @property {HTMLElement[]} selected HTML-элементы выбранных дней в календаре
 * @property {HTMLElement[]} hovered HTML-элементы выделенных наведением дней в календаре
 * @property {Object} days Хеш-таблица всех дней отображаемого месяца (по дате)
 */

class AdsCalendarScreen {
  /**
   * @constructor
   * @param {HTMLElement} container 
   */
  constructor(container) {
    new Dispatcher(this);

    this.selected = [];
    this.hovered = [];
    this.days = Object.create(null);

    this.elements = this._DOMInit(container);
  }

  /** Отображаемый месяц */

  get month() {
    return this._month;
  }
  set month(value) {
    this._month = value;
  }

  /** Отображаемый год */

  get year() {
    return this._year;
  }
  set year(value) {
    if (this._year !== value) {
      this._year = value;
      this.generate();
    }
  }

  /**
   * Возвращает редуцированное значение отображаемого месяца
   */
  get currentReduced() {
    return this._getReduced(this.year, this.month);
  }

  /**
   * Инициализирует DOM-элементы календаря
   * @private
   */
  _DOMInit(container) {
    let monthTable = container.querySelector('.calendar__month');
    let month = container.querySelector('[data-month]');
    let year = container.querySelector('[data-year]');

    return {
      container, monthTable, month, year, 
    };
  }

  /**
   * Возращает количество дней в месяце
   * @private
   * @param {number} year
   * @param {number} month
   * @returns {number}
   */
  _getDaysCount(year, month) {
    return new Date(year, month + 1, 0).getDate();
  }

  /**
   * Генерирует неделю месяца
   * @private
   * @param {number} startDay День недели с которого начинается неделя месяца
   * @param {number} startDate Первая дата на текущей неделе
   * @param {number} finishDate Последнее число месяца
   */
  _generateWeek(startDay, startDate, finishDate) {
  
    let week = document.createElement('tr');
    week.classList.add(AdsCalendarScreen.selectors.week);
    let diff = startDate - startDay;

    for (let i = 1; i <= 7; i++) {
      let day = document.createElement('td');
      week.appendChild(day);
      day.classList.add(AdsCalendarScreen.selectors.cellClass);
      if (i < startDay) continue;

      let date = i + diff;
      if (date > finishDate) continue;

      day.classList.add(AdsCalendarScreen.selectors.dayClass);
      this.days[date] = day;

      day.date = date;
      
      day.textContent = date;

      day.addEventListener('click', () => this._onClick(day));
      day.addEventListener('mouseover', () => this._onHover(day));
      day.addEventListener('mouseout', () => this._onBlur(day));
    }

    return week;
  }

  /**
   * Генерирует таблицу месяца
   * @private
   * @param {number} year 
   * @param {number} month 
   */
  _generateMonth(year, month) {
    let daysCount = this._getDaysCount(year, month);

    let table = document.createDocumentFragment();

    let firstDayOfTheWeek = new Date(year, month, 1).getDay(); 

    let startDay = firstDayOfTheWeek; // первый день текущей недели
    if(startDay === 0) startDay = 7;
    let startDate = 1;

    while(startDate <= daysCount) {
      let week = this._generateWeek(startDay, startDate, daysCount);
      table.appendChild(week);
      startDate = startDate + (7 - startDay) + 1;
      startDay = 1;
    }

    return table;
  }

  /**
   * Генерирует календарь
   */
  generate() {
    this.days = Object.create(null);

    this.elements.month.textContent = AdsCalendarScreen.months[this.month];
    this.elements.year.textContent = this.year;

    let table = this._generateMonth(this.year, this.month);

    this.elements.monthTable.innerHTML = '';
    this.elements.monthTable.appendChild(table);
  }

  /**
   * Выделяет сегодняшний день
   */
  setCurrentDay(day) {
    if (this.year === day.year && this.month === day.month) {
      this.days[day.date].classList.add(AdsCalendarScreen.selectors.currentClass);
    }
  }

  /**
   * Отмечает указанные дни месяца css-классом
   * @param {number} startDate 
   * @param {number} endDate 
   * @param {string} className 
   */
  _selectDays(startDate, endDate, className) {
    let days = [];

    for (let date in this.days) {
      if (date >= startDate && date <= endDate) {
        this.days[date].classList.add(className);
        days.push(this.days[date]);
      }
    }

    return days;
  }

  /**
   * Выделяет дни выбранного периода
   * @param {DateObject} periodStartDate Начало периода
   * @param {DateObject} periodEndDate Конец периода
   * @param {Boolean} preview Предпросмотр периода
   */
  setSelectedDays(periodStartDate, periodEndDate, preview) {
    if (!periodStartDate) return;
    
    let currentReduced = this.currentReduced;
    let periodStartReduced =  periodStartDate.reduced;
    let periodEndReduced = periodEndDate.reduced;

    // срок размещения закончился раньше текущего месяца
    if (currentReduced > periodEndReduced) return;
    // срок размещения начнется позже текущего месяца
    if (currentReduced < periodStartReduced) return;

    let startDate = currentReduced > periodStartReduced ? 0 : periodStartDate.date;
    let endDate = currentReduced < periodEndReduced ? 33 : periodEndDate.date;

    let className = AdsCalendarScreen.selectors[preview ? 'hoverClass' : 'selectedClass'];
    let days = this._selectDays(startDate, endDate, className);

    if (preview) this.hovered = days;
    else this.selected = days;
  }

  /**
   * Выделяет прошедшие дни, которые нельзя выбрать
   */
  setInactiveDays(nowDay) {
    let nowReduced = nowDay.reduced;
    let currentReduced = this.currentReduced;

    if (currentReduced > nowReduced) {
      return;
    }

    let startDate = 0;
    let finishDate = 33;

    if (currentReduced >= nowReduced) {
      finishDate = nowDay.date - 1;
    }

    let days = this._selectDays(startDate, finishDate, AdsCalendarScreen.selectors.inactiveClass);
    days.forEach(day => day.inactive = true);
  }

  /**
   * Обрабатывает клик по дню
   * @param {HTMLElement} day 
   */
  _onClick(day) {
    if (day.inactive) return;
    this.trigger(AdsCalendarScreen.events.select, new Date(this.year, this.month, day.date));
  }

  /**
   * Обрабатывает наведение курсора на день
   * @param {HTMLElement} day 
   */
  _onHover(day) {
    this.trigger(AdsCalendarScreen.events.hover, day.inactive ? null : new Date(this.year, this.month, day.date));
  }

  /**
   * Обрабатывает снятие курсора c дня
   * @param {HTMLElement} day 
   */
  _onBlur() {
    this.hovered.forEach(day => day.classList.remove(AdsCalendarScreen.selectors.hoverClass));
    this.hovered.length = [];

    this.trigger(AdsCalendarScreen.events.blur);
  }

  clearSelected() {
    this.selected.forEach(el => el.classList.remove(AdsCalendarScreen.selectors.selectedClass));
    this.selected.length = 0;
  }

  clearHovered() {
    this.hovered.forEach(day => day.classList.remove(AdsCalendarScreen.selectors.hoverClass));
    this.hovered.lenght = 0;
  }
}

AdsCalendarScreen.months = [
  'январь', 'февраль', 
  'март', 'апрель', 'май', 
  'июнь', 'июль', 'август', 
  'сентябрь', 'октябрь', 'ноябрь', 
  'декабрь'
];

AdsCalendarScreen.selectors = {
  selectedClass: "selected",
  currentClass: "current",
  inactiveClass: "inactive",
  hoverClass: "hover",
  weekClass: "calendar__week",
  dayClass: "calendar__day",
  cellClass: "calendar__cell",
};

AdsCalendarScreen.events = {
  select: 'calendar.events.select.day',
  hover: 'calendar.events.hover.day',
  blue: 'calendar.events.blur'
}

export default AdsCalendarScreen;