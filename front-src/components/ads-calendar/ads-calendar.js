import "./ads-calendar.scss";
import Dispatcher from "../../services/dispatcher";
import AdsCalendarScreen from "./ads-calendar-screen";

/**
 * @typedef {Object} DateObject
 * @description Объект хранения даты для календаря
 * @property {Number} year Полное значение года
 * @property {Number} month Индекс месяца [0, 11]
 * @property {Number} date Дата месяца [1, 31]
 */

/**
 * @class AdsCalendar
 * @description Календарь размещения рекламы
 * 
 * @property {HTMLElement} element Контейнера календаря
 * @property {Object} elements 
 * @property {HTMLElement} elements.monthTable HTML-элемент таблицы месяца
 * @property {HTMLElement} elements.month HTML-элемент с названием отображаемого месяца
 * @property {HTMLElement} elements.year HTML-элемент с номером отображаемого года
 * @property {HTMLElement} elements.prev HTML-элемент стрелки Назад
 * @property {HTMLElement} elements.next HTML-элемент стрелки Вперед
 * 
 * @property {HTMLElement[]} selected HTML-элементы выбранных дней в календаре
 * @property {HTMLElement[]} hovered HTML-элементы выделенных наведением дней в календаре
 * @property {Object} days Хеш-таблица всех дней отображаемого месяца (по дате)
 * 
 * @property {DateObject} now Сегодняшний день
 * 
 * @property {Number} period Количество дней, на которое размещается реклама
 * @property {DateObject|null} periodStartDate Первый день выбранного периода
 * @property {DateObject|null} periodEndDate Последний день выбранного периода
 * 
 * @property {Number} year Отображаемый в календаре год
 * @property {Number} month Отображаемый в календаре месяц
 */
class AdsCalendar {
  /**
   * @constructor
   * @param {HTMLElement} container 
   * @param {Object} config Конфигурационный объект
   * @param {Number} config.period Количество дней, на которое размещается реклама
   * @param {Number} [config.initialDate] Первый день выбранного периода
   */
  constructor(container, config) {
    new Dispatcher(this);

    this._period = config.period;
    this.selected = [];
    this.hovered = [];
    this.days = Object.create(null);

    this.element = container;
    this.screens = [];

    this.now = this._dateToObject(new Date());

    if (config.initialDate) {
      this.periodStartDate = this._dateToObject(config.initialDate)
    }
    
    this._month = this.periodStartDate ? this.periodStartDate.month : this.now.month;
    this._year = this.periodStartDate ? this.periodStartDate.year : this.now.year;

    this.elements = this._DOMInit();

    this.generate();
  }

  /** Отображаемый месяц */

  get month() {
    return this._month;
  }
  set month(value) {
    if (this._month !== value) {
      this._month = value;
      this.generate();
    }
  }

  /** Отображаемый год */

  get year() {
    return this._year;
  }
  set year(value) {
    if (this._year !== value) {
      this._year = value;
      this.generate();
    }
  }

  /** Первый день выбранного периода */

  get periodStartDate() {
    return this._periodStartDate || null;
  }
  set periodStartDate(dateObject) {
    if (!dateObject) return;
    
    this._periodStartDate = dateObject;
    this.periodEndDate = this._getEndDate(this._periodStartDate);
    this.screens.forEach(screen => {
      screen.clearSelected();
      screen.setSelectedDays(this.periodStartDate, this.periodEndDate)
    });
    
    this.trigger('selected');
  }

  /** Дата первого дня выбранного периода */
  get periodStart() {
    return this.periodStartDate ? this._objectToDate(this.periodStartDate) : null;
  }

  /** Дата последнего дня выбранного периода */
  get periodEnd() {
    return this.periodEndDate ? this._objectToDate(this.periodEndDate) : null;
  }

  /** Количество дней, на которое размещается реклама */

  get period() {
    return this._period;
  }
  set period(value) {
    this._period = value;

    this.periodStartDate = this.periodStartDate;
  }

  /**
   * Возвращает редуцированное значение отображаемого месяца
   */
  get currentReduced() {
    return this._getReduced(this.year, this.month);
  }

  /**
   * Форматирует объект даты календаря в обычную дату
   * @private
   * @param {DateObject} dateObject 
   */
  _objectToDate(dateObject) {
    return new Date(
      dateObject.year, 
      dateObject.month,
      dateObject.date
    )
  }

  /**
   * Форматирует обычную дату в объект даты календаря
   * @private
   * @returns {DateObject} dateObject 
   */
  _dateToObject(date) {
    let year = date.getFullYear();
    let month = date.getMonth();
    return {
      year: year,
      month: month,
      date: date.getDate(),
      reduced: this._getReduced(year, month)
    };
  }

  /**
   * Возвращает редуцированное значение месяца
   * @param {number} year 
   * @param {number} month 
   */
  _getReduced(year, month) {
    return year * 100 + month;
  }

  /**
   * Инициализирует DOM-элементы календаря
   * @private
   */
  _DOMInit() {
    let screens = Array.from(this.element.querySelectorAll('.calendar__screen'));
    this.screens = screens.map(el => this._initScreen(el));

    let prev = this.element.querySelector('[data-prev]');
    prev.addEventListener('click', () => {
      this.month = this.month - 1;
    });
    let next = this.element.querySelector('[data-next]');
    next.addEventListener('click', () => {
      this.month = this.month + 1;
    });

    return {
      prev, next
    };
  }

  _initScreen(el) {
    let screen = new AdsCalendarScreen(el);
    screen._getReduced = this._getReduced.bind(screen);

    screen.on(AdsCalendarScreen.events.select, (date) => this.selectDay(date));
    screen.on(AdsCalendarScreen.events.hover, (date) => this.previewPeriod(date));
    screen.on(AdsCalendarScreen.events.blur, () => {
      this.screens.forEach(screen => screen.clearHovered());
    });
    return screen;
  }

  /**
   * Вычисляет последний день выбранного периода по первому
   * @private
   * @param {DateObject} startDate
   */
  _getEndDate(startDate) {
    let endDate;
    if (this.period === "month") {
      endDate = new Date(
        startDate.year, 
        startDate.month + 1,
        startDate.date - 1
      );
    }
    return this._dateToObject(endDate);
  }

  /**
   * Генерирует календарь
   */
  generate() {
    let date, screen;

    for (let i = 0; i < this.screens.length; i++) {
      date = new Date(this.year, this.month + i);
      screen = this.screens[i];
      screen.year = date.getFullYear();
      screen.month = date.getMonth();
      screen.generate();
      screen.setCurrentDay(this.now);
      screen.setSelectedDays(this.periodStartDate, this.periodEndDate);
      screen.setInactiveDays(this.now);
    }
  }

  /**
   * Отмечает выбранный день как начало выбранного периода
   * @param {HTMLElement} day 
   */
  selectDay(date) {
    this.screens.forEach(screen => screen.clearSelected());
    this.periodStartDate = this._dateToObject(date);
  }

  previewPeriod(date) {
    this.screens.forEach(screen => screen.clearHovered());

    if (date) {
      let expectedPeriodStartDate = this._dateToObject(date);
      let expectedPeriodEndDate = this._getEndDate(expectedPeriodStartDate);

      this.screens.forEach(screen => screen.setSelectedDays(expectedPeriodStartDate, expectedPeriodEndDate, "preview"));
    }
  }
}

export default AdsCalendar;