import TestElement from '../test-element/test-element';
import TestVariantsFactory from "../test-variant/test-variants-factory";
import EditorJS from  "@editorjs/editorjs";
import testEvents from "../test-events";

/**
 * @typedef {Object} QuestionData
 * @description Данные вопроса
 * @property {string|Number} id идентификатор вопроса
 * @property {Object[]|string} text текст вопроса
 * @property {Object[]} variants - массив вариантов теста
 * @property {Boolean} multiple - несколько вариантов ответа
 * @property {Object[]|string} description - пояснение к вопросу
 */

/**
 * Вопрос теста
 * @class 
 *
 * @property {Object[]} description пояснение к вопросу
 * 
 * @property {TestVariant[]} variants - массив объектов для вариантов вопроса
 * @property {TestVariantsFactory} factory - фабрика вариантов
 * 
 * @property {HTMLElement} view.variants - dom-элемент списка вариантов
 * @property {HTMLElement} view.description - dom-элемент пояснения к вопросу
 * @property {EditorJS} view.descriptionEdition - редактор пояснения к вопросу
 * @property {HTMLInputElement} view.multiple - чекбокс множественного выбора
 * 
 * @property {Boolean} multiple - вопрос со множественным выбором
 */
class TestQuestion extends TestElement {
  /**
   * @constructor
   * @param {QuestionData} [data] начальные данные
   */
  constructor(data = {}, editorConfig) {
    super(data, testEvents.question, TestQuestion.selectors, editorConfig);
    this._description = this._formatText(data.description);

    let variants = data.variants || [];
    let answers = data.answers || [];
    variants.forEach(variant => {
      variant.correct = answers.indexOf(variant.id) !== -1;
    });

    this.variants = this._handleVariants(variants);

    this._multiple = this.isMultiple() ? true : data.multiple;
  }

  /**
   * @returns {Boolean}
   */
  get multiple() {
    return this._multiple;
  }

  /**
   * @returns {string|Object[]}
   */
  get description() {
    return this._description;
  }

  /**
   * @param {Object[]} value
   */
  set description(value) {
    this._description = value;
  }

  /**
   * @param {Boolean} value
   */
  set multiple(value) {
    this._multiple = !!value;
    if (this.view) {
      this.view.multiple.checked = this._multiple;
    }
  }

  get answers() {
    return this.variants.filter(variant => variant.correct).map(variant => variant.id);
  }
 
  /**
   * JSON-представление для БД
   * @override
   * @returns {QuestionData}
   */
  toJSON() {
    return {
      order: this.order,
      id: this.id,
      text: this.text,
      description: this.description,
      variants: this.variants.map(variant => variant.toJSON()),
      multiple: this.multiple,
      answers: this.variants.filter(variant => variant.correct).map(variant => variant.id),
    };
  }

  /**
   * Создает объекты вариантов теста, устанавливает обработчики
   * @private
   * @param {Object[]} variants 
   * @returns {TestVariant[]}
   */
  _handleVariants(variants = []) {
    return variants.map(variant => {
      let variantObject = TestQuestion.factory.create(variant);
      if (variantObject) {
        variantObject.on(testEvents.variant.change, () => this._onVariantChange());
        variantObject.on(testEvents.variant.delete, () => this._onVariantDelete(variantObject));
      }
      return variantObject;
    }).filter(variant => variant);
  }

  /**
   * Создание представления для редактирования 
   * @override
   * @returns {HTMLElement}
   */
  _createView() {
    let view = super._createView();

    /* пояснение к вопросу */
    let description = view.element.querySelector(TestQuestion.selectors.description);
    description.id = this._getEditorHolderId("description");
    let descriptionEditor = new EditorJS(this._getDescriptionEditorConfig(description));

    /* выбрать тип варианта */
    let showPopupButton = view.element.querySelector(TestQuestion.selectors.chooseVariantType);
    let variantsPopup = $(view.element.querySelector(TestQuestion.selectors.variantsPopup));
    showPopupButton.addEventListener('click', () => variantsPopup.modal('show'));
    /* создать вариант нужного типа */
    let types = Array.from(view.element.querySelectorAll(TestQuestion.selectors.createVariant));
    types.forEach(type => {
      type.addEventListener('click', () => {
        variantsPopup.modal('hide');
        this._createVariant(type.dataset[TestQuestion.selectors.createVariantProp]);
      });
    });

    /* список вариантов */
    let variants = view.element.querySelector(TestQuestion.selectors.list);
    this.variants.forEach(variant => {
      variants.appendChild(variant.getView());
    });

    /* чекбокс множественный выбор */
    let multiple = view.element.querySelector(TestQuestion.selectors.multiple);
    multiple.addEventListener('change', () => this.toggleMultiple());
    multiple.checked = this.multiple;

    Object.assign(view, {
      variants: variants, 
      multiple: multiple,
      description: description,
      descriptionEditor: descriptionEditor,
    });
    
    return view;
  }

  /**
   * Конфигурация редактора пояснения к вопросу
   * @private
   * @param {string} holderId - уникальный идентификатор контейнера для редактора
   * @param {Function} [onReady] - коллбэк для полной инициализации редактора
   * @returns {Object}
   */
  _getDescriptionEditorConfig(holder, onReady) {
    let tools = this._getEditorToolsConfig(['image', 'code', 'header', 'embed']);

    holder.addEventListener('editor-change', e => {
      this._saveDescriptionEditor()
          .then(() => {
            this.trigger(testEvents.question.change);
          });
    });
    let config = {
      "holder": holder,
      "tools": tools,
      "onChange": () => {
        this._saveDescriptionEditor()
          .then(() => {
            this.trigger(testEvents.question.change);
          });
      },
      "data": {
        blocks: this.description
      }
    };

    if (onReady) config.onReady = onReady;

    return config;
  }

  /**
   * Сохранение редактора пояснения к вопросу
   * @private
   * @returns {Promise}
   */
  _saveDescriptionEditor() {
    return this.view.descriptionEditor.save()
      .then(data => {
        this.description = data.blocks;
      });
  }

  /**
   * Создание нового варианта ответа
   * @private
   * @param {String} type 
   */
  _createVariant(type) {
    let variant = TestQuestion.factory.create({ type: type });
    this.variants.push(variant);
    this.view.variants.appendChild(variant.getView());
    variant.focus();
    this.trigger(testEvents.question.change);
    variant.on(testEvents.variant.change, () => this._onVariantChange());
    variant.on(testEvents.variant.delete, () => this._onVariantDelete(variant));
  }

  /**
   * Коллбэк для изменения варианта
   */
  _onVariantChange() {
    this.multiple = this.isMultiple();
    this.trigger(testEvents.question.change);
  }

  /**
   * Коллбэк для удаления варианта
   * @param {TestVariant} variant 
   */
  _onVariantDelete(variant) {
    let index = this.variants.indexOf(variant);
    this.variants.splice(index, 1);
    this._onVariantChange(variant);
  }

  /**
   * @override
   */
  getValidationErrors() {
    let errors = [];
    if (!this.text.length) errors.push('Нет текста вопроса');
    if (!this.variants.length) errors.push('Нет ни одного варианта');
    if (!this.variants.every(variant => variant.validate().isValid)) errors.push('Ошибка создания варианта');
    if (!this.variants.some(variant => variant.isCorrect())) errors.push('Нет правильных вариантов');
    return errors;
  }

  /**
   * @override
   */
  _saveData() {
    this.variants.forEach(variant => variant.save());
    
    return Promise.all([
        this._saveEditor,
        this._saveDescriptionEditor
      ]);
  }

  /**
   * @override
   */
  _onDelete() {
    this.variants.forEach(variant => variant.delete());
  }

  /**
   * Считает количество правильных ответов
   * @returns {Boolean}
   */
  isMultiple() {
    let correctVariants = this.variants.filter(variant => variant.correct);
    return correctVariants.length > 1;
  }

  /**
   * Изменяет параметр множественного выбора
   */
  toggleMultiple() {
    let current = this.multiple;
    let isMultiple = this.isMultiple();

    if (isMultiple && current) this.multiple = current;
    else {
      this.multiple = !current;
      this.trigger(testEvents.question.change);
    }
  }
}

/**
 * @static
 */
TestQuestion.counter = 0;

/**
 * @static
 */
TestQuestion.factory = new TestVariantsFactory();
/**
 * @static
 */
TestQuestion.selectors = {
  "viewTemplateId": "test-question-template",
  "previewTemplateId": "test-question-preview-template",
  "description": "[data-test-question-description]",
  "chooseVariantType": "[data-test-choose-variant]",
  "createVariant": "[data-test-create-variant]",
  "createVariantProp": "testCreateVariant",
  "variantsPopup": ".test-question__variants-popup",
  "list": "[data-test-variants]",
  "multiple": "[data-test-question-multiple]",
}

export default TestQuestion;

