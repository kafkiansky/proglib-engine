import TestQuestion from "./test-question";

/**
 * Фабрика вопросов теста
 * @class
 */
class TestQuestionsFactory {
  constructor(config) {
    this.config = config;
  }
  /**
   * Создание вопроса
   * @param {QuestionData=} data 
   * @returns {TestQuestion}
   */
  create(data) {
    try {
      let question = new TestQuestion(data, this.config);
      return question;
    } catch(e) {
      return false;
    }
  }
}

export default TestQuestionsFactory;