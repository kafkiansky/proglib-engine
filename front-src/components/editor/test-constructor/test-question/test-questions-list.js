import TestElementsList from "../test-element/test-elements-list";
import TestQuestionsFactory from "./test-questions-factory";
import testEvents from "../test-events";

class TestQuestionsList extends TestElementsList {
  constructor(config) {
    config.events = testEvents.question;
    config.factory = TestQuestionsFactory;
    super(config);

    this.saveUrl = config.saveUrl;
    this.deleteUrl = config.deleteUrl;

    this.on(testEvents.question.save, (data) => {
      let json = data.element.toJSON();
      let formData = new FormData();
      formData.append('testId', this.testId);
      formData.append('question', JSON.stringify(json));

      fetch(this.saveUrl, {
        method: "POST",
        body: formData
      })
        .then(res => {

        })
        .catch(err => {
          console.error(err);
        });
    });

    this.on(testEvents.question.delete, (data) => {
      let formData = new FormData();
      formData.append('testId', this.testId);
      formData.append('questionId', data.element.id);

      fetch(this.deleteUrl, {
        method: "POST",
        body: formData
      })
        .then(res => {

        })
        .catch(err => {
          console.error(err);
        });
    });
  }
}

export default TestQuestionsList;