import TestResult from "./test-result";

/**
 * Фабрика результатов теста
 * @class
 */
class TestResultsFactory {
  constructor(config) {
    this.config = config;
  }
  /**
   * Создание результата
   * @param {ResultData=} data 
   * @returns {TestResult}
   */
  create(data) {
    try {
      let result = new TestResult(data, this.config);
      return result;
    } catch(e) {
      return false;
    }
  }
}

export default TestResultsFactory;