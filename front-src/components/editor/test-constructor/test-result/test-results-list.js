import TestElementsList from "../test-element/test-elements-list";
import TestResultsFactory from "./test-results-factory";
import testEvents from "../test-events";

class TestResultsList extends TestElementsList {
  constructor(config) {
    config.factory = TestResultsFactory;
    config.events = testEvents.result;
    super(config);

    this.on(testEvents.result.sort, () => this.recountResultsRanges());
    this.on(testEvents.result.update, () => this.recountResultsRanges());
    this.on(testEvents.result.range, (data) => this.onChangeRange(data));
  }

  recountResultsRanges() {
    let count = this.length;
    let diff = Math.floor(100 / count);

    this.every((element, i) => {
      let from = diff * i;
      let to = from + diff;
      if (i == count - 1) to = 100;
      element.range = {
        from: from,
        to: to
      };
      return true;
    });

    this.trigger(testEvents.result.change);
  }

  /**
   * Коллбэк для изменения диапазона результата теста
   * @param {TestResult} result 
   * @param {string} boundary 
   */
  onChangeRange(data) {
    let result = data.element;
    let newValue = data.data;

    if (this.isFirst(result) && newValue.from) {
      result.range = result.range;
      return;
    };
    if (this.isLast(result) && newValue.to) {
      result.range = result.range;
      return;
    }

    if (newValue.from) {
      let prev = this.prev(result);
      result.range = {
        from: newValue.from
      };
      prev.range = {
        to: newValue.from
      };
    } else if (newValue.to) {
      let next = this.next(result);
      result.range = {
        to: newValue.to
      };
      next.range = {
        from: newValue.to
      };
    }

    this.trigger(testEvents.result.change);
  }
}

export default TestResultsList;