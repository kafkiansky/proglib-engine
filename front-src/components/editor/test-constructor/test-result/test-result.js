import TestElement from '../test-element/test-element';
import testEvents from "../test-events";

/**
 * @typedef Range 
 * @description диапазон значений в процентах [from, to]
 * @property {Number} from начало диапазона (>=0)
 * @property {Number} to конец диапазон (<=100)
 */

/**
 * @typedef {Object} ResultData
 * @description Данные результата
 * @property {string|Number} id идентификатор вопроса
 * @property {Object[]|string} text текст результата
 * @property {Range} диапазон правильных ответов
 * 
 * @property {HTMLInputElement} preview.rangeFrom
 * @property {HTMLInputElement} preview.rangeTo
 */

/**
 * Результат теста
 * @class
 * 
 * @property {Range} range диапазон правильных ответов
 */
class TestResult extends TestElement {
  /**
   * @constructor
   * @param {ResultData} [data] начальные данные
   */
  constructor(data = {}, editorConfig) {
    super(data, testEvents.result, TestResult.selectors, editorConfig);

    this.range = data.range;
  }

  get range() {
    return this._range;
  }

  set range(range) {
    this._range = this.formatRange(range);

    if (this.preview) {
      this.preview.rangeFrom.value = this._range.from;
      this.preview.rangeTo.value = this._range.to;
    }
  }

  /**
   * JSON-представление для БД
   * @override
   * @returns {ResultData}
   */
  toJSON() {
    return {
      id: this.id,
      text: this.text,
      range: this.range
    };
  }

  /**
   * Форматирует объект диапазона
   * @param {Range} [range] 
   */
  formatRange(range = {}) {
    let formatted = { from: 0, to: 100};
    Object.assign(formatted, this.range);
    
    if (range.from >= 0 && range.from < 100) formatted.from = range.from;
    if (range.to > 0 && range.to <= 100) formatted.to = range.to;

    return formatted;
  }

  _getTextToolsConfig() {
    return this._getEditorToolsConfig(['image', 'code', 'embed', 'header', 'button']);
  }

  /**
   * @override
   */
  _createPreview() {
    let preview = super._createPreview();

    let range = preview.element.querySelector(TestResult.selectors.range);
    let rangeFrom = range.querySelector(TestResult.selectors.rangeFrom);
    rangeFrom.addEventListener("change", () => {
      this.trigger(testEvents.result.range, { "from": rangeFrom.value });
    });
    rangeFrom.value = this.range.from;
    let rangeTo = range.querySelector(TestResult.selectors.rangeTo);
    rangeTo.addEventListener("change", () => {
      this.trigger(testEvents.result.range, { "to": rangeTo.value });
    })
    rangeTo.value = this.range.to;

    preview.rangeFrom = rangeFrom;
    preview.rangeTo = rangeTo;

    return preview;
  }

  /**
   * Создание представления для редактирования 
   * @override
   * @returns {HTMLElement}
   */
  _createView() {
    let view = super._createView();

    // TODO: add range
    
    return view;
  }

  /**
   * @override
   */
  _saveData() {
    // TODO: save range
    
    return super._saveData();
  }

  /**
   * @override
   */
  getValidationErrors() {
    let errors = [];
    if (!this.text.length) errors.push('Нет текста результата');

    // TODO add range validation

    return errors;
  }
}

/**
 * @static
 */
TestResult.counter = 0;

/**
 * @static
 */
TestResult.selectors = {
  "viewTemplateId": "test-result-template",
  "previewTemplateId": "test-result-preview-template",
  "range": "[data-range]",
  "rangeFrom": "[data-range-from]",
  "rangeTo": "[data-range-to]",
}

export default TestResult;