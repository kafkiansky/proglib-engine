export default {
  "question": {
    "edit": "test.question.events.edit", // кнопка Редактировать
    "save": "test.question.events.save", // кнопка Сохранить
    "change": "test.question.events.change", // любое изменение
    "delete": "test.question.events.delete", // удаление

    "sort": "test.question.events.sort", // пересортировка списка
    "update": "test.question.events.update", // обновление списка
  },
  "result": {
    "edit": "test.result.events.edit", // кнопка Редактировать
    "save": "test.result.events.save", // кнопка Сохранить
    "change": "test.result.events.change", // любое изменение
    "delete": "test.result.events.delete", // удаление

    "sort": "test.result.events.sort", // пересоортировка списка
    "update": "test.result.events.update", // обновление списка
    
    "range": "test.result.events.range", // изменение диапазона
  },
  "variant": {
    "change": "test.variant.events.change", // любое изменение
    "delete": "test.variant.events.delete", // удаление
  }
};