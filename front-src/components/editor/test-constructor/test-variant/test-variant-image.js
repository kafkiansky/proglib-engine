import TestVariant from "./test-variant";
import CustomFileLoader from "../../../../components/loader/modules/loader";
import testEvents from "../test-events";

/**
 * Вариант вопроса в виде картинки
 * @class 
 * @property {string} url адрес картинки
 * @property {CustomFileLoader} loader загрузчик изображения
 */
class TestVariantImage extends TestVariant {
  /**
   * @constructor
   * @param {VariantData=} data данные существующего варианта
   */
  constructor(data = {}) {
    super("test-variant-image-template", data);
    this.url = data.url || "";
  }

  
  toJSON() {
    return {
      id: this.id,
      type: "image",  
      url: this.url
    };
  }

  createView() {
    let view = super.createView();

    let loader = view.element.querySelector(TestVariantImage.selectors.loader);
    this.loader = new CustomFileLoader(loader, {
      onAddUrl: src => {
       
        this.url = src;
        this.trigger(testEvents.variant.change);
      },
      onRemoveUrl: () => {

        this.url = "";
        this.trigger(testEvents.variant.change);
      },
    });

    if (this.url) this.loader.addUrl(this.url);
    
    return view;
  }

  validate() {
    let errors = [];
    if (this.url.length > 0) {
      this.isValid = true;
    } else {
      this.isValid = false;
      errors.push('image');
    }

    return {
      isValid: this.isValid,
      errors: errors
    }
  }
}

TestVariantImage.selectors = {
  loader: ".file-loader",
}

export default TestVariantImage;