import TestVariant from "./test-variant";
import testEvents from "../test-events";

/**
 * Вариант вопроса в виде текста
 * @class 
 * @property {string} text текст вопроса
 */
class TestVariantText extends TestVariant {
  /**
   * @constructor
   * @param {VariantData=} data данные существующего варианта
   */
  constructor(data) {
    super("test-variant-text-template", data);
    this.text = data.text || "";
  }

  toJSON() {
    return {
      id: this.id, 
      type: "text",
      text: this.text,
    };
  }

  createView() {
    let view = super.createView();

    let text = view.element.querySelector(TestVariantText.selectors.text);
    text.innerHTML = this.text;
    text.addEventListener('input', () => {
      this.text = text.innerHTML.trim();
      this.trigger(testEvents.variant.change);
    });

    view.text = text;
    return view;
  }

  validate() {
    let errors = [];
    if (this.text.length > 0) {
      this.isValid = true;
    } else {
      this.isValid = false;
      errors.push('text');
    }

    return {
      isValid: this.isValid,
      errors: errors
    }
  }

  save() {
    this.text = this.view.text.innerHTML;
  }

  focus() {
    this.view.text.focus();
  }
}

/**
 * @static
 */
TestVariantText.selectors = {
  text: ".test-variant__text",
};

export default TestVariantText;