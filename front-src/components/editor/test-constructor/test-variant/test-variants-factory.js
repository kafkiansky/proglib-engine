import TestVariantText from "./test-variant-text";
import TestVariantCode from "./test-variant-code";
import TestVariantImage from "./test-variant-image";

const variantsTypes = {
  "text": TestVariantText,
  "code": TestVariantCode,
  "image": TestVariantImage
};

/**
 * Фабрика вариантов для вопросов теста
 * @class
 */
class TestVariantsFactory {
  /**
   * Создание варианта
   * @param {VariantData=} data 
   * @returns {TestVariant}
   */
  create(data) {
    let variantConstructor = variantsTypes[data.type];
    try {
      if (!variantConstructor) throw new Error("Unknown variant type: " + data.type);
      return new variantConstructor(data);
    } catch(e) {
      return false;
    }
  }
}

export default TestVariantsFactory;

