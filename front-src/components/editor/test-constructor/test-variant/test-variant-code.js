import TestVariant from "./test-variant";
import testEvents from "../test-events";

/**
 * Вариант вопроса в виде текста
 * @class 
 * @property {string} text текст вопроса
 */
class TestVariantCode extends TestVariant {
  /**
   * @constructor
   * @param {VariantData=} data данные существующего варианта
   */
  constructor(data = {}) {
    super("test-variant-code-template", data);
    this.text = data.text || "";
  }

  toJSON() {
    return {
      id: this.id,
      type: "code",
      text: this.text
    };
  }

  createView() {
    let view = super.createView();

    let text = view.element.querySelector(TestVariantCode.selectors.text);
    text.value = this.text;
    text.addEventListener('input', () => {
      this.text = text.value;
      this.trigger(testEvents.variant.change);
    });

    view.text = text;
    return view;
  }

  validate() {
    let errors = [];
    if (this.text.length > 0) {
      this.isValid = true;
    } else {
      this.isValid = false;
      errors.push('text');
    }

    return {
      isValid: this.isValid,
      errors: errors
    }
  }

  save() {
    this.text = this.view.text.value;
  }

  focus() {
    this.view.text.focus();
  }
}

/**
 * @static
 */
TestVariantCode.selectors = {
  text: ".test-variant__code",
};

export default TestVariantCode;