import Dispatcher from "../../../../services/dispatcher";
import testEvents from "../test-events";
import ConfirmButton from "../../../../components/c-form/confirm-button";
import uuidv4 from "uuid";

/**
 * @typedef {Object} VariantData
 * @description Данные вопроса
 * @property {string=} text текст варианта
 * @property {string=} url адрес картинки
 * @property {Object|null} view представление варианта в DOM
 * @property {Boolean} isValid валидность варианта
 */

/**
 * Вариант вопроса
 * @class 
 */
class TestVariant {
  constructor(templateId, data = {}) {
    this._id = data.id || uuidv4();
    this.templateId = templateId;
    this._correct = data.correct;
    this.isValid = true;
    new Dispatcher(this)
  }

  toJSON() {
    return {
      id: this.id
    }
  }

  get id() {
    return this._id;
  }

  get correct() {
    return this._correct;
  }

  set correct(value) {
    this._correct = !!value;
    if (this.view)
      this.view.correct.classList.toggle('active', this._correct);
      this.view.correct.classList.toggle('btn-outline-success', this._correct);
  }

  /**
   * Представление варианта
   * @returns {HTMLElement}
   */
  getView() {
    if (!this.view) {
      this.view = this.createView();
    }
    return this.view.element;
  }

  /**
   * Создание представления
   */
  createView() {
    let template = document.getElementById(this.templateId);
    let view = template.content.cloneNode(true).firstElementChild;

    let deleteBtn = new ConfirmButton(view.querySelector(TestVariant.selectors.delete), () => this.delete());
   
    let correctBtn = view.querySelector(TestVariant.selectors.correct);
    correctBtn.addEventListener('click', () => this.toggleCorrect());
    
    correctBtn.classList.toggle('active', !!this.correct);
    correctBtn.classList.toggle('btn-outline-success', !!this.correct);

    return {
      element: view,
      delete: deleteBtn,
      correct: correctBtn
    };
  }

  /**
   * Валидация варианта
   */
  validate() {
    return {
      isValid: this.isValid,
      errors: []
    };
  }

  /**
   * Верный вариант
   */
  toggleCorrect() {
    let currentCorrect = this.correct;
    
    this.correct = !currentCorrect;
    this.trigger(testEvents.variant.change);
  }

  /**
   * Верный вариант
   * @returns {Boolean}
   */
  isCorrect() {
    return this.correct;
  }

  /**
   * Удалить вариант
   * @param {Boolean} silent
   */
  delete(silent) {
    if (this.view) {
      this.view.element.remove();
      this.view = null;
    }
    if (!silent) this.trigger(testEvents.variant.delete);
  }

  /**
   * Сфокусироваться на варианте
   */
  focus() {

  }

  /**
   * Сохранить данные из dom
   */
  save() {

  }
}

/**
 * @static
 */
TestVariant.selectors = {
  correct: "[data-correct]",
  delete: "[data-delete]"
}

export default TestVariant;




