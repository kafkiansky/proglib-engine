import Dispatcher from "../../../../services/dispatcher";
import sortable from "html5sortable/dist/html5sortable.es";

/**
 * Список элементов теста (вопросов, результатов)
 * @class
 * @property {Function} factory фабрика элементов
 * @property {Object} events объект с названиями событий
 */
class TestElementsList {
  /**
   * @constructor
   * @param {Object} config 
   * @param {Object[]} [config.listData] исходный массив данных элементов
   * @param {Object} config.events объект с названиями событий
   * @param {string} config.events.save
   * @param {string} config.events.change
   * @param {string} config.events.edit
   * @param {string} config.events.delete
   * @param {string} config.events.sort
   */
  constructor(config) {
    new Dispatcher(this);
  
    this.factory = new config.factory(config.editorConfig);

    this.events = config.events;

    this.testId = config.testId;

    this.list = this.handleList(config.listData);
    this.list.forEach((element, index) => {
      this.handleElement(element, index);
    });
  }

  /**
   * @returns {Number}
   */
  get length() {
    return this.list.length;
  }

  /**
   * Возвращает уникальный идентификатор для контейнера списка элементов
   * @returns {string}
   */
  getUniqueId() {
    return "test-elements-list-" + ++TestElementsList.counter;
  }

  /**
   * Возвращает JSON-представление списка элементов
   * @returns {Object[]}
   */
  toJSON() {
    return JSON.stringify(this.list.map(element => element.toJSON()));
  }

  /**
   * Перебор всех элементов списка
   * @param {Function} cb коллбэк для каждого элемента 
   */
  every(cb) {
    let length = this.length;
    for (let i = 0; i < length; i++) {
      let element = this.list[i];
      let result = cb(element, i);
      if (result == null) break;
    }
  }

  /**
   * Возвращает порядковый номер элемента в списке
   * @param {TestElement} element 
   * @returns {Number}
   */
  indexOf(element) {
    return this.list.indexOf(element);
  }

  /**
   * Возвращает элемент по его индексу
   * @param {Number} index 
   * @returns {TestElement}
   */
  getByIndex(index) {
    return this.list[index];
  }

  /**
   * Возвращает предыдущий элемент в списке
   * @param {TestElement} 
   * @returns {TestElement|undefined}
   */
  prev(element) {
    return this.getByIndex(element.listOrder - 1);
  }

  /**
   * Возвращает следующий элемент в списке
   * @param {TestElement} 
   * @returns {TestElement|undefined}
   */
  next(element) {
    return this.getByIndex(element.listOrder + 1);
  }

  /**
   * Проверяет, является ли элемент первым в списке
   * @param {TestElement} 
   * @returns {Boolean}
   */
  isFirst(element) {
    return this.list[0] === element;
  }

  /**
   * Проверяет, является ли элемент последним в списке
   * @param {TestElement} 
   * @returns {Boolean}
   */
  isLast(element) {
    return this.list[this.length - 1] === element;
  }

  /**
   * Инициализация dom-элементов
   * @param {Object} config 
   * @param {HTMLElement} config.list контейнер списка элементов
   * @param {HTMLElement} config.element контейнер для редактирования элемента
   * @param {HTMLElement} config.create кнопка Создать новый элемент
   */
  init(config) {
    this.listContainer = config.list;
    this.elementContainer = config.element;
    this.createButton = config.create;

    this.listContainer.id = this.getUniqueId();

    this.createButton.addEventListener('click', () => this.createElement());

    this.list.forEach(element => this.handleElementPreview(element));
    this.initSortableList("silent");
  }

  /**
   * Создает объекты элементов из данных
   * @param {Object[]} listData 
   * @returns {TestElement[]}
   */
  handleList(listData) {
    listData = listData || [];
    return listData.map(elementData => this.factory.create(elementData)).filter(element => element);
  }

  /**
   * Обработка событий элемента
   * @param {TestElement} element 
   */
  handleElement(element, index) {
    element.order = index;
    element.on(this.events.edit, () => {
      this.editElement(element);
    });
    element.on(this.events.delete, () => {
      let index = this.list.indexOf(element);
      this.list.splice(index, 1);
      this.initSortableList();
    });

    for (let event in this.events) {
      element.on(this.events[event], (data) => {
        this.trigger(this.events[event], { element: element, data: data });
      })
    }
  }

  /**
   * Добавление превью элемента
   * @param {TestElement} element 
   */
  handleElementPreview(element) {
    let preview = element.getPreview();
    this.listContainer.appendChild(preview);
  }

  /**
   * Создание нового элемента
   */
  createElement() {
    let element = this.factory.create();
    this.showElement(element);
    element.once(this.events.save, () => {
      let order = this.list.length;
      this.list.push(element);
      this.handleElementPreview(element);
      this.handleElement(element, order);
      this.initSortableList();

      this.trigger(this.events.save, { element: element });
    })
  }

  /**
   * Открытие элемента для редактирования
   * @param {TestElement} element 
   */
  editElement(element) {
    this.showElement(element);
  }

  /**
   * Вывод элемента для редактирования на страницу
   * @param {TestElement} element 
   */
  showElement(element) {
    this.elementContainer.innerHTML = "";
    this.elementContainer.appendChild(element.getView());
    this.elementContainer.classList.remove('hidden');

    this.elementContainer.scrollIntoView({
      behavior: "smooth",
      block: "center"
    });
  }

  /**
   * Валидация списка
   * @param {Boolean} silent 
   */
  validate(silent) {
    let isValid = true;
    let errors = [];
    this.list.forEach(element => {
      let validation = element.validate(silent);
      errors = [...errors, ...validation.errors];
      if (!validation.isValid) isValid = false;
    });
    return {
      isValid: isValid,
      errors: errors
    };
  }

  /**
   * Пересортировка элементов в списке
   */
  sortList() {
    Array.from(this.listContainer.children).forEach((el, index) => {
      el.listIndex = index;
    });

    this.list.sort((a, b) => {
      return a.preview.element.listIndex - b.preview.element.listIndex;
    });

    this.list.forEach((element, order) => {
      element.order = order;
    });

    this.trigger(this.events.sort);
  }

  /**
   * Инициализация сортируемого dom-списка
   * @param {Boolean} silent триггер события обновления
   */
  initSortableList(silent) {
    sortable("#" + this.listContainer.id, {
      forcePlaceholderSize: true
    })[0].addEventListener('sortupdate', () => {
      this.sortList();
      this.list.forEach((element, index) => {
        element.listOrder = index;
      });
    });
    this.list.forEach((element, index) => {
      element.listOrder = index;
    });
    if (!silent)
      this.trigger(this.events.update);
  }
}

TestElementsList.counter = 0;

export default TestElementsList;