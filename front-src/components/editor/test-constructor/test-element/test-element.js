import Dispatcher from "../../../../services/dispatcher";

import EditorJS from  "@editorjs/editorjs";
import getToolsConfig from "../../../../components/editor/editor-js/get-tools-config";

import ConfirmButton from "../../../../components/c-form/confirm-button";

import uuidv4 from "uuid";

/**
 * Элемент теста (вопрос, результат)
 * @class
 * 
 * @property {Number} index уникальный номер при создании
 * @property {string} id уникальный идентификатор
 * @property {Number} order порядок в списке
 * 
 * @property {Object} events объект с названиями событий
 * @property {string} events.edit кнопка Редактировать
 * @property {string} events.delete удаление 
 * @property {string} events.save сохранение
 * @property {string} events.change любое изменение
 * 
 * @property {Object} selectors объект с селекторами элементов
 * @property {string} selectors.previewTemplateId идентификатор шаблона для превью
 * @property {string} selectors.viewTemplateId идентификатор шаблона для редактора
 * 
 * @property {Object[]} text текст (объект данных editorjs)
 * 
 * @property {Object|null} preview - html-представление в списке
 * @property {HTMLElement} preview.element - dom-элемент превью
 * @property {HTMLElement} preview.text - dom-элемент текста превью
 * @property {HTMLElement} preview.error - dom-элемент текста ошибки
 * 
 * @property {Object|null} view - html-представление для редактирования
 * @property {HTMLElement} view.element - dom-элемент представления
 * @property {HTMLElement} view.save - кнопка сохранить 
 * @property {HTMLElement} view.text - контейнер для редактора текста 
 * @property {EditorJS} view.editor - редактор текста 
 * @property {HTMLInputElement} view.error - dom-элемент текста ошибки
 * 
 * @property {Boolean} saved - элемент теста сохранен
 * @property {Boolean} isValid - элемент теста валиден
 */
class TestElement {
  /**
   * @constructor
   * @param {Object} data исходные данные элемента
   * @param {Class} elementClass конструктор элемента
   */
  constructor(data = {}, events, selectors, editorConfig = {}) {
    new Dispatcher(this);

    this.text = this._formatText(data.text);

    this.preview = null;
    this.view = null;

    this._saved = true;
    this.isValid = false;

    this.index = ++TestElement.counter;
    this._id = data.id || uuidv4();

    this._order = -1;

    this.events = events;
    this.selectors = selectors;

    this._getEditorToolsConfig = getToolsConfig(editorConfig);

    this.on(this.events.change, () => this._onChange());
  }

  get id() {
    return this._id;
  }

  get order() {
    return this._order + 1;
  }

  set order(value) {
    if (typeof value === "number") this._order = Math.max(-1, value);
    else this._order = -1;
  }

  /**
   * @param {Boolean} value
   */
  set saved(value) {
    this._saved = !!value;
    if (this.view) {
      if (this._saved) {
        this.view.save.setAttribute('data-save', 'saved');
        this.view.save.setAttribute('disabled', '');
      } else {
        this.view.save.setAttribute('data-save', '');
        this.view.save.removeAttribute('disabled');
      }
    }
  }

  /**
   * JSON-представление для БД
   * @returns {Object}
   */
  toJSON() {
    return {
      order: this.order + 1,
      id: this.id,
      text: this.text,
    };
  }

  /**
   * Форматирует текст в виде строки в конфиг для редактора editor.js
   * @private
   * @param {string} textString 
   * @returns {Object[]}
   */
  _formatText(text = "") {
    if (typeof text === "string") {
      try {
        text = JSON.parse(text);
      } catch(e) {}
    }

    if (Array.isArray(text)) return text;

    if (typeof text === "string") {
      return [
        {
          "type": "paragraph",
          "data": {
            "text": text
          }
        }
      ];
    };

    return [];
  }

  /**
   * Обработчик события изменения данных элемента
   */
  _onChange() {
    this.saved = false;
    this.validate("silence");
  }

  /**
   * Представление в списке
   * @returns {HTMLElement}
   */
  getPreview() {
    if (!this.preview) {
      this.preview = this._createPreview();
    }
    return this.preview.element;
  }

  /**
   * Создание представления в списке 
   * @private
   * @returns {Object}
   */
  _createPreview() {
    let template = document.getElementById(this.selectors.previewTemplateId);
    let preview = template.content.cloneNode(true).firstElementChild;

    let text = preview.querySelector(TestElement.selectors.text);
    text.innerHTML = this._getPreviewText();

    new ConfirmButton(preview.querySelector(TestElement.selectors.delete), () => this.delete());

    let editBtn = preview.querySelector(TestElement.selectors.edit);
    editBtn.addEventListener('click', () => this.trigger(this.events.edit));

    let error = preview.querySelector(TestElement.selectors.error);
    
    return {
      element: preview,
      text: text,
      error: error,
    };
  }

  /**
   * Текст для превью вопроса в списке
   * @private
   * @returns {string}
   */
  _getPreviewText() {
    let text = "";
    this.text.forEach(block => {
      if (block.type == "paragraph") text += block.data.text + " ";
      if (block.type == "image") text += "&lt;Изображение" + (block.data.caption ? ": " + block.data.caption : "") + "&gt; ";
      if (block.type == "code") text += "&lt;Код&gt; ";
    });
    return text.trim();
  }

  /**
   * Выводит ошибки в превью вопроса
   * @private
   * @param {string[]|null} errors 
   */
  _setPreviewError(errors) {
    if (!this.preview) return;
    if (errors && errors.length) {
      this.preview.element.classList.add('error');
      this.preview.error.style.display = "block";
      this.preview.error.innerHTML = errors.join('<br>');
    } else {
      this.preview.element.classList.remove('error');
      this.preview.error.style.display = "none";
      this.preview.error.innerHTML = "";
    }
  }

  /**
   * Представление для редактирования
   * @returns {HTMLElement}
   */
  getView() {
    if (!this.view) {
      this.view = this._createView();
      this.validate("silent");
    }
    return this.view.element;
  }

  /**
   * Создание представления для редактирования 
   * @private
   * @returns {HTMLElement}
   */
  _createView() {
    let template = document.getElementById(this.selectors.viewTemplateId);
    let view = template.content.cloneNode(true).firstElementChild;

    /* текст вопроса */
    let text = view.querySelector(TestElement.selectors.text);
    text.id = this._getEditorHolderId();
    let editor = new EditorJS(this._getTextEditorConfig(text));

    /* кнопка сохранить */
    let save = view.querySelector(TestElement.selectors.save);
    save.addEventListener('click', () => {
      this.save();
    });
    save.setAttribute('disabled', '');

    /* кнопка удалить */
    new ConfirmButton(view.querySelector(TestElement.selectors.delete), () => this.delete());

    let error = view.querySelector(TestElement.selectors.error);
    
    return {
      element: view,
      save: save,
      text: text,
      editor: editor,
      error: error
    };
  }

  /**
   * Выводит ошибки в редакторе вопроса
   * @private
   * @param {string[]|null} errors 
   */
  _setViewError(errors) {
    if (!this.view) return;
    if (errors && errors.length) {
      this.view.element.classList.add('error');
      this.view.error.style.display = "block";
      this.view.error.innerHTML = errors.join('<br>');
    } else {
      this.view.element.classList.remove('error');
      this.view.error.style.display = "none";
      this.view.error.innerHTML = '';
    }
  }

  /**
   * Уникальный в пределах страницы id для контейнера редактора
   * @private
   * @returns {string}
   */
  _getEditorHolderId(prefix) {
    return `test-element-${ prefix ? prefix + '-' : '' }${ this.index }`;
  }

  /**
   * Конфигурация редактора текста
   * @private
   * @param {string} holderId - уникальный идентификатор контейнера для редактора
   * @param {Function} [onReady] - коллбэк для полной инициализации редактора
   * @returns {Object}
   */
  _getTextEditorConfig(holder, onReady) {
    let tools = this._getTextToolsConfig();
    holder.addEventListener('editor-change', () => {
      this._saveEditor()
          .then(() => {
            this.trigger(this.events.change);
          });
    })
    let configuration = {
      "holder": holder,
      "tools": tools,
      "onChange": () => {
        this._saveEditor()
          .then(() => {
            this.trigger(this.events.change);
          })
      },
      "data": {
        "blocks": this.text
      },
      "autofocus": true
    };

    if (onReady) configuration.onReady = onReady;

    return configuration;
  }

  _getTextToolsConfig() {
    return this._getEditorToolsConfig(['image', 'code', 'embed', 'header']);
  }

  /**
   * Сохранение редактора текста вопроса
   * @private
   * @returns {Promise}
   */
  _saveEditor() {
    return this.view.editor.save()
      .then(data => {
        this.text = data.blocks;
      });
  }

  /**
   * Валидация
   * @param {Boolean} silent отображение ошибки в превью
   * @returns {Boolean}
   */
  validate(silent) {
    let errors = this.getValidationErrors();
    
    this.isValid = errors.length === 0;
    if (this.view) {
      this.view.save.classList.toggle('disabled', !this.isValid);
    }
    if (!silent) {
      this._setPreviewError(errors);
    }

    return {
      isValid: this.isValid,
      errors: errors
    }
  }

  /**
   * Ошибки валидации
   * @returns {string[]}
   */
  getValidationErrors() {
    let errors = [];
    if (!this.text.length) errors.push('Нет текста');
    return errors;
  }

  /**
   * Сохранить вопрос после изменения
   * Кнопка Сохранить
   */
  save() {
    
    let validation = this.validate("silent");
    if (!validation.isValid) {
      this._setViewError(validation.errors);
      return;
    } 

    this._setPreviewError(false);
    this._setViewError(false);

    if (this.view) {
      this.view.save.setAttribute('data-save', 'saving');
    }
    
    this._saveData()
      .then(() => {
        this.trigger(this.events.save);
        if (this.preview) {
          this.preview.text.innerHTML = this._getPreviewText();
        }
        this.saved = true;
      })
      .catch(e => console.error(e));
  }

  /**
   * Сохранение данных
   * @private
   * @returns {Promise}
   */
  _saveData() {
    return this._saveEditor(); 
  }

  /**
   * Удалить вопрос
   * @param {Boolean} silent
   */
  delete(silent) {
    this._onDelete();

    if (this.view) {
      this.view.element.remove();
      this.view = null;
    }
    if (this.preview) {
      this.preview.element.remove();
      this.preview = null;
    }

    if (!silent) this.trigger(this.events.delete);
  }

  /**
   * Дополнительные действия при удалении
   */
  _onDelete() {

  }
}

TestElement.counter = 0;

TestElement.selectors = {
  "text": "[data-text]",
  "save": "[data-save]",
  "delete": "[data-delete]",
  "edit": "[data-edit]",
  "error": "[data-error]",
};

export default TestElement;