import XEditor from "../x-editor/x-editor";

import TestQuestionsList from "./test-question/test-questions-list";
import TestResultsList from "./test-result/test-results-list";
import testEvents from "./test-events";

/**
 * @typedef {Object} TestEditorExtraConfig
 * @property {string} questionSaveUrl урл для сохранения вопросов
 * @property {string} questionDeleteUrl урл для удаления вопросов
 * 
 * @typedef {XEditroBaseConfig & TestEditorExtraConfig} TestEditorConfig
 * @description Конфигурация редактора теста
 */

/**
 * @typedef {Object} TestEditorExtraValidationRules
 * @property {ValidationRule} questions Валидация вопросов
 * @property {ValidationRule} results Валидация результатов
 * 
 * @typedef {XEditorValidationRules & TestEditorExtraValidationRules} TestEditorValidationRules
 * @description Правила валидации теста
 */

/**
 * @typedef {Object} TestEditorExtraData
 * @property {Object[]} questions - массив вопросов теста
 * @property {Object[]} results - массив результатов теста
 * 
 * @typedef {XEditorData & TestEditorExtraData} TestEditorData
 * @description Данные теста
 */

/**
 * @class TestConstructor
 * @description  Конструктор теста
 * 
 * @property {TestEditorConfig} config конфигурация конструктора
 * 
 * @property {TestElementsList} questions - список вопросов теста
 * @property {TestElementsList} resultsList - список результатов теста
 */
class TestConstructor extends XEditor {
  /**
   * @constructor
   * @param {TestEditorConfig} config конфигурация конструктора
   * @param {TestEditorData} [data] данные существующего теста
   */
  constructor(config, data = {}) {
    super(config, data);

    this.questions = new TestQuestionsList({
      listData: data.questions,
      saveUrl: config.questionSaveUrl,
      deleteUrl: config.questionDeleteUrl,
      testId: this.id,
      editorConfig: this.editorConfig
    });
    this.questions.on(testEvents.question.save, () => {
      this._checkUrl(this.id);
      this.isChanged = true;
      this.isChanged = false;
    });
    this.questions.on(testEvents.question.delete, () => this.isChanged = true);
    this.questions.on(testEvents.question.sort, () => this.isChanged = true);

    this.results = new TestResultsList({
      listData: data.results,
      testId: this.id,
      editorConfig: this.editorConfig
    });
    this.results.on(testEvents.result.save, () => this.isChanged = true);
    this.results.on(testEvents.result.delete, () => this.isChanged = true);
    this.results.on(testEvents.result.sort, () => this.isChanged = true);
    this.results.on(testEvents.result.change, () => this.isChanged = true);

    this._DOMInit();
  }

  /**
   * @override
   * @returns {TestEditorValidationRules}
   */
  _formatValidationRules(rules = {}) {
    rules = super._formatValidationRules(rules);

    let formatted = {
      questions: {
        required: true,
        min: false,
        max: false,

        minMessage: 'too-few-questions',
        maxMessage: 'too-many-questions',
        empty: 'no-questions',

        default: 'question-error'
      },
      results: {
        required: true,
        min: false,
        max: false,

        minMessage: 'too-few-results',
        maxMessage: 'too-many-results',
        empty: 'no-results',

        default: 'result-error'
      }
    };

    return Object.assign(formatted, rules);
  }

  /**
   * @override
   */
  _DOMExtraInit() {
    let container = this.elements.container;

    this.questions.init({
      list: container.querySelector(TestConstructor.selectors.questionsList),
      element: container.querySelector(TestConstructor.selectors.question),
      create: container.querySelector(TestConstructor.selectors.createQuestion),
    });

    this.results.init({
      list: container.querySelector(TestConstructor.selectors.resultsList),
      element: container.querySelector(TestConstructor.selectors.result),
      create: container.querySelector(TestConstructor.selectors.createResult),
    });
  }

  toJSON() {
    let json = super.toJSON();

    json.questions = this.questions.toJSON();
    json.results = this.results.toJSON();

    return json;
  }

  /**
   * @override
   * @param {string} errorType 
   * @returns {string}
   */
  getValidationErrorMessage(errorType) {
    switch(errorType) {
      case this.validationRules.questions.empty:
        return "В тесте нет ни одного вопроса";
      case this.validationRules.questions.minMessage: 
        return `Минимальное количество вопросов: ${this.validationRules.questions.min}`;
      case this.validationRules.questions.maxMessage: 
        return `Максимальное количество вопросов: ${this.validationRules.questions.max}`;
      case this.validationRules.questions.default:
        return "Ошибка при создании вопроса";

      case this.validationRules.results.empty:  
        return "В тесте нет ни одного результата";
      case this.validationRules.results.minMessage:
        return `Минимальное количество результатов: ${this.validationRules.results.min}`;
      case this.validationRules.results.maxMessage:
        return `Максимальное количество результатов: ${this.validationRules.results.max}`;
      case this.validationRules.results.default:
        return "Ошибка при создании результата";
    }

    return super.getValidationErrorMessage(errorType);
  }

  
  /**
   * @override
   */
  _extraValidate(silent) {
    let questionsErrors = this.validator.validateCount(this.questions, 'questions');
    if (!this.questions.validate(silent).isValid) questionsErrors.push(this.getValidationErrorMessage(this.validationRules.questions.default));

    let resultsErrors = this.validator.validateCount(this.results, 'results');
    if (!this.results.validate(silent).isValid) resultsErrors.push(this.getValidationErrorMessage(this.validationRules.results.default)); 

    return [...questionsErrors, ...resultsErrors];
  }
}

/**
 * @static
 */
TestConstructor.selectors = {
  questionsList: '[data-test-questions-list]',
  question: '[data-test-question]',
  createQuestion: '[data-test-create-question]',

  resultsList: '[data-test-results-list]',
  result: '[data-test-result]',
  createResult: '[data-test-create-result]',
};

export default TestConstructor;