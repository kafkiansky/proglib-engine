import uuidv4 from "uuid";

import CustomFileLoader from "../../loader/modules/loader";

import CSelect from "../../c-form/c-select/c-select";

import ConfirmButton from "../../c-form/confirm-button";

import LengthChecker from "../../../modules/dom/length-checker";

import debounce from "../../../helpers/debounce";

import Validator from "../../../components/validator/validator";

import packFormData from "../../../helpers/pack-form-data";

/**
 * @typedef {Object} ValidationRule
 * @description Правило валидации элемента
 * @property {Boolean} [required] Наличие элемента (хоть один символ, хоть один тег)
 * @property {Number|Boolean} [min] Минимальная длина/минимальное количество
 * @property {Number|Boolean} [max] Максимальная длина/максимальное количество
 * @property {string} [minMessage] Сообщение о недостаточной длине/недостаточном количестве
 * @property {string} [maxMessage] Сообщение об избыточной длине/избыточном количестве
 * @property {string} [empty] Сообщение об отсутствии элемента
 */


/**
 * @typedef {Object} XEditorValidationRules
 * @description Правила для валидации сущности
 * 
 * @property {ValidationRule} title Валидация заголовка
 * @property {ValidationRule} preview Валидация описания
 * @property {ValidationRule} tags Валидация тегов
 * @property {ValidationRule} image Валидация главного изображения
 */

/**
 * @typedef {Object} XEditorValidateOutput
 * @description Результат валидации редактора
 * @property {Boolean} isValid Валидность
 * @property {string[]} Массив ошибок
 */

/**
 * @typedef {Object} XEditorBaseConfig
 * @description Базовый объект конфигурации для редактора
 * @property {Object} validationRules Правила для валидации
 * @property {Function} getCreatedUrl Генератор урла для новой сущности
 * @property {string} saveUrl урл для сохранения
 * @property {string} publishUrl урл для публикации
 * @property {string} deleteUrl урл для удаления теста
 * @property {string} redirectAfterDeleteUrl урл для переадресации после удаления сущности
 * @property {string} redirectAfterPublishUrl урл для переадресации после публикации сущности
 * @property {Object} editorConfig настройки редактора editorjs
 * @property {Boolean} backgroundSave фоновое сохранение данных
 * @property {String[]} backgroundSaveExcept данные при изменении которых не требуется фоновое сохранение 
 */

/**
 * @typedef {Object} XEditorData
 * @description Данные сущности, созданной редактором
 * @property {string} id uuid сущности
 * @property {string} title Заголовок сущности
 * @property {string} preview Описание сущности
 * @property {string} image Главная картинка сущности
 * @property {string[]} tags Массив тегов сущности
 */

/**
 * @class XEditor
 * @description Редактор сущностей (статей, тестов) с базовыми функциями
 * 
 * @property {Object} config объект конфигурации редактора
 * @property {XEditorData} данные сущности 
 * 
 * @property {Object} urls
 * @property {string} urls.save Сохранение сущности
 * @property {string} urls.publish Публикация сущности
 * @property {string} urls.delete Удаление сущности
 * @property {string} urls.redirectAfterDelete Урл для переадресации после удаления сущности
 * @property {string} urls.redirectAfterPublish Урл для переадресации после публикации сущности
 * 
 * @property {Boolean} isNew Создание новой сущности (не редактирование)
 * 
 * @property {Object} elements HTML-элементы редактора
 * @property {HTMLElement} elements.container Общий контейнер редактора
 * @property {HTMLElement} elements.title Поле для заголовка сущности
 * @property {HTMLElement} elements.preview Поле для описания сущности
 * @property {HTMLElement} elements.tagsSelect Селект выбора тегов
 * @property {HTMLElement} elements.save Кнопка Сохранить
 * @property {HTMLElement} elements.publish Кнопка Опубликовать
 * @property {HTMLElement} elements.error Контейнер для вывода ошибок валидации
 * @property {HTMLElement} elements.delete Кнопка Удалить
 * @property {HTMLElement} elements.previewLink Кнопка предпросмотра сущности
 * @property {HTMLElement} elements.lastSaving Последнее сохранение
 * 
 * @property {Boolean} isValid Валидность сущности
 * @property {Boolean} isChanged Есть несохраненные изменения
 * @property {Boolean} _inProcess В процессе запроса к серверу
 * 
 * @property {string[]} _backgroundSaveExcept Данные, при изменении которых не требуется фоновое сохранение
 * 
 * @property {XEditorValidationRules} validationRules Правила для валидации
 * @property {Object} editorConfig
 * 
 * @property {Validator} validator
 */

class XEditor {
  /**
   * @constructor
   * @param {Object} config Объект конфигурации
   * @param {Object} [data] Данные сущности для редактирования
   */
  constructor(config, data = {}) {
    this._isNew = true;
    this._isValid = true;
    this._isChanged = false;

    this.config = config;

    this.editorConfig = config.editorConfig;

    this._getCreatedUrl = config.getCreatedUrl || (() => window.location.href);
    this._getPreviewUrl = config.getPreviewUrl;

    this.urls = {
      save: config.saveUrl,
      publish: config.publishUrl,
      delete: config.deleteUrl,
      redirectAfterDelete: config.redirectAfterDeleteUrl,
      redirectAfterPublish: config.redirectAfterPublishUrl
    };

    this.elements = {};
    this.validationRules = this._formatValidationRules(config.validationRules);
    this.validator = new Validator(this);
    this.data = this._formatData(data);

    if (config.backgroundSave) {
      this._backgroundSaveExcept = config.backgroundSaveExcept || [];
      this._backgroundSave = debounce(() => {
        this.save();
      }, config.saveDelay || (200));
    }
  }

  get id() {
    return this.data.id;
  }

  /**
   * Форматирует адресную строку и ссылку предпросмотра после первого сохранения
   */
  _setId() {
    if (this._checked) return;
    this._checked = true;
    this._checkUrl(this.data.id);
    this._setPreviewLink(this.data.id);
  }

  get isNew() {
    return this._isNew;
  }
  set isNew(value) {
    this._isNew = Boolean(value);

    /**
     * Для новой сущности кнопка Удалить неактивна
     */
    if (this.elements.delete) {
      this.elements.delete.disabled = this._isNew;
    } else {
      console.warn(XEditor.messages.dom.delete);
    }
  }

  get isChanged() {
    return this._isChanged;
  }
  set isChanged(value) {
    this._isChanged = Boolean(value);

    this._setDOMChanged(this._isChanged);

    if (this._isChanged) {
      this.validate('silent');
    }
  }

  _setDOMChanged(value) {
    if (this.elements.save) {
      if (value) {
        this.elements.save.setAttribute('data-save', '');
        this.elements.save.removeAttribute('disabled');
      } else {
        this.elements.save.setAttribute('data-save', 'saved');
        this.elements.save.setAttribute('disabled', '');
      }
    } else {
      console.warn(XEditor.messages.dom.save);
    }
  }

  get isValid() {
    return this._isValid;
  }
  set isValid(value) {
    this._isValid = Boolean(value);
  }

  /**
   * Форматирует правила для валидации сущности
   * @param {Object} rules Правила для валидации
   * @returns {XEditorValidationRules}
   */
  _formatValidationRules(rules = {}) {
    let validationRules = {
      title: {
        required: true,
        min: false,
        max: false,

        minMessage: 'too-short-title',
        maxMessage: 'too-long-title',
        empty: 'no-title',
      },
      preview: {
        required: true,
        min: false,
        max: false,

        minMessage: 'too-short-preview',
        maxMessage: 'too-long-preview',
        empty: 'no-preview',
      },
      tags: {
        required: false,
        min: false,
        max: false,

        minMessage: 'too-few-tags',
        maxMessage: 'too-many-tags',
        empty: 'no-tags'
      },
      image: {
        required: false,

        empty: 'no-image'
      }
    };

    return Object.assign(validationRules, rules);
  }

  /**
   * Форматирует данные сущности
   * @param {Object} data Исходные данные
   * @returns {Object} Отформатированные данные
   */
  _formatData(data) {
    data = data || {};
    let id;

    if (data.id) {
      id = data.id;
      this._checkUrl(id);
      this.isNew = false;
    } else {
      id = uuidv4();
    }

    return {
      id: id,
      title: data.title || '',
      preview: data.preview || '',
      image: data.image || '',
      tags: data.tags || []
    }
  }

  /**
   * Обновляет url страницы, добавляет к нему созданные uuid сущности
   * @param {string} id uuid сущности
   */
  _checkUrl(id) {
    if (!this.isNew) return;

    let newUrl = this._getCreatedUrl(id, window.location.href);
    history.pushState(null, null, newUrl);
    this.isNew = false;
  }

  /**
   * Представление для БД
   * @returns {XEditorData}
   */
  toJSON() {
    return this.data;
  }

  /**
   * Собирает данные редактора для сохранения в БД
   * @returns {Promise}
   */
  _getData() {
    return Promise.resolve(this.toJSON());
  }

  /**
   * Инициализация DOM-элементов
   */
  _DOMInit() {
    window.onbeforeunload = () => {
      if (this.isChanged) return "Данные не сохранены. Сохраните тест перед закрытием страницы.";
    }

    document.execCommand("defaultParagraphSeparator", false, "p");

    let container = document.querySelector(XEditor.selectors.container);
    if (!container) throw new Error(XEditor.messages.dom.container);
    this.elements.container = container;

    this._DOMTitleInit();
    this._DOMPreviewInit();
    this._DOMImageInit();
    this._DOMTagsInit();
    this._DOMControlsInit();

    this._DOMExtraInit();

    this._initLengthCheckers();

    this.validate("silent");
  }

  /**
   * Инициализирует счетчики количества символов
   */
  _initLengthCheckers() {
    let checkers = Array.from(document.querySelectorAll('[data-length-checker]'));
    checkers.forEach(checker => new LengthChecker(checker));
  }

  /**
   * Инициализирует поле для ввода заголовка
   */
  _DOMTitleInit() {
    this.elements.title = this.elements.container.querySelector(XEditor.selectors.title);
    if (this.elements.title) {
      this.elements.title.textContent = this.data.title;
      this.elements.title.addEventListener('input', () => {
        this.data.title = this.elements.title.textContent;
        this.isChanged = true;
        this._bgSave('title');
      });
    } else {
      console.warn(XEditor.messages.dom.title);
    }
  }

  /**
   * Инициализирует поле для ввода краткого описания
   */
  _DOMPreviewInit() {
    this.elements.preview = this.elements.container.querySelector(XEditor.selectors.preview);
    if (this.elements.preview) {
      this.elements.preview.textContent = this.data.preview;
      this.elements.preview.addEventListener('input', () => {
        this.data.preview = this.elements.preview.textContent;
        this.isChanged = true;
        this._bgSave('preview');
      });
    } else {
      console.warn(XEditor.messages.dom.preview);
    }
  }

  /**
   * Инициализирует загрузчик изображения
   */
  _DOMImageInit() {
    let imageLoaderElement = this.elements.container.querySelector(XEditor.selectors.imageLoader);
    if (imageLoaderElement) {
      let initialImages = [];
      if (this.data.image) initialImages.push(this.data.image);
      new CustomFileLoader(imageLoaderElement, {
        progressDelay: 400,
        previewClass: "x-editor-preview",
        initialPreviews: initialImages,
        onAddUrl: src => {
          this.data.image = src;
          this.isChanged = true;
          this._bgSave('image');
        },
        onRemoveUrl: () => {
          this.data.image = "";
          this.isChanged = true;
          this._bgSave('image');
        },
        getStatusMessage: status => {
          if (status == "dimensions") return "Изображение не соответствует требуемым параметрам";
          return false;
        }
      });
    } else {
      console.warn(XEditor.messages.dom.image);
    }
  }

  /**
   * Инициализирует поле выбора тегов
   */
  _DOMTagsInit() {
    let tagsSelectElement = this.elements.container.querySelector(XEditor.selectors.tags);
    this.elements.tagsSelect = tagsSelectElement;
    if (tagsSelectElement) {
      let tagsSelect = new CSelect(tagsSelectElement);
      tagsSelect.value = this.data.tags;
      tagsSelect.on('change', (value) => {
        this.data.tags = value;
        this.isChanged = true;
        this._bgSave('tags');
      });
    }
  }

  /**
   * Инициализирует кнопки и вспомогательные элементы
   */
  _DOMControlsInit() {
    this.elements.save = this.elements.container.querySelector(XEditor.selectors.save);
    if (this.elements.save) {
      this.elements.save.addEventListener('click', () => this.save());
    } else {
      console.warn(XEditor.messages.dom.save);
    }

    this.elements.publish = this.elements.container.querySelector(XEditor.selectors.publish);
    if (this.elements.publish) {
      this.elements.publish.addEventListener('click', () => this.publish());
    }

    this.elements.error = this.elements.container.querySelector(XEditor.selectors.error);

    let deleteButton = this.elements.container.querySelector(XEditor.selectors.delete);
    if (deleteButton) {
      this.elements.delete = new ConfirmButton(deleteButton, () => this.delete());
      this.elements.delete.disabled = !this.data.id;
    } else {
      console.warn(XEditor.messages.dom.delete);
    }

    this.elements.previewLink = this.elements.container.querySelector(XEditor.selectors.previewLink);
    this._setPreviewLink();

    this.elements.lastSaving = this.elements.container.querySelector(XEditor.selectors.lastSaving);
  }

  /**
   * Инициализация дополнительных DOM-элементов
   */
  _DOMExtraInit() {

  }

  _setPreviewLink() {
    if (this.elements.previewLink && this._getPreviewUrl && this.data.id) {
      this.elements.previewLink.href = this._getPreviewUrl(this.data.id);
      this.elements.previewLink.classList.remove('hidden');
    }
  }

  /**
   * Валидирует редактор
   * @param {Boolean} silent Вывод ошибок
   * @returns {XEditorValidateOutput}
   */
  validate(silent) {
    silent = this.validator.showError ? false : silent;

    let errors = [];
    errors = [...errors, ...this._validateTitle(silent)];
    errors = [...errors, ...this._validatePreview(silent)];
    errors = [...errors, ...this._validateImage(silent)];
    errors = [...errors, ...this._validateTags(silent)];

    errors = [...errors, ...this._extraValidate(silent)];

    this.isValid = errors.length === 0;

    if (!silent) {
      if (!this.isValid) {
        console.warn(XEditor.messages.validation.client.fail, errors);
      } else {
        console.log(XEditor.messages.validation.client.passed);
      }
      this.validator.setError(errors);
    }

    return {
      isValid: this.isValid,
      errors: errors
    };
  }

  /**
   * Возвращает текст для ошибок валидации
   * @param {string} errorType 
   * @returns {string}
   */
  getValidationErrorMessage(errorType) {
    switch(errorType) {

      case this.validationRules.title.empty:
        return "Нет названия";
      case this.validationRules.title.minMessage:
        return `Минимальная длина названия: ${this.validationRules.title.min}`;
      case this.validationRules.title.maxMessage:
        return `Максимальная длина названия: ${this.validationRules.title.max}`;

      case this.validationRules.preview.empty:
        return "Нет описания";
      case this.validationRules.preview.minMessage:
        return `Минимальная длина описания: ${this.validationRules.preview.min}`;
      case this.validationRules.preview.maxMessage:
        return `Максимальная длина описания: ${this.validationRules.preview.max}`;

      case this.validationRules.tags.empty:
        return "Нет ни одного тега";
      case this.validationRules.tags.minMessage:
        return `Минимальное количество тегов: ${this.validationRules.tags.min}`;
      case this.validationRules.tags.maxMessage:
        return `Максимальное количество тегов: ${this.validationRules.tags.max}`;

      case this.validationRules.image.empty:
        return "Нет изображения";

      default:
        return "Ошибка валидации";
    }
  }



  /**
   * Валидирует заголовок
   * @param {Boolean} silent Вывод ошибок
   * @returns {string[]} Массив ошибок
   */
  _validateTitle(silent) {
    let errors = this.validator.validateCount(this.data.title, 'title');

    if (this.elements.title) {
      if (!silent && errors.length > 0) this.elements.title.classList.add(XEditor.selectors.invalidClass);
      else this.elements.title.classList.remove(XEditor.selectors.invalidClass);
    } else {
      console.warn(XEditor.messages.dom.title);
    }
    
    return errors;
  }

  /**
   * Валидирует краткое описание
   * @param {Boolean} silent Вывод ошибок
   * @returns {string[]} Массив ошибок
   */
  _validatePreview(silent) {
    let errors = this.validator.validateCount(this.data.preview, 'preview');

    if (this.elements.preview) {
      if (!silent && errors.length > 0) this.elements.preview.classList.add(XEditor.selectors.invalidClass);
      else this.elements.preview.classList.remove(XEditor.selectors.invalidClass);
    } else {
      console.warn(XEditor.messages.dom.preview);
    }
      
    return errors;
  }

  /**
   * Валидирует изображение
   * @param {Boolean} silent Вывод ошибок
   * @returns {string[]} Массив ошибок
   */
  _validateImage(silent) {
    let errors = [];

    if (!this.validationRules.image.required) return errors;

    if (!this.data.image) errors.push(this.validationRules.image.empty);

    return errors;
  }

  /**
   * Валидирует теги
   * @param {Boolean} silent Вывод ошибок
   * @returns {string[]} Массив ошибок
   */
  _validateTags(silent) {
    let errors = this.validator.validateCount(this.data.tags, 'tags');

    if (this.elements.tagsSelect) {
      if (!silent && errors.length > 0) this.elements.tagsSelect.classList.add(XEditor.selectors.invalidClass);
      else this.elements.tagsSelect.classList.remove(XEditor.selectors.invalidClass);
    } else {
      console.warn(XEditor.messages.dom.tags);
    }

    return errors;
  }

  /**
   * Дополнительная валидация редактора
   * @param {Boolean} silent Вывод ошибок
   * @returns {string[]} Массив ошибок 
   */
  _extraValidate(silent) {
    return [];
  }

  /**
   * Валидация перед публикацией
   * @returns {Promise}
   */
  _publishValidate() {
    return Promise.resolve([]);
  }

  _bgSave(changed) {
    if (!this._backgroundSave) return;
    if (this._backgroundSaveExcept.indexOf(changed) !== -1) return;

    this._backgroundSave();
  }

  /**
   * Отправка данных на сервер
   * Кнопка Сохранить
   */
  save() {
    if (!this.urls.save) {
      throw new Error(XEditor.messages.urls.save);
    }

    if (this.elements.save) {
      this.elements.save.setAttribute('data-save', 'saving');
    } else {
      console.warn(XEditor.messages.dom.save);
    }

    this._inProcess = true;

    this._getData()
      .then(data => {
        console.info('x-editor saving', data);

        let formData = packFormData(data);

        return fetch(this.urls.save, {
          method: 'POST',
          body: formData
        });
      })
      .then(res => res.json())
      .then(res => {
        if (res.success) {
          this._afterSave();
        } else {
          console.error(XEditor.messages.validation.server.fail, res.error);
          this.validator.setError([res.error]);
        }
        this._inProcess = false;
      })
      .catch(err => {
        console.error(XEditor.messages.server.default, err);
        if (this.elements.save) {
          this._setDOMChanged(true);
          this._inProcess = false;
        }
      });
  }

  /**
   * Публикация сущности
   * Кнопка Опубликовать
   */
  publish() {
    if (!this.urls.publish) {
      throw new Error(XEditor.messages.urls.publish);
    }

    let validation = this.validate();
    if (!validation.isValid) {
      this.validator.setError(validation.errors);
      return;
    }

    this._inProcess = true;

    this._publishValidate(
        () => {},
        (errors) => {
          this.validator.setError(errors);
          throw new Error();
        }
      )
      .then(() => {
        if (this.elements.error)
          this.elements.error.innerHTML = "";

        if (this.elements.publish)
          this.elements.publish.setAttribute('data-loading', '');

        if (this.elements.save) {
          this.elements.save.setAttribute('data-save', 'saving');
        }

        return this._getData();
      })
      .then(data => {
        let formData = packFormData(data);
        return fetch(this.urls.publish, {
                  method: 'POST',
                  body: formData
                })
                .then(res => res.json())
      })
      .then(res => {
        if (this.elements.publish)
            this.elements.publish.removeAttribute('data-loading');
        if (res.success) {
          this._afterSave();
          this._inProcess = false;

          if (this.urls.redirectAfterPublish) {
            location = this.urls.redirectAfterPublish;
          }
        } else {
          let errors = res.error;
          if (!errors) errors = [];
          else if (typeof errors == "string") errors = [errors];

          this.validator.setError(errors);
          throw new Error();
        }
      })
      .catch(error => {
        if (error.message) console.error(error);
        else if (!this.validator.showError) this.validator.setError(['Что-то пошло не так']);
        if (this.elements.publish)
          this.elements.publish.removeAttribute('data-loading');
        this._setDOMChanged(true);
        this._inProcess = false;
      });
  }

  _afterSave() {
    this._setId();
    this.isChanged = false;

    if (this.elements.lastSaving) {
      let time = new Date();
      this.elements.lastSaving.textContent = "Сохранено " + time.toLocaleString("ru");
    }
  }

  /**
   * Удаление сущности
   */
  delete() {
    if (!this.urls.delete) {
      throw new Error(XEditor.messages.urls.delete);
    }

    let data = new FormData();
    data.append('testId', this.id);
    fetch(this.urls.delete, {
      method: "POST",
      body: data
    })
      .then(res => res.json)
      .then(res => {
        if (res.success) {
          if (this.urls.redirectAfterDelete)
            location = this.urls.redirectAfterDelete;
        } else {
          this.validator.setError(["Не удалось удалить"]);
        }
      })
      .catch(err => {
        console.error(XEditor.messages.server.default, err);
      })
  }
}

XEditor.messages = {
  "dom": {
    "container": "XEditor DOM error: there is no editor container (.x-editor element)",
    "title": "XEditor DOM warning: there is no entity title field",
    "preview": "XEditor DOM warning: there is no entity preview field",
    "tags": "XEditor DOM warning: there is no entity tags select",
    "image": "XEditor DOM warning: there is no entity image loader",
    "save": "XEditor DOM warning: there is no button to delete entity",
    "delete": "XEditor DOM warning: there is no button to delete entity",
  },
  "validation": {
    "server": {
      "fail": "XEditor validation error: Server validation failed",
    },
    "client": {
      "fail": "XEditor validation error: Client validation failed",
      "passed": "XEditor validation message: Client validation passed"
    }
  },
  "server": {
    "default": "XEditor error: Unable to contact server"
  },
  "urls": {
    "save": "XEditor data error: Save url is not defined",
    "publish": "XEditor data error: Publish url is not defined",
    "delete": "XEditor data error: Delete url is not defined",
  }
};

XEditor.selectors = {
  container: ".x-editor",
  title: ".x-editor__title",
  preview: ".x-editor__preview",
  imageLoader: ".x-editor__image-loader",
  tags: ".tags-select",

  error: ".x-editor__error",
  save: ".x-editor__save",
  publish: ".x-editor__publish",
  delete: ".x-editor__delete",
  previewLink: ".x-editor__preview-link",
  lastSaving: ".x-editor__last-saving",

  invalidClass: 'is-invalid'
};

export default XEditor;