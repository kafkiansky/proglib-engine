import XEditor from "../x-editor/x-editor";
import EditorJS from "@editorjs/editorjs";
import getToolsConfig from "../editor-js/get-tools-config";

/**
 * @typedef {Object} PostEditorExtraConfig
 * @property {Boolean} backgroundSave Фоновое сохранение
 * @property {Number} saveDelay Задержка фонового сохранения в мс
 * 
 * @typedef {XEditroBaseConfig & PostEditorExtraConfig} PostEditorConfig
 * @description Конфигурация редактора поста
 */

/**
 * @typedef {Object} PostEditorExtraValidationRules
 * @property {ValidationRule} body Валидация текста поста
 * 
 * @typedef {XEditorValidationRules & PostEditorExtraValidationRules} PostEditorValidationRules
 * @description Правила валидации поста
 */

/**
 * @class PostEditor
 * @description Редактор поста
 * 
 * @property {EditorJS} editor
 */

class PostEditor extends XEditor {
  /**
   * @constructor
   * @param {PostEditorConfig} config Объект конфигурации
   * @param {XEditorData} [data] Данные поста
   */
  constructor(config, data = {}) {
    super(config, data);

    this._getToolsConfig = getToolsConfig(config.editorConfig);

    this.editorData = data.editorData;
    this.editor = this._initEditor(this.editorData);
    
    this._DOMInit();
  }

  /**
   * @override
   */
  _getData() {
    let data = this.toJSON();
    return this.editor.save()
            .then(editorData => {
              data.editorData = editorData;
              return data;
            });
  }

  /**
   * @override
   * @returns {PostEditorValidationRules}
   */
  _formatValidationRules(rules = {}) {
    let validationRules = {
      body: {
        required: true,
        min: false,
        max: false,

        minMessage: 'too-short-content',
        maxMessage: 'too-long-content',
        empty: 'no-content',
      },
    };
    Object.assign(validationRules, super._formatValidationRules(rules));
    return validationRules;
  }

  /**
   * Инициализировать редактор статьи
   * @param {*} editorData 
   */
  _initEditor(editorData = {}) {
    let holder = document.getElementById(PostEditor.selectors.editorHolderId);
    holder.addEventListener('editor-change', e => {
      this.isChanged = true;
      this._bgSave('content');
    });

    return new EditorJS({
      data: editorData,
      holder: holder,
      autofocus: true,
      tools: this._getToolsConfig('all'),
      onChange: () => {
        this.isChanged = true;
        this._bgSave('content');
      }
    });
  }

  /**
   * @override
   */
  getValidationErrorMessage(errorType) {
    switch(errorType) {
      case this.validationRules.body.empty:
        return "Напишите хоть что-нибудь";
      case this.validationRules.body.minMessage:
        return `Минимальная длина статьи ${this.validationRules.body.min}`;
      case this.validationRules.body.maxMessage:
        return `Максимальная длина статьи ${this.validationRules.body.max}`;
    }

    return super.getValidationErrorMessage(errorType);
  }

  /**
   * Рассчитывает общий объем текста статьи
   * @returns {Number}
   */
  _getContentLength(blocks) {
    let content = blocks.map(block => {
      if (!PostEditor.getBlockTypesContent[block.type]) return "";
      return PostEditor.getBlockTypesContent[block.type](block);
    }).join("");
    return content.replace(/<\/?[^>]+>/g,'').length;
  }

  /**
   * @override
   */
  _publishValidate(res, rej) {
    return this.editor.save()
            .then(editorData => {
              
              let contentLength = this._getContentLength(editorData.blocks);
              
              let errors = this.validator.validateCount({ length: contentLength }, 'body');

              if (errors.length) rej(errors);
              else res()
            });
  }
}

/**
 * @static
 */
PostEditor.selectors = {
  editorHolderId: "editor-content",
};

/**
 * @static
 */
PostEditor.getBlockTypesContent = {
  'paragraph': block => block.data.text,
  'header': block => block.data.text,
  'image': block => block.data.caption,
  'button': block => block.data.text,
  'checkList': block => block.data.items.reduce((total, current) => total + current.text, ""),
  'list': block => block.data.items.join(''),
  'quote': block => block.data.caption + block.data.text,
  'spoiler': block => block.data.shown + block.data.hidden,
  'table': block => block.data.content.reduce((totalTable, row) => totalTable + row.join(''), ""),
  'warning': block => block.data.message + block.data.title
};

export default PostEditor;