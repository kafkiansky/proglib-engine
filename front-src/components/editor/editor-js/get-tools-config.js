/* inline */
import InlineCode from "@editorjs/inline-code";
import Marker from "@editorjs/marker";

/* block */
import ImageTool from "@editorjs/image";
// import CodeTool from "@editorjs/code";
import CodeTool from "./plugins/code-plus/src/index";
import Header from "@editorjs/header";
import Button from "./plugins/button/src/index";
import Embed from "@editorjs/embed";
import Checklist from "@editorjs/checklist";
import Delimiter from "@editorjs/delimiter";
import Gist from "./plugins/gist/src/index";
import List from "@editorjs/list";
import Quote from "@editorjs/quote";
import Spoiler from "./plugins/spoiler/src/index";
import Table from "@editorjs/table";
import Warning from "@editorjs/warning";

export default (config) => {
  let loadImageByFile = config.loadImageByFile;
  let loadImageByUrl = config.loadImageByUrl;

  let getUploader = () => {
    return {
      uploadByFile(file) {
        let data = new FormData();
        data.append('image', file);
        return fetch(loadImageByFile, {
          method: "POST",
          body: data
        })
          .then(res => res.json())
          .catch(error => {
            return {
              success: 0
            }
          })
      },
      uploadByUrl(url) {
        let data = new FormData();
        data.append('link', url);
        return fetch(loadImageByUrl, {
          method: "POST",
          body: data
        })
          .then(res => res.json())
          .catch(error => {
            return {
              success: 0
            }
          })
      }
    }
  }

  return (tools) => {
    if (tools == 'all') {
      tools = ['image', 'code', 'header', 'button', 'embed', 'checkList', 'list', 'gist', 'quote', 'spoiler', 'table', 'warning'];
    }

    let config = {
      inlineCode: {
        class: InlineCode,
        shortcut: 'CMD+SHIFT+С',
      },
      marker: {
        class: Marker,
        shortcut: 'CMD+SHIFT+M',
      },

      delimiter: Delimiter,
    };

    tools.forEach(tool => {
      switch(tool) {
        case "code": 
          config.code = {
            class: CodeTool,
          };
          break;

        case "header": 
          config.header = {
            class: Header,
            config: {
                placeholder: 'Enter a header'
            },
            shortcut: 'CMD+SHIFT+H',
          }
          break;

        case "image": 
          config.image = {
            class: ImageTool,
            config: {
              uploader: getUploader()
            }
          };
          break;

        case "button":
          config.button = {
            class: Button,
            inlineToolbar: true,
            config: {}
          };
          break;

        case "embed":
          config.embed = {
            class: Embed,
            inlineToolbar: true,
            config: {
                services: {
                    youtube: true,
                    coub: true,
                    codepen: true,
                    vimeo: true,
                    vine: true,
                    imgur: true,
                    gfycat: true,
                    'twitch-channel': true,
                    'twitch-video': true,
                    'yandex-music-album': true,
                    'yandex-music-track': true,
                    'yandex-music-playlist': true
                }
            },
          };
          break;

        case "checkList":
          config.checkList = {
            class: Checklist,
            inlineToolbar: true,
          };
          break;

        case "list":
          config.list = {
            class: List,
            inlineToolbar: true,
          };
          break;

        case "quote": 
          config.quote = {
            class: Quote,
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+O',
            config: {
              quotePlaceholder: 'Enter a quote',
              captionPlaceholder: 'Quote\'s author',
            },
          };
          break;

        case "table":
          config.table = {
            class: Table,
            inlineToolbar: true,
            config: {
              rows: 2,
              cols: 3,
            },
          };
          break;

        case "warning":
          config.warning = {
            class: Warning, 
            inlineToolbar: true,
            shortcut: 'CMD+SHIFT+W',
            config: {
                titlePlaceholder: 'Title',
                messagePlaceholder: 'Message',
            },
          };
          break;

        case "spoiler":
          config.spoiler = {
            class: Spoiler,
            inlineToolbar: true
          };
          break;

        case "gist":
          config.gist = {
            class: Gist,
            inlineToolbar: true
          }
      }
    });
    return config;
  }
}