
import TextareaResizer from '../../c-form/textarea-resize';
import mentionHandler from './mention';
import CustomFileLocalLoader from '../../loader/modules/local-loader';


class CommentsForm {
    /**
     * @constructor
     * @param { HTMLElement } [form] 
     */
    constructor(form, methods) {
        this.selectors = {
            postIdProp: "postId",
            commentsFormField: ".comments-form__field",
            replyAuthor: ".parent-author",
            help: ".comments-form__help",
            onHelpClass: "help",
            cancelEdit: ".comments-form__cancel-edit",
        };

        this.postId = null;

        if (form) {
            this.form = form;
            this.postId = this.form.dataset[this.selectors.postIdProp]
        }
        else this.form = this.create();

        this.clearFields = ["comment-content", "parentId"];

        this.field = this.getField();
        this.fieldResizer = new TextareaResizer(this.field, 30);

        this.loader = null; // загрузчик изображений
        this.isEditForm = false; // редактирование комментария
        this.commentId = null; // родительский или редактируемый комментарий

        this._inProcess = false;
        this.edited = false;
        this.helpBlock = null;

        this.methods = methods;

        this.handle();
    }

    get inProcess() {
        return this._inProcess;
    }

    set inProcess(value) {
        this._inProcess = Boolean(value);
        this.form.classList.toggle('in-process', this._inProcess);
    }

    /**
     * @returns { HTMLElement } field: textarea
     */
    getField() {
        return this.form.querySelector(this.selectors.commentsFormField);
    }

    /**
     * @returns { HTMLElement } form
     */
    create() {
        let template = document.getElementById(CommentsForm.templateId);
        if (!template) throw new Error('No template for comments form');
        let form = template.content.querySelector('form');
        form.id = CommentsForm.replyFormId;
        return form;
    }

    /**
     * Handle form
     */
    handle() {
        this.setLoader();
        this.setAuthCheck();
        this.form.addEventListener('submit', e => this.submitHandler(e));
        this.helpBtn = this.form.querySelector(this.selectors.help);
        
        if (this.helpBtn) {
            this.helpBtn.addEventListener('click', () => {
                if (this.helpBlock) {
                    this.hideHelp();
                } else {
                    this.showHelp();
                }
            });
        }

        this.cancelBtn = this.form.querySelector(this.selectors.cancelEdit);
        if (this.cancelBtn) {
            this.cancelBtn.addEventListener('click', () => {
                if (this.isEditForm) {
                    this.cancelEdit();
                }
            })
        }

        mentionHandler(this.form);
    }

    showHelp() {
        this.helpBlock = CommentsForm.helpBlock.get();
        this.helpBtn.parentElement.appendChild(this.helpBlock);
        this.form.classList.add(this.selectors.onHelpClass);

        this.helpBlock.slideDown(300);
    }

    hideHelp() {
        if (!this.helpBlock) return;

        this.helpBlock.slideUp(300, () => {
            CommentsForm.helpBlock.remove();
        });
        this.helpBlock = null;
        this.form.classList.remove(this.selectors.onHelpClass);
    }

    /**
     * Set file loader
     */
    setLoader() {
        let loader = new CustomFileLocalLoader(
            this.form, 
            {
                multiple: true,
                previewClass: "comment-preview",
                onlyImages: true
            }
        );
        this.loader = loader;
    }

    /**
     * Check auth on focus event
     */
    setAuthCheck() {
        /* проверка на авторизацию при начале ввода */
        this.field.addEventListener('focus', () => {
            window.checkAuth();
        });
    }

    /**
     * Check if form is empty
     */
    checkForm() {
        /* проверка заполненности формы */
        if (this.field.value) return true; // есть текст
        if (this.loader && this.loader.hasFiles()) return true; // есть изображения
        return false;
    }

    /**
     * Submit form handler
     * @param { Event } e 
     */
    submitHandler(e) {
        e.preventDefault();

        if (!window.checkAuth()) return;
        if (this.inProcess) return;
        if (!this.checkForm()) return;

        let formData = this.getData();
        this.inProcess = true;

        if (this.isEditForm) {
            this.methods.update(formData, this.postId, this.commentId)
                .then(response => {
                    this.onSubmit(response);
                    this.inProcess = false;
                })
                .catch(error => {
                    this.inProcess = false;
                });
        } else {
            this.methods.create(formData, this.postId)
                .then(response => {
                    this.onSubmit(response);
                    this.inProcess = false;
                })
                .catch(error => {
                    this.inProcess = false;
                });
        }
    }

    onSubmit() {}

    getPostId() {
        return this.postId;
    }

    /**
     * Get form data
     * @return { FormData }
     */
    getData() {
        let formData = new FormData(this.form); // данные формы
        if (this.loader) { // добавление загруженных файлов
            let files = this.loader.getFiles();
            files.forEach(file => {
                formData.append('commentFiles[]', file);
            });
            let urls = this.loader.getUrls();
            urls.forEach(url => {
                formData.append('commentImgUrls[]', url);
            });
        }
        return formData;
    }

    /**
     * Set comment data in form
     * @param { CommentRenderer } parentBlock 
     * @param { Object } data 
     */
    setData(parentBlock, data) {
        data = data || {};

        // id родительского или редактируемого комментария
        this.commentId = data.id || parentBlock.getId();
        if (!this.isEditForm) {
            this.form.parentId.value = this.commentId;  
        }  

        // id поста
        this.postId = parentBlock.getPostId();

        this.field.value = data.comment || "";

        // изображения (лоадер)
        if (data.images) {
            let images = JSON.parse(data.images);
            images.forEach(src => this.loader.addUrl(src, null, "silent"));
        }
    }

    onInsert() {
        this.fieldResizer.resize();
        this.focus();
    }

    /**
     * Form opened as edit form
     * @param { Boolean } isEdit 
     */
    setEdit(isEdit) {
        this.edited = isEdit;
        this.isEditForm = isEdit;
        this.form.classList.toggle('edit-form', isEdit);
        this.field.placeholder = CommentsForm.placeholders[this.isEditForm ? "edit" : "reply"];
        this.form.id = isEdit ? CommentsForm.editFormId : CommentsForm.replyFormId;
    }

    /**
     * Clear form
     */
    clear() {
        this.cancelEdit();

        this.clearFields.forEach(field => {
            this.form[field].value = "";
        });
        
        if (this.loader) {
            this.loader.clear();
        }
        
        if (this.showHelp) {
            this.hideHelp();
        }
        
    }

    /**
     * Remove form from page
     */
    remove() {
        this.form.remove();
    }

    /**
     * Cancel changes if in edit
     */
    cancelEdit() {
        if (this.edited) {
            this.form.dispatchEvent(new Event('cancel-edit'));
            this.edited = false;
        }
    }

    /**
     * Focus on form
     */
    focus() {
        this.form.scrollIntoView({
            behavior: "smooth",
            block: "center"
        });
        this.field.focus();
    }

    setText(text) {
        this.field.value = text;
    }
}

CommentsForm.templateId = "template-comment-form";
CommentsForm.replyFormId = "reply-form";
CommentsForm.editFormId = "edit-form";
CommentsForm.placeholders = {
    "reply": "Напишите ответ (можно использовать markdown)",
    "edit": "Отредактируйте свой комментарий (можно использовать markdown)"
};

CommentsForm.helpBlock = (function() {
    let selector = ".markdown-help";
    let block = null;

    return {
        get: () => {
            if (!block) {
                block = sslide(document.querySelector(selector));
                
            }
            block.classList.add('hidden');
            return block;
        },
        remove: () => {
            if (block) {
                block.remove();
            }
        }
    }
})();

export default CommentsForm;