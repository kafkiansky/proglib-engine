let updateCommentsCounters = () => {
    let selectors = {
        "postCommentsCountContentAttr": "data-comments", // количество комментариев поста
        "postCommentsCountAttr": "data-comments-count", // количество комментариев поста
    };
    
    let commentsContentCounters = Array.from(document.querySelectorAll('[' + selectors.postCommentsCountContentAttr + ']')); // счетчики комментариев у поста
    let commentsAttrsCounters = Array.from(document.querySelectorAll('[' + selectors.postCommentsCountAttr + ']')); // счетчики комментариев у поста

    return (count) => {
        commentsContentCounters.forEach(counter => counter.textContent = count);
        commentsAttrsCounters.forEach(counter => counter.setAttribute(selectors.postCommentsCountAttr, count));
    }
};

export default updateCommentsCounters;
