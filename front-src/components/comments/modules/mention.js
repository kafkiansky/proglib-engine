import { Textarea, Textcomplete } from 'textcomplete';

export default (form) => {
  let field = form.querySelector('textarea');

  let editor = new Textarea(field);
  let textcomplete = new Textcomplete(editor);

  textcomplete.register([{
    match: /(^|\s)@(\w+)$/,
    template: function(item) {
      return `<div class="suggest-user flex-middle">
                <div class="suggest-user__pic mr-2">
                  <img src="${item.userPic}">
                </div>
                <div class="suggest-user__name fgrow-1">${item.username}</div>
              </div>`;
    },
    search: function (term, callback) {
      let data = new FormData();
      data.append('mention', term);
      API("user_username_fetch", {}, data)
        .then(res => {
          if (!res.success || !res.users) throw new Error("Incorrect response format");
          callback(res.users);
        })
        .catch(err => {
          console.error(err);
          callback([])
        });
    },
    replace: function (value) {
      return '$1@' + value.username;
    },
    cache: true
  }]);

  textcomplete.on("rendered", () => {
    textcomplete.dropdown.items[0].activate();
  });
}