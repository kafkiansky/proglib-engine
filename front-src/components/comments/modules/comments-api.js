const commentsApi = {
    /* получить данные комментария для редактирования */
    get: function(postId, commentId) {
        return API("comment_edit", { comment: commentId, post: postId })
            .then(response => {
                if (response.success) {
                    return response;
                } else {
                    console.error('Error', response);
                }
            })
            .catch(function(err) {
                console.error('Error', err);
            })
    },
    /* создать новый комментарий */
    create: function(formData, postId) {
        return API('comment_post', 
                    { id: postId }, 
                    formData
                ).then(function (response) {
                    if (response.success) {
                        return response;
                    } else {
                        throw new Error("Incorrect response");
                    }
                });
    },
    /* ответить на существующий комментарий */
    answer: function(formData, postId) {
        return API('comment_answer_post', 
                    { id: postId },
                    formData
                ).then(function(response) {
                    if (response.success) {
                        return response;
                    } else {
                        throw new Error("Incorrect response");
                    }
                });
    },
    /* обновить существующий комментарий */
    update: function(formData, postId, commentId) {
        return API('comment_edit', 
                    { comment: commentId, post: postId }, 
                    formData
                ).then(function (response) {
                    if (response.success) {
                        return response;
                    } else {
                        throw new Error("Incorrect response");
                    }
                });
    },
    /* удалить комментарий */
    delete: function(commentId) {
        return API('comment_remove',  { id: commentId } )
            .then(function (response) {
                if (response.success) {
                    return response;
                } else {
                    throw new Error("Incorrect response");
                }
            });
    }
};

export default commentsApi;