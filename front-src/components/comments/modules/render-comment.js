/**
 * Объект ветки комментариев
 * @class
 * @property el - HTMLElement
 * @property commentBlock - HTMLElement
 */
class CommentRenderer {

    /**
     * @static
     * @param { HTMLElement } el 
     * @returns { CommentRenderer|null }
     */
    static getComment(el) {
        if (!el) return null;
        let branch = el.closest(`${CommentRenderer.selectors.branch}`);
        if (!branch) return null;
        return new CommentRenderer(branch);
    }

    /**
     * Контейнер со всеми комментариями
     * @static
     * @returns { HTMLElement }
     */
    static getCommonList() {
        return document.querySelector(CommentRenderer.selectors.commonList);
    }

    /**
     * @constructor
     * @param {HTMLElement=} branch 
     */
    constructor(branch) {
        this.el = branch;
        this.commentBlock = this.el.querySelector(`${CommentRenderer.selectors.mainComment}`);
    }

    /**
     * id комментария
     * @returns { string }
     */
    getId() {
        return this.el.dataset[CommentRenderer.selectors.commentIdProp];
    }

    /**
     * id поста, к которому относится комментарий
     * @returns { string }
     */
    getPostId() {
        return this.commentBlock.dataset[CommentRenderer.selectors.postIdProp];
    }

    /**
     * имя пользователя, оставившего комментарий
     */
    getUser() {
        return this.el.querySelector(CommentRenderer.selectors.userName).textContent.trim();
    }

    /**
     * @returns { HTMLElement } блок главного комментария в ветке
     */
    getCommentBlock() {
        return this.commentBlock;
    }

    /**
     * @returns { HTMLElement } блок дочерних комментариев
     */
    getList() {
        return this.el.querySelector(CommentRenderer.selectors.children);
    }

    /**
     * Отмечает на контейнере комментария, что у него появились дочерние комментарии
     */
    addChild() {
        this.el.setAttribute(CommentRenderer.selectors.hasChildrenAttr, '');
    }

    /**
     * Удаляет родительский комментарий ветки 
     */
    remove() {
        
        // let list = this.el.parentElement;
        // this.el.remove();
        // if (list.matches(CommentRenderer.selectors.children)) {
        //     if (!list.firstElementChild) {
        //         let parent = list.closest(CommentRenderer.selectors.branch);
        //         if (parent) parent.removeAttribute(CommentRenderer.selectors.hasChildrenAttr);
        //     }
        // }
       
        // this.commentBlock.innerHTML = "Комментарий удален";
        
    }

    /**
     * Обновляет HTML-код ветки
     * @param { string } html 
     */
    update(html) {
        this.el.insertAdjacentHTML("beforeBegin", html);
        this.el.remove();
    }
}

CommentRenderer.selectors = {
    "branch": "[data-comment-branch]", // отдельная ветка комментариев
    "mainComment": "[data-comment]", // главный комментарий в ветке
    "children": ".comments-branch__children", // блок дочерних комментариев
    "userName": ".comment__user", // имя пользователя

    "commentIdProp": "id", // dataset-свойство с id комментария
    "postIdProp": "postId", // dataset-свойство с id поста

    "commonList": ".comments__branches", // контейнер для всех комментариев

    "hasChildrenAttr": "data-has-children", // у комментария есть дочерние комментарии
};

export default CommentRenderer;
