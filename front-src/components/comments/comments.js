/**
 * comments functionality
 */

import "./comments.scss";

import CommentRenderer from './modules/render-comment';
import CommentsForm from './modules/comments-form';
import updateCommentsCounters from './modules/update-counters';
import commentsApi from './modules/comments-api';

window.onReady('dom', () => {
    /**
     * главная форма комментирования
     */
    let $mainForm = document.getElementById("comments-form");
    if ($mainForm) {
        let mainForm = new CommentsForm($mainForm, {
            update: commentsApi.update,
            create: commentsApi.create,
        });
        mainForm.onSubmit = response => createCommentBlock(null, response, mainForm);
    }

    /**
     * форма ответа и редактирования
     */
    let replyForm = new CommentsForm(null, {
        update: commentsApi.update,
        create: commentsApi.answer,
    }); // 
    let currentParent = null; // комментарий, у которого сейчас открыта форма ответа
    let closeReplyForm = function closeForm() {
        replyForm.remove();
        currentParent = null;
    };

    /**
     * Получить ветку родительского или редактируемого комментария
     * @param { HTMLElement } el 
     * @return { CommentRenderer }
     */
    let getCommentBranch = function(el) {
        let branchObj = CommentRenderer.getComment(el);
        return branchObj;
    }

    /* рендер поля для ввода комментария */
    let renderForm = function renderForm(parentBlock, data) {
        replyForm.clear();

        replyForm.setData(parentBlock, data);
        
        if (data) {
            replyForm.setEdit("edit-form");
            replyForm.onSubmit = response => editCommentBlock(parentBlock, response, replyForm);
        } else {
            replyForm.setEdit(false, parentBlock);
            replyForm.setText("@" + parentBlock.getUser() + ", ");
            replyForm.onSubmit = response => createCommentBlock(parentBlock, response, replyForm);
        }
        
        return replyForm;
    };

    /* создание блока нового комментария */
    let createCommentBlock = function(branch, data, form) {
        let comment = data.html;
        let list = branch ? branch.getList() : CommentRenderer.getCommonList();
        if (branch) branch.addChild();
        branch = null;
        list.insertAdjacentHTML("afterBegin", comment);

        if (window.highlight) {
            setTimeout(() => {
              window.highlight.initBlock(list)
            }, 0);
        }
       
        document.body.dispatchEvent(new Event('feed.events.update'));
        list.firstElementChild.scrollIntoView({
            behavior: "smooth",
            block: "center"
        });

        updateCommentsCounters(data.count);
        form.clear();
        closeReplyForm(); 
    };

    /* редактирование блока комментария */
    let editCommentBlock = function(branch, data, form) {
        branch.update(data.html);
        if (window.highlight) {
            setTimeout(() => {
              window.highlight.initBlock(branch)
            }, 0);
        }
        branch = null;
        document.body.dispatchEvent(new Event('feed.events.update'));
        form.clear();
        closeReplyForm();
    };

    /* Нажатие на кнопку ответить */
    let replyToComment = function (el) {
        /* проверить авторизацию */
        if (!window.checkAuth()) return;

        /* родительский комментарий */
        let branch = getCommentBranch(el);
        if (!branch) return;

        let parentBlock = branch.getCommentBlock();
        // не переоткрывать форму у того же коммента
        if (currentParent == parentBlock) return;
        currentParent = parentBlock;

        /* открыть пустую форму под родительским комментарием */
        let form = renderForm(branch, null);
        parentBlock.appendChild(form.form);
        form.onInsert();
    };

    /* Нажатие на кнопку Редактировать комментарий */
    let editComment = function editComment(el) {
        let branch = getCommentBranch(el);
        if (!branch) return;

        // получить данные комментария с сервера
        commentsApi.get(branch.getPostId(), branch.getId())
            .then(response => {

                let commentBlock = branch.getCommentBlock();
                let form = renderForm(branch, response);

                // вставить форму рядом с блоком комментария
                // блок скрыть
                commentBlock.parentElement.insertBefore(form.form, commentBlock);
                form.onInsert();
                commentBlock.style.display = "none";

                let cancelEdit = () => {
                    form.form.removeEventListener('cancel-edit', cancelEdit);

                    form.clear();
                    closeReplyForm();
                    commentBlock.style.display = "";
                };

                form.form.addEventListener('cancel-edit', cancelEdit);
            })
            .catch(e => console.error(e));
    }

    /* Нажатие на кнопку Удалить комментарий */
    let deleteComment = function deleteComment(el) {
        let branch = getCommentBranch(el);
        if (!branch) return;

        window.confirm(
            {
                question: "Точно удалить комментарий?", 
                confirm: "Точно-точно",
            },
            () => {
                
                commentsApi.delete(branch.getId())
                    .then(response => {
                        branch.el.parentElement.innerHTML = response.html;
                        updateCommentsCounters(response.count);
                    })
                    .catch(e => console.error(e));
            }
        )
    };

    addHandler('click', function (e) {
        /* кнопка Ответить */
        if (e.target.hasAttribute('data-comment-reply')) {
            replyToComment(e.target);
        }

        /* кнопка удалить */
        if (e.target.hasAttribute('data-comment-delete')) {
            deleteComment(e.target);
        }

        /* кнопка редактировать */
        if (e.target.hasAttribute('data-comment-edit')) {
            editComment(e.target);
        }
    });
})

