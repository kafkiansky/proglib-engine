/**
 * Вывод сообщений (сбоку экрана)
 * @function
 * @param {string} alertClass класс для элемента сообщения
 */
const showAlert = (alertClass) => {
    let alert, alertText, hideTimer;
    let alertDuration = 3 * 1000;

    let selectors = {
        placementAttr: 'data-placement',
        stateAttr: 'data-state',
        shownClass: 'shown'
    };

    let create = () => {
        alert = document.createElement('div');
        alert.classList.add(alertClass);

        alertText = document.createElement('div');
        alertText.classList.add(alertClass + '__text');

        alert.addEventListener('click', () => hide());

        alert.appendChild(alertText);

        alert.style.visibility = "hidden";
        document.body.appendChild(alert);
    }

    let show = (message, placement, state, time) => {
        if (!alert) create();

        alertText.innerHTML = message;
        alert.setAttribute(selectors.placementAttr, placement || "");
        alert.setAttribute(selectors.stateAttr, state || "");
        alert.style.visibility = "visible";

        setTimeout(() => alert.classList.add(selectors.shownClass), 10);
        
        hideTimer = setTimeout(hide, time || alertDuration);
    }

    let hide = () => {
        clearTimeout(hideTimer);
        alert.classList.remove(selectors.shownClass);
        alert.style.visibility = "hidden";
    }

    return show;
};

export default showAlert;