import "./popup.scss";

/**
 * Всплывающее окно
 * @class Popup
 */

class Popup {
    constructor(el, config) {
        this.el = el;
        config = config || {};

        for (let prop in config) {
            this[prop] = config[prop];
        }

        this.$close = this.el.querySelector(Popup.selectors.close);

        this.el.remove();
        this.el.style.visibility = "visible";

        Popup.init();
    }

    open() {
        this.listen();
        if (this.setListeners) this.setListeners();

        document.body.appendChild(this.el);
        Popup.add(this);
    }

    close(silent) {
        this.stopListen();
        if (this.removeListeners) this.removeListeners();

        this.el.remove();
        Popup.remove(this);

        if (!silent && this.onClose) this.onClose();
    }

    listen() {
        if (this.$close)
            this.$close.addEventListener('click', this.close.bind(this));
    }

    stopListen() {
        if (this.$close)
            this.$close.removeEventListener('click', this.close.bind(this));
    }

    removePopup() {
        document.body.classList.remove(popupClass);
        setTimeout(() => popup.remove(), removeDelay);
    };
}

Popup.selectors = {
    close: '.popup__close'
};
Popup.maskId = "mask";
Popup.inited = false;
Popup.init = () => {
    if (Popup.inited) return;
    Popup.mask = document.getElementById(Popup.maskId);
    if (!Popup.mask) {
        Popup.mask = document.createElement('div');
        Popup.mask.id = Popup.maskId;
        Popup.mask.classList.add(Popup.maskId);
    }
    Popup.mask.addEventListener('click', () => {
        Popup.instances.forEach(popup => popup.close());
    });
    Popup.inited = true;
};
Popup.bodyClass = "popup-opened";
Popup.instances = [];
Popup.add = (popup) => {
    Popup.instances.push(popup);

    if (Popup.instances.length == 1) {
        Popup.mask.style.display = "block";
        setTimeout(() => {
            document.body.classList.add(Popup.bodyClass);
        }, 0);
    }
    
};
Popup.remove = (popup) => {
    for (let i = 0, count = Popup.instances.length; i < count; i++) {
        if (Popup.instances[i] == popup) {
            Popup.instances.splice(i, 1);
            break;
        }
    }

    if (Popup.instances.length == 0) {
        setTimeout(() => {
            document.body.classList.remove(Popup.bodyClass);
            Popup.mask.style.display = "none";
        }, 0);
    }
};

export default Popup;