import "./auth-popup.scss";

import Popup from "./popup";
import addHandler from "../../services/events-handlers";

export default new Promise((res, rej) => {
  window.onReady('dom', () => {
    let $authPopup = document.getElementById('authorization');

    if (!$authPopup) rej("Auth popup: no element");

    let authPopup = new Popup($authPopup);
    addHandler("click", e => {
      if (e.target.closest("[data-auth]")) {
        authPopup.open();
      }
    })

    res(authPopup);
  })
});





