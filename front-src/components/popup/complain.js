import "./complain.scss";

import Popup from "./popup";
import addHandler from "../../services/events-handlers";

window.onReady('dom', () => {
  let $complainPopup = document.getElementById('complain');
  if (!$complainPopup) return;
  let complainPopup = new Popup($complainPopup);
  let complainForm = $complainPopup.querySelector('.complain__form');
  let submit = complainForm.querySelector('[type="submit"]');
  let field = complainForm.querySelector('.complain__field');
  let reasons = Array.from(complainForm.reasons);

  let checkForm = () => {
    return reasons.some(btn => btn.checked) || field.value;
  }

  field.addEventListener('input', e => {
    submit.disabled = !checkForm();
  });

  complainForm.addEventListener('change', e => {
    submit.disabled = !checkForm();
  });

  complainForm.addEventListener('submit', (e) => {
    e.preventDefault();

    if (checkForm()) {
      let data = new FormData(complainForm);
      API("users_user_complaint_send", null, data)
        .then(res => {
          complainPopup.close();
          window.showAlert("Ваша жалоба отправлена", "top-right", "success");
        })
        .catch(err => {
          console.error(err);
          complainPopup.close();
          window.showAlert("Не удалось отправить жалобу", "top-right", "error");
        })
    };
  });

  addHandler('click', (e) => {
    let btn = e.target.closest('[data-complain]');
    if (btn) {
      $complainPopup.setAttribute('data-type', btn.dataset.complain);
      complainForm['complain-type'].value = btn.dataset.complain;
      complainForm['complain-id'].value = btn.dataset.complainId;

      complainPopup.open();
    }
  })
})
