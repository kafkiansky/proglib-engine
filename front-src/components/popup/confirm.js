import Popup from "./popup";

let confirmPopup, text, button;

let selectors = {
    text: '.popup__text',
    button: '.popup__button button',
    mask: '#mask'
};

let createPopup = () => {
  let popup = document.getElementById('confirm');
  
  if (popup) {
      confirmPopup = new Popup(popup);
      text = popup.querySelector(selectors.text);
      button = popup.querySelector(selectors.button);
      return confirmPopup;
  }

  return false;
}

const confirm = (config, confirmCb, cancelCb) => {
    if (!confirmPopup) {
      createPopup();
      if (!confirmPopup) return;
    }

    let confirmAction = () => {
        confirmPopup.close("silent");
        if (confirmCb) confirmCb();
    };

    let cancelAction = () => {
        if (cancelCb) cancelCb();
    };

    confirmPopup.setListeners = () => {
        button.addEventListener('click', confirmAction);
    };

    confirmPopup.removeListeners = () => {
        button.removeEventListener('click', confirmAction);
    };

    confirmPopup.onClose = () => {
        cancelAction();
    };

    if (config.question)
        text.innerHTML = config.question;
    if (config.confirm)
        button.innerHTML = config.confirm;

    confirmPopup.open();
};

export default confirm;