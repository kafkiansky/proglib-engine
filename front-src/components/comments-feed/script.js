let commentsFeed;

let addComment = (html) => {
  if (!commentsFeed) {
    commentsFeed = document.querySelector('.comments-feed');
  }

  if (!commentsFeed) return;

  commentsFeed.lastElementChild.remove();
  commentsFeed.insertAdjacentHTML('afterBegin', html);
  
  document.body.dispatchEvent(new Event('feed.events.update'));
}

export {
  addComment
};