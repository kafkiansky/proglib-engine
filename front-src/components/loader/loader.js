import CustomFileLoader from './modules/loader';
window.CustomFileLoader = CustomFileLoader;
if (window.ready)
  window.ready("custom-loader");

import CustomFileLocalLoader from './modules/local-loader';
window.CustomFileLocalLoader = CustomFileLocalLoader;
if (window.ready)
  window.ready("custom-local-loader");

