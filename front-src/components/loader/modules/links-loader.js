import regexps from "../../../helpers/regexps";

/**
 * Загрузка изображений по ссылкам
 * @class
 * @property {HTMLElement} content поле для ввода текста
 * @property {String} method имя метода в API для отправки ссылки
 * @property {Function} onLoad обработчик загруженного изображения
 * @property {Function} canInsert 
 */

class LinksLoader {
    /**
     * @constructor
     * @param {HTMLElement} content поле для ввода текста
     * @param {Object} config объект конфигурации
     * @param {Function} [config.onLoad] обработчик загруженного изображения
     * @param {Function} [config.canInsert] проверка, нужно ли загружать файл после вставки ссылки
     */
    constructor(content, config) {
        this.content = content;
        this.re = regexps.imgUrl;
        if (this.content) this.method = this.content.dataset.loadMethod;

        this.onLoad = config.onLoad;
        this.canInsert = config.canInsert;
    }

    /**
     * Установка обработчика
     */
    init() {
        if (!this.content || !this.method) return;
        this.content.addEventListener('paste', (e) => this.onPaste(e));
    }

    /**
     * Обработчик вставки
     * @param {Event} e 
     */
    onPaste(e) {
        let paste = (e.clipboardData || window.clipboardData).getData('text');
        let links = paste.match(this.re);

        if (links && links.length) {
            links.forEach(link => { 
                if (this.canInsert && !this.canInsert(link)) return;
            
                let data = new FormData();
                data.append('link', link);
                /**
                 * Отправка запроса через API
                 * FormData: 
                 * {String} link - вставленная ссылка
                 * Response:
                 * {Boolean} success
                 */
                API(this.method, {}, data, (e) => this.onProgress(e, intermediate))
                    .then(res => {
                        if (res.success) { // изображение загружено 
                            let url = res.file.url;
                            this.onLoad ? this.onLoad({ url }) : null;
                        } 
                    })
                    .catch(error => {});
            });
        }
    }
}

export default LinksLoader;