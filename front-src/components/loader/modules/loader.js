import "./loader.scss";

import Dispatcher from "../../../services/dispatcher";

import CustomLoaderPreview from "./loader-preview";
import LocalFileReader from "./local-file-reader";
import DndHandler from "./dnd-handler";
import LinksLoader from "./links-loader";
import countAjaxProgress from "../../../helpers/count-ajax-progress";

/**
 * @typedef {Object} LoaderUrl
 * @property {String} file - урл файла
 * @property {CustomLoaderPreview} preview - превью файла
 */

/**
 * Кастомный загрузчик файлов (input type="file")
 * @class
 * @property {Object|null} config объект конфигурации
 * 
 * @property {HTMLElement} loader Родительский HTML-элемент для установки атрибутов
 * @property {HTMLElement} message Блок для вывода сообщения
 * @property {HTMLElement} progress Блок прогресс-бара всего лоадера
 * @property {HTMLElement} input input type=file
 * @property {HTMLElement|null} preview зона вставки превью
 * @property {HTMLElement} content текстовое поле
 * 
 * @property {String[]} initialPreview массив урлов изначально загруженных файлов
 * @property {String[]} urls урлы всех загруженных 
 * @property {CustomLoaderPreview[]} previews созданные превью
 * @property {Number} loadings количество текущих загрузок
 * 
 * @property {String} _loadFileName имя поля, в котором файл будет отпраляться на сервер (default: image)
 * @property {String} action урл для загрузки файлов на сервер
 * @property {Boolean} onlyImages только изображения
 * @property {Boolean} onlyDocs только документы
 * @property {Number} progressDelay минимальное время показа прогресс-бара
 * @property {Number} maxFileSize максимальный размер файла
 * @property {Number} maxCount максимальное количество файлов
 * @property {Boolean} multiple поддержка загрузки сразу нескольких файлов
 * @property {Boolean} withoutPreview не создавать превью
 * 
 * @property {LocalFileReader} fileReader локальное чтение файлов
 * @property {DndHandler} dndHandler поддержка drag'n'drop
 * @property {LinksLoader} linksLoader загрузка картинок по ссылкам
 */

class CustomFileLoader {
    /**
     * @constructor
     * @param {HTMLElement} loader Родительский HTML-элемент для установки атрибутов
     * @param {Object} [config] объект конфигурации
     * @param {String} [config.loadFieldName] имя поля, в котором файл будет отпраляться на сервер (default: image)
     * @param {Array} [config.initialPreviews] изначально приложенные файлы
     * @param {Number} [config.progressDelay] минимальное время показа прогресс-бара
     * @param {Number} [config.maxFileSize] максимальный размер файла
     * @param {Number} [config.maxCount] максимальное количество файлов
     * @param {Boolean} [config.multiple] поддержка загрузки сразу нескольких файлов
     * @param {Boolean} [config.withoutPreview] не создавать превью
     * @param {Function} [config.getStatusMessage] сообщения для ошибок
     * @param {Function} [config.onStatusChange] коллбэк для изменения статуса
     * @param {Function} [config.afterInit] коллбэк для окончания инициализации
     * @param {Function} [config.onAddUrl] коллбэк для добавления урла
     * @param {Function} [config.onChangeUrl] коллбэк для изменения набора урлов
     * @param {Function} [config.onChange] коллбэк для изменения состояния лоадера
     */
    constructor(loader, config = {}) {
        new Dispatcher(this);

        this.config = config;

        this.loader = loader;
        this.loader.loader = this;

        this.on('change', () => {
            this.loader.dispatchEvent(new Event('loader.events.change'));
        })

        this.input = this.loader.querySelector(CustomFileLoader.selectors.input);
        //this.input.name = "";
        this.message = this.loader.querySelector(CustomFileLoader.selectors.messageBlock);
        if (this.message) {
            this.message.addEventListener("click", () => {
                this._hideMessage();
                this._removeStatus("error");
            });
        }
        this.progress = this.loader.querySelector(CustomFileLoader.selectors.progress);
        this.preview = this.loader.querySelector(CustomFileLoader.selectors.preview);
        this.content = this.loader.querySelector(CustomFileLoader.selectors.content);

        this.initialPreviews = this._getInitial(config.initialPreviews);
        this.urls = [];
        this.previews = [];
        this.loadings = 0;

        this.loadFieldName = config.loadFieldName || this.loader.getAttribute(CustomFileLoader.selectors.loadFieldNameAttr) || "image";
        this.action = this.loader.dataset.action;
        this.onlyImages = config.onlyImages || this.loader.hasAttribute(CustomFileLoader.selectors.onlyImagesAttr);
        this.onlyDocs = config.onlyDocs || this.loader.hasAttribute(CustomFileLoader.selectors.onlyDocsAttr);
        this.progressDelay = config.progressDelay || 0;
        this.maxFileSize = config.maxFileSize || loader.hasAttribute(CustomFileLoader.selectors.maxFileSizeAttr) ? parseInt(loader.getAttribute(CustomFileLoader.selectors.maxFileSizeAttr)) : 1000000;
        this.maxCount = config.maxCount || loader.hasAttribute(CustomFileLoader.selectors.maxCountAttr) ? parseInt(loader.getAttribute(CustomFileLoader.selectors.maxCountAttr)) : 10;
        this.multiple = config.multiple || this.loader.hasAttribute(CustomFileLoader.selectors.multipleAttr);
        this.withoutPreview = !this.preview || config.withoutPreview || loader.hasAttribute(CustomFileLoader.selectors.withoutPreview);

        this.fileReader = new LocalFileReader();
        this.dndHandler = new DndHandler(CustomFileLoader.selectors.dndZone, this.loader, files => this._loadFiles(files));
        this.linksLoader = new LinksLoader(
            this.content,
            {
                canInsert: () => {
                    return this.getFilesCount() < this.maxCount;
                },
                onLoad: (data) => {
                    let preview = this._createPreview();
                    this.addUrl(data.url, preview);
                }
            }
        );

        this._init();
    }

    _getInitial(initial = []) {
        let initialInputs = Array.from(this.loader.querySelectorAll('[name="file-path"]'));
        initialInputs.forEach(input => {
            initial.push(input.value);
            input.remove();
        });
        return initial;
    }

    /**
     * Получение текста ошибки
     * @private
     * @param {String} status статус ошибки
     * @param {Object|null} data дополнительные данные
     * @returns {String}
     */
    _getMessage(status, data) {
        if (status === "extension" && this.onlyDocs) {
            status = "notDocs";
        }
        switch(status) {
            case 500:
                return "Не удалось загрузить файл";
            case "notImages":
                    return "Можно загружать только изображения";
            case "notDocs":
                return "Допустимые форматы: " + CustomFileLoader.docsFormats.join(', ');
            case "tooLarge":
                let units = ['байт', 'Кбайт', 'Мбайт'];
                let value = this.maxFileSize;
                let unit = 0;
                while (value > 1024) {
                    value = (value / 1024).toFixed(2);
                    unit++;
                }
                return data.file ? data.file.name + ": размер файла больше " + value + units[unit] :
                    "Размер файла больше " + value + units[unit];
            case "countLimit":
                return "Не более " + this.maxCount + " файлов";
            default: 
                let message;
                if (this.config.getStatusMessage) {
                    message = this.config.getStatusMessage(status, data);
                }
                return message || "Что-то пошло не так, попробуйте еще раз";
        }
    }
    
    /**
     * Установка атрибута статуса
     * @private
     * @param {String} attr имя атрибута
     * @param {String} status статус
     */
    _setStatus(attr, status) {
        this.loader.setAttribute(CustomFileLoader.attrs[attr], "");

        if (this.config.onStatusChange) this.config.onStatusChange.call(this, status);
    };

    /**
     * Удаление атрибута статуса
     * @private
     * @param {String} attr имя атрибута
     */
    _removeStatus(attr) {
        this.loader.removeAttribute(CustomFileLoader.attrs[attr]);
    }

    /**
     * Вывод сообщения
     * @private
     * @param {String|String[]} message текст сообщения/сообщений
     */
    _showMessage(message) {
        if (!this.message) return;

        let text = "";

        if (!Array.isArray(message)) message = [message];
        message = [...new Set(message)];

        text = document.createTextNode(message.join("<br>"));
        
        this.message.appendChild(text);
        this.message.style.display = "block";
    }

    /**
     * Скрытие сообщения
     * @private
     */
    _hideMessage() {
        if (!this.message) return;
        this.message.innerHTML = "";
        this.message.style.display = "none";
    }

    /**
     * Вывод ошибки
     * @private
     * @param {String|String[]} errors 
     */
    _showError(errors) {
        this._setStatus("error");
        if (errors)
            this._showMessage(errors);
    }

    /**
     * Скрытие ошибок
     * @private
     */
    _clearError() {
        this._removeErrorPreviews();
        this._hideMessage();
        this._removeStatus("error");
    }

    /**
     * Инициализация загрузчика
     * @private
     */
    _init() {
        // инициализировать события drag-n-drop
        this.dndHandler.init();
        // инициализировать инпут для загрузки файлов
        this._initInput(this.input);
        // загрузить начальные файлы
        this._setInitial();
        // отслеживать вставку ссылок
        this.linksLoader.init();
        if (this.config.afterInit) this.config.afterInit.call(this);
    }

    /**
     * Инициализация input
     * @private
     * @param {HTMLInputElement} input 
     */
    _initInput(input) {
        input.onchange = (e) => this._onInputChange(e);
    }

    /**
     * Изменение input - выбор юзером файлов
     * @private
     * @param {ChangeEvent} e 
     */
    _onInputChange(e) {
        this._loadFiles(e.target.files);
        this._updateInput(); 
    }

    /**
     * Очищение инпута после загрузки
     * @private
     */
    _updateInput() {
        
        let newInput = document.createElement('input');
        newInput.type = "file";
        newInput.name = this.input.name;
        newInput.multiple = this.input.multiple;
        
        newInput.className = this.input.className;
        for (let i = 0, count = this.input.attributes.length; i < count; i++) {
            newInput.setAttribute(this.input.attributes[i].name, this.input.attributes[i].value);
        }
        this.input.parentElement.replaceChild(newInput, this.input);
        this.input = newInput;
        this._initInput(this.input);
        
    }

    /**
     * Изменение прогресса загрузки (для всего лоадера)
     * @public
     * @param {Object} progress 
     */
    onProgress(progress) {
        if (this.progress && progress) {
            this.progress.style.width = countAjaxProgress(progress) + '%';
        }
    }

    /**
     * Сброс прогресса загрузки
     * @public
     */
    clearProgress() {
        if (this.progressBar) this.progressBar.style.width = 0;
        this._removeStatus("progress");
    }

    /**
     * Есть ли загруженные файлы
     * @public
     * @returns {Boolean}
     */
    hasFiles() {
        return this.urls.length > 0;
    }

    /**
     * Есть ли выведенные превью
     * @public
     * @returns {Boolean}
     */
    hasPreviews() {
        this._removeErrorPreviews();
        this.previews = this.previews.filter(preview => !preview.removed);
        return this.previews.length > 0;
    }

    /**
     * Получение урлов загруженных файлов
     * @public
     * @returns {String[]}
     */
    getUrls() {
        return this.urls.map(url => url.file);
    }

    /**
     * Количество загруженных файлов
     * @returns {Number}
     */
    getFilesCount() {
        return this.urls.length;
    }

    /**
     * Создание превью для файла
     * @private
     * @returns {CustomLoaderPreview}
     */
    _createPreview() {
        if (this.withoutPreview) return null;
        let preview = new CustomLoaderPreview(this.config, this.preview);
        this.previews.push(preview);

        this._setStatus("previews");

        return preview;
    }

    /**
     * Удаление превью с ошибками загрузки
     * @private
     */
    _removeErrorPreviews() {
        this.previews = this.previews.filter(preview => {
            if (preview.error) {
                preview.remove();
                return false;
            }
            return true;
        })
    }

    /**
     * Добавление загруженного файла
     * @public
     * @param {String} url урл файла
     * @param {CustomLoaderPreview} preview превью файла
     * @param {Boolean} silent вызывать ли коллбэки события добавления
     */
    addUrl(url, preview, silent) {
        if (!preview && !this.withoutPreview) {
            preview = this._createPreview();
        }

        if (preview) {
            preview.addImage(url);
            preview.onRemove = () => this.removeUrl(url);
        }
        
        this.urls.push({
            file: url,
            preview: preview
        });

        if (!silent) {
            this.config.onAddUrl ? this.config.onAddUrl.call(this, url) : null;
            this.config.onChangeUrl ? this.config.onChangeUrl.call(this, url) : null;
            this.config.onChange ? this.config.onChange.call(this, url) : null;
        }

        this._setStatus("files");

        this.trigger('change');
    }

    /**
     * Удаление файла или урла из массива файлов или урлов
     * @private
     * @param {LoaderUrl[]} arr массив для удаления
     * @param {LoaderUrl} file 
     */
    _removeItem(arr, file) {
        let removed = false;
        this._clearError();
        for (let i = 0, count = arr.length; i < count; i++) {
            if (arr[i].file === file) {
                if (arr[i].preview) {
                    arr[i].preview.remove();
                }
                arr.splice(i, 1);
                removed = true;
                break;
            }
        }
        if (!this.hasFiles()) {
            this._removeStatus("files");
        }

        if (!this.hasPreviews()) {
            this._removeStatus("previews");
        }

        return removed;
    }

    /**
     * Удаление урла
     * @public
     * @param {String} url урл загруженного файла
     * @param {Boolean} silent вызывать ли коллбэки события удаления
     */
    removeUrl(url, silent) {
        if (this._removeItem(this.urls, url)) {
            if (!silent) {
                this.config.onRemoveUrl ? this.config.onRemoveUrl.call(this, url) : null;
                this.config.onChangeUrl ? this.config.onChangeUrl.call(this, url) : null;
                this.config.onChange ? this.config.onChange.call(this, url) : null;
            }
            this.trigger('change');
        }
    }

    /**
     * Удаление всех файлов и превью
     * @private
     */
    _reset() {
        this.urls.forEach(url => {
            this.removeUrl(url.file);
        });
        this.previews = this.previews.filter(preview => {
            preview.remove();
            return false;
        });
    }

    /**
     * Сброс загрузчика
     * @public
     */
    clear() {
        this._reset();
        this._hideMessage();
        for (let attr in CustomFileLoader.attrs) {
            this._removeStatus(attr);
        }
    }

    /**
     * Обработка начальных файлов
     * @private
     */
    _setInitial() {
        this.initialPreviews = this.initialPreviews.filter(src => src);
        if (this.initialPreviews.length) {
            this.initialPreviews.forEach(src => {
                let preview = this._createPreview();
                this.addUrl(src, preview, "silent");
            });
        } else {
            this._removeStatus("files");
        }
    }

    /**
     * Загрузка файлов
     * @private
     * @param {Files} files 
     */
    _loadFiles(files) {
        this._clearError(); 
        files = Array.from(files);

        if (files.length) {
            let excess = false;

            if (!this.multiple) {
                this._reset();
                files = [files[0]];
            } else {
                let filesCount = this.getFilesCount();

                if (files.length + filesCount > this.maxCount) {
                    files.length = this.maxCount - filesCount;
                    excess = true;
                }
            }

            let errors = [];

            files.forEach(file => {
                // проверить размеры файлов
                let check = this._checkFile(file);
                if (check.error) {
                    errors.push(this._getMessage(check.error, { file: file }));
                    return;
                } 
                this._setStatus("progress");
                this.loadings++;
                this._loadFile(file);
            });
    
            if (errors.length) {
                this._showError(errors);
            } else {
                this._clearError();
            }

            if (excess) {
                this._showError(this._getMessage("countLimit"));
            }
        }
    }

    /**
     * Проверка файла перед загрузкой
     * @private
     * @param {File} file 
     * @returns {Object} report
     * @returns {String|Boolean} report.error Статус ошибки
     */
    _checkFile(file) {
        if (this.onlyImages && !file.type.match('image.*')) {
            return {
                error: "notImages"
            };
        }
        if (this.onlyDocs) {
            let fileName = file.name.split(".");
            let ext = fileName[fileName.length - 1];

            if (CustomFileLoader.docsFormats.indexOf(ext) == -1)  {
                return { 
                    error: "notDocs"
                };
            };
        }

        if (file.size > this.maxFileSize) {
            return {
                error: "tooLarge"
            };
        }
        
        return {
            error: false
        };
    }
    
    /**
     * Уменьшение количество текущих загрузок после успешной загрузки файла
     * @private
     */
    _onFileLoad() {
        this.loadings--;
        if (this.loadings <= 0) {
            this.loadings = 0;
            setTimeout(() => this._removeStatus("progress"), this.progressDelay);
        }
    }

    /**
     * Выбор способа загрузки файла
     * @private
     * @param {File} file 
     */
    _loadFile(file) {
        this._loadOnServer(file);
    }

    /**
     * Загрузка файла сразу на сервер
     * @private
     * @param {File} file 
     */
    _loadOnServer(file) {
        let preview = this._createPreview();

        let onFinish = (event) => {
            
            if (event.target.readyState == 4) {
                this._onFileLoad();
                if (event.target.status == 200) {
                    try {
                        let response = JSON.parse(event.target.responseText);
                        if (response.success) {
                            let file = response.file.url;
                            setTimeout(() => {
                                this.addUrl(file, preview);
                            }, this.progressDelay);
                        } else {
                            if (preview)
                                preview.setError();    
                            let error = response.error;
                            this._showError(this._getMessage(error));
                        }
                        
                    } catch(e) {
                        if (preview)
                            preview.setError();
                        this._showError();
                    }
                } else {
                    if (preview)
                        preview.setError();       
                    this._showError(this._getMessage(event.target.status)); 
                }
            }
        };

        let xhr = new XMLHttpRequest();
        xhr.upload.addEventListener('progress', (e) => {
            this.onProgress(e);
            if (preview)
                preview.setProgress(e);
        }, false);
        xhr.onreadystatechange = onFinish;

        xhr.open('POST', this.action);
        
        let formData = new FormData();
        formData.append(this.loadFieldName, file);
        xhr.send(formData);
    }
}

/**
 * Допустимые форматы документов
 * @static
 */
CustomFileLoader.docsFormats = ["pdf", "doc", "docx"];
/**
 * Селекторы
 * @static
 */
CustomFileLoader.selectors = {
    onlyImagesAttr: 'data-loader-images',
    onlyDocsAttr: 'data-loader-docs',
    multipleAttr: 'data-multiple',
    maxFileSizeAttr: 'data-max',
    maxCountAttr: 'data-max-count',
    withoutPreview: 'data-without-preview',
    loadFieldNameAttr: 'data-field-name',

    messageBlock: "[data-loader-message]",
    progress: '[data-loader-progress]',
    dndZone: '[data-loader-dropzone]',
    preview: '[data-loader-preview]',
    input: '[data-loader-input]',

    content: '[data-loader-content]'
};
/**
 * Атрибуты состояний лоадера
 * @static
 */
CustomFileLoader.attrs = {
    "files": "data-files", // загружены файлы
    "previews": "data-previews", // есть превьюшки (возможно еще не загружены)
    "progress": "data-progress", // идет загрузка
    "error": "data-error", // ошибка
};

export default CustomFileLoader;
