import CustomFileLoader from "./loader";

/**
 * @typedef {Object} LoaderFile
 * @property {File} file - объект файла
 * @property {CustomLoaderPreview} preview - превью файла
 */

/**
 * Загрузчик с локальными превью
 * @class
 * @extends CustomFileLoader
 * @property {LoaderFile[]} files массив файлов
 */
class CustomFileLocalLoader extends CustomFileLoader {
    /**
     * @constructor
     * @param {HTMLElement} loader 
     * @param {Object} [config] 
     * @param {Function} [config.onAddFile] коллбэк для добавления файла
     * @param {Function} [config.onChangeFile] коллбэк для изменения набора файлов
     */
    constructor(loader, config = {}) {
        super(loader, config);
        // объекты файлов
        this.files = [];
    }

    /**
     * @override
     */
    _onInputChange(e) {
        this._loadFiles(e.target.files);
        if (this.multiple)
            this._updateInput(); 
    }

    /**
     * @override
     */
    _reset() {
        super._reset();
         
        this.files.forEach(file => {
            this.removeFile(file.file);
        });
    }

    /**
     * @override
     */
    _loadFile(file) {
        this._readFile(file);
    }

    /**
     * @override
     */
    hasFiles() {
        return this.files.length || this.urls.length;
    }

    /**
     * @override
     */
    getFiles() {
        return this.files.map(file => file.file);
    }

    /**
     * @override
     */
    getFilesCount() {
        return this.urls.length + this.files.length;
    }

    /**
     * Локальная обработка файла с созданием превью
     * @param {File} file 
     */
    _readFile(file) {
        let preview = this._createPreview();
        this.fileReader.read(file)
            .then(res => {
                this._onFileLoad();
                if (res.fileType == "image" && res.imgSrc) {
                    preview.addImage(res.imgSrc);
                } else {
                    preview.addText(res.fileName);
                }
                this.addFile(file, preview);
            });
    }

    /**
     * Добавление файла
     * @public
     * @param {File} file 
     * @param {CustomLoaderPreview} preview 
     */
    addFile(file, preview) {
        preview.onRemove = () => {
            this.removeFile(file);
        }

        this.files.push({
            file: file,
            preview: preview
        });

        this.config.onAddFile ? this.config.onAddFile(file) : null;
        this.config.onChangeFile ? this.config.onChangeFile(file) : null;
        this.config.onChange ? this.config.onChange(file) : null;
        
        this._setStatus("files");

        this.trigger('change');
    }

    /**
     * Удаление файла
     * @public
     * @param {File} file 
     */
    removeFile(file) {
        if (this._removeItem(this.files, file)) {
            this._updateInput();
            
            this.config.onRemoveFile ? this.config.onRemoveFile(file) : null;
            this.config.onChangeFile ? this.config.onChangeFile(file) : null;
            this.config.onChange ? this.config.onChange(file) : null;

            this.trigger('change')
        }
    }
}

export default CustomFileLocalLoader;