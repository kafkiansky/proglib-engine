/**
 * Обработка drag'n'drop
 * @class
 * @property {HTMLElement} container контейнер для атрибутов
 * @property {HTMLElement} area активная зона для перетаскивания
 * @property {Function} onLoad обработчик загрузки файлов
 */
class DndHandler {
    /**
     * @constructor
     * @param {string} selector css-селектор активной зоны
     * @param {*} container контейнер для атрибутов
     * @param {*} onLoad обработчик загрузки файлов
     */
    constructor(selector, container, onLoad) {
        this.container = container;
        if (this.container) this.area = container.querySelector(selector);
        this.onLoad = onLoad;
    }

    /**
     * Установка наблюдателей
     */
    init() {
        if (!this.area) return;

        const preventDefaults = (e) => {
            e.preventDefault();
            e.stopPropagation();
        }

        /**
         * Файл над активной зоной
         */
        const onHover = () => {
            this.container.setAttribute(DndHandler.dropAttr, "hover");
        }
        /**
         * Файл покинул активную зону
         */
        const onLeave = () => {
            this.container.removeAttribute(DndHandler.dropAttr);
        }
        /**
         * Файл отпущен над активной зоной
         * Событие drop
         * @param {Event} e 
         */
        const onDrop = (e) => {
            this.container.setAttribute(DndHandler.dropAttr, "drop");
            
            let files = e.dataTransfer.files;

            this.onLoad(files);

            setTimeout(() => {
                this.container.removeAttribute(DndHandler.dropAttr);
            }, 400);
        }

        ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            this.area.addEventListener(eventName, preventDefaults, false)
        });
        ['dragenter', 'dragover'].forEach(eventName => {
            this.area.addEventListener(eventName, onHover, false);
        });
        this.area.addEventListener("dragleave", onLeave, false);
        this.area.addEventListener("drop", onDrop, false);
    }
}

/**
 * атрибут контейнера при перетаскивании
 * @static
 */
DndHandler.dropAttr = "data-drop";

export default DndHandler;