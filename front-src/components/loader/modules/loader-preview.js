import countAjaxProgress from "../../../helpers/count-ajax-progress";

/**
 * Превью кастомного загрузчика файлов
 * @class
 * @property {String} previewClass дополнительный класс для превью
 * @property {HTMLElement} el HTML-элемент превью
 * @property {Function} onRemove обработчик удаления превью
 */

class CustomLoaderPreview {
    /**
     * @constructor
     * @param {Object|null} config объект конфигурации
     * @param {String} [config.previewClass] дополнительный класс для превью
     * @param {HTMLElement} container контейнер для вставки превью
     */
    constructor(config = {}, container) {
        this.previewClass = config.previewClass;
        this.el = this.createElement();
        container.appendChild(this.el);
    }

    /**
     * Создание пустого элемента превью с прелоадером
     * @returns {HTMLDivElement}
     */
    createElement() {
        let preview = document.createElement('div');
        preview.classList.add(CustomLoaderPreview.selectors.previewPrefix);
        if (this.previewClass) preview.classList.add(this.previewClass);
        preview.classList.add(CustomLoaderPreview.selectors.loadingClass);

        let preloader = this.createPreloader();
        preview.appendChild(preloader);
        return preview;
    }

    /**
     * Создание прелоадера
     * @returns {HTMLDivElement}
     */
    createPreloader() {
        this.preloader = document.createElement('div');
        this.preloader.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__preloader");

        let progress = document.createElement('div');
        progress.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__progress");
        this.progressText = document.createElement('span');
        this.progressText.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__progress-text");
        this.progressBar = document.createElement('span');
        this.progressBar.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__progress-bar");

        progress.appendChild(this.progressText);
        progress.appendChild(this.progressBar);

        this.preloader.appendChild(progress);
        return this.preloader;
    }

    /**
     * Отображение прогресса загрузки
     * @param {Object} progress прогресс выполнения ajax-запроса
     * 
     */
    setProgress(progress) {
        if (typeof progress === "object") {
            progress = countAjaxProgress(progress);
        }
        this.progressText.textContent = progress + "%";
        this.progressText.setAttribute('data-progress', progress);
        this.progressBar.style.width = progress + "%";
    } 

    /**
     * Создание превью с картинкой
     * @param {String} src путь к изображению
     */
    addImage(src) {
        let img = this.createImage(src);
        this.createPreview(img);
    }

    /**
     * Создание превью с текстом
     * @param {String} text текст превью
     */
    addText(text) {
        let txt = this.createText(text);
        this.createPreview(txt);
    }

    /**
     * Создание элемента изображения
     * @param {String} src путь к изображению
     * @returns {HTMLImageElement}
     */
    createImage(src) {
        let img = document.createElement('img');
        img.src = src;
        let wrapper = document.createElement('div');
        wrapper.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__image");
        wrapper.appendChild(img);
        return wrapper;
    }

    /**
     * Создание текста
     * @param {String} text текст превью
     * @returns {HTMLSpanElement}
     */
    createText(text) {
        let $text = document.createElement('span');
        $text.innerHTML = text;
        $text.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__text");
        return $text;
    }

    /**
     * Заполнение пустого превью контентом
     * @param {HTMLElement} content текст или картинка
     */
    createPreview(content) {
        this.preloader.remove();

        this.el.appendChild(content);
        let controls = this.createControls();
        this.el.appendChild(controls);
        this.el.classList.remove(CustomLoaderPreview.selectors.loadingClass);
    }

    /**
     * Создание управляющих элементов превью 
     * Удаление
     * @returns {HTMLDivElement}
     */
    createControls() {
        let controls = document.createElement('div');
        controls.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__controls");
        
        this.clear = this.createClear();
        controls.appendChild(this.clear);
        this.clear.addEventListener('click', () => this.remove());

        return controls;
    }

    /**
     * Создание кнопки удаления
     * @returns {HTMLDivElement}
     */
    createClear() {
        let clear = document.createElement('div');
        clear.textContent = "Удалить";
        clear.classList.add(CustomLoaderPreview.selectors.previewPrefix + "__clear");

        let clearIcon = document.createElement('div');
        clearIcon.classList.add('ico');
        clearIcon.classList.add('ico-close');
        clearIcon.classList.add('ico_48');

        clear.appendChild(clearIcon);
        return clear;
    }

    /**
     * Удаление превью
     */
    remove() {
        if (this.removed) return;

        this.removed = true;
        this.el.remove();
        if (this.onRemove) this.onRemove();
    }

    /**
     * Вывод ошибки
     */
    setError() {
        this.error = true;
        this.el.classList.add(CustomLoaderPreview.selectors.errorClass);
        this.el.classList.remove(CustomLoaderPreview.selectors.loadingClass);
    }
}

/**
 * @static
 */
CustomLoaderPreview.selectors = {
    previewPrefix: "file-preview",
    loadingClass: "loading",
    errorClass: "error"
};

export default CustomLoaderPreview;