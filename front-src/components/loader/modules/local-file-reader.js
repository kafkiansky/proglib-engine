/**
 * Локальная загрузка превью файла
 * @class
 * @property {Boolean} hasReader поддержка FileReader в браузере
 */
class LocalFileReader {
    /**
     * @constructor
     */
    constructor() {
        this.hasReader = typeof window.FileReader !== undefined;
    }

    /**
     * Обработка файла
     * @param {*} file 
     * @returns {Promise}
     */
    read(file) {
        return new Promise((res, rej) => {
            let response = {
                fileName: file.name,
            };
            if (file.type.match('image.*') && this.hasReader) {
                response.fileType = "image";
                let reader = new FileReader();
                reader.onload = (file => e =>{
                    response.imgSrc = e.target.result;
                    res(response);
                })(file);
                reader.readAsDataURL(file);
            } else {
                res(response);
            } 
        });
    }
}

export default LocalFileReader;