import debounce from "../../helpers/debounce";
let counter = 1;
window.onReady('dom', () => {
    let searchBlocks = document.querySelectorAll('[data-search-toggle]');
    searchBlocks.forEach(block => {
      block.removeAttribute('data-search-toggle');
      window.onReady('toggle-block', function() {
          new ToggleBlock(block, {
              beforeOpen: function(cb) {
                  document.body.classList.add('search-shown');
                  cb();
              },
              beforeClose: function(cb) {
                  document.body.classList.remove('search-shown');
                  cb();
              }
          })
      });
      let searchForm = block.querySelector('.search-form');
      if (!searchForm) return;

      let searchField = searchForm.querySelector('[name="q"]');
      let searchResultsContainer = searchForm.querySelector('.search-form__results');
      if (!searchField || !searchResultsContainer) return;

      let showSearchResults = (query) => {
        let data = new FormData();
        data.append('query', query);

        API('get_search_results', null, data)
          .then(result => {
            if (result.success) {
              searchResultsContainer.innerHTML = result.html;
            } else {
              searchResultsContainer.innerHTML = "";
            }
          })
          .catch(error => {
            console.error(error);
            searchResultsContainer.innerHTML = "";
          })
      };
      showSearchResults = debounce(showSearchResults, 400);

      searchField.addEventListener('input', e => {
        let query = searchField.value;

        if (query.length < 3) {
          searchResultsContainer.innerHTML = "";
        } else {
          showSearchResults(query);
        }
      });
    })
    

})