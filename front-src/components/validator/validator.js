import regexps from "../../helpers/regexps";

/**
 * @typedef {Object} ValidatorBaseRules
 * @description Базовый набор настроек валидации
 * 
 * @property {Boolean} [required] Наличие значения обязательно: не пустая строка/не пустой массив
 * @property {string} [empty] Код ошибки при нарушении правила required
 * @property {Boolean} [min] Минимальная длина значения (строки/массива)
 * @property {string} [minMessage] Код ошибки при нарушении правила min
 * @property {Boolean} [max] Максимальная длина значения (строки/массива)
 * @property {string} [maxMessage] Код ошибки при нарушении правила max
 */

/**
 * @class Validator
 * @description Валидатор
 * 
 * @property {ValidatorBaseRules} rules Набор правил валидации
 * @property {Function} getMessage Функция, возвращающая сообщение по коду ошибки
 * @property {HTMLElement} message Контейнер для вывода сообщиений об ошибке
 * @property {Boolean} showError Ошибка выведена в данный момент
 */

class Validator {
  /**
   * @constructor
   * @param {ValidatorBaseRules} [rules] 
   * @param {Function} getMessage Функция, возвращающая сообщение по коду ошибки
   * @param {HTMLElement} messageContainer Контейнер для вывода сообщиений об ошибке
   */
  constructor(form) {
    if (form) {
      this.form = form;
      this.rules = form.validationRules;
      this.getMessage = form.getValidationErrorMessage.bind(form) || (errorCode => errorCode);
    }
    
    this.showError = false;
  }

  validateRegex(value, re) {
    if (re in regexps) re = regexps[re];
    if (!re.test) return true;
    return re.test(value);
  } 

  validateCount(element, ruleField) {
    let errors = [];

    let length = 0;

    if (element) {
      if (typeof element == "number") length = element;
      else length = element.length || 0;
    }

    let rules = this.rules[ruleField] || {};

    if (rules.relates) {
      for (let fieldName in rules.relates) {
        let needValue = rules.relates[fieldName];
        let realValue = this.form.fields[fieldName].value;
        if (needValue !== realValue) return errors;
      }
    }

    if (rules.required && length === 0) {
      errors.push(rules.empty);
    } else {
      if (typeof rules.min == "number" && length < rules.min) {
        errors.push(rules.minMessage);
      } else if (typeof rules.max == "number" && length > rules.max) {
        errors.push(rules.maxMessage);
      }
    }

    return errors;
  }

  _hideError() {
    this.showError = false;
    if (this.form.elements.error) {
      this.form.elements.error.style.display = "none";
      this.form.elements.error.innerHTML = "";
    }
  }

  _showError(html) {
    this.showError = true;
    if (this.form.elements.error) {
      this.form.elements.error.style.display = "block";
      this.form.elements.error.innerHTML = html;
    }
  }

  setError(errors) {
    if (!errors) {
      this._hideError();
      return;
    }

    if (!Array.isArray(errors)) errors = [errors];

    if (!errors.length) {
      this._hideError();
      return;
    }

    this._showError(errors.map(error => this.getMessage(error)).join('<br>'));
  }
}

export default Validator;