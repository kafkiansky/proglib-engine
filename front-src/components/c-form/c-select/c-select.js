import "./c-select.scss";

import ItemsSearch from "./items-search";
import sslide from "../../../modules/libs/sslide";
import Dispatcher from "../../../services/dispatcher";
import addHandler from "../../../services/events-handlers";

class CSelect {
  constructor(element) {
    new Dispatcher(this);
    this.multiple = false;
    this._DOMInit(element);
  }

  get opened() {
    return this._opened;
  }

  set opened(value) {
    this._opened = Boolean(value);
    this.element.classList.toggle('opened', this._opened);
    if (this._opened) {
      this.hidden.slideDown(400, () => {
        this.expanded = true;
      });
    } else {
      this.hidden.slideUp(400, () => {
        this.expanded = false;
      });
    }
  }

  get checked() {
    return this.items.filter(item => item.input.checked);
  }

  get value() {
    return this.checked.map(item => item.value);
  }

  set value(items = []) {
    this.items.forEach(item => {
      item.input.checked = items.indexOf(item.value) !== -1;
    });
    this._update();
  }

  get textValue() {
    return this.checked.map(item => item.text);
  }

  _DOMInit(element) {
    this.element = element;

    this.element.select = this;

    this.title = this.element.querySelector(CSelect.selectors.title);
    this.hidden = sslide(this.element.querySelector(CSelect.selectors.hidden));
    this.selected = this.element.querySelector(CSelect.selectors.selected);

    this._opened = this.element.classList.contains('opened');
    this.hidden.style.display = this._opened ? "block" : "none";

    this.title.addEventListener('click', (e) => {
      this.opened = !this.opened;
    });

    let search = this.element.querySelector(CSelect.selectors.search);
    if (search)
      this.search = new ItemsSearch(search);

    this.items = Array.from(this.element.querySelectorAll(CSelect.selectors.item))
      .map(item => {
        let input = item.querySelector('input');

        if (input.type == "checkbox") this.multiple = true;

        return {
          item: item,
          input: input,
          text: item.dataset[CSelect.selectors.textProp],
          name: input.name,
          value: input.value
        }
      });

    this._update();

    
    this.element.addEventListener('change', () => this._onChange());

    addHandler('click', e => {
      if (!this.expanded) return;

      if (!this.element.contains(e.target)) {
        this.opened = false;
      }
    })
  }

  _onChange() {
    let value = this.value;
    this._update();
    this.trigger('change', value);
    this.element.dispatchEvent(new Event('select-change'));
    
    if (!this.multiple) {
      this.opened = false;
    }
  }

  _update() {
    if (this.selected) {
      this.selected.innerHTML = this.textValue.join(', ') || 'Ничего не выбрано';
    }
  }
}

CSelect.init = (element = document.body) => {
  let selects = Array.from(document.querySelectorAll(CSelect.selectors.element));
  selects.forEach(select => new CSelect(select));
}

CSelect.selectors = {
  element: '.c-select',
  title: '.c-select__title',
  hidden: '.c-select__hidden',
  item: '.c-select__item',
  textProp: 'text',
  selected: '.c-select__selected',
  search: '.items-search',
}

export default CSelect;