import CFormFieldsFactory from "./c-form-fields/c-form-fields-factory";
import Validator from "../validator/validator";
import packFormData from "../../helpers/pack-form-data";

/**
 * @class CForm
 * @description Класс формы
 * 
 * @property {HTMLElement} form
 * @property {Object} elements
 * @property {HTMLElement} elements.error
 * @property {HTMLElement} elements.submit
 * @property {string} method
 * @property {Validator} validator
 * @property {CFormFieldsFactory} factory
 * @property {CFormField[]} fields 
 * @property {Boolean} valid 
 */
class CForm {
  constructor(form, config) {
    this.form = form;
    this.elements = this._DOMInit();

    this._beforeSubmit = config.beforeSubmit;
    this._onSuccess = config.onSuccess;

    this.method = config.method;

    this.validationRules = config.validationRules;
    this.getValidationErrorMessage = config.getValidationErrorMessage;

    this.validator = new Validator(this);
    this.factory = new CFormFieldsFactory(this.validator);

    this.fields = [];
    this._init();
  }

  _DOMInit() {
    this.form.addEventListener('submit', e => {
      e.preventDefault();
      this.submit();
    });

    let error = this.form.querySelector(CForm.selectors.error);
    let submit = this.form.querySelector("[type='submit']");

    return {
      error, submit
    };
  }

  _init() {
    let fields = Array.from(document.querySelectorAll(CForm.selectors.field));
    this.fields = fields.map(field => {
      let formField = this.factory.create(field);
      if (formField) {
        formField.on('change', () => this._onChange())
      }
      return formField;
    }).filter(field => field);
  }

  get valid() {
    return this._valid;
  }

  set valid(value) {
    this._valid = Boolean(value);
  }

  get inProcess() {
    return this._inProcess;
  }

  set inProcess(value) {
    this._inProcess = Boolean(value);
    this.elements.submit.disabled = this._inProcess;
    if (this._inProcess) {
      this.form.setAttribute('data-submiting', '');
    } else {
      this.form.removeAttribute('data-submiting');
    }
  }

  validate(silent) {
    if (this._validating) return;

    this._validating = true;
    let errors = [];
    let firstErrorField = null;

    this.fields.forEach(field => {
      errors = [...errors, ...field.validate(silent)];
      if (!firstErrorField && !field.valid) {
        firstErrorField = field;
      }
    });

    if (firstErrorField && !silent) firstErrorField.focus();

    this.valid = errors.length === 0;

    this._validating = false;
  }

  _onChange() {
    this.validate("silent");
  }


  submit() {
    
    this.validate();

    if (!this.valid) {
      window.showAlert(
        "Пожалуйста, заполните обязательные поля",
        "top-right",
        "error"
      );
      return;
    }

    if (this._beforeSubmit) {
      this._beforeSubmit();
      return;
    }

    this.inProcess = true;

    let fieldsData = {};
    this.fields.forEach(field => {
      let data = field.getData();
      for (let name in data) {
        fieldsData[name] = data[name];
      }
    });
    let data = packFormData(fieldsData);

    API(this.method, null, data)
      .then(result => {
        if (result.success) {
          this._onSuccess ? this._onSuccess(result) : null;
        } else {

          if (!result.errors) {
            this.validator.setError([result.error || "Что-то пошло не так"]);
          } else {
            this.setErrors(result.errors);
          }
          this.form.scrollIntoView({
            behavior: 'smooth'
          });
          this.inProcess = false;
        }
      })
      .catch(error => {
        this.inProcess = false;
        console.error(error);
        this.validator.setError(["Что-то пошло не так"]);
      })
  }

  setErrors(errors) {
    this.fields.forEach(field => {
      if (errors[field.name]) {
        field.setInvalid();
        field.setError(true, this.validator.getMessage(errors[field.name]));
      }
    })
  }
}

CForm.selectors = {
  field: '[data-c-form-field]',
  error: '.c-form__error'
}

export default CForm;