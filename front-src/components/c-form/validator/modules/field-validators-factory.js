import FieldValidator from './field-validator';
import EmailFieldValidator from "./email-field-validator";
import TextFieldValidator from "./text-field-validator";
import TextareaValidator from "./textarea-validator";

class FieldValidatorsFactory {
  getValidator(input) {
    if (input.type === "email") return new EmailFieldValidator(input);

    if (input.type === "text") return new TextFieldValidator(input);

    if (input.tagName.toUpperCase() === "textarea") return new TextareaValidator(input);

    return new FieldValidator(input);
  }
}

export default FieldValidatorsFactory;