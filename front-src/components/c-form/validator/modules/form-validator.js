import FieldValidatorsFactory from "./field-validators-factory";

class FormValidator {
    constructor(form, config) {
        this.form = form;
        this.form.isValid = false;

        this.form.novalidate = true;

        this.validators = [];

        this.factory = new FieldValidatorsFactory();

        [...this.form.elements].forEach(element => {
            let validator = this.factory.getValidator(element);
            if (validator) this.validators.push(validator);
        });

        let onSubmit = e => {
            e.preventDefault();
            if (this.validate()) {
                this.form.isValid = true;
                this.form.dispatchEvent(new Event(FormValidator.events.validated));
                if (!this.form.hasAttribute('data-on-submit')) {
                    this.form.submit();
                }
            } else {
                this.form.isValid = false;
            }
        };

        this.form.addEventListener('submit', onSubmit);
        this.form.addEventListener('reset', () => {
            this.validators.forEach(input => input.reset());
        })
    }

    validate() {
        let errors = 0;
        let firstError = null;
        this.validators.forEach(validator => {
            if (!validator.validate()) {
                if (!firstError) firstError = validator;
                errors++;
            } 
        });

        if (firstError) {
            firstError.input.scrollIntoView({
                behavior: 'smooth'
            });
            firstError.input.focus();
        }

        return errors === 0;
    }
}

FormValidator.events = {
  validated: "FormValidator.events.validated"
};

FormValidator.init = () => {
  let forms = Array.from(document.querySelectorAll('form[data-validate]'));
  forms.forEach(form => {
      if (form.validator) return;
      form.validator = new FormValidator(form);
  });
};

export default FormValidator;