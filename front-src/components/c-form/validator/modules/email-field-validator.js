import FieldValidator from "./field-validator";

class EmailFieldValidator extends FieldValidator {
  constructor(input) {
      super(input);
      this.re = /^[A-Z0-9._%+-]+@[A-Z0-9-]+\.[A-Z]{2,4}$/i;
      this.errorText = "Некорректный адрес электронной почты";
  }
}

export default EmailFieldValidator;