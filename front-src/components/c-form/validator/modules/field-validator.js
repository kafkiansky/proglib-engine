class FieldValidator {
    constructor(input) {
        this.input = input;
        this.required = this.input.hasAttribute('required');

        this.re = false;

        this.validClass = "valid";
        this.invalidClass = "invalid";
        this.errorText = "";
        this.messageClass = "fs-1 pl-2 error-message color_error";

        this.validate = this.validate.bind(this);
    }  

    get value() {
        return this.input.value;
    }

    get valid() {
        return this._valid;
    }

    set valid(value) {
        this._valid = Boolean(value);

        if (this._valid) {
            this.input.classList.add(this.validClass);
            this.input.classList.remove(this.invalidClass);
            if (this.input.errorBlock) this.input.errorBlock.remove();
        } else {
            if (!this.input.errorBlock) {
                this.input.errorBlock = document.createElement('div');
                this.input.errorBlock.textContent = this.errorText;
                this.input.errorBlock.className = this.messageClass;
            }
    
            this.input.parentElement.insertBefore(this.input.errorBlock, this.input.nextElementSibling);
    
            this.input.classList.remove(this.validClass);
            this.input.classList.add(this.invalidClass);
        }   
    }

    validate() {
        this.listen();

        let valid = this.input.checkValidity();

        if (valid) {
            let value = this.value;

            if (!value && !this.required) valid = true;
            else if (this.re) valid = this.re.test(value);
            else valid = this.checkValue(value);
        }

        this.valid = valid;
        
        return valid;
    }


    checkValue() {
        return true;
    }

    listen() {
        if (this.input.hasValidatorListener) return;
        this.input.hasValidatorListener = true;
        this.setListener();
    }

    stopListen() {
        if (!this.input.hasValidatorListener) return;
        this.input.hasValidatorListener = false;
        this.removeListener();
    }

    setListener() {
        this.input.addEventListener('change', this.validate);
        this.input.addEventListener('input', this.validate);
    }

    removeListener() {
        this.input.removeEventListener('change', this.validate);
        this.input.removeEventListener('input', this.validate);
    }

    reset() {
        if (this.input.errorBlock) this.input.errorBlock.remove();
    }
}

export default FieldValidator;