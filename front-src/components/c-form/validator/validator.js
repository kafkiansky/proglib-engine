import FormValidator from "./modules/form-validator";
window.FormValidator = FormValidator;
window.ready('validator');
window.onReady('dom', () => {
  FormValidator.init();
});