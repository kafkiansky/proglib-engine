import TextareaResizer from '../textarea-resize';

class InputsList {
  constructor(element) {
    this.element = element;

    this.items = [];
    this.name = this.element.dataset[InputsList.selectors.nameProperty] + "[]";

    this.required = this.element.hasAttribute('required');

    this._init();
  }

  _init() {
    let items = Array.from(this.element.querySelectorAll('.' + InputsList.selectors.itemClass));
    items.forEach((item) => {
      this._initItem(item);
    });
    
    this._onUpdate();
  }

  _initItem(element) {
    let input = element.querySelector('.' + InputsList.selectors.inputClass);

    new TextareaResizer(input, 0);

    let item = {
      element: element,
      input: input
    };   

    input.addEventListener('focus', () => this._onFocus(item));
    input.addEventListener('blur', () => this._onBlur(item));

    input.addEventListener('keydown', e => {
      if (e.keyCode == 8 && !input.value) this._onRemove(item);
      else if (e.keyCode == 13) {
        e.preventDefault();
        this._toNext(item);
      }
    });

    this.items.push(item);
  }

  get last() {
    return this.items[this.items.length - 1];
  }

  get value() {
    return this.items.map(item => item.input.value).filter(value => value);
  }

  _onFocus(item) {
    if (this._removing) return;
    if (item === this.last) {
      this._createItem();
    }
  }

  _onBlur(item) {

  }

  _toNext(item) {
    if (item == this.last) {
      let newItem = this._createItem();
      newItem.input.focus();
    } else {
      let index = this.items.indexOf(item);
      if (index == -1) return;
      this.items[index + 1].input.focus();
    }
  }

  _onRemove(item) {
    if (this.items.length < 2) return;

    this._removing = true;

    let index = this.items.indexOf(item);
    if (index !== -1) this.items.splice(index, 1);
    item.element.remove();

    let prevIndex = Math.max(0, index - 1);
    this.items[prevIndex].input.focus();

    this._removing = false;

    this._onUpdate();
  }

  _createItemElement(value) {
    let item = document.createElement('label');
    item.classList.add(InputsList.selectors.itemClass);

    let input = document.createElement('textarea');
    input.classList.add(InputsList.selectors.inputClass);
    input.name = this.name;

    if (value) input.value = value;

    item.appendChild(input);

    return item;
  }

  _createItem(value) {
    let newItem = this._createItemElement(value);
    this._initItem(newItem);
    this.element.appendChild(newItem);
    this._onUpdate();
    return newItem;
  }

  _onUpdate() {
    this.items.forEach((item, index) => {
      item.input.required = index === 0;
    });
    this.element.dispatchEvent(new Event(InputsList.events.change));
  }
}

InputsList.init = (element = document.body) => {
  let lists = Array.from(document.querySelectorAll(InputsList.selectors.element));
  lists.forEach(list => new InputsList(list));
}

InputsList.selectors = {
  element: ".inputs-list",
  itemClass: "inputs-list__item",
  inputClass: "inputs-list__input",
  nameProperty: "name"
};

InputsList.events = {
  change: 'inputList.events.change'
};

export default InputsList;