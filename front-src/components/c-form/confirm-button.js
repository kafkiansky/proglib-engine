class ConfirmButton {
  constructor(button, cb) {
    this.button = button;
    this.cb = cb;
    this._waiting = false;
    ConfirmButton.instances.push(this);
    if (!ConfirmButton.inited) ConfirmButton.init();
    return button;
  }

  get waiting() {
    return this._waiting;
  }

  set waiting(value) {
    this._waiting = Boolean(value);
    this.button.classList.toggle('waiting-confirm', this._waiting);
  }

  is(element) {
    return this.button.contains(element);
  }
}

ConfirmButton.instances = [];
ConfirmButton.inited = false;
ConfirmButton.init = () => {
  if (ConfirmButton.inited) return;
  ConfirmButton.inited = true;
  document.addEventListener('click', (e) => {
    for (let i = 0; i < ConfirmButton.instances.length; i++) {
      let btn = ConfirmButton.instances[i];
      if (!btn.is(e.target)) {
        btn.waiting = false;
      } else if (btn.waiting) {
        btn.waiting = false;
        btn.cb();
      } else {
        btn.waiting = true;
      }
    }
  });
}


export default ConfirmButton;