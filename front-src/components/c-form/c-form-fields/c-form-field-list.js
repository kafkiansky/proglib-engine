import CFormFieldMulti from "./c-form-field-multi";

class CFormFieldList extends CFormFieldMulti {
  constructor(...data) {
    super(...data);
    this.field.addEventListener('inputList.events.change', () => {
      this.inputs.forEach(input => this.initInput(input));
    });
  }

  get checked() {
    return this.inputs.filter(input => input.value.trim());
  }

  get inputs() {
    return Array.from(this.field.querySelectorAll('input, textarea'));
  }

  get changeEventName() {
    return "input";
  }

  setInvalid() {
    super.setInvalid();
    let input = this.inputs.find(input => !input.value.trim());
    if (input) input.classList.add('invalid');
  }

  setValid() {
    super.setValid();
    this.inputs.forEach(input => input.classList.remove('invalid'));
  }

  focus() {
    super.focus();
    let first = this.inputs[0];
    if (first) first.focus();
  }
}

export default CFormFieldList;