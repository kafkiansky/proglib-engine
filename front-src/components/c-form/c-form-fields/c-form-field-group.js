import CFormFieldMulti from "./c-form-field-multi";

class CFormFieldGroup extends CFormFieldMulti {
  get inputs() {
    return Array.from(this.field.querySelectorAll('[type="checkbox"]'));
  }
}

export default CFormFieldGroup;