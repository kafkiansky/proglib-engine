import CFormFieldValue from "./c-form-field-value";

class CFormFieldMail extends CFormFieldValue {
  get re() {
    return "mail";
  }
}

export default CFormFieldMail;