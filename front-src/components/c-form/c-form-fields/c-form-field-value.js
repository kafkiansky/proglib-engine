import CFormField from "./c-form-field";

class CFormFieldValue extends CFormField {

  get value() {
    return this.input.value;
  }

  get re() {
    return null;
  }

  setInvalid() {
    super.setInvalid();
    this.input.classList.add('invalid');
  }

  setValid() {
    super.setValid();
    this.input.classList.remove('invalid');
  }

  init() {
    this.input = this.field.querySelector('input, textarea');
    this.input.addEventListener('input', () => this._onChange());
  }

  specialValidate(silent) {
    let value = this.value;
    let errors = this.validator.validateCount(value, this.name);
    let re = this.re;
    if (re && !this.validator.validateRegex(value, re)) {
      errors.push(re + "-re");
    }
    return errors;
  }

  focus() {
    super.focus();
    this.input.focus();
  }
  
}

export default CFormFieldValue;