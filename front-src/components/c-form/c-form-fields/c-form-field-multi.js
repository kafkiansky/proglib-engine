import CFormField from "./c-form-field";

class CFormFieldMulti extends CFormField {
  get checked() {
    return this.inputs.filter(input => input.checked);
  }

  get value() {
    return this.checked.map(input => input.value);
  }

  get inputs() {
    return Array.from(this.field.querySelectorAll('input'));
  }

  get changeEventName() {
    return "change";
  }

  init() {
    this.inputs.forEach(input => this.initInput(input));
  }

  initInput(input) {
    if (input.inited) return;
    input.inited = true;
    input.addEventListener(this.changeEventName, () => this._onChange());
  }

  specialValidate(silent) {
    let value = this.value;
    let errors = this.validator.validateCount(value, this.name);
    return errors;
  }
}

export default CFormFieldMulti;