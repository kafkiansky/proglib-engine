import Dispatcher from "../../../services/dispatcher";

class CFormField {
  constructor(field, validator) {
    new Dispatcher(this);

    this.field = field;
    this.name = this.field.dataset[CFormField.selectors.nameProp];
    this.validator = validator;

    this.error = document.createElement('div');
    this.error.className = 'error color_error';

    this.showError = false;

    this.init();
  } 

  get valid() {
    return this._valid;
  }

  set valid(value) {
    let oldValue = this._valid;
    this._valid = Boolean(value);

    if (oldValue !== this._valid) this.trigger('change');
  }

  get value() {

  }

  getData() {
    let data = Object.create(null);
    data[this.name] = this.value;
    return data;
  }

  init() {

  }

  focus() {
    this.field.scrollIntoView({
      behavior: 'smooth',
      block: 'center'
    });
  }

  setInvalid() {
    this.field.classList.add('invalid');
  }

  setValid() {
    this.field.classList.remove('invalid');
  }

  validate(silent) {
    if (this.field.novalidate) {
      this.valid = true;
      this.setValid();
      this.setError(false);
      return [];
    };

    let errors = this.specialValidate(silent);

    if (errors.length) {
      this.valid = false;
      if (!silent) {
        this.setInvalid();
        this.setError(true, this.validator.getMessage(errors[0]));
      }
    } else {
      this.valid = true;
      this.setValid();
      this.setError(false);
    }
    return errors;
  }

  specialValidate(silent) {
    return [];
  }

  _onChange() {
    if (this.showError) {
      let errors = this.validate();
    }
  }

  setError(hasError, html) {
    if (!hasError) {
      this.showError = false;
      this.hideErrorMessage();
    } else {
      this.showError = true;
      this.showErrorMessage(html);
    }
  }

  showErrorMessage(html) {
    this.error.innerHTML = html;
    this.field.appendChild(this.error);
  }

  hideErrorMessage() {
    this.error.remove();
    this.error.innerHTML = '';
  }
}

CFormField.selectors = {
  nameProp: 'name'
};

export default CFormField;