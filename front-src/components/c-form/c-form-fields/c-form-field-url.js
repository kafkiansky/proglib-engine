import CFormFieldValue from "./c-form-field-value";

class CFormFieldUrl extends CFormFieldValue {
  get re() {
    return "url";
  }
}

export default CFormFieldUrl;