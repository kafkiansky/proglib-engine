import CFormFieldValue from "./c-form-field-value";

class CFormFieldNumber extends CFormFieldValue {
  get value() {
    return Number(this.input.value);
  }
}

export default CFormFieldNumber;