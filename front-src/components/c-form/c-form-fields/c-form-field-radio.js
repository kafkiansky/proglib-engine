import CFormFieldMulti from "./c-form-field-multi";

class CFormFieldRadio extends CFormFieldMulti {
  get inputs() {
    return Array.from(this.field.querySelectorAll('[type="radio"]'));
  }

  getData() {
    let data = Object.create(null);
    data[this.name] = this.value[0];
    return data;
  }
}

export default CFormFieldRadio;