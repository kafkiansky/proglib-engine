import CFormField from "./c-form-field";

class CFormFieldLoader extends CFormField {
  get value() {
    return this.loader.getUrls();
  }

  getData() {
    let value = this.value;
    let data = Object.create(null);

    if (this.loader.multiple) {
      data[this.name] = value;
    } else {
      data[this.name] = value[0];
    }

    return data;
  }

  get input() {
    return this.field.querySelector('input[type="file"]');
  }

  init() {
    this.loader = this.field.loader;
    this.field.addEventListener('loader.events.change', () => this._onChange());
  }

  specialValidate(silent) {
    let value = this.value;
    return this.validator.validateCount({ length: value.length }, this.name);
  }
}

export default CFormFieldLoader;