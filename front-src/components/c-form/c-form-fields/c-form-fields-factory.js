import CFormFieldText from "./c-form-field-text";
import CFormFieldNumber from "./c-form-field-number";
import CFormFieldGroup from "./c-form-field-group";
import CFormFieldList from "./c-form-field-list";
import CFormFieldLoader from "./c-form-field-loader";
import CFormFieldMail from "./c-form-field-mail";
import CFormFieldUrl from "./c-form-field-url";
import CFormFieldRadio from "./c-form-field-radio";

class CFormFieldsFactory {
  constructor(validator) {
    this.validator = validator;
  }

  create(field) {
    let type = field.dataset.validate;
    

    switch(type) {
      case "text":
        return new CFormFieldText(field, this.validator);
      case "mail":
        return new CFormFieldMail(field, this.validator);
      case "url":
        return new CFormFieldUrl(field, this.validator);
      case "number":
        return new CFormFieldNumber(field, this.validator);
      case "group":
        return new CFormFieldGroup(field, this.validator);
      case "radio":
        return new CFormFieldRadio(field, this.validator);
      case "list":
        return new CFormFieldList(field, this.validator);
      case "loader":
        return new CFormFieldLoader(field, this.validator);
      default:
        return false;
    }
  }
}

export default CFormFieldsFactory;