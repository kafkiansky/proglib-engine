import addHandler from "../../services/events-handlers";

class TextareaResizer {
    constructor(textarea, offset = 30) {
        this.field = textarea;

        this.clone = this.getClone();
        this.updateClone();

        this.offset = offset;

        this.initField();
    }

    getClone() {
        let clone = document.createElement('div');
        
        clone.style.position = "absolute";
        clone.style.visibility = "hidden";
        clone.style.color = "transparent";

        this.field.parentElement.insertBefore(clone, this.field);

        return clone;
    }

    updateClone() {
        let style = window.getComputedStyle(this.field);

        this.clone.style.padding = style.padding;
        this.clone.style.font = style.font;
        this.clone.style.lineHeight = parseInt(style.lineHeight) + 2 + "px";

        let width = this.field.offsetWidth;
        this.clone.style.width = width + "px";

        let text = "";
        let value = this.field.value;
        value = value.replace(/[<>]/g, '_');
        value = value.split("\n");
        value.forEach(function(s) {
            text = text + '<div>' + s.replace(/\s\s/g, ' &nbsp;') + '&nbsp;</div>'+"\n";
        } );
        this.clone.innerHTML = text;
    }

    resize() {
        if (this.field.value == "") {
            this.field.style.height = "";
            return;
        }
        this.updateClone();
        let height = this.clone.offsetHeight + this.offset;
        this.field.style.height = height + "px";
    }

    initField() {
        this.field.style.resize = "none";
        this.field.style.overflow = "hidden";
        this.field.addEventListener('keyup', () => this.resize());

        addHandler('resize', () => this.resize());
    }
}

window.ready('textarea-resizer');

export default TextareaResizer;