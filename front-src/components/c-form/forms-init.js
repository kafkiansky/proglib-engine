import CSelect from "./c-select/c-select";
import InputsList from "./inputs-list/inputs-list";

const formsInit = (config = {}) => {
  CSelect.init();
  InputsList.init();
}

export default formsInit;