export default function countAjaxProgress(e) {
  return parseInt(e.loaded / e.total * 100);    
}