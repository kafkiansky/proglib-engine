let regexps = {
    imgUrl: /(http(s?):)*([/|.|\w|\s|-])*\.(?:jpe?g|gif|png)/g,
    uuid: /[0-9A-Fa-f]{8}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{4}\-[0-9A-Fa-f]{12}/,
    mail: /^[A-Z0-9._%+-]+@[A-Z0-9-]+\.[A-Z]{2,4}$/i,
    url: /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/,
};

export default regexps;