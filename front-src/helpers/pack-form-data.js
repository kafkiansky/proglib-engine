export default (data) => {
  let formData = new FormData();

  if (!data) return formData;
  for (let propertyName in data) {
    let propertyValue = data[propertyName];
    if (typeof propertyValue === "number" || typeof propertyValue === "string") 
      formData.append(propertyName, propertyValue);
    else if (Array.isArray(propertyValue)) {
      propertyValue.forEach(value => {
        formData.append(propertyName + "[]", value);
      });
    }
    else if (typeof propertyValue === "object") {
      formData.append(propertyName, JSON.stringify(propertyValue));
    }
  }

  return formData;
}