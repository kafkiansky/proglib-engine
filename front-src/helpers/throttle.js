/**
 * throttler
 * @param {function} funcToFire Функция для отложенного запуска
 * @param {number} holdTime Задержка в мс
 */
function throttler(funcToFire, holdTime) {
    let isThrottled = false,
        savedArguments,
        savedThis;

    function action(...args) {
        if (isThrottled) {
            savedArguments = args;
            savedThis = this;
            return;
        }

        funcToFire.apply(this, args);
        isThrottled = true;

        setTimeout(() => {
            isThrottled = false;
            if (savedArguments) {
                action.apply(savedThis, savedArguments);
                savedThis = savedArguments = null;
            }
        }, holdTime);
    }

    return action;
}

export default throttler;