function getWordEnding(number, titles) {
  let cases = [2, 0, 1, 1, 1, 2];  
  return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}

getWordEnding.TAGS = {
  dat: ['тег', 'тега', 'тегов'],
  gen: ['тег', 'тега', 'тегов']
};
getWordEnding.SYMBOLS = {
  dat: ['символ', 'символа', 'символов'],
  gen: ['символа', 'символов', 'символов'],
};
getWordEnding.WORDS = {
  dat: ['слово', 'слова', 'слов'],
  gen: ['слова', 'слов', 'слов'],
};

export default getWordEnding;