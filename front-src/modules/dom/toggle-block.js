import addHandler from "../../services/events-handlers";
import sslide from "../libs/sslide";

class ToggleBlock {
    constructor(el, config = {}) {
        this.el = el;

        if (!el) throw new Error('No toggle block root element');

        el.toggleBlock = this;

        this.config = Object.create(ToggleBlock.defaultConfig);
        Object.assign(this.config, config);
        
        this.name = this.config.name || "toggle-block";

        this.openers = []; // элементы, клик по которым открывает блок 
        this.closers = []; // элементы, клик по которым закрывает блок
        this.togglers = []; // элементы, клик по которым изменяет состояние блока
        this.hidden = null; // скрытая часть
        this.ignore = []; // части блока, клик по которым не закрывает его

        this.needFocus = false;

        this.opened = false;

        
        
        this.init();
    }

    init() {
        this.el.setAttribute(ToggleBlock.defaultElementAttr, "");

        if (this.el.classList.contains(this.config.openedClass) || this.el.hasAttribute(this.config.openedAttr)) {
            this.opened = true;
        }

        if (this.config.hidden) {
            this.hidden = this.el.querySelector(this.config.hidden);
            if (this.hidden) this.hidden = sslide(this.hidden);
        }

        if (this.config.opener) {
            this.openers = Array.from(this.el.querySelectorAll(this.config.opener));

            this.openers.forEach(opener => {
                opener.addEventListener('click', e => {
                    this.justOpened = false;

                    if (!this.opened) {
                        this.onOpen();
                    }
                })
            })
        }
        
        if (this.config.closer) {
            this.closers = Array.from(this.el.querySelectorAll(this.config.closer));
            this.closers.forEach(closer => {
                closer.addEventListener('click', (e) => {
                    if (this.opened) {
                        this.onClose();
                    }   
                });
            });
        }

        if (this.config.toggler) {
            this.togglers = Array.from(this.el.querySelectorAll(this.config.toggler));
            this.togglers.forEach(toggler => {
                toggler.addEventListener('click', (e) => {
                    this.opened ? this.onClose() : this.onOpen();
                });
            });
        }

        if (this.config.ignore) {
            this.ignore = Array.from(this.el.querySelectorAll(this.config.ignore));
        }

        if (this.hidden) {
            this.ignore.push(this.hidden);
        }
        
        if (this.el.hasAttribute(this.config.slideAttr)) {
            this.config.slide = true;

            try {
                let duration = this.el.getAttribute(this.config.slideAttr);
                this.config.slideDuration = parseInt(duration);
            } catch(e) {}
        }

        if (this.el.hasAttribute(this.config.outsideClickAttr)) {
            this.config.closeOnOutsideClick = false;
        }

        if (this.config.closeOnOutsideClick) {
            addHandler('click', (e) => {
                if (!this.check()) {
                    return;
                }
    
                if (!this.opened) return;
    
                if (this.justOpened) {
                    this.justOpened = false;
                    return;
                }
                
                if (!this.el.contains(e.target)) this.onClose();
                let inIgnoreArea = this.ignore.some(ignore => {
                    return ignore && ignore.contains(e.target)
                })
                if (!inIgnoreArea) {
                    this.onClose();
                }
            })
        }

        if (this.el.hasAttribute(this.config.focusAfterOpenAttr)) {
            this.needFocus = true;
        }
        
    }

    onOpen() {
        if (!this.check()) {
            return;
        }

        if (this.config.beforeOpen) {
            this.config.beforeOpen((silent) => this.open(silent));
        } else {
            this.open();
        }
    }

    open(silent) {
        if (!this.check()) {
            return;
        }

        if (this.opened) return;

        this.opened = true;
        this.justOpened = true;

        if (this.config.openedClass) this.el.classList.add(this.config.openedClass);
        if (this.config.closedClass) this.el.classList.remove(this.config.closedClass);

        if (this.config.openedAttr) this.el.setAttribute(this.config.openedAttr, "");
        if (this.config.closedAttr) this.el.removeAttribute(this.config.closedAttr);

        if (this.config.slide && this.hidden) {
            this.hidden.slideDown(this.config.slideDuration);
        }

        if (this.needFocus) {
            let input = this.el.querySelector('input');
            if (input) input.focus();
        }
        
        if (!silent && this.config.onOpen) this.config.onOpen();
    }

    onClose() {
        if (!this.check()) {
            return;
        }

        if (!this.opened) return;

        if (this.config.beforeClose) {
            this.config.beforeClose(silent => this.close(silent));
        } else {
            this.close();
        }
    }

    close(silent) {
        if (!this.check()) {
            return;
        }

        if (!this.opened) return;
        this.opened = false;
        this.justOpened = false;

        if (this.config.openedClass) this.el.classList.remove(this.config.openedClass);
        if (this.config.closedClass) this.el.classList.add(this.config.closedClass);

        if (this.config.openedAttr) this.el.removeAttribute(this.config.openedAttr);
        if (this.config.closedAttr) this.el.setAttribute(this.config.closedAttr, "");

        if (this.config.slide && this.hidden) {
            this.hidden.slideUp(this.config.slideDuration);
        }

        if (!silent && this.config.onClose) this.config.onClose();
    }

    check() {
        if (!this.el) {
            delete this;
            return false;
        }
        return true;
    }
}

ToggleBlock.defaultElementAttr = "data-toggle-block";

ToggleBlock.defaultConfig = {
    openedClass: "opened",
    openedAttr: "data-opened",
    closedClass: false,
    closedAttr: false,
    closeOnOutsideClick: true,
    outsideClickAttr: "data-toggle-inside",
    slide: false,
    slideDuration: 400,
    slideAttr: 'data-toggle-slide',
    opener: "[data-toggle-open]",
    closer: "[data-toggle-close]",
    toggler: "[data-toggle-toggle]",
    ignore: "[data-toggle-ignore]",
    hidden: "[data-toggle-hidden]",
    focusAfterOpenAttr: "data-toggle-focus"
};

ToggleBlock.init = () => {
    let handleToggleBlocks = () => {
        let toggleBlocks = Array.from(document.querySelectorAll(`[${ToggleBlock.defaultElementAttr}]`));
        toggleBlocks.forEach(block => {
            if (block.toggleBlock) return;
            new ToggleBlock(block);
        });
    }

    handleToggleBlocks();

    document.body.addEventListener('feed.events.update', handleToggleBlocks);
}

export default ToggleBlock;
