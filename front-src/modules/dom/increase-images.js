import "./increase-images.scss";

import addHandler from "../../services/events-handlers";
import Popup from "../../components/popup/popup";

;(function() {
    let increaseContainers = [".post", ".comment"]

    /* image increase */
    let imgPreview = document.getElementById("image-preview");

    if (!imgPreview) return;

    let previewPopup = new Popup(imgPreview);

    addHandler("click", e => {
        if (e.target.tagName.toUpperCase() == "IMG" && e.target.src) {
            for (let i = 0, count = increaseContainers.length; i < count; i++) {
                let parent = e.target.closest(increaseContainers[i])
                if (parent) {
                    e.stopPropagation();
                    imgPreview.querySelector("img").src = e.target.src;
                    previewPopup.open();
                    break;

                    // TODO: slider
                }
            }
            
        }
    });
})();