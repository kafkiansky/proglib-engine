/**
 * Липкий блок с комментариями в сайдбаре
 */

window.onReady('dom1', () => {
  // dom-элементы

  /* 
  блок комментариев 
  липкое поведение
  при достижении верха экрана фиксируется, пока родительский элемент не подхватит его снизу
  ограничен родительским элементом
  */
  let commentsBlock = document.querySelector('.comments-block'); 
  if (!commentsBlock) return;

  /*
  сайдбар
  родительский элемент, внутри которого двигается блок комментариев
  */
  let sidebar = commentsBlock.parentElement; 
  sidebar.style.position = "relative";

  /*
  предыдущий блок перед блоком комментариев
  */
  let prevBlock = commentsBlock.previousElementSibling;

  // отступы

  /*
  отступ блока комментариев в зафиксированном состоянии сверху (с учетом высоты шапки)
  */
  let fixTopPosition = 65; 
  /*
  отступ блока комментариев в залипшем состоянии снизу
  */
 let fixBottomPosition = 30; 
  /*
  минимальный отступ от нижней границы зафиксированного блока до нижней границы родителя,
  при котором происходит приклеивание к низу родителя
  */
  let bottomOffsetToStick = fixBottomPosition;
  /*
  максимальный выступ нижней границы предыдущего элемента от верхней границы экрана,
  при котором блок отлипает от верха и встраивается в общий поток,
  */
  let topOffsetToUnfix = fixTopPosition;
  /*
  минимальный отступ от верхней границы блока в свободном потоке до верхней границы страницы, 
  при котором происходит фиксация
  */
  let topOffsetToFix = fixTopPosition;

  // состояния блока

  let fixed = false; // зафиксирован в верхней точке
  let sticked = false; // прилеплен к низу родительского блока

  // состояния страницы

  let inited = false; // после загрузки положение блока в сайдбаре проанализировано
  let needStick = true; // при прокрутке нужно реализовывать прилипание

  /*
  фиксация блока в верхней точке
  */
  let fixBlock = () => {
    fixed = true;
    sticked = false;
    commentsBlock.classList.add('fixed');
    commentsBlock.classList.remove('sticked');
    commentsBlock.style.position = "fixed";
    commentsBlock.style.top = fixTopPosition + "px";
    commentsBlock.style.bottom = "";
    checkHeight();
  }

  /*
  возвращение блока в общий поток
  */
  let unfixBlock = () => {
    commentsBlock.classList.remove('fixed');
    commentsBlock.classList.remove('sticked');
    fixed = false;
    sticked = false;
    commentsBlock.style.position = "";
    commentsBlock.style.top = "";
    checkHeight();
  }

  /*
  приклеивание блока к низу родителя
  */
  let stickBlock = () => {
    commentsBlock.classList.remove('fixed');
    commentsBlock.classList.add('sticked');
    sticked = true;
    fixed = false;
    commentsBlock.style.position = "absolute";
    commentsBlock.style.top = "";
    commentsBlock.style.bottom = fixBottomPosition + "px";
    checkHeight();
  }

  let checkHeight = () => {
    let isOverflow = commentsBlock.scrollHeight - commentsBlock.offsetHeight > 20;
    commentsBlock.classList.toggle('overflow', isOverflow);
  }

  /*
  анализ положения блока комментариев в сайдбаре
  */
  let init = () => {
    if (sticked) {
      fixBlock();
    }

    let blockRect = commentsBlock.getBoundingClientRect(); // метрики блока комментариев
    let parentRect = sidebar.getBoundingClientRect(); // метрики родительского контейнера (сайдбара)

    // в сайдбаре нет места для размещения фиксированного блока
    if (parentRect.bottom - blockRect.bottom < blockRect.height + fixTopPosition) {
      needStick = false;
      unfixBlock();
    }
    else needStick = true;

    inited = true;
  }

  /*
  перерасчет положения блока в сайдбаре после обновления фида
  */
  document.body.addEventListener('feed.events.update', init);

  /*
  определение текущего состояния блока
  */
  let setBlock = () => {
    if (!inited) {
      init();
    }

    if (!needStick) return;

    let blockRect = commentsBlock.getBoundingClientRect(); // метрики блока комментариев
    let parentRect = sidebar.getBoundingClientRect(); // метрики родительского контейнера (сайдбара)
    let prevRect = prevBlock.getBoundingClientRect(); // метрики предыдущего блока внутри родителя

    let blockHeight = blockRect.height; // высота блока комментарией
    let parentBottom = parentRect.top + parentRect.height; // отступ низа родительского контейнера от верхней границы экрана
    let prevBottom = prevRect.top + prevRect.height; // отступ низа предыдущего блока от верхней границы экрана

    // блок зафиксирован (приклеен к верху страницы)
    if (fixed) {
      /*
      от низа блока до низа родительского контейнера
      осталось меньше места, чем требуется для фиксированного блока с его отступом
      */
      if (parentBottom - blockHeight - fixTopPosition  < bottomOffsetToStick) {
        // приклеить блок к низу родителя
        stickBlock();
      }
      /*
      предыдущий блок опустился на страницу больше, 
      чем на topOffsetToUnfix пикселей
      */
      else if (prevBottom > topOffsetToUnfix) {
        // вернуть блок в общий поток
        unfixBlock();
      }
    } 
    // блок приклеен к низу родителя
    else if (sticked) {
      /*
      отступ верхнего края блока до верхнего края страницы
      превысил topOffsetToFix пикселей
      */
      if (blockRect.top > topOffsetToFix) {
        // зафиксировать блок сверху
        fixBlock();
      }
    } 
    // блок в общем потоке
    else {
      /*
      отступ верхнего края блока до верхнего края страницы
      стал меньше, чем topOffsetToFix пикселей
      */
      if (blockRect.top < topOffsetToFix) {
        // зафиксировать блок сверху
        fixBlock();
      }
    }
    checkHeight();
  }

  setBlock();

  window.addEventListener('scroll', setBlock, {
    passive: true
  });
  window.addEventListener('resize', checkHeight, {
    passive: true
  });
})
