import "./items-search.scss";

class ItemsSearch {
    constructor(container) {
        this.minSearchLength = 2;
        this.container = container;

        this.items = [];

        this.init();
    }

    init() {
        if (!this.container) return;

        this.field = this.container.querySelector('[data-search-field]');
        if (!this.field) return;

        this.handleField();

        let items = Array.from(this.container.querySelectorAll('[data-search-item]'));
        if (items.length == 0) return;

        this.items = items.map(item => {
            let $text = item.querySelector('[data-search-text]');
            let text = $text.textContent;
            return {
                $el: item,
                $text: $text,
                text: text.toLowerCase(),
                origin: text
            }
        });           
    }

    handleField() { 
        this.field.addEventListener('input', () => {
            let value = this.field.value.toLowerCase();
            if (value && value.length >= this.minSearchLength) {
                this.search(value);
            } else {
                this.clear();
            }
        });
    }

    search(value) {
        this.items.forEach(item => {
            if (item.text.indexOf(value) !== -1) {
                this.showItem(item, value);
            } else {
                this.hideItem(item);
            }
        });
    }

    clear() {
        this.items.forEach(item => this.showItem(item));
    }

    showItem(item, value) {
        item.$el.style.display = "";
        if (value) this.select(item, value);
        else this.deselect(item);
    }

    hideItem(item) {
        item.$el.style.display = "none";
        this.deselect(item);
    }

    select(item, value) {
        this.deselect(item);
        let index = item.text.indexOf(value);
        let last = index + value.length;
        let html = item.text.substring(0, index) + "<b class='marked'>" + value + "</b>" + item.text.substring(last);
        item.$text.innerHTML = html;
    }

    deselect(item) {
        item.$text.textContent = item.origin;
    }
}

ItemsSearch.init = () => {
    let searchContainers = Array.from(document.querySelectorAll('[data-search]'));
    if (searchContainers.length == 0) return;
    searchContainers.forEach(container => new ItemsSearch(container));
}

export default ItemsSearch;