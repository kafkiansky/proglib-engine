/**
 * smooth scrolling to anchors
 */

import addHandler from "../../services/events-handlers";

addHandler("click", e => {
    if (!e.target.hasAttribute("href")) return;
    if (e.target.tagName.toUpperCase() === "USE") return;

    let href= e.target.getAttribute("href");
    if (href.length === 0 || href[0] !== "#") return;

    let targetId = href.slice(1);
    let target = document.getElementById(targetId);
    if (!target) return;

    e.preventDefault();
    target.scrollIntoView({
        behavior: "smooth"
    });
    setTimeout(() => location.hash = targetId, 100);
});