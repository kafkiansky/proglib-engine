import LazyLoad from "lazyload";

let loadImages = () => {
  let images = document.querySelectorAll('.lazyload');
  new LazyLoad(images);
  images.forEach(img => img.classList.remove('lazyload'));
}

export default loadImages;