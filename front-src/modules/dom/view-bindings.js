/**
 * view bindings
 */
import addHandler from "../../services/events-handlers";

;(function() {
    const viewBindings = Array.from(document.querySelectorAll('[data-view]'));

    viewBindings.forEach(el => {
        const container = el.closest(el.dataset.view);
        const offset = el.dataset.viewOffset || 50;
        const outClass = el.dataset.viewOutClass || 'out-of';
        const inClass = el.dataset.viewInClass || 'out-in';

        let isOut = undefined;
        let prevPosition = 0;

        const cb = () => {
            let elMetrics = el.getBoundingClientRect();
            let containerMetrics = container.getBoundingClientRect();

            let elBottom = elMetrics.top + elMetrics.height;
            let containerBottom = containerMetrics.top + containerMetrics.height;
            let isGoDown = containerMetrics.top < prevPosition;
            
            if (isOut && isGoDown) {} 
            else if (containerBottom - elBottom > offset) {
                isOut = false;
                el.classList.remove(outClass);
                el.classList.add(inClass);
                prevPosition = containerMetrics.top;
            } else {
                isOut = true;
                el.classList.add(outClass);
                el.classList.remove(inClass);
            }
        };

        addHandler("scroll", cb);
        cb();
        return;
    });
})();