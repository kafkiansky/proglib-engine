/**
 * Слайдер карточек + слайдер тегов
 * class Slider
 */

import "./slider.scss";

const Slider = class {

    constructor(el) {
        this.el = {};
        this.el.container = el; 
        this.el.animator = this.el.container ? this.el.container.querySelector('.slider__viewport') : undefined; 
        this.el.wrapper = this.el.container ? this.el.container.querySelector('.slides') : undefined;
        this.el.anchors = this.el.animator ? this.el.animator.getElementsByTagName('A') : [];
        this.el.thumb = this.el.container ? this.el.container.querySelector('.slider__thumb') : undefined;
        this.el.left = this.el.container ? this.el.container.querySelector('.slider__left') : undefined;
        this.el.right = this.el.container ? this.el.container.querySelector('.slider__right') : undefined;

        this.items = Array.from(this.el.container.querySelectorAll('.slide'));
        this.shown = this.items;
        this.hidden = [];

        this.started = false;
        this.isScrolled = false;
        this.finished = false;

        this.needSwitch = this.el.container.hasAttribute('data-switch');
        this.scrollBlocked = false;

        this.spare = 5;

        this.rest;
        this.restShow;
        this.restHide;
        this.restList;

        this.events = {};

        for (let i = 0; i < this.el.anchors.length; i++) {
            this.on(this.el.anchors[i], 'click', this.onAnchorClick);
            this.on(this.el.anchors[i], 'touchend', this.onAnchorClick);
        }

        this.on(this.el.container, 'click', this.finish);
        this.on(this.el.container, 'touchend', this.finish);

        this.params = {
            velocity: undefined,
            amplitude: 10,
            posX: 0,
            targetX: undefined,
            lastPosX: undefined,
            lastTouchX: undefined,
            lastTimestamp: undefined,
            ticker: undefined,
            speed: 100
        };
  
        this.init();
    }
  
    init() {
        if (this.el.container && this.el.animator) {
            this.el.container.classList.remove('inactive');
            this.setControls();
            this.setThumbPosition();
            if (this.needSwitch) this.switch();
            this.bindEvents(); 

            if (this.el.left) {
                this.el.left.addEventListener('click', () => this.toLeft());
            }

            if (this.el.right) {
                this.el.right.addEventListener('click', () => this.toRight());
            }
        }
    }

    

    setThumbPosition() {
        if (this.el.thumb) {
            let animatorDiff = this.el.animator.scrollWidth - this.el.animator.offsetWidth;
            let thumbDiff = this.el.animator.offsetWidth - this.el.thumb.offsetWidth;
            let animatorProgress = this.el.animator.scrollLeft / animatorDiff;
            let thumbProgress = thumbDiff * animatorProgress;
            this.el.thumb.style.left = thumbProgress + 'px';

            if (this.el.left) {
                if (thumbProgress < 1) {
                    this.el.left.classList.add('inactive');
                } else {
                    this.el.left.classList.remove('inactive');
                }
            }
           
            if (this.el.right) {
                if (thumbDiff - thumbProgress < 2) {
                    this.el.right.classList.add('inactive');
                } else {
                    this.el.right.classList.remove('inactive');
                }
            }

            
        }
    }

    switchToBlock() {
        this.scrollBlocked = true; // заблокировать тачи
        if (!this.unfold) {
            this.setRest();
            this.updateTrack(); // вырезать элементы из ленты
        }
        
        if (this.el.thumb) {
            this.el.thumb.classList.add('hidden');
        }
        this.el.container.setAttribute('data-view', 'block');
    }

    switchToSlider() {
        if (this.scrollBlocked) { // если было заблокировано
            // вернуть все элементы в ленту
            this.clearRest();
        }
        this.scrollBlocked = false; // снять блокировку тачей
        if (this.el.thumb) {
            this.el.thumb.classList.remove('hidden');
        }
        this.el.container.setAttribute('data-view', 'slider');
    }

    switch() {
        let canScroll = window.getComputedStyle(this.el.wrapper).overflow !== "hidden";
        
        if (canScroll) { // можно скроллить (mobile)
            this.switchToSlider();
        } else { // нельзя скроллить (desktop)
            this.switchToBlock();
        }
    }

    setRest() { // создать блок невидимых слайдов и кнопку показа
        if (!this.rest) {
            this.rest = document.createElement('div');
            this.rest.classList.add('slider-rest');
            this.rest.style.display = "inline-block";

            this.restShow = document.createElement('div');
            this.restShow.className = "btn btn-outline-primary";
            this.restShow.classList.add('slider-rest__more');
            this.rest.appendChild(this.restShow);

            this.restHide = this.el.container.querySelector('.hide-rest');
            this.restHide.addEventListener('click', () => this.hideAll());

            this.restList = document.createElement('div');
            this.restList.style.display = "none";
            this.rest.appendChild(this.restList);

            this.restShow.addEventListener('click', () => this.showAll());
        }
        this.el.container.appendChild(this.rest);
    }

    showAll() {
        this.clearRest();
        this.el.container.classList.add('unfold');
        this.unfold = true;
    }

    hideAll() {
        this.unfold = false;
        this.el.container.classList.remove('unfold');
        this.switchToBlock();
    }

    clearRest() { // вернуть все слайды в ленту
        this.rest.remove();

        while (this.hidden.length) {
            let slide = this.hidden.pop();
            this.shown.push(slide);
            this.el.wrapper.appendChild(slide);
            slide.classList.remove('hidden');
        }
    }

    hideSlide(slide) {
        this.hidden.push(slide);
        this.restList.append(slide);
    }

    showSlide(slide) {
        this.shown.push(slide);
    }

    updateTrack() { // обновить набор видимых слайдов
        let more = this.restShow;
        let moreWidth = more.getBoundingClientRect().width + this.spare;

        let slides = this.el.wrapper;
        let slidesWidth = slides.scrollWidth;

        let container = this.el.container;
        let containerWidth = container.getBoundingClientRect().width;

        let availableWidth = containerWidth - moreWidth;

        if (slidesWidth >= availableWidth) {
            while (slidesWidth >= availableWidth) {
                let lastSlide = this.shown.pop();
                if (!lastSlide) break;
                this.hideSlide(lastSlide);
                
                slidesWidth = slides.scrollWidth;
            }
        } else {
            if (this.hidden.length) {
                while (slidesWidth < availableWidth) {
                    let firstSlide = this.hidden.pop();
                    slides.appendChild(firstSlide);
                    slidesWidth = slides.scrollWidth;
                    if (slidesWidth >= availableWidth) {
                        this.hideSlide(firstSlide);
                        break;
                    } else {
                        this.showSlide(firstSlide);
                    }
                }
            }
        }

        if (!this.hidden.length) more.style.display = "none";
        else more.style.display = "";
    }
  
    on(el, evt, callback) {
        el.id = el.id || Math.random().toString(36).substring(7);
        if (!this.events.hasOwnProperty(el.id)) {
            this.events[el.id] = {};
        }
        callback = callback.bind(this);
        this.events[el.id][evt] = callback;
        el.addEventListener(evt, callback);
    }
  
    off(el, evt) {
        if (
            this.events.hasOwnProperty(el.id) &&
            this.events[el.id].hasOwnProperty(evt)
        ) {
            const callback = this.events[el.id][evt];
            el.removeEventListener(evt, callback);
            delete this.events[el.id][evt];
            if (Object.keys(this.events[el.id]).length === 0) {
                delete this.events[el.id];
            }
        }
    }

    onAnchorClick(e) {
        if (this.isScrolled) {
            this.isScrolled = false;
            this.preventDefault(e);
        }
    }
  
    bindEvents() {
        this.on(this.el.animator, 'touchstart', this.touchStart);
        this.on(this.el.animator, 'mousedown', this.touchStart);
        addHandler('resize', () => {
            this.setControls();
            if (this.needSwitch) this.switch(this);
        });
    }
  
    eventCoords(event) {
        const touchEvents = ['touchmove', 'touchstart', 'touchend'];
        const isTouchEvent = touchEvents.indexOf(event.type) > -1;
        if (isTouchEvent) {
            let touch = event.targetTouches[0] || event.changedTouches[0];
            return {
                x: touch.clientX,
                y: touch.clientY,
            };
        } else {
            return {
                x: event.clientX,
                y: event.clientY,
            };
        }
    }
  
    startTracking() {
        if (this.params.ticker) this.stopTracking();
        if (this.el.animator.offsetWidth < this.el.animator.scrollWidth)
            this.el.container.classList.add('moving');
        this.params.ticker = window.setInterval(this.track.bind(this), 100);
        this.on(document, 'dragstart', this.preventDefault);
        this.on(document, 'touchmove', this.touchMove);
        this.on(document, 'mousemove', this.touchMove);
        this.on(document, 'touchend', this.touchEnd);
        this.on(document, 'mouseup', this.touchEnd);
    }
  
    stopTracking() {
        this.el.container.classList.remove('moving');
        window.clearInterval(this.params.ticker);
        this.off(document, 'touchmove', this.touchMove);
        this.off(document, 'mousemove', this.touchMove);
        this.off(document, 'touchend', this.touchEnd);
        this.off(document, 'mouseup', this.touchEnd);
        this.off(document, 'dragstart', this.preventDefault);
    }
  
    track() {
        const now = Date.now();
        const timeElapsed = now - this.params.lastTimestamp;
        const delta = this.params.posX - this.params.lastPosX;
        const v = 1000 * delta / (1 + timeElapsed);
        this.params.velocity = 0.8 * v + 0.2 * this.params.velocity;
        this.params.lastPosX = this.params.posX;
        this.params.lastTimestamp = now;
    }

    setPosition(posX) {
        this.el.animator.scrollLeft = -posX;
        this.params.posX = -this.el.animator.scrollLeft;
        this.el.animator.setAttribute('data-scroll', -this.params.posX);
        this.setThumbPosition(); 
    }
  
    easePosition() {
        let startPos = this.params.posX;
        let progress = Date.now() - this.params.lastTimestamp;
        let delta = Math.ceil(this.params.speed * progress / 1000);
        let diff = this.params.targetX - this.params.posX;
        let direction = diff < 0 ? 1 : -1;

        delta *= direction;
        if (Math.abs(delta) < Math.abs(diff)) {
            this.setPosition(this.params.posX - delta);
            if (this.params.posX != startPos)
                this.requestAnimation(this.easePosition);
        } else {
            this.setPosition(this.params.targetX);
        }
        

        if (this.finished) {
            this.finish();
        }
    }
  
    requestAnimation(callback) {
        callback = callback.bind(this);
        if (window.requestAnimationFrame) {
            window.requestAnimationFrame(callback);
        } else {
            window.setTimeout(callback, 10);
        }
    }
  
    preventDefault(event) {
        event.preventDefault();
        return false;
    }
  
    touchStart(event) {
        if (this.scrollBlocked) return;
        if (this.started) {
            this.finish();
            return;
        }
        this.started = true;
        this.params.velocity = 0;
        this.params.amplitude = 0;
        this.params.posX = this.params.posX || 0;
        this.params.lastPosX = this.params.posX;
        this.params.lastTimestamp = Date.now();
        this.params.lastTouchX = this.eventCoords(event).x;
        this.startTracking();
    }
  
    touchMove(event) {
        const touchX = this.eventCoords(event).x;
        const delta = touchX - this.params.lastTouchX;
        if (delta > 2 || delta < -2) {
            this.isScrolled = true;
            this.params.lastTouchX = touchX;
            this.setPosition(this.params.posX + delta);
        }
    }
  
    touchEnd() {
        this.finished = true;
        this.started = true;
        this.stopTracking();
    }

    toLeft() {
        let diff = this.items[0].offsetWidth;
        this.params.targetX = this.params.posX + diff;
        this.params.lastTimestamp = Date.now();
        this.requestAnimation(this.easePosition);
    }

    toRight() {
        let diff = this.items[0].offsetWidth;
        this.params.targetX = this.params.posX - diff;
        this.params.lastTimestamp = Date.now();
        this.requestAnimation(this.easePosition);
    }

    finish() {
        this.started = false;
        this.finished = false;
        this.isScrolled = false;
    }

    setControls() {
        let width = this.el.animator.offsetWidth * 100 / this.el.animator.scrollWidth;
        this.setThumbWidth(width);
        this.toggleArrows(width);
    }

    setThumbWidth(width) {
        if (this.el.thumb) {
            if (width < 100) {
                this.el.thumb.style.width = width + "%";
                this.el.thumb.style.display = "";
            } else {
                this.el.thumb.style.display = "none";
            }
        }
    }

    toggleArrows(width) {
        if (!this.el.left || !this.el.right) return;
        if (width < 100) {
            this.el.left.style.display = "";
            this.el.right.style.display = "";
        } else {
            this.el.left.style.display = "none";
            this.el.right.style.display = "none";
        }
    }
};

Slider.init = function() {
    Array.from(document.querySelectorAll('[data-slider]')).forEach(inst => {
        if (inst.handled) return;
        inst.handled = true;
        new Slider(inst);
    });
}

export default Slider;
