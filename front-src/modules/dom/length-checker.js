class LengthChecker {
  constructor(element) {
    this.el = element;
    this.field = this.el.querySelector('[data-length-field]');
    this.counter = this.el.querySelector('[data-length-counter]');
    this.update();
    this.field.addEventListener('input', () => this.update());
  }

  get length() {
    let value =  this.field.contentEditable ? this.field.textContent : this.field.value;
    return value.length;
  }

  update() {
    this.counter.textContent = this.length;
  }
}

export default LengthChecker;