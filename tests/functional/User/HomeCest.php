<?php

declare(strict_types=1);

namespace App\Tests\functional\User;

use App\Tests\FunctionalTester;

class HomeCest
{
    /**
     * @param FunctionalTester $I
     */
    public function tryToTestHomePageAsAnonymousUser(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->seeInTitle('Библиотека программиста');
        $I->see('Последние статьи');
        $I->see('Тесты');
        $I->dontSee('Написать');
        $I->canSeeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToTestHomePageAsAuthUser(FunctionalTester $I)
    {
        $I->setAuth();
        $I->amOnPage('/');
        $I->seeInTitle('Библиотека программиста');
        $I->see('Последние статьи');
        $I->see('Тесты');
        $I->see('Написать');
        $I->canSeeResponseCodeIs(200);
    }
}
