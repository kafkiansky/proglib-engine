<?php

declare(strict_types=1);

namespace App\Tests\functional\User;

use App\Tests\FunctionalTester;

class CabinetCest
{
    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('cabinet');
        $I->dontSee('Настройки');
        $I->dontSee('Уведомления');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('cabinet');
        $I->see('Настройки');
        $I->see('Уведомления');
        $I->seeInCurrentUrl('/user/cabinet');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetBookmarksAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('bookmarks');
        $I->dontSee('Закладки');
        $I->dontSee('Публикации');
        $I->dontSee('Комментарии');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetBookmarksAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('bookmarks');
        $I->see('Закладки');
        $I->see('Публикации');
        $I->see('Комментарии');
        $I->seeInCurrentUrl('/user/bookmarks');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetCommentsAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('user_comments');
        $I->dontSee('Комментарии');
        $I->dontSee('По порядку');
        $I->dontSee('Популярные');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetCommentsAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('user_comments');
        $I->see('Комментарии');
        $I->see('По порядку');
        $I->seeInCurrentUrl('/user/comments');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetPublicationsAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('user_publications');
        $I->dontSee('Публикации');
        $I->dontSee('Опубликованные');
        $I->dontSee('Черновики');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetPublicationsAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('user_publications');
        $I->see('Публикации');
        $I->see('Опубликованные');
        $I->see('Черновики');
        $I->seeInCurrentUrl('/user/publications');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetNotificationsAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('user_notifications');
        $I->dontSee('Уведомления');
        $I->dontSee('Все');
        $I->dontSee('Непрочитанные');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetNotificationsAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('user_notifications');
        $I->see('Уведомления');
        $I->see('Все');
        $I->see('Непрочитанные');
        $I->seeInCurrentUrl('/user/notifications');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetSubscriptionsAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('api_subscriptions');
        $I->dontSee('Подписки');
        $I->dontSee('Сохранить');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeUserCabinetSubscriptionsAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('api_subscriptions');
        $I->see('Подписки');
        $I->see('Сохранить');
        $I->seeInCurrentUrl('/user/subscriptions');
        $I->seeResponseCodeIs(200);
    }
}
