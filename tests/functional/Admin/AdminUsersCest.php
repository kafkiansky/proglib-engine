<?php

declare(strict_types=1);

namespace App\Tests\functional\Admin;

use App\Tests\FunctionalTester;

class AdminUsersCest
{
    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeAllUsersAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_users_all');
        $I->dontSee('Фильтровать');
        $I->dontSee(' Сбросить');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeAllUsersAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_users_all');
        $I->see('Фильтровать');
        $I->see(' Сбросить');
        $I->dontSeeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeOneUserAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_users_user_show', ['email' => getenv('test_user_email')]);
        $I->dontSee('Поменять роль');
        $I->dontSee('Сделать автором');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeOneUserAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_users_user_show', ['email' => getenv('test_user_email')]);
        $I->see('Поменять роль');
        $I->see('Сделать автором');
        $I->dontSeeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToChangeUserRoleAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_users_user_role', ['email' => getenv('test_user_email')]);
        $I->dontSee('Роль');
        $I->dontSee('Сохранить');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToChangeRoleAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_users_user_role', ['email' => getenv('test_user_email')]);
        $I->submitForm('form[name=form]', [
            'form[role]' => 'ROLE_ADMIN'
        ]);
        $I->see('Роль успешно изменена');
        $I->seeInCurrentUrl('/extra/admin/management/users/'. getenv('test_user_email'));
        $I->seeResponseCodeIs(200);
    }

    public function tryToMakeUserAuthorAsAnonymousUser(FunctionalTester $I)
    {
        $I->amOnRoute('admin_users_user_assign', ['email' => getenv('test_user_email')]);
        $I->dontSee('Автор создан');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }
}