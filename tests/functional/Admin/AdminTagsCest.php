<?php

declare(strict_types=1);

namespace App\Tests\functional\Admin;

use App\Tests\FunctionalTester;

class AdminTagsCest
{
    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeTagsListAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_tags_index');
        $I->dontSee('Имя');
        $I->dontSee('Действия');
        $I->dontSee('Добавить тег');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeTagsListAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_tags_index');
        $I->see('Имя');
        $I->see('Действия');
        $I->see('Добавить тег');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToEditTagAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_tags_edit', ['name' => getenv('test_tag_id')]);
        $I->dontSee('Сохранить');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToEditTagAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_tags_edit', ['name' => getenv('test_tag_id')]);
        $I->see('Сохранить');
        $I->seeInCurrentUrl('/extra/admin/management/tags');
        $I->submitForm('form[name=form]', [
            'form[name]' => 'Kafka_1'
        ]);
        $I->seeInCurrentUrl('/extra/admin/management/tags');
        $I->see('Имя');
        $I->see('Действия');
        $I->see('Тег изменен');
        $I->see('Добавить тег');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeOneTagAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_tags_show', ['name' => getenv('test_tag_id')]);
        $I->dontSee('Удалить');
        $I->dontSee('Редактировать');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToSeeOneTagAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_tags_show', ['name' => getenv('test_tag_id')]);
        $I->see('Удалить');
        $I->see('Редактировать');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToDeleteOneTagAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_tags_delete', ['name' => getenv('test_tag_id')]);
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToDeleteOneTagAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_tags_index');
        $I->submitForm('form[name=form-delete]', []);
        $I->seeInCurrentUrl('/extra/admin/management/tags');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToCreateOneTagAsAnonymousUser(FunctionalTester $I): void
    {
        $I->amOnRoute('admin_tags_create');
        $I->dontSee('Создать');
        $I->seeInCurrentUrl('/login');
        $I->seeResponseCodeIs(200);
    }

    /**
     * @param FunctionalTester $I
     */
    public function tryToCreateTagAsAuthUser(FunctionalTester $I): void
    {
        $I->setAuth();
        $I->amOnRoute('admin_tags_create');
        $I->see('Создать');
        $I->submitForm('form[name=form]', [
            'form[name]' => 'Kafka'
        ]);
        $I->seeInCurrentUrl('/extra/admin/management/tags');
        $I->see('Имя');
        $I->see('Действия');
        $I->see('Тег создан');
        $I->see('Добавить тег');
        $I->seeResponseCodeIs(200);
    }
}
