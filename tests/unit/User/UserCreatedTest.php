<?php

declare(strict_types=1);

namespace tests\unit\User;

use App\Domain\User\Model\Entity\Request;
use App\Domain\User\Model\Entity\User;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserCreatedTest extends WebTestCase
{
    public function testGoogleCreatedSuccess()
    {
        $user = User::fromGoogleRequest(
            Uuid::uuid4()->toString(),
            'test@email.ru',
            'userNick',
            'http://placehold.it/100',
            ''
        );

        $this->assertTrue(Uuid::isValid($user->getId()));
        $this->assertNotEmpty($user->getClientId());
        $this->assertNotEmpty($user->getUsername());
        $this->assertNotEmpty($user->getNickName());
        $this->assertNotEmpty($user->getUserPic());
        $this->assertEquals(Request::google()->getRequest(), $user->getOauthType());

        $this->assertTrue(in_array('ROLE_USER', $user->getRoles()));
    }

    public function testGithubCreatedSuccess()
    {
        $user = User::fromGithubRequest(
            Uuid::uuid4()->toString(),
            'test@email.ru',
            'http://placehold.it/100',
            '',
            '',
            '',
            'nickname'
        );

        $this->assertTrue(Uuid::isValid($user->getId()));
        $this->assertNotEmpty($user->getClientId());
        $this->assertNotEmpty($user->getUsername());
        $this->assertNotEmpty($user->getUserPic());
        $this->assertEquals(Request::github()->getRequest(), $user->getOauthType());
    }

    public function testFacebookCreatedSuccess()
    {
        $user = User::fromFacebookRequest(
            Uuid::uuid4()->toString(),
            'test@email.ru',            // email
            'nickname',                 // nickname
            'http://placehold.it/100',  // userPic
            ''                          // locale
        );

        $this->assertTrue(Uuid::isValid($user->getId()));
        $this->assertNotEmpty($user->getClientId());
        $this->assertNotEmpty($user->getUsername());
        $this->assertEquals(Request::facebook()->getRequest(), $user->getOauthType());
    }

    public function testVkCreatedSuccess()
    {
        $user = User::fromVkRequest(
            rand(),
            'test@email.ru' // email
        );

        $this->assertTrue(is_string($user->getId()));
        $this->assertNotEmpty($user->getClientId());
        $this->assertNotEmpty($user->getUsername());
        $this->assertEquals(Request::vk()->getRequest(), $user->getOauthType());
    }
}