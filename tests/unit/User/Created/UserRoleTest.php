<?php

declare(strict_types=1);

namespace App\Tests\unit\User\Created;

use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use App\Tests\unit\Builder\UserBuilder;
use Exception;
use PHPUnit\Framework\TestCase;

class UserRoleTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testSuccess(): void
    {
        $user = (new UserBuilder())->throughVk('mail@mail.ru')
            ->build();

        $this->assertEquals([Role::ROLE_USER], $user->getRoles());
        $this->assertNotEquals([Role::ROLE_SIMPLE_ADMIN], $user->getRoles());
    }

    /**
     * @throws Exception
     */
    public function testUserAdmin(): void
    {
        $user = (new UserBuilder())->throughVk()
            ->withRole(Role::assignSimpleAdmin())
            ->build();

        $this->assertEquals([Role::ROLE_SIMPLE_ADMIN], $user->getRoles());
    }

    /**
     * @throws Exception
     */
    public function testUserCanChangeRole(): void
    {
        $user = (new UserBuilder())->throughVk()->build();
        $user->changeRole(Role::assignEditor()->getRole());

        $this->assertEquals([Role::ROLE_EDITOR], $user->getRoles());
    }
}
