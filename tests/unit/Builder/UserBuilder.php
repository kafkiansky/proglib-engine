<?php

declare(strict_types=1);

namespace App\Tests\unit\Builder;

use App\Domain\User\Model\Entity\Request;
use App\Domain\User\Model\Entity\Role;
use App\Domain\User\Model\Entity\User;
use DateTimeImmutable;
use Exception;
use Ramsey\Uuid\Uuid;

class UserBuilder
{
    private $id;
    private $email;
    private $clientId;
    private $name;
    private $createdAt;
    private $updatedAt;
    private $oauthType;
    private $role;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->createdAt = new DateTimeImmutable();
        $this->updatedAt = new DateTimeImmutable();
    }

    /**
     * @param string|null $email
     * @param int|null $clientId
     *
     * @return UserBuilder
     *
     * @throws Exception
     */
    public function throughVk(string $email = null, int $clientId = null): UserBuilder
    {
        $clone = clone $this;
        $clone->email = $email ?? 'blackbox237@mail.ru';
        $clone->name = 'John Doe';
        $clone->oauthType = Request::vk()->getRequest();
        $clone->clientId = $clientId ?? $this->generateClientId();
        $clone->updatedAt = new DateTimeImmutable();

        return $clone;
    }

    /**
     * @param Role $role
     *
     * @return UserBuilder
     */
    public function withRole(Role $role): UserBuilder
    {
        $clone = clone $this;
        $clone->role = $role->getRole();

        return $clone;
    }

    /**
     * @return User|null
     *
     * @throws Exception
     */
    public function build(): ?User
    {
        $user = null;

        if ($this->oauthType === Request::vk()->getRequest()) {
            $user = User::fromVkRequest(
                $this->clientId,
                $this->email
            );
        }

        if ($this->role) {
            $user->changeRole($this->role);
        }

        return $user;
    }

    /**
     * @return int
     */
    private function generateClientId()
    {
        return mt_rand(10000, 1000000);
    }
}
