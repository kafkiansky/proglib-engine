<?php

declare(strict_types=1);

namespace tests\unit\Post;

use App\Domain\Post\Model\Entity\Post;
use App\Tests\unit\Builder\UserBuilder;
use Cocur\Slugify\Slugify;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PostCreatedTest extends WebTestCase
{
    public function testDraftCreatedSuccess()
    {
        $slugger = Slugify::create();

        $post = Post::fromDraftRequest(
            Uuid::uuid4()->toString(),
            (new UserBuilder())->throughVk('blackbox237@mail.ru')->build(),
            $title = 'Заголовок',
            $slugger->slugify($title),
            '<h1>Content</h1>',
            'preview',
            ''
            );

        $this->assertTrue(Uuid::isValid($post->getId()));
        $this->assertEquals(Post::DRAFT, $post->getStatus());
        $this->assertNotEmpty($post->getTitle());
        $this->assertNotEmpty($post->getSlug());
        $this->assertNotEmpty($post->getRenderedContent());
        $this->assertTrue($post->isDraft());
    }
}