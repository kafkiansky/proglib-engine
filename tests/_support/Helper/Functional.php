<?php
namespace App\Tests\Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use App\Domain\User\Model\Entity\User;
use Codeception\Exception\ModuleException;
use Codeception\Module;
use Codeception\Module\Symfony;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class Functional extends Module
{
    public function setAuth($role = 'ROLE_USER')
    {
        try {
            /** @var Symfony $symfony */
            $symfony = $this->getModule('Symfony');
        } catch (ModuleException $exception) {
            $this->fail('Unable to get module Symfony');
        }

        $container = $symfony->_getContainer();
        $doctrine = $container->get('doctrine');

        /** @var User $user */
        $user = $doctrine
            ->getRepository(User::class)
            ->findOneBy(['email' => getenv('test_auth_user')]);

        $firewall = 'main';
        $token = new UsernamePasswordToken($user, null, $firewall, [$role]);

        $session = $symfony->grabService('session');
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $symfony->client->getCookieJar()->set($cookie);
    }
}
