up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear proglib-clear docker-pull docker-build docker-up proglib-init
test: manager-test
db: drop-db update-db fixtures-load

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

proglib-init: proglib-composer-install proglib-assets-install fixtures-load proglib-ready

proglib-clear:
	docker run --rm -v ${PWD}/:/app --workdir=/app alpine rm -f .ready

proglib-composer-install:
	docker-compose run --rm proglib-engine-php-cli composer install

proglib-assets-install:
	docker-compose run --rm proglib-engine-node yarn install
	docker-compose run --rm proglib-engine-node npm rebuild node-sass

drop-db:
	docker-compose run --rm proglib-engine-php-cli php bin/console doctrine:schema:drop -f
update-db:
	docker-compose run --rm proglib-engine-php-cli php bin/console doctrine:schema:up -f
fixtures-load:
	docker-compose run --rm proglib-engine-php-cli php bin/console doctrine:fixtures:load -n

proglib-ready:
	docker run --rm -v ${PWD}/:/app --workdir=/app alpine touch .ready

proglib-assets-dev:
	docker-compose run --rm proglib-engine-node npm run dev

proglib-test:
	docker-compose run --rm proglib-engine-php-cli php bin/phpunit

build-production:
	docker build --pull --file=docker/production/nginx.docker --tag ${REGISTRY_ADDRESS}/proglib-engine-nginx:${IMAGE_TAG} proglib
	docker build --pull --file=docker/production/php-fpm.docker --tag ${REGISTRY_ADDRESS}/proglib-engine-php-fpm:${IMAGE_TAG} proglib
	docker build --pull --file=docker/production/php-cli.docker --tag ${REGISTRY_ADDRESS}/proglib-engine-php-cli:${IMAGE_TAG} proglib
	docker build --pull --file=docker/production/postgres.docker --tag ${REGISTRY_ADDRESS}/proglib-engine-mysql:${IMAGE_TAG} proglib

push-production:
	docker push ${REGISTRY_ADDRESS}/proglib-engine-nginx:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/proglib-engine-php-fpm:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/proglib-engine-php-cli:${IMAGE_TAG}
	docker push ${REGISTRY_ADDRESS}/proglib-engine-mysql:${IMAGE_TAG}

deploy-production:
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'rm -rf docker-compose.yml .env'
	scp -o StrictHostKeyChecking=no -P ${PRODUCTION_PORT} docker-compose-production.yml ${PRODUCTION_HOST}:docker-compose.yml
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "REGISTRY_ADDRESS=${REGISTRY_ADDRESS}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "IMAGE_TAG=${IMAGE_TAG}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "MANAGER_APP_SECRET=${MANAGER_APP_SECRET}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "MANAGER_DB_PASSWORD=${MANAGER_DB_PASSWORD}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "MANAGER_REDIS_PASSWORD=${MANAGER_REDIS_PASSWORD}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'echo "MANAGER_OAUTH_FACEBOOK_SECRET=${MANAGER_OAUTH_FACEBOOK_SECRET}" >> .env'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose pull'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose --build -d'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'until docker-compose exec -T manager-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done'
	ssh -o StrictHostKeyChecking=no ${PRODUCTION_HOST} -p ${PRODUCTION_PORT} 'docker-compose run --rm manager-php-cli php bin/console doctrine:migrations:migrate --no-interaction'