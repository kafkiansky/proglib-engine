const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const src = path.join(__dirname, "front-src");
const dist = path.join(__dirname, "public");

module.exports = {
  entry: {
    /* скрипт в head, синхронный */
    "assets/service": path.join(src, "service.js"),
    /* основной скрипт в конце body, асинхронный */
    "assets/main": path.join(src, "app.js"),

    /* комментарии */
    "assets/comments/comments": path.join(src, "components/comments/comments.js"),
    /* фид */
    "assets/feed/feed": path.join(src, "components/feed/feed.js"),

    /* подсветка кода */
    "assets/services/highlight/highlight": path.join(src, "services/highlight.js"),
    /* загрузчик файлов */
    "assets/services/loader/loader": path.join(src, "components/loader/loader.js"),

    /* личный кабинет, общий скрипт */
    "assets/account/account": path.join(src, "pages/account/account.js"),
    /* личный кабинет, приватный скрипт */
    "assets/account/account-private": path.join(src, "pages/account/account-private.js"),

    /* статья */
    "assets/post/post": path.join(src, "pages/post/post.js"),
    /* тест */
    "assets/post/test": path.join(src, "pages/test/test.js"),
    /* редактор статей на сайте */
    "assets/post/new-post": path.join(src, "pages/new-post/new-post.js"),

    /* форма вакансии */
    "assets/com/vacancy-form": path.join(src, "pages/commercial/vacancy-form.js"),
    /* фид вакансий */
    "assets/com/vacancy-feed": path.join(src, "pages/commercial/vacancy-feed.js"),
    /* оплата вакансии */
    "assets/com/vacancy-payment": path.join(src, "pages/commercial/vacancy-payment.js"),
    /* форма рекламы */
    "assets/com/com-form": path.join(src, "pages/commercial/com-form.js"),
    
    
    /* админка */
    "admin/admin": path.join(src, "pages/admin/admin.js"),
    "admin/tests": path.join(src, "pages/admin/tests/tests.js"),
    "admin/post": path.join(src, "pages/admin/post/post.js"),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            query: {
              presets: [ '@babel/preset-env' ],
            },
          }
        ]
      },
      {
        test: /\.pcss$/,
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader?removeSVGTagAttrs=false'
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
            options: {
              url: false,
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                require('postcss-nested-ancestors'),
                require('postcss-nested')
              ]
            }
          }
          
        ], 
      }
    ]
  },
  output: {
    path: dist,
    filename: './[name].min.js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].min.css",
    })
  ]
};
